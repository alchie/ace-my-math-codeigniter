<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "welcome";
#$route['404_override'] = 'welcome/page_not_found';

$route['lesson'] = "lessons";
$route['lesson/(:any)'] = "lessons/lesson/$1";
$route['lesson/(:any)/(:any)'] = "lessons/lesson/$1/$2";

$route['search'] = "lessons/search";
$route['search/(:any)'] = "lessons/search/$1";

$route['topic'] = "lessons/topic";
$route['topic/(:any)'] = "lessons/topic/$1";

$route['login'] = "account/login";
$route['register'] = "account/register";
$route['forgotpassword'] = "account/forgot_password";
$route['upgrade/(:num)'] = "account/upgrade/$1";
$route['logout'] = "my/logout";

// pages
$route['try-it-now'] = "pages/index/try-it-now";
$route['guarantee'] = "pages/index/guarantee";
$route['what-you-get'] = "pages/index/what-you-get";
$route['free-samples'] = "pages/index/free-samples";
$route['our-method'] = "pages/index/our-method";
$route['about-us'] = "pages/index/about-us";

/* End of file routes.php */
/* Location: ./application/config/routes.php */
