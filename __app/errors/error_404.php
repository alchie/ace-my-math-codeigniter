
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo $heading; ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <link rel="stylesheet" href="/assets/css/yeti.css">
		<link rel="stylesheet" href="/assets/css/custom.css">
		<link rel="stylesheet" href="/assets/css/responsive.css">
		<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Sue+Ellen+Francisco' type='text/css' media='all' />
<script type='text/javascript' src='/assets/js/jquery-1.9.1.min.js'></script>
<script type='text/javascript' src='/assets/js/bootstrap.min.js?ver=2014-03-03'></script>
<script type='text/javascript' src='/assets/js/acemymath.js'></script>

    </head>
    <body>
        
<div id="wrap">
    
<div id="header-container">
    <div class="container">
    <div class="row">
        <div class="col-md-12">
            
           
 <ul class="social-icons pull-right">
                <li><a href="#email" class="email">Email</a></li>
                <li><a href="#facebook" class="facebook">Facebook</a></li>
                <li><a href="#twitter" class="twitter">Twitter</a></li>
                <li><a href="#rss" class="rss">RSS</a></li>                                                
 </ul>
 <div class="header-account-form">
          <form name="loginform" id="loginform" class="login-container" action="/index.php/account/login" method="post">
        <div class="login-form">
            <label>User Login</label>
            <input type="text" name="username" placeholder="Username" value="">
            <input type="password" name="password" placeholder="Password" value="">       
            <input type="submit" name="wp-submit" value="Login" class="btn btn-success btn-sm"> 
            <input type="hidden" name="redirect_to" value="http://acemymath">
		    <input type="hidden" name="testcookie" value="1">
        </div>
    </form>
    <div class="register">
        <small>Not yet a member?</small>
        <a href="/index.php/account/register" class="btn btn-danger btn-sm">Signup</a>
    </div>
      </div>

            
              <div id="branding">
                <h1 id="site-title"><a class="home-link" href="/" title="Ace My Math" rel="home">
                 Ace My Math</a></h1>                
             </div>

<div id="site-navigation">
 
<nav id="header-primary" class="navbar navbar-default">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand visible-xs" href="#">Navigation</a>
  </div>
  <div class="collapse navbar-collapse navbar-ex1-collapse" id="bs-example-navbar-collapse-1">
		<ul id="menu-primary-menu" class="nav navbar-nav navbar-left">
			<li><a title="lorem ipsum" href="/">Home</a></li>
			<li><a href="/index.php/try-it-now">Try it Now</a></li>
			<li><a href="/index.php/guarantee">Guarantee</a></li>
			<li><a href="/index.php/what-you-get">What You Get</a></li>
			<li><a href="/index.php/lessons">Lessons</a></li>
			<li><a href="/index.php/free-samples">Free Samples</a></li>
			<li><a href="/index.php/our-method">Our Method</a></li>
			<li><a href="/index.php/about-us">About Us</a></li>
		</ul>

<form role="search" method="get" class="search-form" action="/index.php/search">
				<label>
					<span class="screen-reader-text">Search for:</span>
					<input id="lesson-search" type="search" class="search-field" placeholder="Search …" value="" name="s" title="Search for:" autocomplete="off">
				</label>
				<input type="submit" class="search-submit" value="Search">
			</form></div></nav>
</div>

</div>
</div>
</div>
</div>


<div id="main-container">
    <div class="container">
        <div class="container-inner">
      <div class="row">
        <div class="col-md-12 main-content">
			<h1><?php echo $heading; ?></h1>
				<p><?php echo $message; ?></p>
        </div>
	  </div><!-- row -->
      </div><!-- container-inner -->
    </div><!-- container-->
   </div>


<!----------------------------------------------------------------------------------------------- //-->
</div>

<div id="footer">
     <div id="footer-widgets-container">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                
<div id="secondary">
		<div id="frontpage-footer" class="frontpage-footer widget-area" role="complementary">
		<aside id="nav_menu-4" class="block-inner widget widget_nav_menu">
			<div class="widget-header"><h3 class="widget-title">Ace My Math</h3><em></em></div>
			<div class="menu-primary-menu-container">
				<ul id="menu-primary-menu" class="menu">
			<li><a title="lorem ipsum" href="/">Home</a></li>
			<li><a href="/index.php/try-it-now">Try it Now</a></li>
			<li><a href="/index.php/guarantee">Guarantee</a></li>
			<li><a href="/index.php/what-you-get">What You Get</a></li>
			<li><a href="/index.php/lessons">Lessons</a></li>
			<li><a href="/index.php/free-samples">Free Samples</a></li>
			<li><a href="/index.php/our-methods">Our Method</a></li>
			<li><a href="/index.php/about-us">About Us</a></li>
				</ul>
			</div>
			</aside>	
</div><!-- #primary-sidebar -->
	</div>
	<!-- #secondary -->

                </div>
                
                   
            </div>
        </div>
    </div>

    <div id="bottom-container">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <span class="copyright"> Copyrigth © 2014 - Ace My Math - All rights reserved</span>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
var baseURL = '/';
var ajaxURL = '/ajax';
$('#main-content').aceMyMath();
</script>
    </body>
</html>
