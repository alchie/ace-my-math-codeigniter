<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->_mustLogin();
		
		$this->load->model(array(
			'Users_model', 
			'Users_sponsors_model', 
			'Users_logs_model', 
			'Users_lessons_sessions_model',
			'Tax_levels_model', 
			'Subscription_plans_model', 
			'Subscription_attributes_model'
		));
		$this->load->library(array('form_validation'));
		$this->load->helper(array('date'));
	}
	
	public function index()
	{
		$this->template_data->set('main_page', 'my' );
		$this->load->view('my/dashboard', $this->template_data->get() );
	}

	public function dashboard()
	{
		$this->template_data->set('main_page', 'my' );
		
		if( $this->session->userdata('userType') == 'parent' ) {
			$this->students();
		} elseif( $this->session->userdata('userType') == 'student' ) {
			$this->student( $this->session->userdata('userName'), 'profile');
		}
		
	}

	public function add_student()
	{
		$this->template_data->set('main_page', 'my' );
		$this->template_data->set('sub_page', 'add_student' );
		
		$this->form_validation->set_rules('gradelevel', 'Grade Level', 'numeric|required');
		$this->form_validation->set_rules('email', 'Email Address', 'trim|valid_email');
		$this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
		$this->form_validation->set_rules('lastname', 'Last Name', 'trim');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[users.user_username]');
		$this->form_validation->set_rules('newpassword', 'Password', 'trim|required|sha1|md5');
		
		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
		        $this->template_data->alert( validation_errors(), 'danger');
		    }
		} 
		else {
			$addnew = new $this->Users_model;
			$addnew->setUserLevel( $this->input->post('gradelevel') );
			$addnew->setUserFirstname( $this->input->post('firstname') );
			$addnew->setUserLastname( $this->input->post('lastname') );
			$addnew->setUserEmail( $this->input->post('email') );
			$addnew->setUserUsername( $this->input->post('username') );
			$addnew->setUserPassword( $this->input->post('newpassword') );
			$addnew->setUserType('student');
			$addnew->setUserCreated( unix_to_human( time(), TRUE, 'eu' ) );
			
			if( $addnew->insert() ) {
				$sponsorship = new $this->Users_sponsors_model;
				$sponsorship->setUserId( $addnew->getUserId() );
				$sponsorship->setSponsorId( $this->session->userdata('userId') );
				$sponsorship->setDateSponsored( unix_to_human( time(), TRUE, 'eu' ) );
				$sponsorship->setActive( 1 );
				if( $sponsorship->insert() ) {
					redirect('my/student/' . $addnew->getUserUsername() . '/activate' , 'refresh');
					exit;
				}
			}
			//$this->template_data->alert( "Successfully Added!", 'success');
		}
		
		$levels = new $this->Tax_levels_model;
		$this->template_data->set('grade_levels', $levels->populate() );
		
		$students = new $this->Users_sponsors_model;
		$students->setSponsorId( $this->session->userdata('userId'), true );
		$students->setActive( 1, true );
		
		$students->setSelect('users_sponsors.sponsor_id');
		$students->setSelect('users.user_username, users.user_firstname, users.user_plan, users.user_active');
		$students->setSelect('tax_levels.level_name');
		
		$students->setJoin('users', 'users.user_id = users_sponsors.user_id');
		$students->setJoin('tax_levels', 'users.user_level = tax_levels.level_id');
		
		$pStudents = $students->populate();
				
		$this->template_data->set('students', $pStudents );
		
		$this->load->view('my/add_student', $this->template_data->get() );
	}
	
	public function students()
	{
		
		$this->template_data->set('main_page', 'my' );
		$this->template_data->set('sub_page', 'students' );
		
		$students = new $this->Users_sponsors_model;
		$students->setSponsorId( $this->session->userdata('userId'), true );
		$students->setActive( 1, true );
		
		$students->setSelect('users_sponsors.sponsor_id, users_sponsors.user_id');
		$students->setSelect('users.user_username, users.user_firstname, users.user_plan, users.user_active');
		$students->setSelect('tax_levels.level_name');
		
		$students->setJoin('users', 'users.user_id = users_sponsors.user_id');
		$students->setJoin('tax_levels', 'users.user_level = tax_levels.level_id');
		
		$pStudents = $students->populate();
		

		if( count( $pStudents ) == 0 ) {
			redirect('my/add_student', 'refresh');
			exit;
		} else {
			$stud_ids = array();
			foreach( $pStudents as $stud ) {
				$stud_ids[] = $stud->user_id;
			}
			$logs = new $this->Users_lessons_sessions_model;
			$logs->setWhereIn('users_lessons_sessions.user_id', $stud_ids);
			$logs->setWhere('users_lessons_sessions.ended IS NOT NULL');
			$logs->setGroupBy('users_lessons_sessions.lesson_id');
			$logs->setOrder('users_lessons_sessions.ended', 'DESC');
			
			$logs->setJoin('ci_lessons', 'ci_lessons.lesson_id = users_lessons_sessions.lesson_id', 'LEFT OUTER');
			$logs->setJoin('ci_users', 'ci_users.user_id = users_lessons_sessions.user_id', 'LEFT OUTER');
			
			$this->template_data->set('students_logs', $logs->populate() );
		}
		
		
				
		$this->template_data->set('students', $pStudents );
		
		
		$this->load->view('my/students', $this->template_data->get() );
	}
	
	public function student($id='', $action='profile')
	{
		if( $id=='') {
			redirect('my/students');
			exit;
		}
		
		$this->template_data->set('main_page', 'my' );
		$this->template_data->set('sub_page', 'student_' . $action);
		$this->template_data->set('action', $action);
		
		$current = new $this->Users_model;
		$current->setUserUsername( $id, true);
		
		$current->setSelect('users.*');
		$current->setSelect('subscription_plans.s_plan_name');
		$current->setJoin('subscription_plans', 'users.user_plan = subscription_plans.s_plan_id');
		
		$current->setSelect('tax_levels.level_name');
		$current->setJoin('tax_levels', 'users.user_level = tax_levels.level_id');
		
		$current_student = $current->get();
		
		$view_file = '404';
		
		switch($action) {
			case 'activities':
			case 'profile':
				$this->template_data->set('action', 'activities');
				$view_file = 'my/student-activities';
				
				$this->load->model( 'Users_lessons_sessions_model' );
				$activities = new $this->Users_lessons_sessions_model;
				$activities->setSelect('ci_users_lessons_sessions.*, ci_lessons.lesson_title');
				$activities->setUserId( $current_student->user_id , true );
				$activities->setWhere('ci_users_lessons_sessions.ended = (SELECT max(ended) FROM ci_users_lessons_sessions uls2 WHERE uls2.lesson_id = ci_users_lessons_sessions.lesson_id AND uls2.user_id = ci_users_lessons_sessions.user_id)');
				$activities->setJoin('ci_lessons', 'ci_lessons.lesson_id = ci_users_lessons_sessions.lesson_id');
				$activities->setGroupBy('ci_users_lessons_sessions.lesson_id');

				$this->template_data->set('activities', $activities->populate() );
				
				break;
			case 'lesson_result':
				$this->template_data->set('action', 'activities');
				$view_file = 'my/student-lesson_result';
				
				$this->load->model( 'Users_lessons_sessions_model' );
				$activities = new $this->Users_lessons_sessions_model;
				$activities->setUlsId( $this->input->get('session') , true );
				$activities->setUserId( $current_student->user_id , true );
				$activities->setJoin('ci_lessons', 'ci_lessons.lesson_id = ci_users_lessons_sessions.lesson_id');
				
				$this->template_data->set('activity', $activities->get() );
				
				$this->load->model( 'Users_lessons_quizzes_model' );
				$quizzes = new $this->Users_lessons_quizzes_model;
				$quizzes->setSessionId( $this->input->get('session'), TRUE );
				$quizzes->setOrder('ci_users_lessons_quizzes.quiz_number', 'ASC');
				$quizzes->setJoin('ci_lessons_quiz','ci_lessons_quiz.lesson_id = ci_users_lessons_quizzes.lesson_id AND ci_lessons_quiz.quiz_number = ci_users_lessons_quizzes.quiz_number');
				$quizzes->setLimit(0);
				$this->template_data->set('quizzes', $quizzes->populate() );
				
				break;
			case 'manage':
				$view_file = 'my/student-manage';
				
				$levels = new $this->Tax_levels_model;
				$this->template_data->set('grade_levels', $levels->populate() );

				$this->form_validation->set_rules('gradelevel', 'Grade Level', 'numeric|required');
				$this->form_validation->set_rules('email', 'Email Address', 'trim|valid_email');
				$this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
				$this->form_validation->set_rules('lastname', 'Last Name', 'trim');
				$this->form_validation->set_rules('newpassword', 'Password', 'trim|sha1|md5');
				
				if ($this->form_validation->run() == FALSE)
				{
					if( $this->input->post() ) {
						$this->template_data->alert( validation_errors(), 'danger');
					}
				} 
				else {
					$current->setUserLevel( $this->input->post('gradelevel'), false, true );
					$current->setUserFirstname( $this->input->post('firstname'), false, true );
					$current->setUserLastname( $this->input->post('lastname'), false, true );
					$current->setUserEmail( $this->input->post('email'), false, true );
					if( $this->input->post('newpassword') != '' ) {
						$current->setUserPassword( $this->input->post('newpassword'), false, true );
					}
					$current->update();
					$this->template_data->alert( "Successfully Updated!", 'success');
				}
		
			break;
			case 'progress':
				$view_file = 'my/student-progress';
			break;
			case 'lessons':
				$view_file = 'my/student-lessons';
			break;
			case 'goals':
				$view_file = 'my/student-goals';
			break;
			case 'certificates':
				$view_file = 'my/student-certificates';
			break;
			case 'awards':
				$view_file = 'my/student-awards';
			break;
			case 'activate':
			case 'upgrade':
				
				$this->form_validation->set_rules('plan', 'Membership Plan', 'trim|required');
				
				
				if ($this->form_validation->run() == FALSE)
				{
					if( $this->input->post() ) {
						$this->template_data->alert( validation_errors(), 'danger');
					}
				} 
				else {
					$current->setUserActive( 1, false, true);
					$current->setUserPlan( $this->input->post('plan'), false, true);
					$current->setUserExpiry( unix_to_human( (time() + (30 * 24 * 60 * 60)), TRUE, 'eu' ), false, true );
					if( $current->update() ) {
						redirect('my/student/' . $id . '/profile', 'refresh');
						exit;
					}
				}

				$view_file = 'my/student-upgrade';
				
			break;
			case 'cancel':
				$view_file = 'my/student-cancel';
			break;
			case 'update':
				
				$this->form_validation->set_rules('gradelevel', 'Grade Level', 'numeric|required');
				$this->form_validation->set_rules('email', 'Email Address', 'trim|valid_email');
				$this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
				$this->form_validation->set_rules('lastname', 'Last Name', 'trim');
				$this->form_validation->set_rules('newpassword', 'Password', 'trim|sha1|md5');
				
				if ($this->form_validation->run() == FALSE)
				{
					if( $this->input->post() ) {
						$this->template_data->alert( validation_errors(), 'danger');
					}
				} 
				else {
					
					$current->setUserLevel( $this->input->post('gradelevel'), false, true );
					$current->setUserFirstname( $this->input->post('firstname'), false, true );
					$current->setUserLastname( $this->input->post('lastname'), false, true );
					$current->setUserEmail( $this->input->post('email'), false, true );
					if( $this->input->post('newpassword') != '' ) {
						$current->setUserPassword( $this->input->post('newpassword'), false, true );
					}
					if( $current->update() ) {
						$current_student = $current->get();
					}
					$this->template_data->alert( "Successfully Updated!", 'success');
				}
				
				$view_file = 'my/student-update';
				$levels = new $this->Tax_levels_model;
				$this->template_data->set('grade_levels', $levels->populate() );

			break;
			default:
				$view_file = '404';
			break;
		}
		
		if( $current_student->user_active == 0 ) {
			$view_file = 'my/student-upgrade';
		} 
		
		$this->template_data->set('current_student',  $current_student );
		$this->load->view($view_file, $this->template_data->get() );
	}
	
	public function account($page='manage')
	{
		$this->template_data->set('main_page', 'my' );
		$this->template_data->set('sub_page', 'account' );
		
		$current = new $this->Users_model;
		$current->setUserId($this->session->userdata('userId'), true);
		
		switch($page) {
			default:
			case 'manage':
				$view_file = 'my/account';
				
					if( $this->session->userdata('userType') == 'student' ) {
						$this->form_validation->set_rules('gradelevel', 'Grade Level', 'numeric|required');
						$this->form_validation->set_rules('email', 'Email Address', 'trim|valid_email');
					}
					if( $this->session->userdata('userType') == 'parent' ) {
						$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email');
					}
					$this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
					$this->form_validation->set_rules('lastname', 'Last Name', 'trim');
					$this->form_validation->set_rules('newpassword', 'Password', 'trim|sha1|md5');
					
					if ($this->form_validation->run() == FALSE)
					{
						if( $this->input->post() ) {
							$this->template_data->alert( validation_errors(), 'danger');
						}
					} 
					else {
						if( $this->session->userdata('userType') == 'student' ) {
							$current->setUserLevel( $this->input->post('gradelevel'), false, true );
						}
						$current->setUserFirstname( $this->input->post('firstname'), false, true );
						$current->setUserLastname( $this->input->post('lastname'), false, true );
						$current->setUserEmail( $this->input->post('email'), false, true );
						if( $this->input->post('newpassword') != '' ) {
							$current->setUserPassword( $this->input->post('newpassword'), false, true );
						}
						$current->update();
						$this->template_data->alert( "Successfully Updated!", 'success');
					}
				
			break;
			case 'logs':
				$view_file = 'my/account-logs';
				$this->load->model( 'Users_logs_model' );
				$log = new $this->Users_logs_model;
				$log->setUserId( $this->session->userdata('userId'), true );
				$log->setOrder('log_date', 'DESC');
				$this->template_data->set('logs', $log->populate() );
				
			break;
		}

		
		$this->template_data->set('current_account', $current->get() );
		
		$levels = new $this->Tax_levels_model;
		$this->template_data->set('grade_levels', $levels->populate() );
		
		$this->load->view($view_file, $this->template_data->get() );
	}

	public function progress()
	{
		$this->template_data->set('main_page', 'my' );
		$this->template_data->set('sub_page', 'progress' );
		$this->load->view('my/progress', $this->template_data->get() );
	}
	
	public function goals($action=NULL)
	{
		$view_file = '404';
		$this->template_data->set('action', $action );
		if( $this->session->userdata('userType') == 'student' ) { 
			$this->student( $this->session->userdata('userName'), 'goals');
		} else {
			switch( $action ) {
				case 'add':
					$view_file = 'my/add_goal';
				break;
			}
		}
		$this->load->view($view_file, $this->template_data->get() );
	}
	
	public function activities() {
		if( $this->session->userdata('userType') == 'student' ) { 
			$this->student( $this->session->userdata('userName'), 'activities');
		}
	}
	
	public function lessons() {
		if( $this->session->userdata('userType') == 'student' ) { 
			$this->student( $this->session->userdata('userName'), 'lessons');
		}
	}
	
	public function certificates() {
		if( $this->session->userdata('userType') == 'student' ) { 
			$this->student( $this->session->userdata('userName'), 'certificates');
		}
	}
	
	public function awards() {
		if( $this->session->userdata('userType') == 'student' ) { 
			$this->student( $this->session->userdata('userName'), 'awards');
		}
	}
	
	public function lesson_result() {
		if( $this->session->userdata('userType') == 'student' ) { 
			$this->student( $this->session->userdata('userName'), 'lesson_result');
		}
	}
	
	public function logout()
	{
		$this->load->helper( array('date') );
		$this->load->model( 'Users_logs_model' );
		$log = new $this->Users_logs_model;
		$log->setLogDate( unix_to_human( time(), TRUE, 'eu' ));
		$log->setUserId( $this->session->userdata('userId') );
		$log->setLogCode( 'LOGOUT' );
		$log->insert();
		
		 $this->session->sess_destroy();
		 redirect('login', 'refresh');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
