<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller {

	public function index()
	{
		// grade levels
		$this->load->model(array('Tax_levels_model'));
		$levels = new $this->Tax_levels_model;
		$levels->setOrder('level_order');
		$levels->setLimit(0);
		$levels->setLevelActive(1, true);
		$this->template_data->set('levels', $levels->populate());
		
		$this->template_data->set('main_page', 'frontpage' );
		
		$home_title = get_site_settings('home_title', 'Ace My Math');
		$this->template_data->set('page_title', $home_title);
		
		$this->load->view('main_frontpage', $this->template_data->get() );
	}
	
	public function page_not_found()
	{
		$this->template_data->set('main_page', 'frontpage' );
		
		$this->template_data->set('page_title', get_site_settings('home_title', 'Page Not Found!'));
		
		$this->load->view('404', $this->template_data->get() );
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
