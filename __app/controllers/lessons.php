<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lessons extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model(array('Tax_tags_model', 'Tax_levels_model', 'Tax_chapters_model', 'Level_chapters_model'));
	}
	
	public function index()
	{
		// grade levels
		$levels = new $this->Tax_levels_model;
		$levels->setOrder('level_order');
		$levels->setLimit(0);
		$levels->setLevelActive(1, true);
		$this->template_data->set('levels', $levels->populate());
		
		$this->template_data->set('main_page', 'frontpage' );
		$this->load->view('lessons', $this->template_data->get() );
	}
	
	public function level($slug='')
	{
		if( $slug == '' ) {
			redirect('lessons', 'location');
			exit;
		}
		
		// get current grade level
		$current = new $this->Tax_levels_model;
		$current->setLevelSlug( $slug, true );
		if( $current->nonEmpty() === false ) {
			redirect('lessons', 'location');
			exit;
		}
		$this->template_data->set('current_level', $current->getResults()); 
		
		/* get chapters
		$level_chapters = new $this->Level_chapters_model;
		$level_chapters->setJoin('tax_chapters', 'tax_chapters.chapter_id = level_chapters.chapter_id');
		$level_chapters->setLevelId( $current->getResults()->level_id, true );
		$level_chapters->setOrder('tax_chapters.chapter_order');
		$this->template_data->set('level_chapters', $level_chapters->populate()); 
		*/
		
		// grade levels
		$levels = new $this->Tax_levels_model;
		$levels->setOrder('level_order');
		$levels->setLimit(0);
		$levels->setLevelActive(1, true);
		$this->template_data->set('levels', $levels->populate());
		
		
		$this->template_data->set('main_page', 'lessons' );
		$this->load->view('lessons', $this->template_data->get() );
	}
	
	function lesson($slug='', $page='video') {
		if( $slug == '') {
			//redirect('lessons', 'location');
			$this->load->view('404', $this->template_data->get() );			
		} else {
			
			if( strpos($slug, '-') === false ) {
				
				$this->load->view('404', $this->template_data->get() );
				
			} else {
				
				list($lId, $lSlug) = explode('-', $slug, 2);
				$this->template_data->set('lesson_id',  $lId );
				$this->template_data->set('lesson_slug',  $lSlug );

				$this->load->model(array('Lessons_model'));
				$current_lesson = new $this->Lessons_model;
				$current_lesson->setLessonId( $lId, true );
				$current_lesson->setLessonSlug( $lSlug, true );
				$this->template_data->set('current_lesson',  $current_lesson->get() );

				// grade levels
				$levels = new $this->Tax_levels_model;
				$levels->setOrder('level_order');
				$levels->setLimit(0);
				$levels->setLevelActive(1, true);
				$this->template_data->set('levels', $levels->populate());
				
				switch($page) {
					case 'video':
						$this->load->view('lesson_video', $this->template_data->get() );
					break;
					case 'practice':
						$this->load->view('lesson_practice', $this->template_data->get() );
					break;
					case 'worksheet':
						$this->load->view('lesson_worksheet', $this->template_data->get() );
					break;
					case 'report':
						$this->load->view('lesson_report', $this->template_data->get() );
					break;
					case 'quizzes':
						
						$this->load->model(array('Lessons_quiz_model'));
						$quizzes = new $this->Lessons_quiz_model;
						
						if( $this->input->get('delete') != FALSE ) {
							$quizzes->setQuizId( $this->input->get('delete'), TRUE );
							$quizzes->delete();
							redirect( $this->uri->uri_string() );
							exit;
						}
						
						$quizzes->setLessonId( $lId, TRUE );
						$quizzes->setOrder('quiz_number', 'ASC');
						$quizzes->setLimit(0);

						$this->template_data->set('quizzes', $quizzes->populate());
						$this->load->view('lesson_quizzes', $this->template_data->get() );
					
					break;
				}
				
			}
		
		}
	}
	
	function search($keyword='') {
		
		if( $this->input->get('s') ) {
			$keyword = $this->input->get('s');
		}
		
		if( $keyword != '') {
			$this->load->model(array('Tax_tags_model', 'Lessons_model'));
			$this->Tax_tags_model->setFilter('tag_name LIKE', '%'.$keyword.'%');
			$this->Tax_tags_model->setLimit(0);
			$this->Lessons_model->setFilter('lesson_title LIKE', '%'.$keyword.'%');
			$this->Lessons_model->setJoin('tax_levels', 'lessons.lesson_level = tax_levels.level_id', 'left');
			$this->Lessons_model->setLimit(0);
			$this->template_data->set('topics', $this->Tax_tags_model->populate() );
			$this->template_data->set('lessons', $this->Lessons_model->populate() );
		}
		

		
		$this->template_data->set('main_page', 'search' );
		$this->template_data->set('keyword', $keyword );
		$this->load->view('search', $this->template_data->get() );
	}
	
	function topic($keyword='') {
		
		if( $keyword == '' ) {
			redirect("/", "refresh");
		}
		
		$this->load->model(array('Tax_tags_model', 'Lessons_tags_model'));
		$this->Tax_tags_model->setTagSlug($keyword, true);
		$tag = $this->Tax_tags_model->get();
		$this->template_data->set('tag', $tag );
		
		$this->Lessons_tags_model->setTagId( $tag->tag_id, true );
		$this->Lessons_tags_model->setJoin('lessons', 'lessons.lesson_id = lessons_tags.lesson_id', 'left');
		$this->Lessons_tags_model->setJoin('tax_levels', 'lessons.lesson_level = tax_levels.level_id', 'left');
		$this->Lessons_tags_model->setLimit(0);
		$this->template_data->set('lessons', $this->Lessons_tags_model->populate() );
		
		$this->load->view('topic', $this->template_data->get() );
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
