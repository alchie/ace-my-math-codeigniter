<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->_notLoggedIn();
	}
	
	public function index()
	{
		$this->template_data->set('main_page', 'account' );
		$this->load->view('main_account', $this->template_data->get() );
	}
	
	public function login()
	{
		// load necessary files
		$this->load->library(array('form_validation'));
		
		// set template variables
		$this->template_data->set('main_page', 'account' );
		$this->template_data->set('sub_page', 'login' );
		
		// validation 
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|sha1|md5');
		
		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
		        $this->template_data->alert( validation_errors(), 'danger');
		    }
		} 
		else {
			
			$this->load->model('Users_model');
			$account = new $this->Users_model;
			$account->setUserUsername( $this->input->post('username'), true );
			$account->setUserPassword( $this->input->post('password'), true );
			$account->setUserActive( 1, true );
			
			if( $account->nonEmpty() === TRUE ) {
		        $current_user = $account->getResults();

		        $this->session->set_userdata( array(
					'loggedIn' => true,
					'userId' => $current_user->user_id,
					'userName' => $current_user->user_username,
					'userType' => $current_user->user_type,
					'userFirstname' => $current_user->user_firstname,
					'userLevel' => $current_user->user_level,
					'userPlan' => $current_user->user_plan,
				) );
				
				$this->load->helper( array('date') );
				
				$account->setLastLogin( unix_to_human( time(), TRUE, 'eu' ), false, true );
				$account->setLastLoginIp( $this->input->ip_address(), false, true );
				$account->update();
				
				$this->load->model( 'Users_logs_model' );
				$log = new $this->Users_logs_model;
				$log->setLogDate(unix_to_human( time(), TRUE, 'eu' ));
				$log->setUserId( $current_user->user_id );
				$log->setLogCode( 'LOGIN' );
				$log->setLogMsg( $this->input->ip_address() );
				$log->insert();
				
				$r_url = 'my/dashboard';
				if( $this->input->get('r') != FALSE ) {
					$r_url = $this->input->get('r');
				}
				
		        redirect($r_url, 'location');
				exit;
		        
		    } else {
				$this->template_data->alert("Unable to login!", 'danger');
			}
			
		}
		
		$this->load->view('login', $this->template_data->get() );
	}

	public function register1()
	{
		// load necessary files
		$this->load->library(array('form_validation'));
		$this->load->helper(array('date'));
		
		// set template variables
		$this->template_data->set('main_page', 'account' );
		$this->template_data->set('sub_page', 'register' );

		// validation 
		$this->form_validation->set_rules('parentName', 'Parent Name', 'trim|required');
		$this->form_validation->set_rules('parentEmail', 'Parent Email', 'trim|required|is_unique[ci_users.user_email]');
		$this->form_validation->set_rules('userID', 'Parent User ID', 'trim|required|callback_student_id_not_user_id[studentUserID]|is_unique[ci_users.user_username]');
		$this->form_validation->set_rules('password', 'Parent Password', 'trim|required|md5|sha1');
		$this->form_validation->set_rules('confirmPassword', 'Confirm Password', 'trim|required|matches[password]|md5|sha1');
		$this->form_validation->set_rules('studentName', 'Student Name', 'trim|required');
		$this->form_validation->set_rules('studentUserID', 'Student User ID', 'trim|required|callback_student_id_not_user_id[userID]|is_unique[ci_users.user_username]');
		$this->form_validation->set_rules('studentPassword', 'Student Password', 'trim|required|md5|sha1');
		
		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
		        $this->template_data->alert( validation_errors(), 'danger');
		    }
		} 
		else {
			$this->load->model('Users_model');
			$parent = new $this->Users_model;
			$parent->setUserUsername( $this->input->post('userID') );
			$parent->setUserPassword( $this->input->post('password') );
			$parent->setUserEmail(  $this->input->post('parentEmail') );
			$parent->setUserFirstname(  $this->input->post('parentName') );
			$parent->setUserType( 'parent');
			$parent->setUserActive( 1 );
			$parent->setUserPlan( 'free' );
			$parent->setUserCreated( unix_to_human( time(), TRUE, 'eu' ) );
			
			if( $parent->insert() ) {
				$student = new $this->Users_model;
				$student->setUserUsername( $this->input->post('studentUserID') );
				$student->setUserPassword( $this->input->post('studentPassword') );
				$student->setUserFirstname(  $this->input->post('studentName') );
				$student->setUserType( 'student');
				$student->setUserActive( 1 );
				$student->setUserPlan( 'free' );
				$student->setUserCreated( unix_to_human( time(), TRUE, 'eu' ) );
				$student->setUserParent( $parent->getUserId() );
				if( $student->insert() ) {
					redirect('upgrade/' . $student->getUserId(), 'refresh');
					exit;
				}
			}
			
		}
		
		$this->load->view('register', $this->template_data->get() );
	}
	
	public function register()
	{
		// load necessary files
		$this->load->library(array('form_validation'));
		$this->load->helper(array('date'));
		
		// set template variables
		$this->template_data->set('main_page', 'account' );
		$this->template_data->set('sub_page', 'register' );

		// validation 
		$this->form_validation->set_rules('parentName', 'Parent Name', 'trim|required');
		$this->form_validation->set_rules('parentEmail', 'Parent Email', 'trim|required|is_unique[ci_users.user_email]');
		$this->form_validation->set_rules('userID', 'Parent User ID', 'trim|required|is_unique[ci_users.user_username]');
		$this->form_validation->set_rules('password', 'Parent Password', 'trim|required|sha1|md5');
		$this->form_validation->set_rules('confirmPassword', 'Confirm Password', 'trim|required|matches[password]|sha1|md5');

		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
		        $this->template_data->alert( validation_errors(), 'danger');
		    }
		} 
		else {
			$this->load->model('Users_model');
			$parent = new $this->Users_model;
			$parent->setUserUsername( $this->input->post('userID') );
			$parent->setUserPassword( $this->input->post('password') );
			$parent->setUserEmail(  $this->input->post('parentEmail') );
			$parent->setUserFirstname(  $this->input->post('parentName') );
			$parent->setUserType( 'parent');
			$parent->setUserActive( 1 );
			$parent->setUserPlan( get_site_settings( 'free_membership_plan') );
			$parent->setUserCreated( unix_to_human( time(), TRUE, 'eu' ) );
			
			if( $parent->insert() ) {
				
				$this->session->set_userdata( array(
					'loggedIn' => true,
					'userId' => $parent->getUserID(),
					'userName' => $parent->getUserUsername(),
					'userType' => $parent->getUserType(),
					'userFirstname' => $parent->getUserFirstname()
				));
				redirect('my/add_student', 'location');
				exit;
			}
			
		}
		
		$this->load->view('register', $this->template_data->get() );
	}
	
	public function student_id_not_user_id($str,$mid) {
		if( !isset( $_POST[$mid] )) {
			return FALSE;
		}
		if ($str == $_POST[$mid])
		{
			$this->form_validation->set_message('student_id_not_user_id', 'The %s field should not be equal to %s field.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function upgrade($id)
	{
		if( $id == '') {
			redirect('/', 'refresh');
			exit;
		}
		$this->load->library(array('form_validation'));
		$this->load->helper(array('date'));
		
		$this->template_data->set('main_page', 'account' );
		$this->template_data->set('sub_page', 'upgrade' );
		
		$this->template_data->set('user_id', $id );
		
		$this->form_validation->set_rules('user_plan', 'Membership Plan', 'trim|required');
		if ($this->form_validation->run() == FALSE)
		{
			if( $this->input->post() ) {
		        $this->template_data->alert( validation_errors(), 'danger');
		    }
		} 
		else {
			
			$nextMonth = time() + (30 * 24 * 60 * 60);
			$this->load->model('Users_model');
			$student = new $this->Users_model;
			$student->setUserId( $id , true);
			if( $student->nonEmpty() === TRUE ) {
				$student->setUserPlan( $this->input->post('user_plan'), false, true );
				$student->setUserExpiry( unix_to_human( $nextMonth, TRUE, 'eu' ), false, true );
				if( $student->update() ) {
					$current_student = $student->getResults();
					if( $current_student->user_parent > 0 ) {
						$parent = new $this->Users_model;
						$parent->setUserId( $current_student->user_parent, true );
						$parent->setUserPlan( $this->input->post('user_plan'), false, true );
						$parent->setUserExpiry( unix_to_human( $nextMonth, TRUE, 'eu' ), false, true );
						$parent->update();
					}
					redirect('login', 'refresh');
					exit;
				}
			}
		}
		
		$this->load->view('upgrade', $this->template_data->get() );
	}

	public function forgot_password()
	{
		$this->template_data->set('main_page', 'account' );
		$this->load->view('forgot_password', $this->template_data->get() );
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
