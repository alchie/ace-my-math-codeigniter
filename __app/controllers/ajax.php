<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends MY_Controller {

	public function index()
	{
		$json_results = array(
			'is_logged_in' => ($this->session->userdata('loggedIn') == 1),
			'is_student' => ($this->session->userdata('userType') == 'student'),
		);
		
		switch( $this->input->post('action') ) {
			case 'getLevelChapters':
				if( $this->input->post('level_id') != '') {
					$this->load->model(array('Level_chapters_model'));
					$level_chapters = new $this->Level_chapters_model;
					$level_chapters->setLimit( 0 );
					$level_chapters->setJoin('tax_chapters', 'tax_chapters.chapter_id = level_chapters.chapter_id');
					$level_chapters->setLevelId( $this->input->post('level_id'), true );
					$level_chapters->setOrder('tax_chapters.chapter_order');
					echo json_encode( (object) array_merge( $json_results, array(
						'action' => 'getLevelChapters', 
						'results'=> $level_chapters->populate(),
						'level_id' => $this->input->post('level_id')
						)));
					exit;
				}
			break;
			case 'getLessons':
				if( $this->input->post('level_id') != '') {
					
					$limit = ($this->input->post('limit') !== FALSE) ? $this->input->post('limit') : 0;
					
					$this->load->model(array('Lessons_model'));
					$this->Lessons_model->setLimit( $limit );
					$this->Lessons_model->setLessonLevel( $this->input->post('level_id'), true );
					$this->Lessons_model->setLessonActive( 1, true );
					
					if( $this->input->post('chapter_id') != '' ) {
						$this->Lessons_model->setLessonChapter( $this->input->post('chapter_id'), true );
						$this->Lessons_model->setOrder('lesson_number', 'ASC');
					} else {
						$this->Lessons_model->setOrder('lesson_id', 'DESC');
					}
					
					$lessons_results = array(
						'action' => 'getLessons', 
						'results'=> $this->Lessons_model->populate(),
						'level_id' => $this->input->post('level_id'),
						);
					
					if( $this->input->post('chapter_id') != '' ) {
						$lessons_results['chapter_id'] = $this->input->post('chapter_id');
					}
					
					echo json_encode( (object) array_merge( $json_results, $lessons_results));
					exit;
				}
			break;
			case 'getLesson':
				if( $this->input->post('lesson_id') != '' && $this->input->post('lesson_slug') != '') {
					
					$results = array(
						'action' => 'getLesson', 
						'display'=> FALSE,
						'lesson_id' => $this->input->post('lesson_id'),
						'lesson_slug' => $this->input->post('lesson_slug'),
						);
					
					$this->load->model(array('Lessons_model'));
					$lesson = new $this->Lessons_model;
					$lesson->setSelect('lessons.*');
					$lesson->setLessonSlug($this->input->post('lesson_slug'), true);
					$lesson->setLessonId($this->input->post('lesson_id'), true);
					
					if( $this->isLoggedIn() ) { 
						// check if it belongs to lifetime free plan
						$lesson->setSelect('(SELECT s_plan_id FROM ci_lessons_subscriptions WHERE ci_lessons.lesson_id = ci_lessons_subscriptions.lesson_id AND ci_lessons_subscriptions.s_plan_id = '. get_site_settings('free_membership_plan') .') as plan');
					} else {
						// check if it belongs to public plan
						$lesson->setSelect('(SELECT s_plan_id FROM ci_lessons_subscriptions WHERE ci_lessons.lesson_id = ci_lessons_subscriptions.lesson_id and ci_lessons_subscriptions.s_plan_id = '.get_site_settings('public_plan', 0).') as plan');
					}
					
					$lesson_results = $lesson->get();
					if( $lesson_results->plan ) {
						$results['display'] = true;
						$results['results'] = $lesson_results;
					
						// starting session
						if( 
						($this->session->userdata('loggedIn') == 1) && 
						($this->session->userdata('userType') == 'student') &&
						($this->session->userdata('userLevel') == $lesson_results->lesson_level)
						) {
							$this->load->model(array('Users_lessons_sessions_model'));
							$this->load->helper('date');
							
							$lesson_session = new $this->Users_lessons_sessions_model;
							$lesson_session->setUserId( $this->session->userdata('userId') );
							$lesson_session->setLessonId( $this->input->post('lesson_id') );
							$lesson_session->setStarted( unix_to_human( time(), TRUE, 'eu' ) );
							$lesson_session->insert();
							$results['session_id'] = $lesson_session->getUlsId();
						}
					
					}

					echo json_encode( (object) array_merge( $json_results, $results ) );
					exit;
				}
			break;
			case 'getLessonQuizzes':
				if( $this->input->post('lesson_id') != '') {
					$this->load->model(array('Lessons_quiz_model'));
					$this->Lessons_quiz_model->setLessonId($this->input->post('lesson_id'), true);
					$this->Lessons_quiz_model->setLimit(0);
					echo json_encode( (object) array_merge( $json_results, array(
						'action' => 'getLessonQuizzes', 
						'results'=> $this->Lessons_quiz_model->populate(),
						'lesson_id' => $this->input->post('lesson_id'),
						)));
					exit;
				}
			break;
			
			case 'recordLessonSession':
				if( ($this->session->userdata('loggedIn') == 1) && ($this->session->userdata('userType') == 'student') ) {
					
					$this->load->model(array('Users_lessons_quizzes_model'));
					$quiz_session = new $this->Users_lessons_quizzes_model;
					$quiz_session->setUserId( $this->session->userdata('userId') );
					$quiz_session->setSessionId( $this->input->post('session_id') );
					$quiz_session->setLessonId( $this->input->post('lesson_id') );
					$quiz_session->setQuizNumber( $this->input->post('quiz_number') );
					$quiz_session->setTimeTaken( $this->input->post('time_taken') );
					$quiz_session->setMistakes( $this->input->post('mistakes') );
					$quiz_session->insert();
					
					if( $this->input->post('end_session') != FALSE && $this->input->post('end_session') == 1 ) {
						$this->load->model(array('Users_lessons_sessions_model'));
						$this->load->helper('date');
						
						$lesson_session = new $this->Users_lessons_sessions_model;
						$lesson_session->setUserId( $this->session->userdata('userId'), TRUE );
						$lesson_session->setLessonId( $this->input->post('lesson_id'), TRUE );
						$lesson_session->setUlsId( $this->input->post('session_id'), TRUE );
						$lesson_session->setEnded(  unix_to_human( time(), TRUE, 'eu' ), FALSE, TRUE);
						$lesson_session->setTimeTaken( $this->input->post('session_time'), FALSE, TRUE);
						$lesson_session->setMistakes( $this->input->post('total_mistakes'), FALSE, TRUE);
						$lesson_session->update();
					}
					
					if( $this->input->post('quiz_data') != FALSE && $this->input->post('lesson_id') != FALSE ) {
						$quiz_data = $this->input->post('quiz_data');
						$this->load->model(array('Lessons_quiz_model'));
						$new_quiz = new $this->Lessons_quiz_model;
						$new_quiz->setLessonId( $this->input->post('lesson_id'), TRUE, TRUE );
						$new_quiz->setQuizNumber( $quiz_data['number'], TRUE, TRUE );
						$new_quiz->setQuizTime( $quiz_data['time'], FALSE, TRUE );
						$new_quiz->setQuizDetails( $quiz_data['details'], FALSE, TRUE );
						$new_quiz->setQuizQuestion( $quiz_data['question'], FALSE, TRUE );
						$new_quiz->setQuizCorrect( $quiz_data['answer'], FALSE, TRUE );
						$new_quiz->setQuizType( $quiz_data['type'], FALSE, TRUE );
						if( $new_quiz->nonEmpty() == FALSE ) {
							$new_quiz->insert();
						} else {
							//$new_quiz->update();
						}
					}
				}
				
				echo json_encode( (object) array_merge( $json_results, $this->input->post() ));
				exit;
			break;
			
			case 'search':
				if( $this->input->post('key') != '') {
					$this->load->model(array('Tax_tags_model', 'Lessons_model'));
					$this->Tax_tags_model->setFilter('tag_name LIKE', '%'.$this->input->post('key').'%');
					$this->Tax_tags_model->setLimit(10);
					$this->Lessons_model->setFilter('lesson_title LIKE', '%'.$this->input->post('key').'%');
					$this->Lessons_model->setJoin('tax_levels', 'lessons.lesson_level = tax_levels.level_id', 'left');
					$this->Lessons_model->setLimit(10);
					echo json_encode( (object) array_merge( $json_results, array(
						'action' => 'search', 
						'tags'=> $this->Tax_tags_model->populate(),
						'lessons'=> $this->Lessons_model->populate(),
						'key' => $this->input->post('key'),
						)));
					exit;
				}
			break;
			
		}
		echo json_encode(0);
		exit;
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
