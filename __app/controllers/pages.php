<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends MY_Controller {

	public function index($page='default')
	{
		$this->template_data->set('main_page', 'pages' );
		
		$page_title = get_site_settings( $page . '_title', 'Ace My Math');
		$this->template_data->set('page_title', $page_title);
		
		$this->load->view($page, $this->template_data->get() );
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
