<?php

function get_my_settings($controller='site', $name, $default=NULL) {
	$CI =& get_instance();
	$CI->load->model('settings_model');
	
	$settings = new $CI->settings_model;
	$settings->setSettingController($controller, TRUE);
	$settings->setSettingName($name, TRUE);
	
	$value = '';
	if( $settings->nonEmpty() === TRUE ) {
		$results = $settings->getResults();
		$value = $results->setting_value;
	} 
	if( ! is_null($default)  ) {
		if($value == '') {
			$value = $default;
		}
	}
	return $value;
}

function get_site_settings($name, $default=NULL) {
	return get_my_settings('site', $name, $default);
}
