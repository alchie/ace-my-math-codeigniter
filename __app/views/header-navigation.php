 <!-- #site-navigation -->
<div id="site-navigation">
 
<nav id="header-primary" class="navbar navbar-default">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand visible-xs" href="#">Navigation</a>
  </div>

  <div class="collapse navbar-collapse navbar-ex1-collapse" id="bs-example-navbar-collapse-1">
	  
<?php if( isset( $current_session->loggedIn ) ) : ?>

<ul id="menu-members-primary" class="nav navbar-nav navbar-left">
	<li><a href="<?php echo site_url('my/dashboard'); ?>">Home</a></li>
	<?php if( $current_session->userType == 'parent' ) { ?>
		<li><a href="<?php echo site_url('my/students'); ?>">My Students</a></li>
		<li><a href="<?php echo site_url('my/goals'); ?>">Goals</a></li>
	<?php }  ?>
	<?php if( $current_session->userType == 'student' ) { ?>
		<li><a href="<?php echo site_url('my/lessons'); ?>">My Lessons</a></li>
		<li><a href="<?php echo site_url('my/goals'); ?>">My Goals</a></li>
	<?php }  ?>
</ul>



<ul class="nav navbar-nav navbar-right">
    <li><a href="<?php echo site_url('my/account'); ?>">My Account</a></li>
    <li><a href="<?php echo site_url('logout'); ?>">Logout</a></li>    
</ul>

<?php else : ?>

		<ul id="menu-primary-menu" class="nav navbar-nav navbar-left">
			<li><a title="lorem ipsum" href="<?php echo base_url(); ?>">Home</a></li>
			<li><a href="<?php echo site_url('try-it-now'); ?>">Try it Now</a></li>
			<li><a href="<?php echo site_url('guarantee'); ?>">Guarantee</a></li>
			<li><a href="<?php echo site_url('what-you-get'); ?>">What You Get</a></li>
			<li><a href="<?php echo site_url('lessons'); ?>">Lessons</a></li>
			<li><a href="<?php echo site_url('free-samples'); ?>">Free Samples</a></li>
			<li><a href="<?php echo site_url('our-method'); ?>">Our Method</a></li>
			<li><a href="<?php echo site_url('about-us'); ?>">About Us</a></li>
		</ul>

<form role="search" method="get" class="search-form" action="<?php echo site_url('search'); ?>">
				<label>
					<span class="screen-reader-text">Search for:</span>
					<input id="lesson-search" type="search" class="search-field lesson-tag-search" placeholder="Search …" value="<?php echo $this->input->get('s'); ?>" name="s" title="Search for:" autocomplete="off">
				</label>
				<input type="submit" class="search-submit" value="Search">
</form>

<?php endif; ?>

</div>

</nav>
</div>
     
