<?php $this->load->view('overall_header'); ?>
<?php 
$default_level = (isset($current->lesson_level)) ? $current->lesson_level : 0; 
$default_chapter = (isset($current->lesson_chapter)) ? $current->lesson_chapter : 1; 
?>
<div id="main-container" class="lessons-list">
    <div class="container">

      <div class="row">
      <div class="col-md-1">
	
	 <ul class="nav nav-pills nav-stacked nav-gradelevel">
        <?php if( $levels ) foreach($levels as $level) { ?>
          <li <?php echo (isset( $current_level->level_id ) && $current_level->level_id == $level->level_id ) ? 'class="active"' : ''; ?>>
          <a data-id="<?php echo $level->level_id; ?>" class="level-link" id="level-link-<?php echo $level->level_id; ?>" href="<?php echo site_url('lessons/level/' . $level->level_slug); ?>"><?php echo $level->level_name; ?></a>
          </li>
		<?php 
		$default_level = ($default_level == 0) ? $level->level_id : $default_level;
		} ?>
     </ul>
        
		</div>
      <div class="col-md-8">
 
    <div id="main-content" class="whitebox add-padding">

<h4><strong>Lesson <?php echo $current->lesson_number; ?>:</strong> <?php echo $current->lesson_title; ?></h4>

<div class="btn-group btn-group-sm btn-group-justified btn-group-actions">
  <a href="<?php echo site_url('lesson/' .$current->lesson_slug. '/video'); ?>" class="btn btn-primary first btn-lesson-video"><span class="glyphicon glyphicon-film"></span> Video Lesson</a>
  <span class="btn btn-danger btn-lesson-exercise"><span class="glyphicon glyphicon-pencil"></span> Practice</span>
  <a href="<?php echo site_url('lesson/' .$current->lesson_slug. '/worksheet'); ?>" class="btn btn-primary btn-lesson-worksheet"><span class="glyphicon glyphicon-list-alt"></span> Worksheets</a>
  <a href="<?php echo site_url('lesson/' .$current->lesson_slug. '/report'); ?>" class="btn btn-primary last btn-lesson-report"><span class="glyphicon glyphicon-signal"></span> Report</a>
</div>

    </div> <!-- whitebox -->
    
      </div> <!-- column 8 -->
      
      <div class="col-md-3">
        
            <div class="sidebar whitebox">
            
            <div id="secondary">
	<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
		<aside id="testimonials" class="block-inner widget widget_testimonials">		
		<div class="widget-header"><h3 class="widget-title">Testimonials</h3><em></em></div>
				<ul>
					<li>
					    
					                                <img src="<?php echo base_url(); ?>assets/images/no-image-available.jpg" class="" style="width:150px">
                        					    <div class="testi-content">
					        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean dictum tristique erat ac pulvinar. Nam in egestas nunc. Donec id odio augue. Aenean tellus orci, eleifend sollicitudin venenatis et, pharetra in orci. Donec vel nunc sit amet odio bibendum vehicula. Curabitur pharetra sem eget erat feugiat, eget viverra ipsum fringilla. Quisque ac cursus leo.</p>
					    </div>
					     <div class="clearfix"></div>
				        <div class="testi-author"><a href="<?php echo base_url(); ?>testimonial/sam-patrick/">Sam Patrick</a></div>
				       
					</li>
					<li>
					    
					                                <img src="<?php echo base_url(); ?>assets/images/no-image-available.jpg" class="" style="width:150px">
                        					    <div class="testi-content">
					        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean dictum tristique erat ac pulvinar. Nam in egestas nunc. Donec id odio augue. Aenean tellus orci, eleifend sollicitudin venenatis et, pharetra in orci. Donec vel nunc sit amet odio bibendum vehicula. Curabitur pharetra sem eget erat feugiat, eget viverra ipsum fringilla. Quisque ac cursus leo.</p>
					    </div>
					     <div class="clearfix"></div>
				        <div class="testi-author"><a href="<?php echo base_url(); ?>testimonial/romel/">Romel</a></div>
				       
					</li>
					<li>
					    
					                                <img src="<?php echo base_url(); ?>assets/images/no-image-available.jpg" class="" style="width:150px">
                        					    <div class="testi-content">
					        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean dictum tristique erat ac pulvinar. Nam in egestas nunc. Donec id odio augue. Aenean tellus orci, eleifend sollicitudin venenatis et, pharetra in orci. Donec vel nunc sit amet odio bibendum vehicula. Curabitur pharetra sem eget erat feugiat, eget viverra ipsum fringilla. Quisque ac cursus leo.</p>
					    </div>
					     <div class="clearfix"></div>
				        <div class="testi-author"><a href="<?php echo base_url(); ?>testimonial/chester-alan/">Chester Alan</a></div>
				       
					</li>
				</ul>
				 
		</aside>
	</div><!-- #primary-sidebar -->
</div><!-- #secondary -->
            
           </div>
           
        </div>   

      </div><!-- row -->

    </div><!-- container-->
   </div> 
<script>
var baseURL = '<?php echo base_url(); ?>';
var ajaxURL = '<?php echo site_url('ajax'); ?>';
var default_level = <?php echo $default_level; ?>;
var default_chapter = <?php echo $default_chapter; ?>;
$('#level-link-'+ default_level).parent('li').addClass('active');
</script>
<?php $this->load->view('overall_footer'); ?>
