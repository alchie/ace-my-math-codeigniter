<?php $this->load->view('overall_header'); ?>
<div id="main-container" class="lessons-list">
    <div class="container">

      <div class="row">
      <div class="col-md-1">
	
	 <ul class="nav nav-pills nav-stacked nav-gradelevel">
        <?php if( $levels ) foreach($levels as $level) { ?>
          <li <?php echo (isset( $current_level->level_id ) && $current_level->level_id == $level->level_id ) ? 'class="active"' : ''; ?>>
          <a data-id="<?php echo $level->level_id; ?>" class="level-link" id="level-link-<?php echo $level->level_id; ?>" href="<?php echo site_url('lessons/level/' . $level->level_slug); ?>"><?php echo $level->level_name; ?></a>
          </li>
		<?php 
		} ?>
     </ul>
        
		</div>
      <div class="col-md-8">
	<a href="<?php echo site_url(array("lesson", $lesson_id . "-" .$lesson_slug, "quizzes" )); ?>" class="btn btn-xs btn-danger pull-right">Quizzes</a>
    <div id="main-content" class="whitebox add-padding">
	
	<center style="margin-top:150px">
		<img src="<?php echo base_url(); ?>assets/img/bx_loader.gif">
	</center>
	
    </div> <!-- whitebox -->
    
      </div> <!-- column 8 -->
      
      <div class="col-md-3">
        
           <div class="sidebar whitebox">
            
            
           </div> 
           
        </div>   

      </div><!-- row -->

    </div><!-- container-->
   </div> 
<script>
var baseURL = '<?php echo base_url(); ?>';
var ajaxURL = '<?php echo site_url('ajax'); ?>';
var assetsURL = '<?php echo site_url('assets'); ?>';
$('#main-content').aceMyMath('loadLessonVideo', <?php echo $lesson_id; ?>, '<?php echo $lesson_slug; ?>');
</script>
<?php $this->load->view('overall_footer'); ?>
