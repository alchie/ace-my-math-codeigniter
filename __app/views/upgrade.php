<?php $this->load->view('overall_header'); ?>
<div id="main-container">
    <div class="container">
        <div class="container-inner">
      <div class="row">
        <div class="col-md-offset-4 col-md-4 main-content">
			
<?php if ( $alert ) { ?>
<div class="alert alert-<?php echo $alert_status; ?> alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $alert_message; ?>
  </div>
<?php } ?>

			<h3 class="text-center">Upgrade Account</h3>
			<div class="row">
				<div class="col-md-12">
					<form action="" method="post">
						<input type="hidden" name="user_plan" value="free" />
						<button type="submit" class="btn btn-default btn-block">FREE</button>
					</form>
				</div>
				<div class="col-md-12">
					<form action="" method="post">
						<input type="hidden" name="user_plan" value="standard" />
						<button type="submit" class="btn btn-default btn-block">Standard</button>
					</form>
				</div>
				<div class="col-md-12">
					<form action="" method="post">
						<input type="hidden" name="user_plan" value="gold-star" />
						<button type="submit" class="btn btn-default btn-block">Gold Star</button>
					</form>
				</div>
				<div class="col-md-12">
					<form action="" method="post">
						<input type="hidden" name="user_plan" value="gold-star-plus" />
						<button type="submit" class="btn btn-default btn-block">Gold Star Plus</button>
					</form>
				</div>
				<div class="col-md-12">
					<form action="" method="post">
						<input type="hidden" name="user_plan" value="gold-star-deluxe" />
						<button type="submit" class="btn btn-default btn-block">Gold Star Deluxe</button>
					</form>
				</div>
			</div>
			
        </div>
	  </div><!-- row -->
      </div><!-- container-inner -->
    </div><!-- container-->
   </div>
<?php $this->load->view('overall_footer'); ?>
