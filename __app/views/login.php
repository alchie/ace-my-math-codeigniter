<?php $this->load->view('overall_header'); ?>

<div class="container" style="margin-top:50px;">
<div class="row">
    <div class="col-md-offset-4 col-md-4">
    
      <form name="loginform" id="loginform" class="login-container" action="" method="post">
       
       
  <div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Login</h3>
  </div>
  <div class="panel-body">
   
<?php if ( $alert ) { ?>
<div class="alert alert-<?php echo $alert_status; ?> alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $alert_message; ?>
  </div>
<?php } ?>
   
  <div class="form-group">
    <label for="username">Username</label>
    <input type="text" name="username" class="form-control" id="username" placeholder="Enter username">
  </div>
  <div class="form-group">
    <label for="password">Password</label>
    <input type="password" name="password" class="form-control" id="password" placeholder="Password">
  </div>

 <input type="hidden" name="redirect_to" value="http://acemymath">
		    <input type="hidden" name="testcookie" value="1">
		    
   
  </div>
   <div class="panel-footer"><button type="submit" class="btn btn-success btn-sm btn-block">Login</button></div>
</div>
</form>
<a href="<?php echo site_url('forgotpassword'); ?>" class="pull-right">Forgot Password?</a>
<a href="<?php echo site_url('register'); ?>">Create New Account</a> 
</div>
</div>
</div>

<?php $this->load->view('overall_footer'); ?>
