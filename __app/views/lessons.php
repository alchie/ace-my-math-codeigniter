<?php $this->load->view('overall_header'); ?>
<?php 
$default_level = (isset($current_level->level_id)) ? $current_level->level_id : 0; 
$default_chapter = 1;
?>
<div id="main-container" class="lessons-list">
    <div class="container">

      <div class="row">
      <div class="col-md-1">
	
	 <ul class="nav nav-pills nav-stacked nav-gradelevel">
        <?php if( $levels ) foreach($levels as $level) { ?>
          <li <?php echo (isset( $current_level->level_id ) && $current_level->level_id == $level->level_id ) ? 'class="active"' : ''; ?>>
          <a data-id="<?php echo $level->level_id; ?>" class="level-link" id="level-link-<?php echo $level->level_id; ?>" href="<?php echo site_url('lessons/level/' . $level->level_slug); ?>"><?php echo $level->level_name; ?></a>
          </li>
		<?php 
		$default_level = ($default_level == 0) ? $level->level_id : $default_level;
		} ?>
     </ul>
        
		</div>
      <div class="col-md-8">
 
    <div id="main-content" class="whitebox add-padding">

	<center style="margin-top:100px" id="ajax-loader"><img src="<?php echo base_url(); ?>assets/img/bx_loader.gif" /></center>

    </div> <!-- whitebox -->
    
      </div> <!-- column 8 -->
      
      <div class="col-md-3">
        
            <div class="sidebar whitebox">
            
            
           </div>
           
        </div>   

      </div><!-- row -->

    </div><!-- container-->
   </div> 
<script>
var default_level = <?php echo $default_level; ?>;
var default_chapter = <?php echo $default_chapter; ?>;
$('#main-content').aceMyMath('loadLessonsPage', <?php echo $default_level; ?>);
</script>
<?php $this->load->view('overall_footer'); ?>
