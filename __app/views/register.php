<?php $this->load->view('overall_header'); ?>

<div id="main-container" class="registration">
    <div class="container">

 <div class="row">
             <div class="col-md-12">
                 <header class="entry-header">
                
                <h1 class="entry-title">Registration</h1>
                
                 </header>
            </div>

        </div>
        
      <div class="row">
      

        <div class="col-md-9">
        
        
        
            <div class="main-content whitebox">
           

	<div class="entry-content">
		
		<?php if ( $alert ) { ?>
<div class="alert alert-<?php echo $alert_status; ?> alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $alert_message; ?>
  </div>
<?php } ?>	
		
<form class="form-horizontal" role="form" method="post">
 

<h3>Parent Account Information</h3>
    
  <div class="form-group">
    <label for="parentName" class="col-sm-4 control-label">Parent Name</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" name="parentName" id="parentName" placeholder="Parent Name" value="<?php echo $this->input->post('parentName'); ?>">
    </div>
  </div>
  
  <div class="form-group">
    <label for="parentEmail" class="col-sm-4 control-label">Email</label>
    <div class="col-sm-5">
      <input type="email" class="form-control" name="parentEmail" id="parentEmail" placeholder="Email" value="<?php echo $this->input->post('parentEmail'); ?>">
    </div>
  </div>


  <div class="form-group">
    <label for="userID" class="col-sm-4 control-label">User ID</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" id="userID" name="userID" placeholder="User ID" value="<?php echo $this->input->post('userID'); ?>">
    </div>
  </div>
     
    <div class="form-group">
    <label for="password" class="col-sm-4 control-label">Password</label>
    <div class="col-sm-5">
      <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="">
    </div>
  </div>
     
     
   <div class="form-group">
    <label for="confirmPassword" class="col-sm-4 control-label">Confirm Password</label>
    <div class="col-sm-5">
      <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" placeholder="Confirm Password" value="">
    </div>
  </div>
 <?php /*
 <h3>Student Account Information</h3>
 
  <div class="form-group">
    <label for="studentName" class="col-sm-4 control-label">Student Name</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" id="studentName" name="studentName" placeholder="Student Name" value="<?php echo $this->input->post('studentName'); ?>">
    </div>
  </div>
  
    <div class="form-group">
    <label for="studentUserID" class="col-sm-4 control-label">Student User ID</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" id="studentUserID" name="studentUserID" placeholder="Student User ID" value="<?php echo $this->input->post('studentUserID'); ?>">
    </div>
  </div>
  
    <div class="form-group">
    <label for="studentPassword" class="col-sm-4 control-label">Student Password</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" id="studentPassword" name="studentPassword" placeholder="Student Password" value="">
    </div>
  </div>
  */ ?>
      <div class="form-group">
    <div class="col-sm-offset-4 col-sm-10">
      <button type="submit" class="btn btn-default ">Register <span class="glyphicon glyphicon-chevron-right"></span></button>
    </div>
  </div>
      
</form>
		
	</div><!-- .entry-content -->



			</div>
        </div>
        <div class="col-md-3">
            <div class="sidebar whitebox">
            <div id="secondary">
	<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
		</div><!-- #primary-sidebar -->
</div><!-- #secondary -->
           </div>
        </div>
      </div><!-- row -->

    </div><!-- container-->
   </div>
<?php $this->load->view('overall_footer'); ?>
