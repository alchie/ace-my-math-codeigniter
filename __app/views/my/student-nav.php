<ul class="nav nav-pills nav-stacked nav-gradelevel">
	<?php foreach( array(
		'activities'=>'Activities',
		'lessons'=>'Lessons',
		'goals'=>'Goals',
		'certificates'=>'Certificates',
		'awards'=>'Awards',
		'update'=>'Profile',
		'upgrade'=>'Subscription'
	) as $nav=>$label) { 
		if( $this->session->userdata('userType') == 'student' ) { 
			$url = site_url('my/'.$nav);
		} else {
			$url = site_url('my/student/'.$current_student->user_username.'/'.$nav);
		}
		?>
		<li <?php echo ($action==$nav) ? 'class="active"' : ''; ?>>
			<a href="<?php echo $url; ?>"><?php echo $label; ?></a>
		</li>
	<?php }?>
</ul>