<?php $this->load->view('overall_header'); ?>

<div id="main-container" class="lessons-list">
    <div class="container">

      <div class="row">
      <div class="col-md-1">
	
		<?php $this->load->view('my/student-nav'); ?>
        
		</div>
      <div class="col-md-8">
 
    <div id="main-content" class="whitebox add-padding">
<h3>Recent Activities</h3>
<div class="list-group">
	<?php foreach ( $activities as $activity ) {  
		if( $this->session->userdata('userType') == 'student' ) {
			$activity_base_url = site_url(array('my', 'lesson_result'));
		} else {
			$activity_base_url = site_url(array('my', 'student', $current_student->user_username, 'lesson_result'));
		}
	?>
		<a href="<?php echo $activity_base_url; ?>?session=<?php echo $activity->uls_id; ?>" class="list-group-item">
			<h4><?php echo $activity->lesson_title; ?></h4>
		</a>
	<?php } ?>
  
</div>

    </div> <!-- whitebox -->
    
      </div> <!-- column 8 -->
      
      <div class="col-md-3">
        

				
<?php $this->load->view('my/student-profile-sidebar'); ?>

           
        </div>   

      </div><!-- row -->

    </div><!-- container-->
   </div> 



<?php $this->load->view('overall_footer'); ?>
