<?php $this->load->view('overall_header'); ?>

<div id="main-container">
    <div class="container">

      <div class="row">
      
         <div class="col-md-12">


<div style="margin-top:20px;">

<div class="row">
	
<?php foreach( $students as $student ) { ?>
	<div class="col-md-6">
	
	<div class="panel panel-default">
  <div class="panel-heading">
	  <a href="<?php echo site_url('my/student/'.$student->user_username); ?>" class="pull-right">View</a>
    <h3 class="panel-title"><?php echo $student->user_username; ?></strong></h3>
  </div>
  <div class="panel-body"></div>
</div>
	</div>

<?php } ?>

	
</div>
</div>


        </div>
        

      </div><!-- row -->

    </div><!-- container-->
   </div>

<?php $this->load->view('overall_footer'); ?>
