<?php $this->load->view('overall_header'); ?>

<div id="main-container" class="lessons-list">
    <div class="container">
      
	<div class="row">
      <div class="col-md-1">
	
	 <ul class="nav nav-pills nav-stacked nav-gradelevel">
       <?php foreach( $students as $student ) : ?>
          
			  
<?php if ($student->user_active == 1) { ?>
	<li><a href="<?php echo site_url('my/student/' . $student->user_username ); ?>" title="<?php echo $student->user_firstname; ?>"><?php echo $student->user_username; ?></a></li>
<?php } else { ?>
	<li class="inactive"><a href="<?php echo site_url('my/student/' . $student->user_username . '/activate' ); ?>" title="<?php echo $student->user_firstname; ?>"><?php echo $student->user_username; ?></a></li>
<?php } ?> 

          
		<?php endforeach; ?>
			<li><a href="<?php echo site_url('my/add_student' ); ?>"><span class="glyphicon glyphicon-plus" style="font-size:20px;"></span></a></li>
     </ul>
        
		</div>
      <div class="col-md-8">
 
    <div id="main-content" class="whitebox add-padding">
	<h2>Recent Activities</h2>
 	<div class="list-group">
	<?php foreach ( $students_logs as $log ) { ?>
		<a href="<?php echo site_url(array("my", "student", $log->user_username, "lesson_result")); ?>?session=<?php echo $log->uls_id; ?>" class="list-group-item">
		<span class="badge"><?php echo $log->ended; ?></span> 
		<strong><?php echo $log->user_username; ?> - </strong><?php echo $log->lesson_title; ?>
		</a>
	<?php } ?>
</div>
	

    </div> <!-- whitebox -->
    
      </div> <!-- column 8 -->
      
      <div class="col-md-3">
        
            <div class="sidebar whitebox">
            
            
           </div>
           
        </div>   

      </div><!-- row -->

    </div><!-- container-->
   </div> 

<div id="main-container">
    <div class="container">

      <div class="row">
         <div class="col-md-12">




        </div>
      </div><!-- row -->

    </div><!-- container-->
   </div>
   <script>
   <!--
   $('.student-item').popover();
   -->
   </script>
<?php $this->load->view('overall_footer'); ?>
