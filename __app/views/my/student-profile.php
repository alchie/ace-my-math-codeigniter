<?php $this->load->view('overall_header'); ?>

<div id="main-container" class="lessons-list">
    <div class="container">

      <div class="row">
      <div class="col-md-1">
	
	 <ul class="nav nav-pills nav-stacked nav-gradelevel">
       
        <li class="active"><a href="<?php echo site_url('my/student/'.$current_student->user_username.'/activities'); ?>">Activities</a></li>
		<li><a href="<?php echo site_url('my/student/'.$current_student->user_username.'/lessons'); ?>">Lessons</a></li>
		<li><a href="<?php echo site_url('my/student/'.$current_student->user_username.'/goals'); ?>">Goals</a></li>
		<li><a href="<?php echo site_url('my/student/'.$current_student->user_username.'/certificates'); ?>">Certificates</a></li>
		<li><a href="<?php echo site_url('my/student/'.$current_student->user_username.'/awards'); ?>">Awards</a></li>
     </ul>
        
		</div>
      <div class="col-md-8">
 
    <div id="main-content" class="whitebox add-padding">
<h3>Recent Activities</h3>
<div class="list-group">
	<?php foreach ( $activities as $activity ) {  ?>
		<a href="<?php echo site_url(array('my', 'student', $current_student->user_username, 'lesson_result')); ?>?session=<?php echo $activity->uls_id; ?>" class="list-group-item">
			<h4><?php echo $activity->lesson_title; ?></h4>
		</a>
	<?php } ?>
  
</div>

    </div> <!-- whitebox -->
    
      </div> <!-- column 8 -->
      
      <div class="col-md-3">
        

				
<?php $this->load->view('my/student-profile-sidebar'); ?>

           
        </div>   

      </div><!-- row -->

    </div><!-- container-->
   </div> 



<?php $this->load->view('overall_footer'); ?>
