<?php $this->load->view('overall_header'); ?>


<div id="main-container">
    
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="page-header">
      <h3>Activating student: <strong><?php echo $current_student->user_username; ?></strong></h3>
      </div>
    </div>
  </div>
<div class="row">
	

<?php if ( $alert ) { ?>
<div class="col-md-12">
<div class="alert alert-<?php echo $alert_status; ?> alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $alert_message; ?>
  </div>
  </div>
<?php } ?>

<?php if( get_site_settings( 'gold_star_membership_plan') != '' ) { ?>
  <div class="col-md-4">
  <div class="panel panel-danger">
     <div class="panel-heading"><h3 class="text-center">GOLD STAR</h3></div>
         <div class="panel-body text-center">
			<p class="lead" style="font-size:40px"><strong>$20 / month</strong></p>
		</div>
                       <ul class="list-group list-group-flush text-center">
							<li class="list-group-item"><i class="icon-ok text-danger"></i> Personal use</li>
							<li class="list-group-item"><i class="icon-ok text-danger"></i> Unlimited projects</li>
							<li class="list-group-item"><i class="icon-ok text-danger"></i> 27/7 support</li>
						</ul>

    <div class="panel-footer">
		<?php if ( $current_student->user_plan != get_site_settings( 'gold_star_membership_plan') ) { ?>
		<form class="activate-container" action="<?php echo site_url('my/student/'. $current_student->user_username .'/activate'); ?>" method="post">
		<input type="hidden" name="plan" value="<?php echo get_site_settings( 'gold_star_membership_plan'); ?>">
		<button class="btn btn-lg btn-block btn-danger" type="submit">ACTIVATE NOW!</button>
		</form>
		<?php } else { ?>
			<span class="btn btn-lg btn-block btn-success">ACTIVATED</span>
		<?php } ?>
	</div>

 </div>
  </div>
<?php } ?>

<?php if( get_site_settings( 'standard_membership_plan') != '' ) { ?>
   <div class="col-md-4">
  <div class="panel panel-info">
     <div class="panel-heading"><h3 class="text-center">STANDARD</h3></div>
         <div class="panel-body text-center">
			<p class="lead" style="font-size:40px"><strong>$10 / month</strong></p>
		</div>
                       <ul class="list-group list-group-flush text-center">
							<li class="list-group-item"><i class="icon-ok text-danger"></i> Personal use</li>
							<li class="list-group-item"><i class="icon-ok text-danger"></i> Unlimited projects</li>
							<li class="list-group-item"><i class="icon-ok text-danger"></i> 27/7 support</li>
						</ul>
    <div class="panel-footer">
		<?php if ( $current_student->user_plan != get_site_settings( 'standard_membership_plan') ) { ?>
		<form class="activate-container" action="<?php echo site_url('my/student/'. $current_student->user_username .'/activate'); ?>" method="post">
		<input type="hidden" name="plan" value="<?php echo get_site_settings( 'standard_membership_plan'); ?>">
		<button class="btn btn-lg btn-block btn-danger" type="submit">ACTIVATE NOW!</button>
		</form>
		<?php } else { ?>
			<span class="btn btn-lg btn-block btn-success">ACTIVATED</span>
		<?php } ?>
	</div>

 </div>
  </div>
<?php } ?>

<?php if( get_site_settings( 'free_membership_plan') != '' ) { ?>
   <div class="col-md-4">
  <div class="panel panel-success">
    <div class="panel-heading"><h3 class="text-center">FREE TRIAL</h3></div>
         <div class="panel-body text-center">
			<p class="lead" style="font-size:40px"><strong>$0 / month</strong></p>
		</div>
                       <ul class="list-group list-group-flush text-center">
							<li class="list-group-item"><i class="icon-ok text-danger"></i> Personal use</li>
							<li class="list-group-item"><i class="icon-ok text-danger"></i> Unlimited projects</li>
							<li class="list-group-item"><i class="icon-ok text-danger"></i> 27/7 support</li>
						</ul>
    <div class="panel-footer">
		<?php if ( $current_student->user_plan != get_site_settings( 'free_membership_plan') ) { ?>
		<form class="activate-container" action="<?php echo site_url('my/student/'. $current_student->user_username .'/activate'); ?>" method="post">
		<input type="hidden" name="plan" value="<?php echo get_site_settings( 'free_membership_plan'); ?>">
		<button class="btn btn-lg btn-block btn-danger" type="submit">ACTIVATE NOW!</button>
		</form>
		<?php } else { ?>
			<span class="btn btn-lg btn-block btn-success">ACTIVATED</span>
		<?php } ?>
	</div>
 </div>
  </div>
<?php } ?>
  
  </div>
</div>
    
</div>
   
<?php $this->load->view('overall_footer'); ?>
