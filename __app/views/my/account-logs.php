<?php $this->load->view('overall_header'); ?>

<div id="main-container" class="lessons-list">
    <div class="container">

      <div class="row">
      <div class="col-md-1">
	
	 <ul class="nav nav-pills nav-stacked nav-gradelevel">
       
          <li><a href="<?php echo site_url('my/account/manage'); ?>">Manage</a></li>
		<li class="active"><a href="<?php echo site_url('my/account/logs'); ?>">Logs</a></li>
     </ul>
        
		</div>
      <div class="col-md-8">
 
    <div id="main-content" class="whitebox add-padding">

	<ul class="list-group">
	<?php foreach ( $logs as $log ) { ?>
		<li class="list-group-item"><span class="badge"><?php echo $log->log_date; ?></span> <?php echo $log->log_code; ?> - <?php echo $log->log_msg; ?></li>
	<?php } ?>
  
</ul>

	

    </div> <!-- whitebox -->
    
      </div> <!-- column 8 -->
      
      <div class="col-md-3">
        
            <div class="sidebar whitebox">
            


           </div>
           
        </div>   

      </div><!-- row -->

    </div><!-- container-->
   </div> 
   
<?php $this->load->view('overall_footer'); ?>
