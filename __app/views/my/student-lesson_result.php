<?php $this->load->view('overall_header'); ?>

<div id="main-container" class="lessons-list">
    <div class="container">

      <div class="row">
      <div class="col-md-1">
	
		<?php $this->load->view('my/student-nav'); ?>
        
		</div>
      <div class="col-md-8">
 
    <div id="main-content" class="whitebox add-padding">
	<a class="btn btn-xs btn-success pull-right" href="<?php echo site_url(array("lesson", $activity->lesson_id . "-" . $activity->lesson_slug , "video")); ?>">Open</a>
<h3><?php echo $activity->lesson_title; ?></h3>

<div class="table-responsive">
  <table class="table table-striped">
		<thead>
			<tr>
				<th width="2%">#</th>
				<th>Quiz</th>
				<th align="center" width="15%" class="text-center">Time Taken <br>( in seconds )</th>
				<th align="center" width="10%" class="text-center">Mistakes</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach( $quizzes as $quiz ) { ?>
			<tr class="quiz-item">
				<td><?php echo $quiz->quiz_number; ?></td>
				<td><strong>
				<?php echo $quiz->quiz_question; ?>
				</strong>		
					<div class="panel panel-primary quiz-details">
					  <div class="panel-heading">
					  <span class="badge pull-right"><strong><?php echo $quiz->quiz_time; ?></strong></span>
						<h3 class="panel-title"><?php echo $quiz->quiz_question; ?></h3>
					  </div>
					  <?php if( $quiz->quiz_details != '' ) { ?>
					  <div class="panel-body">
						<?php echo $quiz->quiz_details; ?>
					  </div>
					  <?php } ?>
					   <div class="panel-footer">
					   
					   ANSWER: <span style="color:red"><?php echo $quiz->quiz_correct; ?></span>
					   </div>
					</div>
				
				
				</td>
				<td align="center"><?php echo $quiz->time_taken; ?></td>
				<td align="center"><?php echo $quiz->mistakes; ?></td>
			</tr>
		<?php } ?>
		</tbody>
  </table>
</div>
    </div> <!-- whitebox -->
    
      </div> <!-- column 8 -->
      
      <div class="col-md-3">
        

				
<?php $this->load->view('my/student-profile-sidebar'); ?>

           
        </div>   

      </div><!-- row -->

    </div><!-- container-->
   </div> 



<?php $this->load->view('overall_footer'); ?>
