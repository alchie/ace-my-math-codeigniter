<?php $this->load->view('overall_header'); ?>

<div id="main-container" class="lessons-list">
    <div class="container">

      <div class="row">
      <div class="col-md-1">
	
	 <ul class="nav nav-pills nav-stacked nav-gradelevel">
       
          <li class="active"><a href="<?php echo site_url('my/account/manage'); ?>">Manage</a></li>
		<li><a href="<?php echo site_url('my/account/logs'); ?>">Logs</a></li>
     </ul>
        
		</div>
      <div class="col-md-8">
 
    <div id="main-content" class="whitebox add-padding">

	
<?php if ( $alert ) { ?>
<div class="alert alert-<?php echo $alert_status; ?> alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $alert_message; ?>
  </div>
<?php } ?>

<form name="studentform" id="studentform" class="login-container" action="" method="post">
       
       
  <div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Manage Account : <strong><?php echo $current_account->user_username; ?></strong></h3>
  </div>
  <div class="panel-body">
    
    <?php if( $this->session->userdata('userType') == 'student' ) { ?>
  <div class="form-group">
    <label for="gradelevel">Grade Level</label>
    <select name="gradelevel" class="form-control">
        <option value="">- - Select Grade Level - - </option>
        <?php foreach( $grade_levels as $level ) { ?>
            <option value="<?php echo $level->level_id; ?>" <?php echo ($level->level_id == $current_account->user_level) ? 'SELECTED' : ''; ?>><?php echo $level->level_name; ?></option>
        <?php } ?>
        </select>
  </div>
  <?php } ?>
  
 <div class="form-group">
    <label for="name">First Name</label>
    <input type="text" name="firstname" class="form-control" id="name" placeholder="Enter Student Name" value="<?php echo $current_account->user_firstname; ?>">
  </div>
  <div class="form-group">
    <label for="name">Last Name</label>
    <input type="text" name="lastname" class="form-control" id="name" placeholder="Enter Student Name" value="<?php echo $current_account->user_lastname; ?>">
  </div>
 <div class="form-group">
    <label for="email">Email</label>
    <input type="text" name="email" class="form-control" id="email" placeholder="Enter Email" value="<?php echo $current_account->user_email; ?>">
 </div>

  <div class="form-group">
    <label for="newpassword">New Password</label>
    <input type="text" name="newpassword" class="form-control" id="newpassword" placeholder="Enter New Password" value="">
  </div>

		    
  </div>
   <div class="panel-footer">
   
   <div class="btn-group btn-group-justified btn-group-sm">
        <a class="btn btn-success" id="submitButton" href="javascript:document.forms.studentform.submit();">Update</a>
        <a href="javascript:window.history.back(-1);" class="btn btn-danger">Cancel</a>
   </div>
   </div>
</div>
</form>
	
	

    </div> <!-- whitebox -->
    
      </div> <!-- column 8 -->
      
      <div class="col-md-3">
        
            <div class="sidebar whitebox">
            
            
           </div>
           
        </div>   

      </div><!-- row -->

    </div><!-- container-->
   </div> 
   
<?php $this->load->view('overall_footer'); ?>
