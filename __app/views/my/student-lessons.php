<?php $this->load->view('overall_header'); ?>

<div id="main-container" class="lessons-list">
    <div class="container">

      <div class="row">
      <div class="col-md-1">
	
			<?php $this->load->view('my/student-nav'); ?>

		</div>
      <div class="col-md-8">
 
    <div id="main-content" class="whitebox add-padding">

 	<center style="margin-top: 100px; display: none;" id="ajax-loader"><img src="http://acemymath-ci/assets/img/bx_loader.gif"></center>

    </div> <!-- whitebox -->
    
      </div> <!-- column 8 -->
      
      <div class="col-md-3">
        

				
<?php $this->load->view('my/student-profile-sidebar'); ?>

           
        </div>   

      </div><!-- row -->

    </div><!-- container-->
   </div> 

<script>
$('#main-content').aceMyMath('loadLessonsPage', <?php echo $current_student->user_level; ?>);
</script>

<?php $this->load->view('overall_footer'); ?>
