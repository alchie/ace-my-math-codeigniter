<img title="<?php echo $current_student->user_username; ?>" class="img-circle img-responsive" src="<?php echo site_url('assets/images/no-photo.png'); ?>">

<ul class="list-group">
            <li class="list-group-item text-muted">Profile
            <a href="<?php echo site_url('my/student/'.$current_student->user_username.'/update'); ?>" class="pull-right">Update</a>
            </li>

            <li class="list-group-item text-right"><span class="pull-left"><strong>First name</strong></span> <?php echo $current_student->user_firstname; ?></li>
            
             <li class="list-group-item text-right"><span class="pull-left"><strong>Last name</strong></span> <?php echo $current_student->user_lastname; ?></li>
             
             <li class="list-group-item text-right"><span class="pull-left"><strong>Grade Level</strong></span> <?php echo $current_student->level_name; ?></li>
             
                             <li class="list-group-item text-right"><span class="pull-left"><strong>Created</strong></span> <?php echo $current_student->user_created; ?></li>

          </ul> 
          
          		<ul class="list-group">
            <li class="list-group-item text-muted">Subscription Plan 
            <a href="<?php echo site_url('my/student/'.$current_student->user_username.'/upgrade'); ?>" class="pull-right">Upgrade</a></li>
            <li class="list-group-item text-right"><span class="pull-left"><strong>Plan Level</strong></span>&nbsp;<?php echo $current_student->s_plan_name; ?></li>
            <li class="list-group-item text-right"><span class="pull-left"><strong>Expiry</strong></span>&nbsp;<?php echo $current_student->user_expiry; ?></li>
          </ul> 
          
          <ul class="list-group">
            <li class="list-group-item text-muted">Video Lessons Stats </li>
            <li class="list-group-item text-right"><span class="pull-left"><strong>Started</strong></span> 125</li>
            <li class="list-group-item text-right"><span class="pull-left"><strong>Completed</strong></span> 13</li>
          </ul> 
               

          
          <ul class="list-group">
            <li class="list-group-item text-muted">Last Login </li>
            <li class="list-group-item text-right"><span class="pull-left"><strong>Date &amp; Time</strong></span>&nbsp;<?php echo $current_student->last_login; ?></li>
            <li class="list-group-item text-right"><span class="pull-left"><strong>IP Address</strong></span> &nbsp;<?php echo $current_student->last_login_ip; ?></li>
          </ul> 
