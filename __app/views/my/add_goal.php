<?php $this->load->view('overall_header'); ?>

<div id="main-container" class="lessons-list">
    <div class="container">

      <div class="row">
      <div class="col-md-1">
	
	 <ul class="nav nav-pills nav-stacked nav-gradelevel">
       <?php foreach( $students as $student ) : ?>
          <li>
			  
<a href="<?php echo ($student->user_active == 1) ? site_url('my/student/' . $student->user_username ) : site_url('my/student/' . $student->user_username . '/activate' ); ?>"title="<?php echo $student->user_firstname; ?>">
          
          <?php echo $student->user_username; ?></a></li>
		<?php endforeach; ?>
			<li class="active"><a href="<?php echo site_url('my/add_student' ); ?>"><span class="glyphicon glyphicon-plus" style="font-size:20px;"></span></a></li>
     </ul>
        
		</div>
      <div class="col-md-8">
 
    <div id="main-content" class="whitebox add-padding">

 <div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Add New Student</h3>
  </div>
  <div class="panel-body">

<?php if ( $alert ) { ?>
<div class="alert alert-<?php echo $alert_status; ?> alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $alert_message; ?>
  </div>
<?php } ?>

<form name="studentform" id="studentform" class="login-container" action="" method="post">

  <div class="form-group">
    <label for="gradelevel">Grade Level</label>
    <select name="gradelevel" class="form-control">
        <option value="">- - Select Grade Level - - </option>
        <?php foreach( $grade_levels as $level ) { ?>
            <option value="<?php echo $level->level_id; ?>" <?php echo ($level->level_id == $this->input->post('gradelevel')) ? 'SELECTED' : ''; ?>><?php echo $level->level_name; ?></option>
        <?php } ?>
        </select>
  </div>

  
 <div class="form-group">
    <label for="name">First Name</label>
    <input type="text" name="firstname" class="form-control" id="name" placeholder="Enter Student Name" value="<?php echo $this->input->post('firstname'); ?>">
  </div>
  <div class="form-group">
    <label for="name">Last Name</label>
    <input type="text" name="lastname" class="form-control" id="name" placeholder="Enter Student Name" value="<?php echo $this->input->post('lastname'); ?>">
  </div>
 <div class="form-group">
    <label for="email">Email</label>
    <input type="text" name="email" class="form-control" id="email" placeholder="Enter Email" value="<?php echo $this->input->post('email'); ?>">
 </div>
 <div class="form-group">
    <label for="email">Username</label>
    <input type="text" name="username" class="form-control" id="username" placeholder="Enter Username" value="<?php echo $this->input->post('username'); ?>">
 </div>
  <div class="form-group">
    <label for="newpassword">New Password</label>
    <input type="text" name="newpassword" class="form-control" id="newpassword" placeholder="Enter New Password" value="">
  </div>
  		    
  </div>
   <div class="panel-footer">
   
   <div class="btn-group btn-group-justified btn-group-sm">
        <a class="btn btn-success" id="submitButton" href="javascript:document.forms.studentform.submit();">Submit</a>
        <a href="javascript:window.history.back(-1);" class="btn btn-danger">Cancel</a>
   </div>

</form>

</div>
</div>
	

    </div> <!-- whitebox -->
    
      </div> <!-- column 8 -->
      
      <div class="col-md-3">
        
            <div class="sidebar whitebox">
            
            
           </div>
           
        </div>   

      </div><!-- row -->

    </div><!-- container-->
   </div> 



<?php $this->load->view('overall_footer'); ?>
