<?php $this->load->view('overall_header'); ?>

<div id="main-container" class="lessons-list">
    <div class="container">

      <div class="row">
      <div class="col-md-1">
	
	<?php $this->load->view('my/student-nav'); ?>
        
		</div>
      <div class="col-md-8">
 
    <div id="main-content" class="whitebox add-padding">

 <?php if ( $alert ) { ?>
<div class="alert alert-<?php echo $alert_status; ?> alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $alert_message; ?>
  </div>
<?php } ?> 

	<form name="studentform" id="studentform" class="login-container" action="" method="post">
       
       
  <div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Manage Student : <strong><?php echo $current_student->user_username; ?></strong></h3>
  </div>
  <div class="panel-body">
   
            <input type="hidden" id="_wpnonce" name="_wpnonce" value="2499866952"><input type="hidden" name="_wp_http_referer" value="/my-students/?action=manage&amp;uid=8">            <input type="hidden" name="action" value="manage_ace_student">
            <input type="hidden" name="student_id" value="8">
 
  <div class="form-group">
    <label for="gradelevel">Grade Level</label>
    <select name="gradelevel" class="form-control">
        <option value="">- - Select Grade Level - - </option>
        <?php foreach( $grade_levels as $level ) { ?>
            <option value="<?php echo $level->level_id; ?>" <?php echo ($level->level_id == $current_student->user_level) ? 'SELECTED' : ''; ?>><?php echo $level->level_name; ?></option>
        <?php } ?>
        </select>
  </div>
  
 <div class="form-group">
    <label for="name">First Name</label>
    <input type="text" name="firstname" class="form-control" id="name" placeholder="Enter First Name" value="<?php echo $current_student->user_firstname; ?>">
  </div>
  <div class="form-group">
    <label for="name">Last Name</label>
    <input type="text" name="lastname" class="form-control" id="name" placeholder="Enter Last Name" value="<?php echo $current_student->user_lastname; ?>">
  </div>
 <div class="form-group">
    <label for="email">Student Email</label>
    <input type="text" name="email" class="form-control" id="email" placeholder="Enter Email" value="<?php echo $current_student->user_email; ?>">
 </div>

  <div class="form-group">
    <label for="newpassword">New Password</label>
    <input type="text" name="newpassword" class="form-control" id="newpassword" placeholder="Enter New Password" value="">
  </div>
  
		    
  </div>
   <div class="panel-footer">
   
   <div class="btn-group btn-group-justified btn-group-sm">
        <a class="btn btn-success" id="submitButton" href="javascript:document.forms.studentform.submit();">Update</a>
        <a href="<?php echo site_url('my/students'); ?>" class="btn btn-danger">Cancel</a>
   </div>
   </div>
</div>
</form>

	
	
    </div> <!-- whitebox -->
    
	
	
      </div> <!-- column 8 -->
      
      <div class="col-md-3">
        

				
<?php $this->load->view('my/student-profile-sidebar'); ?>

           
        </div>   

      </div><!-- row -->

    </div><!-- container-->
   </div> 



<?php $this->load->view('overall_footer'); ?>
