<!----------------------------------------------------------------------------------------------- //-->
</div>

<div id="footer">
     <div id="footer-widgets-container">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                
<div id="secondary">
		<div id="frontpage-footer" class="frontpage-footer widget-area" role="complementary">
		<aside id="nav_menu-4" class="block-inner widget widget_nav_menu">
			<div class="widget-header"><h3 class="widget-title">Ace My Math</h3><em></em></div>
			<div class="menu-primary-menu-container">
				<ul id="menu-primary-menu" class="menu">
			<li><a title="lorem ipsum" href="<?php echo base_url(); ?>">Home</a></li>
			<li><a href="<?php echo site_url('try-it-now'); ?>">Try it Now</a></li>
			<li><a href="<?php echo site_url('guarantee'); ?>">Guarantee</a></li>
			<li><a href="<?php echo site_url('what-you-get'); ?>">What You Get</a></li>
			<li><a href="<?php echo site_url('lessons'); ?>">Lessons</a></li>
			<li><a href="<?php echo site_url('free-samples'); ?>">Free Samples</a></li>
			<li><a href="<?php echo site_url('our-methods'); ?>">Our Method</a></li>
			<li><a href="<?php echo site_url('about-us'); ?>">About Us</a></li>
				</ul>
			</div>
			</aside>	
</div><!-- #primary-sidebar -->
	</div>
	<!-- #secondary -->

                </div>
                
                   
            </div>
        </div>
    </div>

    <div id="bottom-container">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <span class="copyright"> Copyrigth © 2014 - Ace My Math - All rights reserved</span>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
<!--
	$(document).aceMyMath();
-->
</script>
    </body>
</html>
