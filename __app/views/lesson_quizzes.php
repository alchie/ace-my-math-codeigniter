<?php $this->load->view('overall_header'); ?>
<div id="main-container" class="lessons-list">
    <div class="container">

      <div class="row">
      <div class="col-md-1">
	
	 <ul class="nav nav-pills nav-stacked nav-gradelevel">
        <?php if( $levels ) foreach($levels as $level) { ?>
          <li <?php echo (isset( $current_level->level_id ) && $current_level->level_id == $level->level_id ) ? 'class="active"' : ''; ?>>
          <a data-id="<?php echo $level->level_id; ?>" class="level-link" id="level-link-<?php echo $level->level_id; ?>" href="<?php echo site_url('lessons/level/' . $level->level_slug); ?>"><?php echo $level->level_name; ?></a>
          </li>
		<?php 
		} ?>
     </ul>
        
		</div>
      <div class="col-md-8">
	<a href="<?php echo site_url(array("lesson", $lesson_id . "-" .$lesson_slug, "video" )); ?>" class="btn btn-xs btn-danger pull-right">Video</a>
    <div id="main-content" class="whitebox add-padding">
	
<h3><?php echo $current_lesson->lesson_title; ?></h3>

<?php if( isset( $quizzes ) && count( $quizzes ) > 0 ) { ?>
<div class="table-responsive">
  <table class="table table-striped">
		<thead>
			<tr>
				<th width="2%">#</th>
				<th>Quiz</th>
				<th width="50px">Time</th>
				<th width="50px">Delete</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach( $quizzes as $quiz ) { ?>
			<tr class="quiz-item">
				<td><?php echo $quiz->quiz_number; ?></td>
				<td><strong>
				<?php echo $quiz->quiz_question; ?>
				</strong>		
					<div class="panel panel-primary quiz-details">
					  <div class="panel-heading">
					  <span class="badge pull-right"><strong><?php echo $quiz->quiz_time; ?></strong></span>
						<h3 class="panel-title">
						<?php echo $quiz->quiz_question; ?>
						</h3>
					  </div>
					  <?php if( $quiz->quiz_details != '' ) { ?>
					  <div class="panel-body">
						<?php echo $quiz->quiz_details; ?>
					  </div>
					  <?php } ?>
					   <div class="panel-footer">
					   
					    ANSWER: <span style="color:red"><?php echo $quiz->quiz_correct; ?></span>
					   </div>
					</div>
				</td>
				<td><?php echo $quiz->quiz_time; ?></td>
				<td><a href="<?php echo current_url(); ?>?delete=<?php echo $quiz->quiz_id; ?>">Delete</a></td>
			</tr>
		<?php } ?>
		</tbody>
  </table>
</div>
<?php } ?>

    </div> <!-- whitebox -->
    
      </div> <!-- column 8 -->
      
      <div class="col-md-3">
        
           <div class="sidebar whitebox">
            
            
           </div> 
           
        </div>   

      </div><!-- row -->

    </div><!-- container-->
   </div> 
<script>
var baseURL = '<?php echo base_url(); ?>';
var ajaxURL = '<?php echo site_url('ajax'); ?>';
var assetsURL = '<?php echo site_url('assets'); ?>';
</script>
<?php $this->load->view('overall_footer'); ?>
