<?php $this->load->view('overall_header'); ?>
<div class="container" style="margin-top:50px;">
<div class="row">
    <div class="col-md-offset-4 col-md-4">
    
      <form name="loginform" id="loginform" class="login-container" action="" method="post">
       
  <div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Forgot Password</h3>
  </div>
  <div class="panel-body">
   
   

  <div class="form-group">
    <label for="username">Username or E-mail:</label>
    <input type="text" name="user_login" class="form-control" id="username" placeholder="Please enter your username or email address." required="">
  </div>

  </div>
   <div class="panel-footer"><button type="submit" class="btn btn-success btn-sm btn-block">Retreive Password</button></div>
</div>
</form>
<a href="<?php echo site_url('login'); ?>" class="pull-right">Login</a>
<a href="<?php echo site_url('register'); ?>">Create New Account</a> 
</div>
</div>
</div>
<?php $this->load->view('overall_footer'); ?>
