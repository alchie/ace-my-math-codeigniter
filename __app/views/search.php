<?php $this->load->view('overall_header'); ?>
   
<div id="main-container" class="bordered search-page">
    <div class="container">
        <div class="container-inner">
      <div class="row">
        <div class="col-md-12 main-content">
					
<form role="search" method="get" class="search-form" action="<?php echo site_url('search'); ?>">
	<div class="input-group">
      <input name="s" type="text" class="form-control" value="<?php echo $keyword; ?>">
      <span class="input-group-btn">
        <button class="btn btn-default" type="submit">Search</button>
      </span>
    </div><!-- /input-group -->
</form>

			 <h3>Looking for <em><strong><?php echo $keyword; ?></strong></em></h3>
<?php $nomatch = true; ?>
<?php if(isset($topics) && (count($topics) > 0)) : 
$nomatch = false;
?>
<h3 class="header">Topics</h3>
<ul class="search-list">
 <?php foreach( $topics as $topic ) { ?>
	<li><a href="<?php echo site_url( 'topic/'.$topic->tag_slug); ?>"><?php echo $topic->tag_name; ?></a></li>
 <?php } ?>
</ul>
<?php endif; ?>

<?php if(isset($lessons) && (count($lessons) > 0)) : 
$nomatch = false;
?>
<h3 class="header">Lessons</h3>

<ul class="search-list">
 <?php foreach( $lessons as $lesson ) { ?>
	<li>
		<span class="grade-level"><?php echo $lesson->level_name;  ?></span>
		<a href="<?php echo site_url( 'lesson/'.$lesson->lesson_id."-".$lesson->lesson_slug."/video"); ?>"><?php echo $lesson->lesson_title; ?></a></li>
 <?php } ?>
</ul>
 
<?php endif; ?>

<?php if( $nomatch ) { ?>
	<p></p>No match found!</p>
<?php } ?>
        </div>
	  </div><!-- row -->
      </div><!-- container-inner -->
    </div><!-- container-->
   </div>
   
<?php $this->load->view('overall_footer'); ?>
