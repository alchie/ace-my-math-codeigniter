<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
		<?php $this->load->view('common_head'); ?>
    </head>
    <body>
        
<div id="wrap">
    
<div id="header-container">
    <div class="container">
    <div class="row">
        <div class="col-md-12">
            
           
 <ul class="social-icons pull-right">
                <li><a href="#email" class="email">Email</a></li>
                <li><a href="#facebook" class="facebook">Facebook</a></li>
                <li><a href="#twitter" class="twitter">Twitter</a></li>
                <li><a href="#rss" class="rss">RSS</a></li>                                                
 </ul>
 <div class="header-account-form">
<?php if( isset( $current_session->loggedIn ) ) : ?>

	<div class="my-account" style="font-size:25px;"> Welcome back, <?php echo $current_session->userFirstname; ?>!</div>

<?php else : ?>

    <form name="loginform" id="loginform" class="login-container" action="<?php echo site_url('login'); ?>?r=<?php echo $this->uri->uri_string(); ?>" method="post">
        <div class="login-form">
            <label>User Login</label>
            <input type="text" name="username" placeholder="Username" value="">
            <input type="password" name="password" placeholder="Password" value="">       
            <input type="submit" name="wp-submit" value="Login" class="btn btn-success btn-sm"> 
        </div>
    </form>
    <div class="register">
        <small>Not yet a member?</small>
        <a href="<?php echo site_url('register'); ?>" class="btn btn-danger btn-sm">Signup</a>
    </div>

<?php endif; ?>
</div>

              <div id="branding">
                <h1 id="site-title"><a class="home-link" href="<?php echo base_url(); ?>" title="Ace My Math" rel="home">
                 Ace My Math</a></h1>                
             </div>

 <?php $this->load->view('header-navigation'); ?>

        </div><!-- .col -->  
    </div><!-- .row -->
    </div><!-- .container -->
    </div><!-- #header-container -->
