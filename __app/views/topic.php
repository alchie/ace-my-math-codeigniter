<?php $this->load->view('overall_header'); ?>

<div id="main-container" class="bordered">
    <div class="container">
        <div class="container-inner">
      <div class="row">
        <div class="col-md-12 main-content">
					<h3>Topic : <em><strong><?php echo $tag->tag_name; ?></strong></em></h3>
					
					<ul class="search-list">
					<?php foreach( $lessons  as $lesson ) {  ?>
						<li>
							<span class="grade-level"><?php echo $lesson->level_name;  ?></span>
							<a href="<?php echo site_url("lesson/" . $lesson->lesson_id . "-" .$lesson->lesson_slug . "/video"); ?>"><?php echo $lesson->lesson_title; ?></a></li>
					<?php } ?>
					</ul>
        </div>
	  </div><!-- row -->
      </div><!-- container-inner -->
    </div><!-- container-->
   </div>

<?php $this->load->view('overall_footer'); ?>
