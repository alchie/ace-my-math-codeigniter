<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
		<?php $this->load->view('common_head'); ?>
    </head>
    <body>
        
<div id="wrap">
    
<div id="header-container">
    <div class="container">
    <div class="row">
        <div class="col-md-12">
            
           
 <ul class="social-icons pull-right">
                <li><a href="#email" class="email">Email</a></li>
                <li><a href="#facebook" class="facebook">Facebook</a></li>
                <li><a href="#twitter" class="twitter">Twitter</a></li>
                <li><a href="#rss" class="rss">RSS</a></li>                                                
 </ul>
 <div class="header-account-form">

<?php if( isset( $current_session->loggedIn ) ) : ?>

	<div class="my-account"> Welcome back, <?php echo $current_session->userFirstname; ?>!</div>

<?php else : ?>

    <form name="loginform" id="loginform" class="login-container" action="<?php echo site_url('login'); ?>" method="post">
        <div class="login-form">
            <label>User Login</label>
            <input type="text" name="username" placeholder="Username" value="">
            <input type="password" name="password" placeholder="Password" value="">       
            <input type="submit" name="wp-submit" value="Login" class="btn btn-success btn-sm"> 
        </div>
    </form>
    <div class="register">
        <small>Not yet a member?</small>
        <a href="<?php echo site_url('register'); ?>" class="btn btn-danger btn-sm">Signup</a>
    </div>

<?php endif; ?>

</div>

            
              <div id="branding">
                <h1 id="site-title"><a class="home-link" href="<?php echo base_url(); ?>" title="Ace My Math" rel="home">
                 Ace My Math</a></h1>                
             </div>



<div class="shadowed rounded">
 
 <?php $this->load->view('header-navigation'); ?>
 
   <div id="carousel-slider-container" class="carousel slide" data-ride="carousel">
        
        <div class="carousel-inner">
          <div class="item">
            <img src="<?php echo base_url(); ?>assets/images/slider.jpg" alt="First slide">
          </div>
          <div class="item">
            <img src="<?php echo base_url(); ?>assets/images/slider.jpg" alt="First slide">
          </div>
          <div class="item active">
            <img src="<?php echo base_url(); ?>assets/images/slider.jpg" alt="First slide">
          </div>
        </div>
        <div class="controls">
            <div class="left">
                <ol class="carousel-indicators">
                  <li data-target="#carousel-slider-container" data-slide-to="0" class=""></li>
                  <li data-target="#carousel-slider-container" data-slide-to="1" class=""></li>
                  <li data-target="#carousel-slider-container" data-slide-to="2" class="active"></li>
                </ol>
            </div>
            <div class="right">
                <a class="left carousel-control" href="#carousel-slider-container" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-slider-container" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
        </div>
      </div>
 
</div>

        </div><!-- .col -->  
    </div><!-- .row -->
    </div><!-- .container -->
    </div><!-- #header-container -->
    

<?php if( isset( $levels ) && count($levels) > 0 ) { ?>
<!----------------------------------------------------------------------------------------------- //-->
 <div id="feature-container">
 
 <div class="feature bordered">
    <div class="container">
         <div class="container-inner">
    <div class="row">
    
<?php 
$limit=3;
foreach( $levels as $level ) { 
?>
	
        <div class="col-md-4">
        
        <div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><?php echo $level->level_name; ?> Lessons</h3>
  </div>
  <div class="panel-body lesson-summary" data-level="<?php echo $level->level_id; ?>">
    
      <center><img src="<?php echo base_url(); ?>assets/img/bx_loader.gif"></center>
    
  </div>
  <div class="panel-footer"><a href="<?php echo site_url('lessons/level/' . $level->level_slug); ?>" class="btn btn-success btn-block btn-xs">View More</a></div>
</div>
              
        </div> 


<?php 
$limit--;
if( $limit == 0 ) { 
?>
 </div>
    </div>
    </div>
</div>

<!----------------------------------------------------------------------------------------------- //-->

 <div class="feature bordered">
    <div class="container">
         <div class="container-inner">
    <div class="row">
    
<?php 
$limit=3;
} // if total
} // foreach 
?>
  
        
       
    </div>
    </div>
    </div>
</div>

</div>
<script>
<!--
	$('#feature-container').aceMyMath('loadLessonSummary');
-->
</script>
<?php } ?>
<!----------------------------------------------------------------------------------------------- //-->

 <div id="video-container" class="bordered">
    <div class="container">
        <div class="container-inner">
            <div class="row">
                <div class="col-md-8">
                    <img src="<?php echo base_url(); ?>assets/images/icon1.jpg" class="pull-left">
                    <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus non euismod elit, vel interdum turpis. In hac habitasse platea dictumst. Aenean fringilla velit tincidunt gravida vulputate. Praesent condimentum mauris nisi, in tempus enim porttitor vel. Duis at arcu porttitor, hendrerit velit sed, lacinia neque. Praesent et tincidunt ligula. Nunc volutpat nisi ac augue auctor pharetra. Nam elit nisi, posuere quis mi vel, hendrerit dignissim mauris. Etiam et odio eget libero laoreet scelerisque vehicula non lectus. Etiam sit amet ultricies turpis. Sed sodales lacus at turpis volutpat, dignissim tempor tellus viverra. Sed quis nisl eros. Maecenas rhoncus facilisis tortor volutpat faucibus. Nam commodo semper elit, in luctus quam pharetra at. Sed ut ipsum venenatis quam interdum placerat et ac nunc.
                    </p>
                </div>   
                 <div class="col-md-4">
                    <iframe width="100%" height="220" src="//www.youtube.com/embed/5rLgiClm0Uc" frameborder="0" allowfullscreen=""></iframe>
                </div>  
            </div>
        </div>
    </div>
 </div>
<script>
var baseURL = '<?php echo base_url(); ?>';
var ajaxURL = '<?php echo site_url('ajax'); ?>';
$('#main-content').aceMyMath();
</script>
<?php $this->load->view('overall_footer'); ?>
