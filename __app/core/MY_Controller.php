<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
        
    function __construct() 
    {
        parent::__construct();
                
        $this->template_data->opengraph();
		
        if( $this->session->userdata('loggedIn') )
        {
            $this->template_data->set('current_session', (object) $this->session->all_userdata() );
        } else {
            $this->template_data->set('current_session', (object) array() );
        }
        
        $this->template_data->set('main_page', 'home' );
	    $this->template_data->set('sub_page', 'home' );    
        
        //$this->output->enable_profiler(TRUE);      
        
    }
    
    public function _mustLogin($url='login', $r=FALSE)
    {
        if ( !  $this->session->userdata('loggedIn') ) {
            if ($r===TRUE) {
                redirect($url, 'refresh');
            } else {
                redirect($url, 'location', 301);
            }
        }
    }
    
    public function _notLoggedIn() {
        if( $this->session->userdata('loggedIn') === TRUE ) {
            redirect('my/dashboard', 'location', 301);
            exit;
        } 
        return true;
        
    }
    
    public function isLoggedIn() {
		if( $this->session->userdata('loggedIn') === TRUE ) {
			return TRUE;
		}
		return FALSE;
	}
}
