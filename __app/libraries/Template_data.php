<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class CI_Template_data {
    
    var $CI;
    private $isLoggedIn = false;
    private $user_id = NULL;
    private $data = array();
    private $fb_app_id = NULL;
    public function __construct()
    {
       $this->CI =& get_instance();
       $this->CI->load->helper('url');
       $this->CI->config->load('facebook');
        
        $this->isLoggedIn = $this->CI->session->userdata('logged_in');
        $this->user_id = $this->CI->session->userdata('user_id');    
        $this->fb_app_id = $this->CI->config->item('appId');
        $this->defaults();
    }
    
    function defaults() {
        $this->data = array(
                    /*******
                    * Header
                    ********/
                    'page_title' => 'Ace My Math',
                    'site_title' => 'Ace My Math, LLC',
                    'header_css' => '',
                    'header_js' => '',
                    'header' => '',
                    /**** 
                    URL 
                    *****/
                     'base_url' => base_url(),
                     'site_url' => base_url(),
                     'home_url' => base_url(),
                     'redirect_url' => site_url('my/account'),
                     /*******
                     Session
                     ********/
                     'isLoggedIn' => $this->isLoggedIn(),
                     'user_id' => $this->user_id,
                    /******* 
                     Alert
                    *******/
                     'alert' => FALSE,
                     'alert_status' => 'default',
                     'alert_message' => '',
                    /*******
                    * Header
                    ********/
                     'footer' => '',
                     'footer_js' => '',
                     );
    }
    
    function isLoggedIn()
    {
        return $this->CI->session->userdata('logged_in');
    }
    
    function set($key, $value)
    {
        $this->data[$key] = $value; 
                               
    }
    
    function get($key=false)
    {
        if($key) {
            return $this->data[$key];
        } else {
            return $this->data;
        }
    }
    
    function prepend($key, $value)
    {
        $current_value = $this->get($key);
        $this->set($key, $value . $current_value );  
    }
    
    function append($key, $value)
    {
        $current_value = $this->get($key);
        $this->set($key, $current_value . $value );        
    }
    
    function alert($message='Alert!', $status='default') {
        $this->set('alert', TRUE );
        $this->set('alert_status', $status );       
        $this->set('alert_message', $message );
    }
    
    function alert_off()
    {
        $this->set('alert', FALSE );
    }
    
    function page_title($value='') {
        if($value == '') 
        {
            $value = 'Home';
        }
        $this->set('page_title', $value );
    }
    
    function opengraph( $var=array() ) {
        $defaults = array(
            'title'=> ($this->get('page_title') != 'Home') ? $this->get('page_title') : $this->get('site_title'),
            'site_name'=>'Ace My Math',
            'description'=>'Learn Math',
            'url'=> site_url( trim( uri_string(), "/") ),
            'type'=>'article',
            'image'=> base_url() . 'assets/images/logo2.jpg',
            'image:width'=>'289',
            'image:height'=>'102',
        );
        
        $opengraph = array_merge( (array) $defaults, (array) $var );
        
        $meta = '<meta property="fb:app_id" content="' . $this->fb_app_id . '" />';
        foreach( $opengraph as $tag=>$value ) {
            if( $value != '' ) {
                $meta .= "\n" . '<meta property="og:'.$tag.'" content="'.$value.'" />';
                //$meta .= '<meta name="'.$tag.'" content="'.$value.'" />' . "\n";
                //$meta .= '<meta itemprop="'.$tag.'" content="'.$value.'" />' . "\n";                
            }
        }
        $this->set('opengraph', $meta );
    }
    
}

/* End of file Global_variables.php */
