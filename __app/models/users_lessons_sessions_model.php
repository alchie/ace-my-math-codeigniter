<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Users_lessons_sessions_model Class
 *
 * Manipulates `ci_users_lessons_sessions` table on database

CREATE TABLE `ci_users_lessons_sessions` (
  `uls_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `lesson_id` bigint(20) NOT NULL,
  `started` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `ended` timestamp NULL DEFAULT NULL,
  `time_taken` int(10) DEFAULT NULL,
  `mistakes` int(10) DEFAULT NULL,
  PRIMARY KEY (`uls_id`),
  KEY `user_id` (`user_id`)
);

 * @package			Model
 * @project			Ace My Math, LLC
 * @project_link	http://www.acemymath.com/
 * @author			Chester Alan Tagudin
 * @author_link		http://www.chesteralan.com/
 */
 
class Users_lessons_sessions_model extends CI_Model {

	protected $uls_id;
	protected $user_id;
	protected $lesson_id;
	protected $started;
	protected $ended;
	protected $time_taken;
	protected $mistakes;
	protected $dataFields = array();
	protected $select = array();
	protected $join = array();
	protected $where = array();
	protected $where_or = array();
	protected $where_in = array();
	protected $where_in_or = array();
	protected $where_not_in = array();
	protected $where_not_in_or = array();
	protected $like = array();
	protected $like_or = array();
	protected $like_not = array();
	protected $like_not_or = array();
	protected $having = array();
	protected $having_or = array();
	protected $group_by = array();
	protected $filter = array();
	protected $order = array();
	protected $exclude = array();
	protected $required = array();
	protected $countField = '';
	protected $start = 0;
	protected $limit = 10;
	protected $results = FALSE;
	protected $distinct = FALSE;
	protected $cache_on = FALSE;
	protected $batch = array();

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct() {
		parent::__construct();
	}

	// --------------------------------------------------------------------


	// --------------------------------------------------------------------
	// Start Field: uls_id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `uls_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setUlsId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->uls_id = ( ($value == '') && ($this->uls_id != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'users_lessons_sessions.uls_id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'uls_id';
		}
		return $this;
	}

	/** 
	* Get the value of `uls_id` variable
	* @access public
	* @return String;
	*/

	public function getUlsId() {
		return $this->uls_id;
	}

	/**
	* Get row by `uls_id`
	* @param uls_id
	* @return QueryResult
	**/

	public function getByUlsId() {
		if($this->uls_id != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users_lessons_sessions', array('users_lessons_sessions.uls_id' => $this->uls_id), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `uls_id`
	**/

	public function updateByUlsId() {
		if($this->uls_id != '') {
			$this->setExclude('uls_id');
			if( $this->getData() ) {
				return $this->db->update('users_lessons_sessions', $this->getData(), array('users_lessons_sessions.uls_id' => $this->uls_id ) );
			}
		}
	}


	/**
	* Delete row by `uls_id`
	**/

	public function deleteByUlsId() {
		if($this->uls_id != '') {
			return $this->db->delete('users_lessons_sessions', array('users_lessons_sessions.uls_id' => $this->uls_id ) );
		}
	}

	/**
	* Increment row by `uls_id`
	**/

	public function incrementByUlsId() {
		if($this->uls_id != '' && $this->countField != '') {
			$this->db->where('uls_id', $this->uls_id);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users_lessons_sessions');
		}
	}

	/**
	* Decrement row by `uls_id`
	**/

	public function decrementByUlsId() {
		if($this->uls_id != '' && $this->countField != '') {
			$this->db->where('uls_id', $this->uls_id);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users_lessons_sessions');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: uls_id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: user_id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `user_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setUserId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->user_id = ( ($value == '') && ($this->user_id != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'users_lessons_sessions.user_id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'user_id';
		}
		return $this;
	}

	/** 
	* Get the value of `user_id` variable
	* @access public
	* @return String;
	*/

	public function getUserId() {
		return $this->user_id;
	}

	/**
	* Get row by `user_id`
	* @param user_id
	* @return QueryResult
	**/

	public function getByUserId() {
		if($this->user_id != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users_lessons_sessions', array('users_lessons_sessions.user_id' => $this->user_id), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `user_id`
	**/

	public function updateByUserId() {
		if($this->user_id != '') {
			$this->setExclude('user_id');
			if( $this->getData() ) {
				return $this->db->update('users_lessons_sessions', $this->getData(), array('users_lessons_sessions.user_id' => $this->user_id ) );
			}
		}
	}


	/**
	* Delete row by `user_id`
	**/

	public function deleteByUserId() {
		if($this->user_id != '') {
			return $this->db->delete('users_lessons_sessions', array('users_lessons_sessions.user_id' => $this->user_id ) );
		}
	}

	/**
	* Increment row by `user_id`
	**/

	public function incrementByUserId() {
		if($this->user_id != '' && $this->countField != '') {
			$this->db->where('user_id', $this->user_id);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users_lessons_sessions');
		}
	}

	/**
	* Decrement row by `user_id`
	**/

	public function decrementByUserId() {
		if($this->user_id != '' && $this->countField != '') {
			$this->db->where('user_id', $this->user_id);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users_lessons_sessions');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: user_id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: lesson_id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `lesson_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setLessonId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->lesson_id = ( ($value == '') && ($this->lesson_id != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'users_lessons_sessions.lesson_id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'lesson_id';
		}
		return $this;
	}

	/** 
	* Get the value of `lesson_id` variable
	* @access public
	* @return String;
	*/

	public function getLessonId() {
		return $this->lesson_id;
	}

	/**
	* Get row by `lesson_id`
	* @param lesson_id
	* @return QueryResult
	**/

	public function getByLessonId() {
		if($this->lesson_id != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users_lessons_sessions', array('users_lessons_sessions.lesson_id' => $this->lesson_id), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `lesson_id`
	**/

	public function updateByLessonId() {
		if($this->lesson_id != '') {
			$this->setExclude('lesson_id');
			if( $this->getData() ) {
				return $this->db->update('users_lessons_sessions', $this->getData(), array('users_lessons_sessions.lesson_id' => $this->lesson_id ) );
			}
		}
	}


	/**
	* Delete row by `lesson_id`
	**/

	public function deleteByLessonId() {
		if($this->lesson_id != '') {
			return $this->db->delete('users_lessons_sessions', array('users_lessons_sessions.lesson_id' => $this->lesson_id ) );
		}
	}

	/**
	* Increment row by `lesson_id`
	**/

	public function incrementByLessonId() {
		if($this->lesson_id != '' && $this->countField != '') {
			$this->db->where('lesson_id', $this->lesson_id);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users_lessons_sessions');
		}
	}

	/**
	* Decrement row by `lesson_id`
	**/

	public function decrementByLessonId() {
		if($this->lesson_id != '' && $this->countField != '') {
			$this->db->where('lesson_id', $this->lesson_id);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users_lessons_sessions');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: lesson_id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: started
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `started` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setStarted($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->started = ( ($value == '') && ($this->started != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'users_lessons_sessions.started';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'started';
		}
		return $this;
	}

	/** 
	* Get the value of `started` variable
	* @access public
	* @return String;
	*/

	public function getStarted() {
		return $this->started;
	}

	/**
	* Get row by `started`
	* @param started
	* @return QueryResult
	**/

	public function getByStarted() {
		if($this->started != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users_lessons_sessions', array('users_lessons_sessions.started' => $this->started), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `started`
	**/

	public function updateByStarted() {
		if($this->started != '') {
			$this->setExclude('started');
			if( $this->getData() ) {
				return $this->db->update('users_lessons_sessions', $this->getData(), array('users_lessons_sessions.started' => $this->started ) );
			}
		}
	}


	/**
	* Delete row by `started`
	**/

	public function deleteByStarted() {
		if($this->started != '') {
			return $this->db->delete('users_lessons_sessions', array('users_lessons_sessions.started' => $this->started ) );
		}
	}

	/**
	* Increment row by `started`
	**/

	public function incrementByStarted() {
		if($this->started != '' && $this->countField != '') {
			$this->db->where('started', $this->started);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users_lessons_sessions');
		}
	}

	/**
	* Decrement row by `started`
	**/

	public function decrementByStarted() {
		if($this->started != '' && $this->countField != '') {
			$this->db->where('started', $this->started);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users_lessons_sessions');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: started
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: ended
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `ended` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setEnded($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->ended = ( ($value == '') && ($this->ended != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'users_lessons_sessions.ended';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'ended';
		}
		return $this;
	}

	/** 
	* Get the value of `ended` variable
	* @access public
	* @return String;
	*/

	public function getEnded() {
		return $this->ended;
	}

	/**
	* Get row by `ended`
	* @param ended
	* @return QueryResult
	**/

	public function getByEnded() {
		if($this->ended != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users_lessons_sessions', array('users_lessons_sessions.ended' => $this->ended), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `ended`
	**/

	public function updateByEnded() {
		if($this->ended != '') {
			$this->setExclude('ended');
			if( $this->getData() ) {
				return $this->db->update('users_lessons_sessions', $this->getData(), array('users_lessons_sessions.ended' => $this->ended ) );
			}
		}
	}


	/**
	* Delete row by `ended`
	**/

	public function deleteByEnded() {
		if($this->ended != '') {
			return $this->db->delete('users_lessons_sessions', array('users_lessons_sessions.ended' => $this->ended ) );
		}
	}

	/**
	* Increment row by `ended`
	**/

	public function incrementByEnded() {
		if($this->ended != '' && $this->countField != '') {
			$this->db->where('ended', $this->ended);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users_lessons_sessions');
		}
	}

	/**
	* Decrement row by `ended`
	**/

	public function decrementByEnded() {
		if($this->ended != '' && $this->countField != '') {
			$this->db->where('ended', $this->ended);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users_lessons_sessions');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: ended
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: time_taken
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `time_taken` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setTimeTaken($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->time_taken = ( ($value == '') && ($this->time_taken != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'users_lessons_sessions.time_taken';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'time_taken';
		}
		return $this;
	}

	/** 
	* Get the value of `time_taken` variable
	* @access public
	* @return String;
	*/

	public function getTimeTaken() {
		return $this->time_taken;
	}

	/**
	* Get row by `time_taken`
	* @param time_taken
	* @return QueryResult
	**/

	public function getByTimeTaken() {
		if($this->time_taken != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users_lessons_sessions', array('users_lessons_sessions.time_taken' => $this->time_taken), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `time_taken`
	**/

	public function updateByTimeTaken() {
		if($this->time_taken != '') {
			$this->setExclude('time_taken');
			if( $this->getData() ) {
				return $this->db->update('users_lessons_sessions', $this->getData(), array('users_lessons_sessions.time_taken' => $this->time_taken ) );
			}
		}
	}


	/**
	* Delete row by `time_taken`
	**/

	public function deleteByTimeTaken() {
		if($this->time_taken != '') {
			return $this->db->delete('users_lessons_sessions', array('users_lessons_sessions.time_taken' => $this->time_taken ) );
		}
	}

	/**
	* Increment row by `time_taken`
	**/

	public function incrementByTimeTaken() {
		if($this->time_taken != '' && $this->countField != '') {
			$this->db->where('time_taken', $this->time_taken);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users_lessons_sessions');
		}
	}

	/**
	* Decrement row by `time_taken`
	**/

	public function decrementByTimeTaken() {
		if($this->time_taken != '' && $this->countField != '') {
			$this->db->where('time_taken', $this->time_taken);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users_lessons_sessions');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: time_taken
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: mistakes
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `mistakes` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMistakes($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->mistakes = ( ($value == '') && ($this->mistakes != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'users_lessons_sessions.mistakes';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'mistakes';
		}
		return $this;
	}

	/** 
	* Get the value of `mistakes` variable
	* @access public
	* @return String;
	*/

	public function getMistakes() {
		return $this->mistakes;
	}

	/**
	* Get row by `mistakes`
	* @param mistakes
	* @return QueryResult
	**/

	public function getByMistakes() {
		if($this->mistakes != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users_lessons_sessions', array('users_lessons_sessions.mistakes' => $this->mistakes), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `mistakes`
	**/

	public function updateByMistakes() {
		if($this->mistakes != '') {
			$this->setExclude('mistakes');
			if( $this->getData() ) {
				return $this->db->update('users_lessons_sessions', $this->getData(), array('users_lessons_sessions.mistakes' => $this->mistakes ) );
			}
		}
	}


	/**
	* Delete row by `mistakes`
	**/

	public function deleteByMistakes() {
		if($this->mistakes != '') {
			return $this->db->delete('users_lessons_sessions', array('users_lessons_sessions.mistakes' => $this->mistakes ) );
		}
	}

	/**
	* Increment row by `mistakes`
	**/

	public function incrementByMistakes() {
		if($this->mistakes != '' && $this->countField != '') {
			$this->db->where('mistakes', $this->mistakes);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users_lessons_sessions');
		}
	}

	/**
	* Decrement row by `mistakes`
	**/

	public function decrementByMistakes() {
		if($this->mistakes != '' && $this->countField != '') {
			$this->db->where('mistakes', $this->mistakes);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users_lessons_sessions');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: mistakes
	// --------------------------------------------------------------------


	// --------------------------------------------------------------------

	/**
	* Checks if result not empty 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function nonEmpty() {
		if ( $this->hasConditions() ) {
			
			$this->setupJoin();
			$this->setupConditions();
			$this->setupHaving();
			$this->setupGroupBy();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('users_lessons_sessions', 1);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			$result = $query->result();
			if ( isset( $result[0] ) === true ) {
				$this->results = $result[0];
				return true;
			} else {
				return false;
			}
		}
	}


	/**
	* Get Results 
	* @return Mixed;
	*/

	public function getResults() {
		return $this->results;
	}


	// --------------------------------------------------------------------

	/**
	* Delete 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function delete() {
		if ( $this->hasConditions() ) {
				$this->setupConditions();
				return $this->db->delete('users_lessons_sessions');
		}
	}


	// --------------------------------------------------------------------

	/**
	* Limit Data Fields
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function limitDataFields($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->dataFields = array($fields);
			} else {
				$this->dataFields = $fields;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Update 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function update() {
		if ( ( $this->getData() ) && ( $this->hasConditions() ) ) {
				$this->setupConditions();
				return $this->db->update('users_lessons_sessions', $this->getData() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Insert new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function insert() {
		if( $this->getData() ) {
			if( $this->db->insert('users_lessons_sessions', $this->getData() ) === TRUE ) {
				$this->uls_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}


	// --------------------------------------------------------------------

	/**
	* Replace new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function replace() {
		if( $this->getData() ) {
			if( $this->db->replace('users_lessons_sessions', $this->getData() ) === TRUE ) {
				$this->uls_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Get First Data 
	* @access public
	* @return Object / False;
	*/

	public function get() {
		
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupHaving();
		$this->setupGroupBy();
		
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		$this->db->limit( 1, $this->start);
		
		if( $this->cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('users_lessons_sessions');
		
		if( $this->cache_on ) {
			$this->db->cache_off();
		}
		
		$result = $query->result();
		
		if( isset($result[0]) ) {
			
			$this->results = $result[0];
			
			return $this->results;
			
		}
		
		return false;
	}
	// --------------------------------------------------------------------

	/**
	* Populate Data 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function populate() {
	
		if( $this->distinct ) {
			$this->db->distinct();
		}
		
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select ) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupHaving();
		$this->setupGroupBy();
		
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		if( $this->limit > 0 ) {
			$this->db->limit( $this->limit,$this->start);
		}
		
		if( $this->cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('users_lessons_sessions');
		
		if( $this->cache_on ) {
			$this->db->cache_off();
		}
			
		$this->results = $query->result();
		
		return $this->results;
	}
	// --------------------------------------------------------------------
	/**
	* Recursive 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function recursive($match, $find, $child='uls_id', $level=10, $conn='children') {
			if( $level == 0 ) return;
			if( $this->limit > 0 ) {
				$this->db->limit( $this->limit,$this->start);
			}
			if( $this->select ) {
					$this->db->select( implode(',' , $this->select) );
			}
			
			$this->setupJoin();
			$this->setWhere($match, $find);
			$this->setupConditions();
			$this->setupHaving();
			$this->setupGroupBy();
			$this->clearWhere( $match );
			
			if( $this->order ) {
				foreach( $this->order as $field=>$orientation ) {
					$this->db->order_by( $field, $orientation );
				}
			}
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('users_lessons_sessions');
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			$results=array();
			if( $query->result() ) {
				foreach( $query->result() as $qr ) {
					$children = $this->recursive($match, $qr->$child, $child, ($level-1), $conn ) ;
					$results[] = (object) array_merge( (array) $qr, array($conn=>$children) );
				}
			}
		return $results;
	}

// --------------------------------------------------------------------

	/**
	* Set Field Where Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setFieldWhere($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->where[] = array($fields => $this->$fields);
			} else {
				foreach($fields as $field) {
					if($w != '') {
						$this->where[] = array($field => $this->$field);
					}
				}
			}
		}
	}

// --------------------------------------------------------------------

	/**
	* Set Field Value Manually
	* @access public
	* @param Field Key ; Value
	* @return self;
	*/
	public function setFieldValue($field, $value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL) {
		if( isset( $this->$field ) ) {
			$this->$field = ( ($value == '') && ($this->$field != '') ) ? '' : $value;
			if( $setWhere ) {
				$key = 'users_lessons_sessions.'.$field;
				if ( $whereOperator != NULL && $whereOperator != '' ) {
					$key = $key . ' ' . $whereOperator;
				}
				//$this->where[$key] = $this->$field;
				$this->where[] = array( $key => $this->$field );
			}
			if( $set_data_field ) {
				$this->dataFields[] = $field;
			}
		}
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Prepares data 
	* @access private
	* @return Array;
	*/
	public function getData($exclude=NULL) {
		$data = array();

		$fields = array("uls_id","user_id","lesson_id","started","ended","time_taken","mistakes");
		if( count( $this->dataFields ) > 0 ) {
    		            $fields = $this->dataFields;
		}
		foreach( $fields as $field ) {
		    if( ( in_array( $field, $this->required ) ) 
		    && ($this->$field == '') 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		return false;
    		}
    		if( ( in_array( $field, $this->required ) ) 
    		&& ($this->$field != '') 
    		&& ( ! in_array( $field, $this->exclude ) ) 
    		) {
		        $data[$field] = $this->$field;
		    }
		    if( ( ! in_array( $field, $this->required ) ) 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		$data[$field] = $this->$field;
    		}  
		}
		return $data;   
	}


	// --------------------------------------------------------------------

	/**
	* Setup Conditional Clauses 
	* @access public
	* @return Null;
	*/
	
	protected function __apply_condition($what, $condition) {
		if( is_array( $condition ) ) {
			foreach( $condition as $key => $value ) {
				$this->db->$what( $key , $value );
			}
		} else {
			$this->db->$what( $condition );
		}
	}
	
	private function setupConditions() {
		$return = FALSE;
		
		$conditions = array_merge(
                                $this->where,
                                $this->filter,
                                $this->where_or,
                                $this->where_in,
                                $this->where_in_or,
                                $this->where_not_in,
                                $this->where_not_in_or,
                                $this->like,
                                $this->like_or,
                                $this->like_not,
                                $this->like_not_or
                                );
		
		if( count( $conditions ) > 0 ) {
			$i = 0;
			$sorted = array();
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( ! is_null( $priority ) ) {
						while( isset($sorted[$priority]) ) {
							$i++;
							$priority += $i;
						}
						$sorted[ $priority ] = $options;
					}
				}
			}
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( is_null( $priority )  ) {
						while( isset($sorted[$i]) ) {
							$i++;
						}
						$sorted[$i] = $options;
					} 
				}
			}
			
			ksort( $sorted );
		
			foreach( $sorted as $sortd ) {
				$sortd_key = array_keys( $sortd );
				$sortd_value = array_values( $sortd );
				
				if( isset( $sortd_key[0] ) && $sortd_value[0] ) {
					switch( $sortd_key[0] ) {
						case 'where_or':
							$this->__apply_condition('or_where', $sortd_value[0]);
						break;
						case 'where_in':
							$this->__apply_condition('where_in', $sortd_value[0]);
						break;
						case 'where_in_or':
							$this->__apply_condition('or_where_in', $sortd_value[0]);
						break;
						case 'where_not_in':
							$this->__apply_condition('where_not_in', $sortd_value[0]);
						break;
						case 'where_not_in_or':
							$this->__apply_condition('or_where_not_in', $sortd_value[0]);
						break;
						case 'like':
							$this->__apply_condition('like', $sortd_value[0]);
						break;
						case 'like_or':
							$this->__apply_condition('or_like', $sortd_value[0]);
						break;
						case 'like_not':
							$this->__apply_condition('not_like', $sortd_value[0]);
						break;
						case 'like_not_or':
							$this->__apply_condition('or_not_like', $sortd_value[0]);
						break;
						default:
							$this->__apply_condition('where', $sortd_value[0]);
						break;
					}
				}
			}
			$return = TRUE;
		}
		
		return $return;
	}
	
	private function setupHaving() {
		
		$return = FALSE;
		
		$conditions = array_merge( $this->having, $this->having_or );
		
		if( count( $conditions ) > 0 ) {
			$i = 0;
			$sorted = array();
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( ! is_null( $priority ) ) {
						while( isset($sorted[$priority]) ) {
							$i++;
							$priority += $i;
						}
						$sorted[ $priority ] = $options;
					}
				}
			}
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( is_null( $priority )  ) {
						while( isset($sorted[$i]) ) {
							$i++;
						}
						$sorted[$i] = $options;
					} 
				}
			}
			
			ksort( $sorted );
		
			foreach( $sorted as $sortd ) {
				$sortd_key = array_keys( $sortd );
				$sortd_value = array_values( $sortd );
				
				if( isset( $sortd_key[0] ) && $sortd_value[0] ) {
					switch( $sortd_key[0] ) {
						case 'having':
							$this->__apply_condition('having', $sortd_value[0]);
						break;
						case 'having_or':
							$this->__apply_condition('or_having', $sortd_value[0]);
						break;
					}
				}
			}
			$return = TRUE;
		}
		
		return $return;
	}
	
	/**
	* Check Conditions Availability
	* @access public
	* @return Array;
	*/
	private function hasConditions() {
		$conditions = array_merge(
                                $this->where,
                                $this->filter,
                                $this->where_or,
                                $this->where_in,
                                $this->where_in_or,
                                $this->where_not_in,
                                $this->where_not_in_or,
                                $this->like,
                                $this->like_or,
                                $this->like_not,
                                $this->like_not_or,
                                $this->having,
                                $this->having_or
                        );
		if( count( $conditions ) > 0 ) {
			return true;
		}
		return false;
	}
	

	// --------------------------------------------------------------------
	/**
	* Set Join Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setJoin($table, $connection, $option='left') {
		$this->join[] = array('table'=>$table, 'connection'=>$connection, 'option'=>$option );
	}

	private function setupJoin() {
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Filter 
	* @access public
	* @return Array;
	*/
	public function setFilter($field, $value, $table=NULL, $priority=NULL, $underCondition='where') {
		$key = array();
		if( $table == NULL ) { 
			$table = 'users_lessons_sessions'; 
		} 
		if( $table != '' ) {
			$key[] = $table;
		}
		$key[] = $field;
		
		$newField = implode('.', $key);
		
		if( is_null( $value ) ) {
			$where = $newField;
		} else {
			$where = array( $newField => $value );
		}
		
		$this->filter[] = array( $newField => array( $underCondition => $where, 'priority'=>$priority ) );
	}


	public function clearFilter($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->filter);
			$this->filter = array();
		} else {
			$newfilter = array();
			foreach($this->filter as $filter ) {
				if( ! isset( $filter[$field] ) ) {
					$newfilter[] = $filter;
				}
			}
			$this->filter = $newfilter;
		}
	}

	// --------------------------------------------------------------------

	/**
	* Set Where 
	* @access public
	*/
	public function setWhere($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where[] = array( $field => array( 'where' => $where, 'priority'=>$priority )); 
	}


	public function clearWhere($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where);
			$this->where = array();
		} else {
			$newwhere = array();
			foreach($this->where as $where ) {
				if( ! isset( $where[$field] ) ) {
					$newwhere[] = $where;
				}
			}
			$this->where = $newwhere;
		}
	}
	
	/**
	* Set Or Where 
	* @access public
	*/
	public function setWhereOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_or[] = array( $field => array( 'where_or' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_or);
			$this->where_or = array();
		} else {
			$newwhere_or = array();
			foreach($this->where_or as $where_or ) {
				if( ! isset( $where_or[$field] ) ) {
					$newwhere_or[] = $where_or;
				}
			}
			$this->where_or = $newwhere_or;
		}
	}
	
	/**
	* Set Where In
	* @access public
	*/
	public function setWhereIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_in[] = array( $field => array( 'where_in' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_in);
			$this->where_in = array();
		} else {
			$newwhere_in = array();
			foreach($this->where_in as $where_in ) {
				if( ! isset( $where_in[$field] ) ) {
					$newwhere_in[] = $where_in;
				}
			}
			$this->where_in = $newwhere_in;
		}
	}
	
	/**
	* Set Or Where In
	* @access public
	*/
	public function setWhereInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_in_or[] = array( $field => array( 'where_in_or' => $where, 'priority'=>$priority ));  
	}

	
	public function clearWhereInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_in_or);
			$this->where_in_or = array();
		} else {
			$newwhere_in_or = array();
			foreach($this->where_in_or as $where_in_or ) {
				if( ! isset( $where_in_or[$field] ) ) {
					$newwhere_in_or[] = $where_in_or;
				}
			}
			$this->where_in_or = $newwhere_in_or;
		}
	}
	
	/**
	* Set Where Not In
	* @access public
	*/
	public function setWhereNotIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_not_in[] = array( $field => array( 'where_not_in' => $where, 'priority'=>$priority )); 
	}
	
	public function clearWhereNotIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_not_in);
			$this->where_not_in = array();
		} else {
			$newwhere_not_in = array();
			foreach($this->where_not_in as $where_not_in ) {
				if( ! isset( $where_not_in[$field] ) ) {
					$newwhere_not_in[] = $where_not_in;
				}
			}
			$this->where_not_in = $newwhere_not_in;
		}
	}
	
	/**
	* Set Or Where Not In
	* @access public
	*/
	public function setWhereNotInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_not_in_or[] = array( $field => array( 'where_not_in_or' => $where, 'priority'=>$priority )); 
	}

	public function clearWhereNotInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_not_in_or);
			$this->where_not_in_or = array();
		} else {
			$newwhere_not_in_or = array();
			foreach($this->where_not_in_or as $where_not_in_or ) {
				if( ! isset( $where_not_in_or[$field] ) ) {
					$newwhere_not_in_or[] = $where_not_in_or;
				}
			}
			$this->where_not_in_or = $newwhere_not_in_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Like
	* @access public
	*/
	public function setLike($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like[] = array( $field => array( 'like' => $where, 'priority'=>$priority )); 
	}

	public function clearLike($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like);
			$this->like = array();
		} else {
			$newlike = array();
			foreach($this->like as $like ) {
				if( ! isset( $like[$field] ) ) {
					$newlike[] = $like;
				}
			}
			$this->like = $newlike;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_or[] = array( $field => array( 'like_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_or);
			$this->like_or = array();
		} else {
			$newlike_or = array();
			foreach($this->like_or as $like_or ) {
				if( ! isset( $like_or[$field] ) ) {
					$newlike_or[] = $like_or;
				}
			}
			$this->like_or = $newlike_or;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNot($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_not[] = array( $field => array( 'like_not' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNot($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_not);
			$this->like_not = array();
		} else {
			$newlike_not = array();
			foreach($this->like_not as $like_not ) {
				if( ! isset( $like_not[$field] ) ) {
					$newlike_not[] = $like_not;
				}
			}
			$this->like_not = $newlike_not;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNotOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_not_or[] = array( $field => array( 'like_not_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNotOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_not_or);
			$this->like_not_or = array();
		} else {
			$newlike_not_or = array();
			foreach($this->like_not_or as $like_not_or ) {
				if( ! isset( $like_not_or[$field] ) ) {
					$newlike_not_or[] = $like_not_or;
				}
			}
			$this->like_not_or = $newlike_not_or;
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Having 
	* @access public
	*/
	public function setHaving($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->having[] = array( $field => array( 'having' => $having, 'priority'=>$priority )); 
	}
	
	public function clearHaving($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->having);
			$this->having = array();
		} else {
			$newhaving = array();
			foreach($this->having as $having ) {
				if( ! isset( $having[$field] ) ) {
					$newhaving[] = $having;
				}
			}
			$this->having = $newhaving;
		}
	}
	
	/**
	* Set Or Having 
	* @access public
	*/
	public function setHavingOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->having_or[] = array( $field => array( 'having_or' => $having, 'priority'=>$priority ));  
	}

	public function clearHavingOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->having_or);
			$this->having_or = array();
		} else {
			$newhaving_or = array();
			foreach($this->having_or as $having_or ) {
				if( ! isset( $having_or[$field] ) ) {
					$newhaving_or[] = $having_or;
				}
			}
			$this->having_or = $newhaving_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Group By
	* @access public
	*/
	public function setGroupBy($fields) {
		if( is_array( $fields ) ) { 
			$this->group_by = array_merge( $this->group_by, $fields );
		} else {
			$this->group_by[] = $fields;
		}
	}

	private function setupGroupBy() {
		if( $this->group_by ) {
			$this->db->group_by( $this->group_by ); 
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Start  
	* @access public
	*/
	public function setStart($value) {
		$this->start = $value;
		return $this;
	}

	/**
	* Set Limit  
	* @access public
	*/
	public function setLimit($value) {
		$this->limit = $value;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Order  
	* @access public
	*/

	public function setOrder($field, $orientation='asc') {
		$this->order[$field] = $orientation;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Select  
	* @access public
	*/

	public function setSelect($select, $index=NULL) {
		if( is_null( $index ) ) {
			$this->select[] = $select;
		} else {
			$this->select[$index] = $select;
		}
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function setExclude($exclude) {
		$this->exclude[] = $exclude;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function isDistinct($distinct=TRUE) {
		$this->distinct = $distinct;
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Count All  
	* @access public
	*/

	public function count_all() {
		return  $this->db->count_all('users_lessons_sessions');
	}

	// --------------------------------------------------------------------

	/**
	* Count All Results 
	* @access public
	*/

	public function count_all_results() {
		$this->setupConditions();
		$this->db->from('users_lessons_sessions');
		return  $this->db->count_all_results();
	}
	
	// --------------------------------------------------------------------

	/**
	* Cache Control
	* @access public
	*/
	public function cache_on() {
		$this->cache_on = TRUE;
	}
	
	/**
	* Batch Insert
	* @access public
	*/
	public function updateBatch() {
		$this->batch = array_merge( $this->batch, array($this->getData()) );
	}
	
	public function insert_batch() {
		if( is_array( $this->batch ) && count( $this->batch ) > 0 ) {
			if( $this->db->insert_batch('users_lessons_sessions', $this->batch ) === TRUE ) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
}

/* End of file ci_users_lessons_sessions_model.php */
/* Location: ./application/models/ci_users_lessons_sessions_model.php */
