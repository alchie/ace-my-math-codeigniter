<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Users_model Class
 *
 * Manipulates `ci_users` table on database

CREATE TABLE `ci_users` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_username` varchar(100) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_type` varchar(10) DEFAULT NULL,
  `user_level` bigint(20) DEFAULT NULL,
  `user_firstname` varchar(100) NOT NULL,
  `user_lastname` varchar(100) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `user_plan` varchar(100) DEFAULT 'free',
  `user_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_expiry` timestamp NULL DEFAULT NULL,
  `user_active` int(1) DEFAULT '0',
  `last_login` timestamp NULL DEFAULT NULL,
  `last_login_ip` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
);

 * @package			Model
 * @project			Ace My Math, LLC
 * @project_link	http://www.acemymath.com/
 * @author			Chester Alan Tagudin
 * @author_link		http://www.chesteralan.com/
 */
 
class Users_model extends CI_Model {

	protected $user_id;
	protected $user_username;
	protected $user_password;
	protected $user_type;
	protected $user_level;
	protected $user_firstname;
	protected $user_lastname;
	protected $user_email;
	protected $user_plan;
	protected $user_created;
	protected $user_expiry;
	protected $user_active = '0';
	protected $last_login;
	protected $last_login_ip;
	protected $dataFields = array();
	protected $select = array();
	protected $join = array();
	protected $where = array();
	protected $where_or = array();
	protected $where_in = array();
	protected $where_in_or = array();
	protected $where_not_in = array();
	protected $where_not_in_or = array();
	protected $like = array();
	protected $like_or = array();
	protected $like_not = array();
	protected $like_not_or = array();
	protected $having = array();
	protected $having_or = array();
	protected $group_by = array();
	protected $filter = array();
	protected $order = array();
	protected $exclude = array();
	protected $required = array("user_username","user_email");
	protected $countField = '';
	protected $start = 0;
	protected $limit = 10;
	protected $results = FALSE;
	protected $distinct = FALSE;
	protected $cache_on = FALSE;
	protected $batch = array();

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct() {
		parent::__construct();
	}

	// --------------------------------------------------------------------


	// --------------------------------------------------------------------
	// Start Field: user_id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `user_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setUserId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->user_id = ( ($value == '') && ($this->user_id != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'users.user_id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'user_id';
		}
		return $this;
	}

	/** 
	* Get the value of `user_id` variable
	* @access public
	* @return String;
	*/

	public function getUserId() {
		return $this->user_id;
	}

	/**
	* Get row by `user_id`
	* @param user_id
	* @return QueryResult
	**/

	public function getByUserId() {
		if($this->user_id != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.user_id' => $this->user_id), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `user_id`
	**/

	public function updateByUserId() {
		if($this->user_id != '') {
			$this->setExclude('user_id');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.user_id' => $this->user_id ) );
			}
		}
	}


	/**
	* Delete row by `user_id`
	**/

	public function deleteByUserId() {
		if($this->user_id != '') {
			return $this->db->delete('users', array('users.user_id' => $this->user_id ) );
		}
	}

	/**
	* Increment row by `user_id`
	**/

	public function incrementByUserId() {
		if($this->user_id != '' && $this->countField != '') {
			$this->db->where('user_id', $this->user_id);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `user_id`
	**/

	public function decrementByUserId() {
		if($this->user_id != '' && $this->countField != '') {
			$this->db->where('user_id', $this->user_id);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: user_id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: user_username
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `user_username` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setUserUsername($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->user_username = ( ($value == '') && ($this->user_username != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'users.user_username';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'user_username';
		}
		return $this;
	}

	/** 
	* Get the value of `user_username` variable
	* @access public
	* @return String;
	*/

	public function getUserUsername() {
		return $this->user_username;
	}

	/**
	* Get row by `user_username`
	* @param user_username
	* @return QueryResult
	**/

	public function getByUserUsername() {
		if($this->user_username != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.user_username' => $this->user_username), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `user_username`
	**/

	public function updateByUserUsername() {
		if($this->user_username != '') {
			$this->setExclude('user_username');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.user_username' => $this->user_username ) );
			}
		}
	}


	/**
	* Delete row by `user_username`
	**/

	public function deleteByUserUsername() {
		if($this->user_username != '') {
			return $this->db->delete('users', array('users.user_username' => $this->user_username ) );
		}
	}

	/**
	* Increment row by `user_username`
	**/

	public function incrementByUserUsername() {
		if($this->user_username != '' && $this->countField != '') {
			$this->db->where('user_username', $this->user_username);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `user_username`
	**/

	public function decrementByUserUsername() {
		if($this->user_username != '' && $this->countField != '') {
			$this->db->where('user_username', $this->user_username);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: user_username
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: user_password
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `user_password` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setUserPassword($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->user_password = ( ($value == '') && ($this->user_password != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'users.user_password';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'user_password';
		}
		return $this;
	}

	/** 
	* Get the value of `user_password` variable
	* @access public
	* @return String;
	*/

	public function getUserPassword() {
		return $this->user_password;
	}

	/**
	* Get row by `user_password`
	* @param user_password
	* @return QueryResult
	**/

	public function getByUserPassword() {
		if($this->user_password != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.user_password' => $this->user_password), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `user_password`
	**/

	public function updateByUserPassword() {
		if($this->user_password != '') {
			$this->setExclude('user_password');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.user_password' => $this->user_password ) );
			}
		}
	}


	/**
	* Delete row by `user_password`
	**/

	public function deleteByUserPassword() {
		if($this->user_password != '') {
			return $this->db->delete('users', array('users.user_password' => $this->user_password ) );
		}
	}

	/**
	* Increment row by `user_password`
	**/

	public function incrementByUserPassword() {
		if($this->user_password != '' && $this->countField != '') {
			$this->db->where('user_password', $this->user_password);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `user_password`
	**/

	public function decrementByUserPassword() {
		if($this->user_password != '' && $this->countField != '') {
			$this->db->where('user_password', $this->user_password);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: user_password
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: user_type
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `user_type` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setUserType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->user_type = ( ($value == '') && ($this->user_type != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'users.user_type';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'user_type';
		}
		return $this;
	}

	/** 
	* Get the value of `user_type` variable
	* @access public
	* @return String;
	*/

	public function getUserType() {
		return $this->user_type;
	}

	/**
	* Get row by `user_type`
	* @param user_type
	* @return QueryResult
	**/

	public function getByUserType() {
		if($this->user_type != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.user_type' => $this->user_type), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `user_type`
	**/

	public function updateByUserType() {
		if($this->user_type != '') {
			$this->setExclude('user_type');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.user_type' => $this->user_type ) );
			}
		}
	}


	/**
	* Delete row by `user_type`
	**/

	public function deleteByUserType() {
		if($this->user_type != '') {
			return $this->db->delete('users', array('users.user_type' => $this->user_type ) );
		}
	}

	/**
	* Increment row by `user_type`
	**/

	public function incrementByUserType() {
		if($this->user_type != '' && $this->countField != '') {
			$this->db->where('user_type', $this->user_type);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `user_type`
	**/

	public function decrementByUserType() {
		if($this->user_type != '' && $this->countField != '') {
			$this->db->where('user_type', $this->user_type);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: user_type
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: user_level
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `user_level` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setUserLevel($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->user_level = ( ($value == '') && ($this->user_level != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'users.user_level';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'user_level';
		}
		return $this;
	}

	/** 
	* Get the value of `user_level` variable
	* @access public
	* @return String;
	*/

	public function getUserLevel() {
		return $this->user_level;
	}

	/**
	* Get row by `user_level`
	* @param user_level
	* @return QueryResult
	**/

	public function getByUserLevel() {
		if($this->user_level != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.user_level' => $this->user_level), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `user_level`
	**/

	public function updateByUserLevel() {
		if($this->user_level != '') {
			$this->setExclude('user_level');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.user_level' => $this->user_level ) );
			}
		}
	}


	/**
	* Delete row by `user_level`
	**/

	public function deleteByUserLevel() {
		if($this->user_level != '') {
			return $this->db->delete('users', array('users.user_level' => $this->user_level ) );
		}
	}

	/**
	* Increment row by `user_level`
	**/

	public function incrementByUserLevel() {
		if($this->user_level != '' && $this->countField != '') {
			$this->db->where('user_level', $this->user_level);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `user_level`
	**/

	public function decrementByUserLevel() {
		if($this->user_level != '' && $this->countField != '') {
			$this->db->where('user_level', $this->user_level);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: user_level
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: user_firstname
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `user_firstname` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setUserFirstname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->user_firstname = ( ($value == '') && ($this->user_firstname != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'users.user_firstname';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'user_firstname';
		}
		return $this;
	}

	/** 
	* Get the value of `user_firstname` variable
	* @access public
	* @return String;
	*/

	public function getUserFirstname() {
		return $this->user_firstname;
	}

	/**
	* Get row by `user_firstname`
	* @param user_firstname
	* @return QueryResult
	**/

	public function getByUserFirstname() {
		if($this->user_firstname != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.user_firstname' => $this->user_firstname), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `user_firstname`
	**/

	public function updateByUserFirstname() {
		if($this->user_firstname != '') {
			$this->setExclude('user_firstname');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.user_firstname' => $this->user_firstname ) );
			}
		}
	}


	/**
	* Delete row by `user_firstname`
	**/

	public function deleteByUserFirstname() {
		if($this->user_firstname != '') {
			return $this->db->delete('users', array('users.user_firstname' => $this->user_firstname ) );
		}
	}

	/**
	* Increment row by `user_firstname`
	**/

	public function incrementByUserFirstname() {
		if($this->user_firstname != '' && $this->countField != '') {
			$this->db->where('user_firstname', $this->user_firstname);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `user_firstname`
	**/

	public function decrementByUserFirstname() {
		if($this->user_firstname != '' && $this->countField != '') {
			$this->db->where('user_firstname', $this->user_firstname);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: user_firstname
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: user_lastname
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `user_lastname` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setUserLastname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->user_lastname = ( ($value == '') && ($this->user_lastname != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'users.user_lastname';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'user_lastname';
		}
		return $this;
	}

	/** 
	* Get the value of `user_lastname` variable
	* @access public
	* @return String;
	*/

	public function getUserLastname() {
		return $this->user_lastname;
	}

	/**
	* Get row by `user_lastname`
	* @param user_lastname
	* @return QueryResult
	**/

	public function getByUserLastname() {
		if($this->user_lastname != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.user_lastname' => $this->user_lastname), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `user_lastname`
	**/

	public function updateByUserLastname() {
		if($this->user_lastname != '') {
			$this->setExclude('user_lastname');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.user_lastname' => $this->user_lastname ) );
			}
		}
	}


	/**
	* Delete row by `user_lastname`
	**/

	public function deleteByUserLastname() {
		if($this->user_lastname != '') {
			return $this->db->delete('users', array('users.user_lastname' => $this->user_lastname ) );
		}
	}

	/**
	* Increment row by `user_lastname`
	**/

	public function incrementByUserLastname() {
		if($this->user_lastname != '' && $this->countField != '') {
			$this->db->where('user_lastname', $this->user_lastname);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `user_lastname`
	**/

	public function decrementByUserLastname() {
		if($this->user_lastname != '' && $this->countField != '') {
			$this->db->where('user_lastname', $this->user_lastname);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: user_lastname
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: user_email
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `user_email` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setUserEmail($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->user_email = ( ($value == '') && ($this->user_email != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'users.user_email';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'user_email';
		}
		return $this;
	}

	/** 
	* Get the value of `user_email` variable
	* @access public
	* @return String;
	*/

	public function getUserEmail() {
		return $this->user_email;
	}

	/**
	* Get row by `user_email`
	* @param user_email
	* @return QueryResult
	**/

	public function getByUserEmail() {
		if($this->user_email != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.user_email' => $this->user_email), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `user_email`
	**/

	public function updateByUserEmail() {
		if($this->user_email != '') {
			$this->setExclude('user_email');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.user_email' => $this->user_email ) );
			}
		}
	}


	/**
	* Delete row by `user_email`
	**/

	public function deleteByUserEmail() {
		if($this->user_email != '') {
			return $this->db->delete('users', array('users.user_email' => $this->user_email ) );
		}
	}

	/**
	* Increment row by `user_email`
	**/

	public function incrementByUserEmail() {
		if($this->user_email != '' && $this->countField != '') {
			$this->db->where('user_email', $this->user_email);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `user_email`
	**/

	public function decrementByUserEmail() {
		if($this->user_email != '' && $this->countField != '') {
			$this->db->where('user_email', $this->user_email);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: user_email
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: user_plan
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `user_plan` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setUserPlan($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->user_plan = ( ($value == '') && ($this->user_plan != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'users.user_plan';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'user_plan';
		}
		return $this;
	}

	/** 
	* Get the value of `user_plan` variable
	* @access public
	* @return String;
	*/

	public function getUserPlan() {
		return $this->user_plan;
	}

	/**
	* Get row by `user_plan`
	* @param user_plan
	* @return QueryResult
	**/

	public function getByUserPlan() {
		if($this->user_plan != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.user_plan' => $this->user_plan), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `user_plan`
	**/

	public function updateByUserPlan() {
		if($this->user_plan != '') {
			$this->setExclude('user_plan');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.user_plan' => $this->user_plan ) );
			}
		}
	}


	/**
	* Delete row by `user_plan`
	**/

	public function deleteByUserPlan() {
		if($this->user_plan != '') {
			return $this->db->delete('users', array('users.user_plan' => $this->user_plan ) );
		}
	}

	/**
	* Increment row by `user_plan`
	**/

	public function incrementByUserPlan() {
		if($this->user_plan != '' && $this->countField != '') {
			$this->db->where('user_plan', $this->user_plan);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `user_plan`
	**/

	public function decrementByUserPlan() {
		if($this->user_plan != '' && $this->countField != '') {
			$this->db->where('user_plan', $this->user_plan);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: user_plan
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: user_created
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `user_created` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setUserCreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->user_created = ( ($value == '') && ($this->user_created != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'users.user_created';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'user_created';
		}
		return $this;
	}

	/** 
	* Get the value of `user_created` variable
	* @access public
	* @return String;
	*/

	public function getUserCreated() {
		return $this->user_created;
	}

	/**
	* Get row by `user_created`
	* @param user_created
	* @return QueryResult
	**/

	public function getByUserCreated() {
		if($this->user_created != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.user_created' => $this->user_created), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `user_created`
	**/

	public function updateByUserCreated() {
		if($this->user_created != '') {
			$this->setExclude('user_created');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.user_created' => $this->user_created ) );
			}
		}
	}


	/**
	* Delete row by `user_created`
	**/

	public function deleteByUserCreated() {
		if($this->user_created != '') {
			return $this->db->delete('users', array('users.user_created' => $this->user_created ) );
		}
	}

	/**
	* Increment row by `user_created`
	**/

	public function incrementByUserCreated() {
		if($this->user_created != '' && $this->countField != '') {
			$this->db->where('user_created', $this->user_created);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `user_created`
	**/

	public function decrementByUserCreated() {
		if($this->user_created != '' && $this->countField != '') {
			$this->db->where('user_created', $this->user_created);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: user_created
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: user_expiry
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `user_expiry` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setUserExpiry($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->user_expiry = ( ($value == '') && ($this->user_expiry != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'users.user_expiry';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'user_expiry';
		}
		return $this;
	}

	/** 
	* Get the value of `user_expiry` variable
	* @access public
	* @return String;
	*/

	public function getUserExpiry() {
		return $this->user_expiry;
	}

	/**
	* Get row by `user_expiry`
	* @param user_expiry
	* @return QueryResult
	**/

	public function getByUserExpiry() {
		if($this->user_expiry != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.user_expiry' => $this->user_expiry), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `user_expiry`
	**/

	public function updateByUserExpiry() {
		if($this->user_expiry != '') {
			$this->setExclude('user_expiry');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.user_expiry' => $this->user_expiry ) );
			}
		}
	}


	/**
	* Delete row by `user_expiry`
	**/

	public function deleteByUserExpiry() {
		if($this->user_expiry != '') {
			return $this->db->delete('users', array('users.user_expiry' => $this->user_expiry ) );
		}
	}

	/**
	* Increment row by `user_expiry`
	**/

	public function incrementByUserExpiry() {
		if($this->user_expiry != '' && $this->countField != '') {
			$this->db->where('user_expiry', $this->user_expiry);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `user_expiry`
	**/

	public function decrementByUserExpiry() {
		if($this->user_expiry != '' && $this->countField != '') {
			$this->db->where('user_expiry', $this->user_expiry);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: user_expiry
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: user_active
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `user_active` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setUserActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->user_active = ( ($value == '') && ($this->user_active != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'users.user_active';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'user_active';
		}
		return $this;
	}

	/** 
	* Get the value of `user_active` variable
	* @access public
	* @return String;
	*/

	public function getUserActive() {
		return $this->user_active;
	}

	/**
	* Get row by `user_active`
	* @param user_active
	* @return QueryResult
	**/

	public function getByUserActive() {
		if($this->user_active != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.user_active' => $this->user_active), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `user_active`
	**/

	public function updateByUserActive() {
		if($this->user_active != '') {
			$this->setExclude('user_active');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.user_active' => $this->user_active ) );
			}
		}
	}


	/**
	* Delete row by `user_active`
	**/

	public function deleteByUserActive() {
		if($this->user_active != '') {
			return $this->db->delete('users', array('users.user_active' => $this->user_active ) );
		}
	}

	/**
	* Increment row by `user_active`
	**/

	public function incrementByUserActive() {
		if($this->user_active != '' && $this->countField != '') {
			$this->db->where('user_active', $this->user_active);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `user_active`
	**/

	public function decrementByUserActive() {
		if($this->user_active != '' && $this->countField != '') {
			$this->db->where('user_active', $this->user_active);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: user_active
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: last_login
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `last_login` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setLastLogin($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->last_login = ( ($value == '') && ($this->last_login != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'users.last_login';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'last_login';
		}
		return $this;
	}

	/** 
	* Get the value of `last_login` variable
	* @access public
	* @return String;
	*/

	public function getLastLogin() {
		return $this->last_login;
	}

	/**
	* Get row by `last_login`
	* @param last_login
	* @return QueryResult
	**/

	public function getByLastLogin() {
		if($this->last_login != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.last_login' => $this->last_login), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `last_login`
	**/

	public function updateByLastLogin() {
		if($this->last_login != '') {
			$this->setExclude('last_login');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.last_login' => $this->last_login ) );
			}
		}
	}


	/**
	* Delete row by `last_login`
	**/

	public function deleteByLastLogin() {
		if($this->last_login != '') {
			return $this->db->delete('users', array('users.last_login' => $this->last_login ) );
		}
	}

	/**
	* Increment row by `last_login`
	**/

	public function incrementByLastLogin() {
		if($this->last_login != '' && $this->countField != '') {
			$this->db->where('last_login', $this->last_login);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `last_login`
	**/

	public function decrementByLastLogin() {
		if($this->last_login != '' && $this->countField != '') {
			$this->db->where('last_login', $this->last_login);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: last_login
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: last_login_ip
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `last_login_ip` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setLastLoginIp($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->last_login_ip = ( ($value == '') && ($this->last_login_ip != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'users.last_login_ip';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'last_login_ip';
		}
		return $this;
	}

	/** 
	* Get the value of `last_login_ip` variable
	* @access public
	* @return String;
	*/

	public function getLastLoginIp() {
		return $this->last_login_ip;
	}

	/**
	* Get row by `last_login_ip`
	* @param last_login_ip
	* @return QueryResult
	**/

	public function getByLastLoginIp() {
		if($this->last_login_ip != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.last_login_ip' => $this->last_login_ip), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `last_login_ip`
	**/

	public function updateByLastLoginIp() {
		if($this->last_login_ip != '') {
			$this->setExclude('last_login_ip');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.last_login_ip' => $this->last_login_ip ) );
			}
		}
	}


	/**
	* Delete row by `last_login_ip`
	**/

	public function deleteByLastLoginIp() {
		if($this->last_login_ip != '') {
			return $this->db->delete('users', array('users.last_login_ip' => $this->last_login_ip ) );
		}
	}

	/**
	* Increment row by `last_login_ip`
	**/

	public function incrementByLastLoginIp() {
		if($this->last_login_ip != '' && $this->countField != '') {
			$this->db->where('last_login_ip', $this->last_login_ip);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `last_login_ip`
	**/

	public function decrementByLastLoginIp() {
		if($this->last_login_ip != '' && $this->countField != '') {
			$this->db->where('last_login_ip', $this->last_login_ip);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: last_login_ip
	// --------------------------------------------------------------------


	// --------------------------------------------------------------------

	/**
	* Checks if result not empty 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function nonEmpty() {
		if ( $this->hasConditions() ) {
			
			$this->setupJoin();
			$this->setupConditions();
			$this->setupHaving();
			$this->setupGroupBy();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('users', 1);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			$result = $query->result();
			if ( isset( $result[0] ) === true ) {
				$this->results = $result[0];
				return true;
			} else {
				return false;
			}
		}
	}


	/**
	* Get Results 
	* @return Mixed;
	*/

	public function getResults() {
		return $this->results;
	}


	// --------------------------------------------------------------------

	/**
	* Delete 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function delete() {
		if ( $this->hasConditions() ) {
				$this->setupConditions();
				return $this->db->delete('users');
		}
	}


	// --------------------------------------------------------------------

	/**
	* Limit Data Fields
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function limitDataFields($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->dataFields = array($fields);
			} else {
				$this->dataFields = $fields;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Update 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function update() {
		if ( ( $this->getData() ) && ( $this->hasConditions() ) ) {
				$this->setupConditions();
				return $this->db->update('users', $this->getData() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Insert new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function insert() {
		if( $this->getData() ) {
			if( $this->db->insert('users', $this->getData() ) === TRUE ) {
				$this->user_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}


	// --------------------------------------------------------------------

	/**
	* Replace new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function replace() {
		if( $this->getData() ) {
			if( $this->db->replace('users', $this->getData() ) === TRUE ) {
				$this->user_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Get First Data 
	* @access public
	* @return Object / False;
	*/

	public function get() {
		
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupHaving();
		$this->setupGroupBy();
		
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		$this->db->limit( 1, $this->start);
		
		if( $this->cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('users');
		
		if( $this->cache_on ) {
			$this->db->cache_off();
		}
		
		$result = $query->result();
		
		if( isset($result[0]) ) {
			
			$this->results = $result[0];
			
			return $this->results;
			
		}
		
		return false;
	}
	// --------------------------------------------------------------------

	/**
	* Populate Data 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function populate() {
	
		if( $this->distinct ) {
			$this->db->distinct();
		}
		
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select ) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupHaving();
		$this->setupGroupBy();
		
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		if( $this->limit > 0 ) {
			$this->db->limit( $this->limit,$this->start);
		}
		
		if( $this->cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('users');
		
		if( $this->cache_on ) {
			$this->db->cache_off();
		}
			
		$this->results = $query->result();
		
		return $this->results;
	}
	// --------------------------------------------------------------------
	/**
	* Recursive 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function recursive($match, $find, $child='user_id', $level=10, $conn='children') {
			if( $level == 0 ) return;
			if( $this->limit > 0 ) {
				$this->db->limit( $this->limit,$this->start);
			}
			if( $this->select ) {
					$this->db->select( implode(',' , $this->select) );
			}
			
			$this->setupJoin();
			$this->setWhere($match, $find);
			$this->setupConditions();
			$this->setupHaving();
			$this->setupGroupBy();
			$this->clearWhere( $match );
			
			if( $this->order ) {
				foreach( $this->order as $field=>$orientation ) {
					$this->db->order_by( $field, $orientation );
				}
			}
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('users');
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			$results=array();
			if( $query->result() ) {
				foreach( $query->result() as $qr ) {
					$children = $this->recursive($match, $qr->$child, $child, ($level-1), $conn ) ;
					$results[] = (object) array_merge( (array) $qr, array($conn=>$children) );
				}
			}
		return $results;
	}

// --------------------------------------------------------------------

	/**
	* Set Field Where Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setFieldWhere($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->where[] = array($fields => $this->$fields);
			} else {
				foreach($fields as $field) {
					if($w != '') {
						$this->where[] = array($field => $this->$field);
					}
				}
			}
		}
	}

// --------------------------------------------------------------------

	/**
	* Set Field Value Manually
	* @access public
	* @param Field Key ; Value
	* @return self;
	*/
	public function setFieldValue($field, $value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL) {
		if( isset( $this->$field ) ) {
			$this->$field = ( ($value == '') && ($this->$field != '') ) ? '' : $value;
			if( $setWhere ) {
				$key = 'users.'.$field;
				if ( $whereOperator != NULL && $whereOperator != '' ) {
					$key = $key . ' ' . $whereOperator;
				}
				//$this->where[$key] = $this->$field;
				$this->where[] = array( $key => $this->$field );
			}
			if( $set_data_field ) {
				$this->dataFields[] = $field;
			}
		}
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Prepares data 
	* @access private
	* @return Array;
	*/
	public function getData($exclude=NULL) {
		$data = array();

		$fields = array("user_id","user_username","user_password","user_type","user_level","user_firstname","user_lastname","user_email","user_plan","user_created","user_expiry","user_active","last_login","last_login_ip");
		if( count( $this->dataFields ) > 0 ) {
    		            $fields = $this->dataFields;
		}
		foreach( $fields as $field ) {
		    if( ( in_array( $field, $this->required ) ) 
		    && ($this->$field == '') 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		return false;
    		}
    		if( ( in_array( $field, $this->required ) ) 
    		&& ($this->$field != '') 
    		&& ( ! in_array( $field, $this->exclude ) ) 
    		) {
		        $data[$field] = $this->$field;
		    }
		    if( ( ! in_array( $field, $this->required ) ) 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		$data[$field] = $this->$field;
    		}  
		}
		return $data;   
	}


	// --------------------------------------------------------------------

	/**
	* Setup Conditional Clauses 
	* @access public
	* @return Null;
	*/
	
	protected function __apply_condition($what, $condition) {
		if( is_array( $condition ) ) {
			foreach( $condition as $key => $value ) {
				$this->db->$what( $key , $value );
			}
		} else {
			$this->db->$what( $condition );
		}
	}
	
	private function setupConditions() {
		$return = FALSE;
		
		$conditions = array_merge(
                                $this->where,
                                $this->filter,
                                $this->where_or,
                                $this->where_in,
                                $this->where_in_or,
                                $this->where_not_in,
                                $this->where_not_in_or,
                                $this->like,
                                $this->like_or,
                                $this->like_not,
                                $this->like_not_or
                                );
		
		if( count( $conditions ) > 0 ) {
			$i = 0;
			$sorted = array();
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( ! is_null( $priority ) ) {
						while( isset($sorted[$priority]) ) {
							$i++;
							$priority += $i;
						}
						$sorted[ $priority ] = $options;
					}
				}
			}
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( is_null( $priority )  ) {
						while( isset($sorted[$i]) ) {
							$i++;
						}
						$sorted[$i] = $options;
					} 
				}
			}
			
			ksort( $sorted );
		
			foreach( $sorted as $sortd ) {
				$sortd_key = array_keys( $sortd );
				$sortd_value = array_values( $sortd );
				
				if( isset( $sortd_key[0] ) && $sortd_value[0] ) {
					switch( $sortd_key[0] ) {
						case 'where_or':
							$this->__apply_condition('or_where', $sortd_value[0]);
						break;
						case 'where_in':
							$this->__apply_condition('where_in', $sortd_value[0]);
						break;
						case 'where_in_or':
							$this->__apply_condition('or_where_in', $sortd_value[0]);
						break;
						case 'where_not_in':
							$this->__apply_condition('where_not_in', $sortd_value[0]);
						break;
						case 'where_not_in_or':
							$this->__apply_condition('or_where_not_in', $sortd_value[0]);
						break;
						case 'like':
							$this->__apply_condition('like', $sortd_value[0]);
						break;
						case 'like_or':
							$this->__apply_condition('or_like', $sortd_value[0]);
						break;
						case 'like_not':
							$this->__apply_condition('not_like', $sortd_value[0]);
						break;
						case 'like_not_or':
							$this->__apply_condition('or_not_like', $sortd_value[0]);
						break;
						default:
							$this->__apply_condition('where', $sortd_value[0]);
						break;
					}
				}
			}
			$return = TRUE;
		}
		
		return $return;
	}
	
	private function setupHaving() {
		
		$return = FALSE;
		
		$conditions = array_merge( $this->having, $this->having_or );
		
		if( count( $conditions ) > 0 ) {
			$i = 0;
			$sorted = array();
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( ! is_null( $priority ) ) {
						while( isset($sorted[$priority]) ) {
							$i++;
							$priority += $i;
						}
						$sorted[ $priority ] = $options;
					}
				}
			}
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( is_null( $priority )  ) {
						while( isset($sorted[$i]) ) {
							$i++;
						}
						$sorted[$i] = $options;
					} 
				}
			}
			
			ksort( $sorted );
		
			foreach( $sorted as $sortd ) {
				$sortd_key = array_keys( $sortd );
				$sortd_value = array_values( $sortd );
				
				if( isset( $sortd_key[0] ) && $sortd_value[0] ) {
					switch( $sortd_key[0] ) {
						case 'having':
							$this->__apply_condition('having', $sortd_value[0]);
						break;
						case 'having_or':
							$this->__apply_condition('or_having', $sortd_value[0]);
						break;
					}
				}
			}
			$return = TRUE;
		}
		
		return $return;
	}
	
	/**
	* Check Conditions Availability
	* @access public
	* @return Array;
	*/
	private function hasConditions() {
		$conditions = array_merge(
                                $this->where,
                                $this->filter,
                                $this->where_or,
                                $this->where_in,
                                $this->where_in_or,
                                $this->where_not_in,
                                $this->where_not_in_or,
                                $this->like,
                                $this->like_or,
                                $this->like_not,
                                $this->like_not_or,
                                $this->having,
                                $this->having_or
                        );
		if( count( $conditions ) > 0 ) {
			return true;
		}
		return false;
	}
	

	// --------------------------------------------------------------------
	/**
	* Set Join Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setJoin($table, $connection, $option='left') {
		$this->join[] = array('table'=>$table, 'connection'=>$connection, 'option'=>$option );
	}

	private function setupJoin() {
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Filter 
	* @access public
	* @return Array;
	*/
	public function setFilter($field, $value, $table=NULL, $priority=NULL, $underCondition='where') {
		$key = array();
		if( $table == NULL ) { 
			$table = 'users'; 
		} 
		if( $table != '' ) {
			$key[] = $table;
		}
		$key[] = $field;
		
		$newField = implode('.', $key);
		
		if( is_null( $value ) ) {
			$where = $newField;
		} else {
			$where = array( $newField => $value );
		}
		
		$this->filter[] = array( $newField => array( $underCondition => $where, 'priority'=>$priority ) );
	}


	public function clearFilter($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->filter);
			$this->filter = array();
		} else {
			$newfilter = array();
			foreach($this->filter as $filter ) {
				if( ! isset( $filter[$field] ) ) {
					$newfilter[] = $filter;
				}
			}
			$this->filter = $newfilter;
		}
	}

	// --------------------------------------------------------------------

	/**
	* Set Where 
	* @access public
	*/
	public function setWhere($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where[] = array( $field => array( 'where' => $where, 'priority'=>$priority )); 
	}


	public function clearWhere($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where);
			$this->where = array();
		} else {
			$newwhere = array();
			foreach($this->where as $where ) {
				if( ! isset( $where[$field] ) ) {
					$newwhere[] = $where;
				}
			}
			$this->where = $newwhere;
		}
	}
	
	/**
	* Set Or Where 
	* @access public
	*/
	public function setWhereOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_or[] = array( $field => array( 'where_or' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_or);
			$this->where_or = array();
		} else {
			$newwhere_or = array();
			foreach($this->where_or as $where_or ) {
				if( ! isset( $where_or[$field] ) ) {
					$newwhere_or[] = $where_or;
				}
			}
			$this->where_or = $newwhere_or;
		}
	}
	
	/**
	* Set Where In
	* @access public
	*/
	public function setWhereIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_in[] = array( $field => array( 'where_in' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_in);
			$this->where_in = array();
		} else {
			$newwhere_in = array();
			foreach($this->where_in as $where_in ) {
				if( ! isset( $where_in[$field] ) ) {
					$newwhere_in[] = $where_in;
				}
			}
			$this->where_in = $newwhere_in;
		}
	}
	
	/**
	* Set Or Where In
	* @access public
	*/
	public function setWhereInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_in_or[] = array( $field => array( 'where_in_or' => $where, 'priority'=>$priority ));  
	}

	
	public function clearWhereInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_in_or);
			$this->where_in_or = array();
		} else {
			$newwhere_in_or = array();
			foreach($this->where_in_or as $where_in_or ) {
				if( ! isset( $where_in_or[$field] ) ) {
					$newwhere_in_or[] = $where_in_or;
				}
			}
			$this->where_in_or = $newwhere_in_or;
		}
	}
	
	/**
	* Set Where Not In
	* @access public
	*/
	public function setWhereNotIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_not_in[] = array( $field => array( 'where_not_in' => $where, 'priority'=>$priority )); 
	}
	
	public function clearWhereNotIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_not_in);
			$this->where_not_in = array();
		} else {
			$newwhere_not_in = array();
			foreach($this->where_not_in as $where_not_in ) {
				if( ! isset( $where_not_in[$field] ) ) {
					$newwhere_not_in[] = $where_not_in;
				}
			}
			$this->where_not_in = $newwhere_not_in;
		}
	}
	
	/**
	* Set Or Where Not In
	* @access public
	*/
	public function setWhereNotInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_not_in_or[] = array( $field => array( 'where_not_in_or' => $where, 'priority'=>$priority )); 
	}

	public function clearWhereNotInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_not_in_or);
			$this->where_not_in_or = array();
		} else {
			$newwhere_not_in_or = array();
			foreach($this->where_not_in_or as $where_not_in_or ) {
				if( ! isset( $where_not_in_or[$field] ) ) {
					$newwhere_not_in_or[] = $where_not_in_or;
				}
			}
			$this->where_not_in_or = $newwhere_not_in_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Like
	* @access public
	*/
	public function setLike($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like[] = array( $field => array( 'like' => $where, 'priority'=>$priority )); 
	}

	public function clearLike($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like);
			$this->like = array();
		} else {
			$newlike = array();
			foreach($this->like as $like ) {
				if( ! isset( $like[$field] ) ) {
					$newlike[] = $like;
				}
			}
			$this->like = $newlike;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_or[] = array( $field => array( 'like_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_or);
			$this->like_or = array();
		} else {
			$newlike_or = array();
			foreach($this->like_or as $like_or ) {
				if( ! isset( $like_or[$field] ) ) {
					$newlike_or[] = $like_or;
				}
			}
			$this->like_or = $newlike_or;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNot($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_not[] = array( $field => array( 'like_not' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNot($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_not);
			$this->like_not = array();
		} else {
			$newlike_not = array();
			foreach($this->like_not as $like_not ) {
				if( ! isset( $like_not[$field] ) ) {
					$newlike_not[] = $like_not;
				}
			}
			$this->like_not = $newlike_not;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNotOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_not_or[] = array( $field => array( 'like_not_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNotOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_not_or);
			$this->like_not_or = array();
		} else {
			$newlike_not_or = array();
			foreach($this->like_not_or as $like_not_or ) {
				if( ! isset( $like_not_or[$field] ) ) {
					$newlike_not_or[] = $like_not_or;
				}
			}
			$this->like_not_or = $newlike_not_or;
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Having 
	* @access public
	*/
	public function setHaving($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->having[] = array( $field => array( 'having' => $having, 'priority'=>$priority )); 
	}
	
	public function clearHaving($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->having);
			$this->having = array();
		} else {
			$newhaving = array();
			foreach($this->having as $having ) {
				if( ! isset( $having[$field] ) ) {
					$newhaving[] = $having;
				}
			}
			$this->having = $newhaving;
		}
	}
	
	/**
	* Set Or Having 
	* @access public
	*/
	public function setHavingOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->having_or[] = array( $field => array( 'having_or' => $having, 'priority'=>$priority ));  
	}

	public function clearHavingOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->having_or);
			$this->having_or = array();
		} else {
			$newhaving_or = array();
			foreach($this->having_or as $having_or ) {
				if( ! isset( $having_or[$field] ) ) {
					$newhaving_or[] = $having_or;
				}
			}
			$this->having_or = $newhaving_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Group By
	* @access public
	*/
	public function setGroupBy($fields) {
		if( is_array( $fields ) ) { 
			$this->group_by = array_merge( $this->group_by, $fields );
		} else {
			$this->group_by[] = $fields;
		}
	}

	private function setupGroupBy() {
		if( $this->group_by ) {
			$this->db->group_by( $this->group_by ); 
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Start  
	* @access public
	*/
	public function setStart($value) {
		$this->start = $value;
		return $this;
	}

	/**
	* Set Limit  
	* @access public
	*/
	public function setLimit($value) {
		$this->limit = $value;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Order  
	* @access public
	*/

	public function setOrder($field, $orientation='asc') {
		$this->order[$field] = $orientation;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Select  
	* @access public
	*/

	public function setSelect($select, $index=NULL) {
		if( is_null( $index ) ) {
			$this->select[] = $select;
		} else {
			$this->select[$index] = $select;
		}
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function setExclude($exclude) {
		$this->exclude[] = $exclude;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function isDistinct($distinct=TRUE) {
		$this->distinct = $distinct;
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Count All  
	* @access public
	*/

	public function count_all() {
		return  $this->db->count_all('users');
	}

	// --------------------------------------------------------------------

	/**
	* Count All Results 
	* @access public
	*/

	public function count_all_results() {
		$this->setupConditions();
		$this->db->from('users');
		return  $this->db->count_all_results();
	}
	
	// --------------------------------------------------------------------

	/**
	* Cache Control
	* @access public
	*/
	public function cache_on() {
		$this->cache_on = TRUE;
	}
	
	/**
	* Batch Insert
	* @access public
	*/
	public function updateBatch() {
		$this->batch = array_merge( $this->batch, array($this->getData()) );
	}
	
	public function insert_batch() {
		if( is_array( $this->batch ) && count( $this->batch ) > 0 ) {
			if( $this->db->insert_batch('users', $this->batch ) === TRUE ) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
}

/* End of file ci_users_model.php */
/* Location: ./application/models/ci_users_model.php */
