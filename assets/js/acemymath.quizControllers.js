var QuizControllers = {
	correctButton : function( b ) {
		b.prop('disabled', true).removeClass('btn-default').addClass('btn-success').unbind('click');
	},
	wrongButton : function( b ) {
		b.prop('disabled', true).removeClass('btn-default').addClass('btn-danger').unbind('click');
	},
	multiple_choice_controls : function($, jwp, data, correct_callback, wrong_callback) {
			$('.btn-choices').click(function(){
				var index = $(this).attr('data-index');
				if( index == data.answer ) {
					correct_callback();
					QuizControllers.correctButton( $(this) );
					$('.btn-choices').prop('disabled', true);
				} else {
					wrong_callback();
					QuizControllers.wrongButton( $(this) );
				}
			});
	},
	checkbox_controls : function(cls, $, jwp, data, correct_callback, wrong_callback) {
		$('#submit-answer').click(function(){
			var index = [], n=0;
			$('.'+cls+':checked').each(function(){
				index[n] = parseInt( $(this).attr('data-index') );
				n++;
			});
			if( index.toString() == data.answer.toString() ) {
				correct_callback();
				$('.btn-choices').prop('disabled', true);
			} else {
				wrong_callback();
			}
		});
	},
	checkbox_choices : function(choices) {
		var html = '';
		for( i in choices ) {
			html += '<p><input type="checkbox" class="checkbox-choices" data-index="'+i+'" id="QuizControllersq1b'+i+'"> ' + choices[i] + '</p>';
		}
		return html;
	},
	button_choices : function(choices) {
		var html = '';
		for( i in choices ) {
			html += '<p><button class="btn btn-default btn-sm btn-choices" data-index="'+i+'" id="QuizControllersq1b'+i+'">' + choices[i] + '</button></p>';
		}
		return html;
	},
	image_button_choices : function(choices) {
		var html = '';
		for( i in choices ) {
			html += '<p><img src="'+ choices[i] +'" class="btn btn-default btn-sm btn-choices" data-index="'+i+'" id="QuizControllersq1b'+i+'" style="width:70%" /></p>';
		}
		return html;
	},
	single_input_box : function(id, $, jwp, data, correct_callback, wrong_callback) {
		var submit_answer = function(){
			var index = $('#'+id).val();
			if( QuizControllers.sanitize( index ) == QuizControllers.sanitize( data.answer ) ) {
				correct_callback();
				$('#'+id).prop('disabled', true);
			} else {
				wrong_callback();
				$('#'+id).select();
			}
		};
		$('#'+id).focus().keyup(function(e) { 
			var code = e.which; 
			if(code==13){ e.preventDefault(); submit_answer(); } 
			if( code !== 9 ) {
				var max = $(this).attr("maxlength"), 
				index = $(this).attr("data-index"), 
				count = $(this).val().length;
				
				if( count == max ) {
					$('.input-index-' + (parseInt(index) + 1)).focus().select();
				}
			}
			
		}).click(function(){
			$(this).select();
		});
		$('#submit-answer').click(submit_answer);
	},
	single_input_box_any : function(id, $, jwp, data, correct_callback, wrong_callback) {
		var submit_answer = function(){
			var index = $('#'+id).val();
			if( QuizControllers.inArray( index , data.answer ) ) {
				correct_callback();
				$('#'+id).prop('disabled', true);
			} else {
				wrong_callback();
				$('#'+id).select();
			}
		};
		$('#'+id).focus().keyup(function(e) { 
			var code = e.which; 
			if(code==13){ e.preventDefault(); submit_answer(); } 
			if( code !== 9 ) {
				var max = $(this).attr("maxlength"), 
				index = $(this).attr("data-index"), 
				count = $(this).val().length;
				
				if( count == max ) {
					$('.input-index-' + (parseInt(index) + 1)).focus().select();
				}
			}
			
		}).click(function(){
			$(this).select();
		});
		$('#submit-answer').click(submit_answer);
	},
	multiple_input_box : function(cls, $, jwp, data, correct_callback, wrong_callback) {
		var submit_answer = function(){
			var index = [], n=0, correct=true;
			$('.'+cls).each(function(i){
				var res = QuizControllers.inArray($(this).val(), data.answer[i]);
				if( res===false ) {
					correct = false;
					return false;
				}
			});
			
			if( correct ) { //index.toString() == data.answer.toString() ) {
				correct_callback();
			} else {
				wrong_callback();
			}
			
		};
		$('.autofocus').focus();
		$('#submit-answer').click(submit_answer);
		$(".answer-box").keyup(function(e) { 
			var code = e.which; 
			if(code==13){ e.preventDefault(); submit_answer(); } 
			if( code !== 9 ) {
				var max = $(this).attr("maxlength"), 
				index = $(this).attr("data-index"), 
				count = $(this).val().length;
				
				if( count == max ) {
					$('.input-index-' + (parseInt(index) + 1)).focus().select();
				}
			}
			
		}).click(function(){
			$(this).select();
		});
		
	},
	multiple_input_box2 : function(cls, $, jwp, data, correct_callback, wrong_callback) {
		var submit_answer = function(){
			var index = [], n=0, correct=true;
			$('.'+cls).each(function(i){
				var res = QuizControllers.inArray($(this).val(), data.answer[i]);
				if( res===false ) {
					correct = false;
					return false;
				}
			});
			
			if( correct ) { //index.toString() == data.answer.toString() ) {
				correct_callback();
			} else {
				wrong_callback();
			}
			
		};
		$('.autofocus').focus();
		$('#submit-answer').click(submit_answer);
		$(".answer-box").keyup(function(e) { 
			var code = e.which; 
			if(code==13){ e.preventDefault(); submit_answer(); } 
			if( code !== 9 ) {
				var max = $(this).attr("maxlength"), 
				index = $(this).attr("data-index"), 
				count = $(this).val().length;
				
				if( count == max ) {
					$('.input-index-' + (parseInt(index) + 1)).focus().select();
				}
			}
			
		}).click(function(){
			$(this).select();
		});
	},
	interchangeable_input_box : function(cls, $, jwp, data, correct_callback, wrong_callback) {
		var container = [];
				for( var i in data.answer) {
					container.push(data.answer[i]);
				}
		var submit_answer = function() {
			var index = [], n=0, correct=true;
				$('.'+cls).each(function(i){
					var indx = QuizControllers.findIndex($(this).val(), container);
					if( indx != undefined ) {
						container.splice(indx, 1);
					} else {
						correct = false;
						return false;
					}
				});
			if( correct ) { 
				correct_callback();
			} else {
				wrong_callback();
			}
				container = [];
				for( var i in data.answer) {
					container.push(data.answer[i]);
				}
		};
		$('.autofocus').focus();
		$('#submit-answer').click(submit_answer);
		$(".answer-box").keyup(function(e) { 
			var code = e.which; 
			if(code==13){ e.preventDefault(); submit_answer(); } 
			if( code !== 9 ) {
				var max = $(this).attr("maxlength"), 
				index = $(this).attr("data-index"), 
				count = $(this).val().length;
				
				if( count == max ) {
					$('.input-index-' + (parseInt(index) + 1)).focus().select();
				}
			}
			
		}).click(function(){
			$(this).select();
		});
	},
	inArray : function(needle, haystack) {
		if( typeof haystack != 'undefined' ) {
			if( typeof haystack == 'string' && (QuizControllers.sanitize(haystack) == QuizControllers.sanitize( needle ) )) {
				return true;
			} else if( typeof haystack == 'number' && (QuizControllers.sanitize(haystack) == QuizControllers.sanitize( needle ) )) {
				return true;
			} else if( typeof haystack == 'object' ) {
				for( i in haystack ) {
					var yes =  QuizControllers.inArray(needle, haystack[i]);
					if(yes === true) {
						return true;
						break;
					}
				}
			} 
			return false;
		}
		return false;
	},
	findIndex : function(needle, haystack) {
		if( typeof haystack == 'object' ) {
			for (var i in haystack) {
				if( QuizControllers.sanitize(haystack[i]) == needle ) {
					return i;
				}
			} 
		}
	},
	sanitize : function(text) {
		if (text != undefined) {
			return text.toString().toLowerCase().trim().replace(/\s+/g, '').replace(/,/g, '');
		}
	},
};
