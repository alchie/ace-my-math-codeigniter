var G7C1L1 = {
	title : 'A Plan for Problem Solving',
	duration : '18:01',
	data : [
		{	quiz_number : 1,
			time : '00:29',
			html : function(controllers) {
				var html = '<h2>Which step do you think comes first?</h2>';
				var choices = [
					'A. Solve', 
					'B. Check',
					'C. Understand',
					'D. Plan'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. Understand',
			question : 'Which step do you think comes first?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Solve<br>B. Check<br>C. Understand<br>D. Plan";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2,
			time : '00:52',
			html : function(controllers) {
				var html = '<h2>How about the second step?</h2>';
				var choices = [
					'A. Understand', 
					'B. Plan',
					'C. Solve',
					'D. Check'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. Plan',
			question : 'How about the second step?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Understand<br>B. Plan<br>C. Solve<br>D. Check";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '01:25',
			html : function(controllers) {
				var html = '<h2>Which of the following is the correct order of the four step plan?</h2>';
				var choices = [
					'A. Plan, Solve, Check and Understand', 
					'B. Check, Solve, Understand and Plan',
					'C. Understand, Plan, Solve and Check',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. Understand, Plan, Solve and Check',
			question : 'Which of the following is the correct order of the four step plan?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Plan, Solve, Check and Understand<br>\
B. Check, Solve, Understand and Plan<br>\
C. Understand, Plan, Solve and Check";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '02:42',
			html : function(controllers) {
				var html = '<img src="/assets/images/lessons/G7C1L1_table1.png" style="float:right;" width="60%">';
				html += '<h2>What facts do you know?</h2>';
				var choices = [
					'A. Number of pounds of rice', 
					'B. Number of pounds of wheat',
					'C. All of the above',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. All of the above',
			question : 'What facts do you know?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L1_table1.png"></p>';
				html += "A. Number of pounds of rice<br>\
B. Number of pounds of wheat<br>\
C. All of the above";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5,
			time : '03:10',
			html : function(controllers) {
				var html = '<strong>"I have a friend named Vincent. He has a one acre farm with different crops. Vincent  wants to know how many more pounds of rice is produced than wheat."</strong><h2>What do you need to find?</h2>';
				var choices = [
					'A. how many more pounds of rice is produced than wheat', 
					'B. how many more pounds of wheat is produced than rice',
					'C. none of the above',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. how many more pounds of rice is produced than wheat',
			question : 'What do you need to find?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>"I have a friend named Vincent. He has a one acre farm with different crops. Vincent  wants to know how many more pounds of rice is produced than wheat."</p>';
				html += "A. how many more pounds of rice is produced than wheat<br>\
B. how many more pounds of wheat is produced than rice<br>\
C. none of the above";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 6,
			time : '03:49',
			html : function(controllers) {
				var html = '<img src="/assets/images/lessons/G7C1L1_table1.png" style="float:right;" width="60%">';
				html += '<h2>How many pounds of wheat can one acre of land produce?</h2>';
				var choices = [
					'A. 725 lbs.', 
					'B. 2500 lbs.',
					'C. 5500 lbs.',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 2500 lbs.',
			question : 'How many pounds of wheat can one acre of land produce?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L1_table1.png"></p>';
				html += "A. 725 lbs.<br>B. 2500 lbs.<br>C. 5500 lbs.";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time : '04:20',
			html : function(controllers) {
				var html = '<img src="/assets/images/lessons/G7C1L1_table1.png" style="float:right;" width="60%">';
				html += '<h2>What is the amount of rice produced by one acre land?</h2>';
				var choices = [
					'A. 5000 lbs.', 
					'B. 5500 lbs.',
					'C. 5300 lbs.',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 5500 lbs.',
			question : 'What is the amount of rice produced by one acre land?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L1_table1.png"></p>';
				html += "A. 5000 lbs.<br>B. 5500 lbs.<br>C. 5300 lbs.";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '04:51',
			html : function(controllers) {
				var html = '<img src="/assets/images/lessons/G7C1L1_table1.png" style="float:right;" width="60%">';
				html += '<h2>How many more pounds of rice can one acre produce than wheat in one season?</h2>';
				var choices = [
					'A. 2800 lbs.', 
					'B. 3000 lbs.',
					'C. 3200 lbs.',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 3000 lbs.',
			question : 'How many more pounds of rice can one acre produce than wheat in one season?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L1_table1.png"></p>';
				html += "A. 2800 lbs.<br>B. 3000 lbs.<br>C. 3200 lbs.";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '06:15',
			html : function(controllers) {
				var html = '<h2>How many diagonals does a triangle have?</h2>';
				html += '<img src="/assets/images/lessons/G7C1L1_triangle.png" style="float:right;margin-right:30px;">';
				var choices = [
					'A. 1', 
					'B. 0',
					'C. 2',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 0',
			question : 'How many diagonals does a triangle have?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L1_triangle.png"></p>';
				html += "A. 1<br>B. 0<br>C. 2";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '06:40',
			html : function(controllers) {
				var html = '<h2>How about the diagonals of a square?</h2>';
				html += '<img src="/assets/images/lessons/G7C1L1_square.png" style="float:right;margin-right:30px;">';
				var choices = [
					'A. 3', 
					'B. 2',
					'C. 1',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 2',
			question : 'How about the diagonals of a square?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L1_square.png"></p>';
				html += "A. 3<br>B. 2<br>C. 1";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11,
			time : '07:06',
			html : function(controllers) {
				var html = '<h2>How many diagonals does a pentagon have?</h2>';
				html += '<img src="/assets/images/lessons/G7C1L1_pentagon.png" style="float:right;margin-right:30px;">';
				var choices = [
					'A. 3', 
					'B. 4',
					'C. 5',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 5',
			question : 'How many diagonals does a pentagon have?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L1_pentagon.png"></p>';
				html += "A. 3<br>B. 4<br>C. 5";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 12,
			time : '07:53',
			html : function(controllers) {
				var html = '<h2>What value should be added to find the diagonals of a hexagon?</h2>';
				html += '<img src="/assets/images/lessons/G7C1L1_diagonal_table1.png" style="float:right;margin-right:30px;width:400px">';
				var choices = [
					'A. 4', 
					'B. 5',
					'C. 6',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 4',
			question : 'What value should be added to find the diagonals of a hexagon?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L1_diagonal_table1.png" width="100%"></p>';
				html += "A. 4<br>B. 5<br>C. 6";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 13,
			time : '08:25',
			html : function(controllers) {
				var html = '<h2>How many diagonals does a hexagon (six sided figure) have?</h2>';
				html += '<img src="/assets/images/lessons/G7C1L1_diagonal_table2.png" style="float:right;margin-right:30px;width:400px">';
				var choices = [
					'A. 7 diagonals', 
					'B. 8 diagonals',
					'C. 9 diagonals',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 9 diagonals',
			question : 'How many diagonals does a hexagon (six sided figure) have?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L1_diagonal_table2.png" width="100%"></p>';
				html += "A. 7 diagonals<br>B. 8 diagonals<br>C. 9 diagonals";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 14,
			time :  '08:50',
			html : function(controllers) {
				var html = '<h2>From a hexagon, what value must be added to find the diagonals of a heptagon (seven sided figure)?</h2>';
				html += '<img src="/assets/images/lessons/G7C1L1_diagonal_table3.png" style="float:right;margin-right:30px;width:400px">';
				var choices = [
					'A. 4', 
					'B. 5',
					'C. 6',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 5',
			question : 'From a hexagon, what value must be added to find the diagonals of a heptagon (seven sided figure)?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L1_diagonal_table3.png" width="100%"></p>';
				html += "A. 4<br>B. 5<br>C. 6";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 15,
			time : '09:21',
			html : function(controllers) {
				var html = '<h2>How many diagonals does a heptagon (seven sided figure) have?</h2>';
				html += '<img src="/assets/images/lessons/G7C1L1_diagonal_table4.png" style="float:right;margin-right:30px;width:400px">';
				var choices = [
					'A. 12', 
					'B. 13',
					'C. 14',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 14',
			question : 'How many diagonals does a heptagon (seven sided figure) have?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L1_diagonal_table4.png" width="100%"></p>';
				html += "A. 12<br>B. 13<br>C. 14";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 16,
			time : '10:42',
			html : function(controllers) {
				var html = '<strong>Jason bought 10 CDs at a sale. The first CD purchased costs $12, and each additional costs $6. What was the total cost before tax?</strong>';
				html += '<h2>What are the facts given in the problem?</h2>';
				var choices = [
					'A. cost of the first CD', 
					'B. cost of the additional CD\'s',
					'C. Total number of CD\'s',
					'D. All of the above'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 3,
			answer_text : 'D. All of the above',
			question : 'What are the facts given in the problem?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>Jason bought 10 CDs at a sale. The first CD purchased costs $12, and each additional costs $6. What was the total cost before tax?</p>';
				html += "A. cost of the first CD<br>\
B. cost of the additional CD\'s<br>\
C. Total number of CD\'s<br>\
D. All of the above";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 17,
			time : '11:05',
			html : function(controllers) {
				var html = '<strong>Jason bought 10 CDs at a sale. The first CD purchased costs $12, and each additional costs $6. What was the total cost before tax?</strong>';
				html += '<h2>What do we need to find out?</h2>';
				var choices = [
					'A. Cost of the first CD', 
					'B. Cost of the additional CD\'s',
					'C. Total cost of CD\'s',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. Total cost of CD\'s',
			question : 'What do we need to find out?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>Jason bought 10 CDs at a sale. The first CD purchased costs $12, and each additional costs $6. What was the total cost before tax?</p>';
				html += "A. Cost of the first CD<br>\
B. Cost of the additional CD\'s<br>\
C. Total cost of CD\'s";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 18,
			time : '11:35',
			html : function(controllers) {
				var html = '<strong>Jason bought 10 CDs at a sale. The first CD purchased costs $12, and each additional costs $6. What was the total cost before tax?</strong>';
				html += '<h2>What is missing before we can find the total cost of additional CD\'s?</h2>';
				var choices = [
					'A. Number of additional CD\'s', 
					'B. Number of additional CD\'s and First CD',
					'C. Cost of the first CD',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. Number of additional CD\'s',
			question : 'What is missing before we can find the total cost of additional CD\'s?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>Jason bought 10 CDs at a sale. The first CD purchased costs $12, and each additional costs $6. What was the total cost before tax?</p>';
				html += "A. Number of additional CD\'s<br>\
B. Number of additional CD\'s and First CD<br>\
C. Cost of the first CD";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 19,
			time : '11:58',
			html : function(controllers) {
				var html = '<strong>Jason bought 10 CDs at a sale. The first CD purchased costs $12, and each additional costs $6. What was the total cost before tax?</strong>';
				html += '<h2>How many additional CD\'s are there?</h2>';
				var choices = [
					'A. 7', 
					'B. 8',
					'C. 9',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 9',
			question : 'How many additional CD\'s are there?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>Jason bought 10 CDs at a sale. The first CD purchased costs $12, and each additional costs $6. What was the total cost before tax?</p>';
				html += "A. 7<br>B. 8<br>C. 9";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 20,
			time : '12:39',
			html : function(controllers) {
				var html = '<strong>Jason bought 10 CDs at a sale. The first CD purchased costs $12, and each additional costs $6. What was the total cost before tax?</strong>';
				html += '<h2>Which of the following is the correct way to find the total cost of additional CD\'s?</h2>';
				var choices = [
					'A. $6 x 9 CD\'s', 
					'B. $9 x 10 CD\'s',
					'C. $12 x 10 CD\'s',
					'D. $6 x 10 CD\'s',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. $6 x 9 CD\'s',
			question : 'Which of the following is the correct way to find the total cost of additional CD\'s?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>Jason bought 10 CDs at a sale. The first CD purchased costs $12, and each additional costs $6. What was the total cost before tax?</p>';
				html += "A. $6 x 9 CD\'s<br>\
B. $9 x 10 CD\'s<br>\
C. $12 x 10 CD\'s<br>\
D. $6 x 10 CD\'s";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 21,
			time : '13:12',
			html : function(controllers) {
				var html = '<strong>Jason bought 10 CDs at a sale. The first CD purchased costs $12, and each additional costs $6. What was the total cost before tax?</strong>';
				html += '<h2>What is the total cost of additional CD\'s?</h2>';
				var choices = [
					'A. $53', 
					'B. $54',
					'C. $55',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. $54',
			question : 'What is the total cost of additional CD\'s?',
			type : 'multiple-choice',
			details : function() {
				var html = '<strong>Jason bought 10 CDs at a sale. The first CD purchased costs $12, and each additional costs $6. What was the total cost before tax?</strong>';
				html += "A. $53<br>B. $54<br>C. $55";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 22,
			time : '13:53',
			html : function(controllers) {
				var html = '<strong>Jason bought 10 CDs at a sale. The first CD purchased costs $12, and each additional costs $6. What was the total cost before tax?</strong>';
				html += '<h2>What is the correct way to find the total cost of CD\'s?</h2>';
				var choices = [
					'A. $54 + $12', 
					'B. $54 + $10',
					'C. $54 + $6',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. $54 + $12',
			question : 'What is the correct way to find the total cost of CD\'s?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>Jason bought 10 CDs at a sale. The first CD purchased costs $12, and each additional costs $6. What was the total cost before tax?</p>';
				html += "A. $54 + $12<br>B. $54 + $10<br>C. $54 + $6";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 23,
			time : '14:22',
			html : function(controllers) {
				var html = '<strong>Jason bought 10 CDs at a sale. The first CD purchased costs $12, and each additional costs $6. What was the total cost before tax?</strong>';
				html += '<h2>What is the total cost of the purchased CD\'s?</h2>';
				var choices = [
					'A. $65', 
					'B. $66',
					'C. $67',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. $66',
			question : 'What is the total cost of the purchased CD\'s?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>Jason bought 10 CDs at a sale. The first CD purchased costs $12, and each additional costs $6. What was the total cost before tax?</p>';
				html += "A. $65<br>B. $66<br>C. $67";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 24,
			time : '15:06',
			html : function(controllers) {
				var html = '<h2>What is the best way to check your answer?</h2>';
				var choices = [
					'A. Adding the cost of each CD', 
					'B. Adding the cost of each CD and multiply by 10',
					'C. Multiplying the cost of First CD by 10',
					'D. Multiplying the cost of additional CD\'s by 10'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. Adding the cost of each CD',
			question : 'What is the best way to check your answer?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Adding the cost of each CD<br>\
B. Adding the cost of each CD and multiply by 10<br>\
C. Multiplying the cost of First CD by 10<br>\
D. Multiplying the cost of additional CD\'s by 10";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 25,
			time : '15:35',
			html : function(controllers) {
				var html = '<strong>Jason bought 10 CDs at a sale. The first CD purchased costs $12, and each additional costs $6. What was the total cost before tax?</strong>';
				html += '<h2>What is the cost of First CD?</h2>';
				var choices = [
					'A. $10', 
					'B. $6',
					'C. $12',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. $12',
			question : 'What is the cost of First CD?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>Jason bought 10 CDs at a sale. The first CD purchased costs $12, and each additional costs $6. What was the total cost before tax?</p>';
				html += "A. $10<br>B. $6<br>C. $12";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 26,
			time : '16:02',
			html : function(controllers) {
				var html = '<strong>Jason bought 10 CDs at a sale. The first CD purchased costs $12, and each additional costs $6. What was the total cost before tax?</strong>';
				html += '<h2>What about the cost of each additional CD?</h2>';
				var choices = [
					'A. $6', 
					'B. $10',
					'C. $12',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. $6',
			question : 'What about the cost of each additional CD?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>Jason bought 10 CDs at a sale. The first CD purchased costs $12, and each additional costs $6. What was the total cost before tax?</p>';
				html += "A. $6<br>B. $10<br>C. $12";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 27,
			time : '16:20',
			html : function(controllers) {
				var html = '<strong>Jason bought 10 CDs at a sale. The first CD purchased costs $12, and each additional costs $6. What was the total cost before tax?</strong>';
				html += '<h2>Fill in the table to check if you answer coincides</h2>';
				html += '<table class="table table-bordered">\
				<thead>\
					<tr align="center" style="background-color:rgb(131, 207, 250);">\
						<td>1st CD</td>\
						<td>2nd CD</td>\
						<td>3rd CD</td>\
						<td>4th CD</td>\
						<td>5th CD</td>\
						<td>6th CD</td>\
						<td>7th CD</td>\
						<td>8th CD</td>\
						<td>9th CD</td>\
						<td>10th CD</td>\
						<td>Total</td>\
					</tr>\
				</thead>\
				<tbody>\
					<tr align="center">\
						<td><input data-index="0" class="input-index-0 answer-box autofocus" type="text" style="width:25px;padding:5px 2px;text-align:center" /></td>\
						<td><input data-index="1" class="input-index-1 answer-box" type="text" style="width:25px;padding:5px 2px;text-align:center" /</td>\
						<td><input data-index="2" class="input-index-2 answer-box" type="text" style="width:25px;padding:5px 2px;text-align:center" /</td>\
						<td><input data-index="3" class="input-index-3 answer-box" type="text" style="width:25px;padding:5px 2px;text-align:center" /</td>\
						<td><input data-index="4" class="input-index-4 answer-box" type="text" style="width:25px;padding:5px 2px;text-align:center" /</td>\
						<td><input data-index="5" class="input-index-5 answer-box" type="text" style="width:25px;padding:5px 2px;text-align:center" /</td>\
						<td><input data-index="6" class="input-index-6 answer-box" type="text" style="width:25px;padding:5px 2px;text-align:center" /</td>\
						<td><input data-index="7" class="input-index-7 answer-box" type="text" style="width:25px;padding:5px 2px;text-align:center" /</td>\
						<td><input data-index="8" class="input-index-8 answer-box" type="text" style="width:25px;padding:5px 2px;text-align:center" /</td>\
						<td><input data-index="9" class="input-index-9 answer-box" type="text" style="width:25px;padding:5px 2px;text-align:center" /</td>\
						<td><input data-index="10" class="input-index-10 answer-box" type="text" style="width:25px;padding:5px 2px;text-align:center" /</td>\
					</tr>\
				</tbody>\
				</table>';
				html += '<br><p><button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button></p>';
				return html;
			},
			answer : [12,6,6,6,6,6,6,6,6,6,66],
			answer_text : '12,6,6,6,6,6,6,6,6,6 and 66',
			question : 'Fill in the table to check if you answer coincides',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>Jason bought 10 CDs at a sale. The first CD purchased costs $12, and each additional costs $6. What was the total cost before tax?</p>';
				html += "1st CD = ____<br>\
2nd CD = ____<br>\
3rd CD = ____<br>\
4th CD = ____<br>\
5th CD = ____<br>\
6th CD = ____<br>\
7th CD = ____<br>\
8th CD = ____<br>\
9th CD = ____<br>\
10th CD = ____<br>\
Total = ____";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				var check_answers = function() {
						var all_correct = true, all_filled = true;
						$('.answer-box').each(function() {
							var index = $(this).attr("data-index"), user_answer = $(this).val();
							if( user_answer.length > 0 ) {
								if( controllers.sanitize(user_answer) == controllers.sanitize(data.answer[index]) ) {
									$(this).parent().css('background-color', 'green');
								} else {
									all_correct = false;
									$(this).parent().css('background-color', 'red');
								}
							} else {
								all_correct	 = false;
								all_filled = false;
							}
						});
						if( all_correct ) {
							correct_callback();
						} else {
							if( all_filled ) {
								wrong_callback();
							}
						}
					};
				$('#submit-answer').click( check_answers );
				$('.autofocus').focus();
				$(".answer-box").keyup(function(e) { 
					var code = e.which, 
						max = $(this).attr("maxlength"), 
						index = $(this).attr("data-index"), 
						count = $(this).val().length;
					
					if(code==13){ 
						e.preventDefault(); 
						check_answers(); 
						if( index == 10 ) { index = -1; }
						$( '.input-index-' + (parseInt(index) + 1 )).focus().select();
					} 
					if(code==9){ 
						e.preventDefault(); 
						check_answers(); 
					} 
				}).click(function(){
					$(this).select();
				});
			},
		},
		{	quiz_number : 28,
			time : '17:13',
			html : function(controllers) {
				var html = '<h2>What is the total cost of the CD\'s?</h2>';
				var choices = [
					'A. $66', 
					'B. $67',
					'C. $68',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. $66',
			question : 'What is the total cost of the CD\'s?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. $66<br>B. $67<br>C. $68";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
			end_session : 1,
		},
	],
};
