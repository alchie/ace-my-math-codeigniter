var G7C1L7 = {
	title : 'Algebra: Equations',
	duration : '20:10',
	data : [
		{	quiz_number : 1,
			time : '00:38',
			html : function(controllers) {
				var html = '<h2>In a rectangle, if L is for length and W is for width, what is the equation for the perimeter of the rectangle?</h2>';
				var choices = [
					'A. L + W', 
					'B. 2L + 2W',
					'C. 2L x 2L',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 2L + 2W',
			question : 'In a rectangle, if L is for length and W is for width, what is the equation for the perimeter of the rectangle?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. L + W<br>B. 2L + 2W<br>C. 2L x 2L";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2,
			time : '01:11',
			html : function(controllers) {
				var html = '<h2>What are the variables in the equation 2L + 2W?</h2>';
				var choices = [
					'A. 2 and L', 
					'B. 2, L, and W',
					'C. L and W',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. L and W',
			question : 'What are the variables in the equation 2L + 2W?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 2 and L<br>B. 2, L, and W<br>C. L and W";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '01:57',
			html : function(controllers) {
				var html = '<h2>If L = 5 and W = 2, what is the expression in finding the perimeter of a rectangle?</h2>';
				var choices = [
					'A. 2(5) + 2(2)', 
					'B. 2(5) x 2(2)',
					'C. 2(5) - 2(2)',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 2(5) + 2(2)',
			question : 'If L = 5 and W = 2, what is the expression in finding the perimeter of a rectangle?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 2(5) + 2(2)<br>B. 2(5) x 2(2)<br>C. 2(5) - 2(2)";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '02:33',
			html : function(controllers) {
				var html = '<h2>What is the perimeter of the rectangle? L = 5 and W = 2</h2>';
				var choices = [
					'A. 12', 
					'B. 14',
					'C. 16',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 14',
			question : 'What is the perimeter of the rectangle?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>L = 5 and W = 2</p>A. 12<br>B. 14<br>C. 16";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5,
			time : '04:07',
			html : function(controllers) {
				var html = '<h4>Suppose each team played 34 games. How many losses did each team have?</h4>';
				html += '<style>\
				#table-414 td {\
					padding:2px!important;\
				}\
				#table-414 .answer-box {\
					width:50px;\
					text-align:center;\
				}\
				</style>\
				<table class="table" id="table-414"> \
				<thead>\
					<tr>\
						<th>Team</th>\
						<th>Wins</th>\
						<th>Losses</th>\
					</tr>\
				</thead>\
				<tbody>\
					<tr>\
						<td>North Carolina</td>\
						<td>28</td>\
						<td><input class="answer-box autofocus"></td>\
					</tr>\
					<tr>\
						<td>South Beach</td>\
						<td>13</td>\
						<td><input class="answer-box"></td>\
					</tr>\
					<tr>\
						<td>Kentucky</td>\
						<td>12</td>\
						<td><input class="answer-box"></td>\
					</tr>\
					<tr>\
						<td>Ohio</td>\
						<td>20</td>\
						<td><input class="answer-box"></td>\
					</tr>\
					<tr>\
						<td>Kent State</td>\
						<td>16</td>\
						<td><input class="answer-box"></td>\
					</tr>\
					<tr>\
						<td>Duke</td>\
						<td>14</td>\
						<td><input class="answer-box"></td>\
					</tr>\
				</tbody>\
				</table>';
				html += '<p><button class="btn btn-default btn-sm" id="submit-answer">Submit</button></p>';
				return html;
			},
			answer : [6,21,22,14,18,20],
			answer_text : '6,21,22,14,18,20',
			question : 'How many losses did each team have?',
			type : 'input-box',
			details : function() {
				var html = "<p>Suppose each team played 34 games. </p>";
				html += '<style>\
				#table-414 td, #table-414 th {\
					padding:2px!important;\
					text-align:center;\
				}\
				#table-414 .answer-box {\
					width:50px;\
					text-align:center;\
				}\
				</style>\
				<table class="table" id="table-414"> \
				<thead>\
					<tr>\
						<th>Team</th>\
						<th>Wins</th>\
						<th>Losses</th>\
					</tr>\
				</thead>\
				<tbody>\
					<tr>\
						<td>North Carolina</td>\
						<td>28</td>\
						<td>___</td>\
					</tr>\
					<tr>\
						<td>South Beach</td>\
						<td>13</td>\
						<td>___</td>\
					</tr>\
					<tr>\
						<td>Kentucky</td>\
						<td>12</td>\
						<td>___</td>\
					</tr>\
					<tr>\
						<td>Ohio</td>\
						<td>20</td>\
						<td>___</td>\
					</tr>\
					<tr>\
						<td>Kent State</td>\
						<td>16</td>\
						<td>___</td>\
					</tr>\
					<tr>\
						<td>Duke</td>\
						<td>14</td>\
						<td>___</td>\
					</tr>\
				</tbody>\
				</table>';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},		
		{	quiz_number : 6,
			time : '05:02',
			html : function(controllers) {
				var html = '<h2>Which of the following rule describes how you found the number of losses?</h2>';
				var choices = [
					'A. number of losses = total games - total wins', 
					'B. number of losses = total games + total wins',
					'C. number of losses = total games x total wins',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. number of losses = total games - total wins',
			question : 'Which of the following rule describes how you found the number of losses?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. number of losses = total games - total wins<br>\
B. number of losses = total games + total wins<br>\
C. number of losses = total games x total wins";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time : '05:51',
			html : function(controllers) {
				var html = '<h3>W represents number of wins and L represents the number of losses</h3>\
				<h2>Which of the following expressions is correct in finding the total games?</h2>';
				var choices = [
					'A. W - L = total games', 
					'B. W + L = total games',
					'C. W x L = total games',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. W + L = total games',
			question : 'Which of the following expressions is correct in finding the total games?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. W - L = total games<br>\
B. W + L = total games<br>\
C. W x L = total games";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '06:22',
			html : function(controllers) {
				var html = '<h2>If W = 20 and L = 14, what is the total number of games?</h2>';
				var choices = [
					'A. 34', 
					'B. 35',
					'C. 36',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 34',
			question : 'If W = 20 and L = 14, what is the total number of games?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 34<br>B. 35<br>C. 36";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '07:13',
			html : function(controllers) {
				var html = '<h2>18 = 4 + t<br>What is the variable in the equation?</h2>';
				var choices = [
					'A. 18', 
					'B. 4',
					'C. t',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. t',
			question : 'What is the variable in the equation?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>18 = 4 + t</p>A. 18<br>B. 4<br>C. t";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '07:42',
			html : function(controllers) {
				var html = '<h2>What would be the value of t to make the expression true? 18 = 4 + t</h2>';
				var choices = [
					'A. 12', 
					'B. 14',
					'C. 16',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 14',
			question : 'What would be the value of t to make the expression true?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>18 = 4 + t</p>\
A. 12<br>B. 14<br>C. 16";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11,
			time : '08:19',
			html : function(controllers) {
				var html = '<h3>Suppose we have the equation 18 = 3 x 6.</h3><h2>Is the equation true?</h2>';
				var choices = [
					'A. yes', 
					'B. no',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. yes',
			question : 'Is the equation true?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>Suppose we have the equation 18 = 3 x 6.</p>A. yes<br>B. no";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 12,
			time : '09:06',
			html : function(controllers) {
				var html = '<h2>Which of the following equations is correct if we want to retain 6 on the right side? 18 = 3 x 6</h2>';
				var choices = [
					'A. 18 / 3 = (3x6) / 3', 
					'B. 18 x 3 = (3x6) x 3',
					'C. 18 + 3 = (3x6) + 3',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 18 / 3 = (3x6) / 3',
			question : 'Which of the following equations is correct if we want to retain 6 on the right side?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>18 = 3 x 6</p>\
A. 18 / 3 = (3x6) / 3<br>\
B. 18 x 3 = (3x6) x 3<br>\
C. 18 + 3 = (3x6) + 3";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 13,
			time : '09:26',
			html : function(controllers) {
				var html = '<h2>Write the equation after dividing both sides by 3. <br>18 = 3 x 6</h2>';
				html += '<p><input type="text" class="answer-box autofocus" style="width:50px;text-align:center" maxlength="2"> = <input type="text" class="answer-box" style="width:50px;text-align:center" maxlength="2"></p>'
				html += '<p><button class="btn btn-default btn-sm" id="submit-answer">Submit</button></p>';
				return html;
			},
			answer : [6,6],
			answer_text : '6 = 6',
			question : 'Write the equation after dividing both sides by 3. ',
			type : 'input-box',
			details : function() {
				var html = "<p>18 = 3 x 6</p> ___ = ___ ";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},	
		{	quiz_number : 14,
			time : '10:18',
			html : function(controllers) {
				var html = '<h2>We have the equation 18 = 4 + t, what should we do to remove 4 on the right side of the equation?</h2>';
				var choices = [
					'A. add 4', 
					'B. subtract 4',
					'C. multiply by 4',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1 ,
			answer_text : 'B. subtract 4',
			question : 'We have the equation 18 = 4 + t, what should we do to remove 4 on the right side of the equation?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. add 4<br>B. subtract 4<br>C. multiply by 4";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 15,
			time : '11:09',
			html : function(controllers) {
				var html = '<h2>18 __________ = t <br> Which of the following is correct?</h2>';
				var choices = [
					'A. 18 - 4 = t', 
					'B. 18 + 4 = t',
					'C. 18 x 4 = t',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 18 - 4 = t',
			question : 'Which of the following is correct? ',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>18 = 4 + t</p><p>18 __________ = t</p>\
A. 18 - 4 = t<br>B. 18 + 4 = t<br>C. 18 x 4 = t";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 16,
			time : '11:52',
			html : function(controllers) {
				var html = '<h2>Evaluate 18 - 4 = 4 + t - 4 <br> Which of the following is correct?</h2>';
				var choices = [
					'A. 14 = t', 
					'B. 14 = 4 + t',
					'C. 14 = 4 - t',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 14 = t',
			question : 'Which of the following is correct?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>Evaluate 18 - 4 = 4 + t - 4</p>A. 14 = t<br>\
B. 14 = 4 + t<br>\
C. 14 = 4 - t";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 17,
			time : '13:28',
			html : function(controllers) {
				var html = '<h2>Solve 16 = u/4<br>What operation should be used on both sides in order to retain u on the right side?</h2>';
				var choices = [
					'A. divide both sides by 4', 
					'B. multiply both sides by 4',
					'C. add 4 to both sides',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. multiply both sides by 4',
			question : 'What operation should be used on both sides in order to retain u on the right side?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>Solve 16 = u/4</p>A. divide both sides by 4<br>\
B. multiply both sides by 4<br>C. add 4 to both sides";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 18,
			time : '14:10',
			html : function(controllers) {
				var html = '<h2>What is the product of 4 and 16?<br> 4(16) = 4(u/4)</h2>';
				var choices = [
					'A. 62', 
					'B. 64',
					'C. 66',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 64',
			question : 'What is the product of 4 and 16?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p> 4(16) = 4(u/4)</p>A. 62<br>B. 64<br>C. 66";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 19,
			time : '14:41',
			html : function(controllers) {
				var html = '<h2>What is the product of 4 and u/4?</h2>';
				var choices = [
					'A. 4u', 
					'B. u/16',
					'C. u',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. u',
			question : 'What is the product of 4 and u/4?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 4u<br>B. u/16<br>C. u";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 20,
			time : '15:27',
			html : function(controllers) {
				var html = '<h2>Since we have already multiplied both sides by 4, which of the following equation is correct? 16 = u/4</h2>';
				var choices = [
					'A. 64 = u', 
					'B. 64 = u/4',
					'C. 64 = 4u',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 64 = u',
			question : 'Since we have already multiplied both sides by 4, which of the following equation is correct? 16 = u/4?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 64 = u<br>B. 64 = u/4<br>C. 64 = 4u";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 21,
			time : '16:03',
			html : function(controllers) {
				var html = '<h2>Solve 16 = u/4<br>What would be the value of u to make the equation correct?</h2>';
				var choices = [
					'A. 60', 
					'B. 62',
					'C. 64',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 64',
			question : 'What would be the value of u to make the equation correct?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>Solve 16 = u/4</p>A. 60<br>B. 62<br>C. 64";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 22,
			time : '16:46',
			html : function(controllers) {
				var html = '<h2>Solve 3d=99<br>What operation should be used to retain d on the left side of the equation?</h2>';
				var choices = [
					'A. multiply both sides by 3', 
					'B. add 3 to both sides',
					'C. divide both sides by 3',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. divide both sides by 3',
			question : 'What operation should be used to retain d on the left side of the equation?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>Solve 3d=99</p>A. multiply both sides by 3<br>\
B. add 3 to both sides<br>C. divide both sides by 3";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 23,
			time : '17:06',
			html : function(controllers) {
				var html = '<h2>divide both sides by 3<br>3d/3 = 99/3</h2>';
				html += '<p><input type="text" class="answer-box autofocus" style="width:50px;text-align:center" maxlength="2"> = <input type="text" class="answer-box" style="width:50px;text-align:center" maxlength="2"></p>'
				html += '<p><button class="btn btn-default btn-sm" id="submit-answer">Submit</button></p>';
				return html;
			},
			answer : ['d',33],
			answer_text : 'd = 33',
			question : 'Divide both sides by 3',
			type : 'input-box',
			details : function() {
				var html = "<p>3d/3 = 99/3</p> ___ = ___ ";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 24,
			time : '17:46',
			html : function(controllers) {
				var html = '<h2>Solve 3d=99<br>What is the value of d to make the equation true?</h2>';
				var choices = [
					'A. 33', 
					'B. 34',
					'C. 35',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 33',
			question : 'What is the value of d to make the equation true?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>Solve 3d=99</p>A. 33<br>B. 34<br>C. 35";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 25,
			time : '18:34',
			html : function(controllers) {
				var html = '<h2>Solve 64 = n<sup>2</sup><br>Which of the following operation is used to retain variable n?</h2>';
				var choices = [
					'A. find the squares of both sides', 
					'B. find the square roots of both sides',
					'C. multiply both sides by 2',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. find the square roots of both sides',
			question : 'Which of the following operation is used to retain variable n?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>Solve 64 = n<sup>2</sup></p>\
A. find the squares of both sides<br>\
B. find the square roots of both sides<br>\
C. multiply both sides by 2";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 26,
			time : '19:04',
			html : function(controllers) {
				var html = '<h2>Find the square root of both sides.<br><br> &radic;<span style="position: absolute;width: 50px;border-top: 3px solid #000;padding-bottom: 10px;"></span><span style="">&nbsp;64&nbsp;</span> = &radic;<span style="position: absolute;width: 50px;border-top: 3px solid #000;padding-bottom: 10px;"></span>\
				<span>n<sup><small>2</small></sup>&nbsp;</span></h2>';
				html += '<p><input type="text" class="answer-box autofocus" style="width:50px;text-align:center" maxlength="2"> = <input type="text" class="answer-box" style="width:50px;text-align:center" maxlength="2"></p>'
				html += '<p><button class="btn btn-default btn-sm" id="submit-answer">Submit</button></p>';
				return html;
			},
			answer : [8,'n'],
			answer_text : '8 = n',
			question : 'Find the square root of both sides.',
			type : 'input-box',
			details : function() {
				var html = '<p>&radic;<span style="position: absolute;width: 20px;border-top: 1px solid #000;padding-bottom: 10px;"></span><span style="">&nbsp;64&nbsp;</span>\
				= &radic;<span style="position: absolute;width: 20px;border-top: 1px solid #000;padding-bottom: 10px;"></span>\
				<span>n<sup><small>2</small></sup>&nbsp;</span></p>';
				html += '___ = ___ ';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 27,
			time : '19:41',
			html : function(controllers) {
				var html = '<h2>Solve 64 = n<sup>2</sup><br>What is the value of n to make the equation true?</h2>';
				var choices = [
					'A. 6', 
					'B. 7',
					'C. 8',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 8',
			question : 'What is the value of n to make the equation true?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>Solve 64 = n<sup>2</sup></p>A. 6<br>B. 7<br>C. 8";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
			end_session : 1,
		},
	],
};
