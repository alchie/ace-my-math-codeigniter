var G3C1L1 = {
	title : "Place Value Through Thousands",
	duration : "12:55",
	data : [
		{	quiz_number : 1,
			time : '00:24',
			html : function(controllers) {
				var html = '<h2>In Grade 2, you learned PLACE VALUE up to?</h2>';
				var choices = [
					'A. 10', 
					'B. 100',
					'C. 1000',
					'D. 500'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 1000',
			question : 'In Grade 2, you learned PLACE VALUE up to?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 10<br>B. 100<br>C. 1000<br>D. 500";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2,
			time : '01:00',
			html : function(controllers) {
				var html = '<h2>What do you call a symbol or name that stands for a number, say for 325?</h2>';
				var choices = [
					'A. digit', 
					'B. numeral',
					'C. ones',
					'D. base'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. numeral',
			question : 'What do you call a symbol or name that stands for a number, say for 325?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. digit<br>B. numeral<br>C. ones<br>D. base";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '01:30',
			html : function(controllers) {
				var html = '<h2>How about a symbol used to make NUMERALS?</h2>';
				var choices = [
					'A. ones', 
					'B. tens',
					'C. hundreds',
					'D. digit'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 3,
			answer_text : 'D. digit',
			question : 'How about a symbol used to make NUMERALS?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. ones<br>B. tens<br>C. hundreds<br>D. digit";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '02:09',
			html : function(controllers) {
				var html = '<h2>What do we call the value of where the DIGIT is in the number, such as units, tens, hundreds?</h2>';
				var choices = [
					'A. Ones', 
					'B. Place Value',
					'C. Tens',
					'D. Hundreds'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. Place Value',
			question : 'What do we call the value of where the DIGIT is in the number, such as units, tens, hundreds?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Ones<br>B. Place Value<br>C. Tens<br>D. Hundreds";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5,
			time : '02:35',
			html : function(controllers) {
				var html = '<h2>So for number 738, what is the PLACE VALUE of 8?</h2>';
				var choices = [
					'A. ones', 
					'B. tens',
					'C. hundreds',
					'D. thousands'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. ones',
			question : 'So for number 738, what is the PLACE VALUE of 8?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. ones<br>B. tens<br>C. hundreds<br>D. thousands";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 6,
			time : '02:58',
			html : function(controllers) {
				var html = '<h2>How about the place value of 4 in the number 472?</h2>';
				var choices = [
					'A. ones', 
					'B. tens',
					'C. hundreds',
					'D. thousands'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. hundreds',
			question : 'How about the place value of 4 in the number 472?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. ones<br>B. tens<br>C. hundreds<br>D. thousands";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time : '04:14',
			html : function(controllers) {
				var html = '<h2>146 = 100 + 40 + 6, This form of RENAMING NUMBER 146 is what we call?</h2>';
				var choices = [
					'A. standard form', 
					'B. word form',
					'C. normal form',
					'D. expanded form'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 3,
			answer_text : 'D. expanded form',
			question : '146 = 100 + 40 + 6, This form of RENAMING NUMBER 146 is what we call?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. standard form<br>B. word form<br>C. normal form<br>D. expanded form";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '05:10',
			html : function(controllers) {
				var html = '<h2>What is the PLACE VALUE of 5 in the number 5, 960?</h2>';
				var choices = [
					'A. ones', 
					'B. tens',
					'C. hundreds',
					'D. thousands'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 3,
			answer_text : 'D. thousands',
			question : 'What is the PLACE VALUE of 5 in the number 5, 960?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. ones<br>B. tens<br>C. hundreds<br>D. thousands";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '06:13',
			html : function(controllers) {
				var html = '<h2>Thus if another place value is introduced, ?5960 <br>How many blocks do you think it is?</h2>';
				var choices = [
					'A. 10', 
					'B. 100',
					'C. 1 000',
					'D. 10 000'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 3,
			answer_text : 'D. 10 000',
			question : 'Thus if another place value is introduced, ?5960 <br>How many blocks do you think it is?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 10<br>B. 100<br>C. 1 000<br>D. 10 000";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '06:44',
			html : function(controllers) {
				var html = '<h2>Thus the place value of 7 on Anjela\'s highest score of 75, 960 is?</h2>';
				var choices = [
					'A. ones', 
					'B. tens',
					'C. ten thousands',
					'D. thousands'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. ten thousands',
			question : 'Thus the place value of 7 on Anjela\'s highest score of 75, 960 is?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. ones<br>B. tens<br>C. ten thousands<br>D. thousands";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11,
			time : '07:57',
			html : function(controllers) {
				var html = '<h2>How do you represent 5960?</h2>';
				var choices = [
					baseURL + 'assets/images/lessons/G3C1L1_7_57_a.png', 
					baseURL + 'assets/images/lessons/G3C1L1_7_57_b.png',
					baseURL + 'assets/images/lessons/G3C1L1_7_57_c.png',
				];
				html += controllers.image_button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : '<img src="/assets/images/lessons/G3C1L1_7_57_b.png">',
			question : 'How do you represent 5960?',
			type : 'multiple-choice',
			details : function() {
				var html = '<img src="/assets/images/lessons/G3C1L1_7_57_a.png">' +
					 '<img src="/assets/images/lessons/G3C1L1_7_57_b.png">' +
					 '<img src="/assets/images/lessons/G3C1L1_7_57_c.png">';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 12,
			time : '08:15',
			html : function(controllers) {
				var html = '<h2>How do you represent the score of Anjela 75, 960?</h2>';
				var choices = [
					baseURL + 'assets/images/lessons/G3C1L1_8_15_a.png', 
					baseURL + 'assets/images/lessons/G3C1L1_8_15_b.png',
					baseURL + 'assets/images/lessons/G3C1L1_8_15_c.png',
				];
				html += controllers.image_button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '<img src="/assets/images/lessons/G3C1L1_8_15_c.png">',
			question : 'How do you represent the score of Anjela 75, 960?',
			type : 'multiple-choice',
			details : function() {
				var html = '<img src="/assets/images/lessons/G3C1L1_8_15_a.png">' +
					'<img src="/assets/images/lessons/G3C1L1_8_15_b.png">' +
					'<img src="/assets/images/lessons/G3C1L1_8_15_c.png">';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 13,
			time : '09:08',
			html : function(controllers) {
				var html = '<h2>How will you write 75, 960 in expanded form?</h2>';
				var choices = [
					'A. 700 000 + 5 000 + 900 + 60 + 0', 
					'B. 70 000 + 5 000 + 900 + 60 + 0',
					'C. 90 000 + 6 000 + 500 + 70 + 0',
					'D. 90 000 + 5 000 + 700 + 60 + 0'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 70 000 + 5 000 + 900 + 60 + 0',
			question : 'How will you write 75, 960 in expanded form?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 700 000 + 5 000 + 900 + 60 + 0<br>B. 70 000 + 5 000 + 900 + 60 + 0<br>\
				C. 90 000 + 6 000 + 500 + 70 + 0<br>D. 90 000 + 5 000 + 700 + 60 + 0";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 14,
			time : '09:42',
			html : function(controllers) {
				var html = '<h2>What do we call in writing numbers normally in this way? 75960</h2>';
				var choices = [
					'A. standard form', 
					'B. word form',
					'C. normal form',
					'D. expanded form'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. standard form',
			question : 'What do we call in writing numbers normally in this way? 75960',
			type : 'multiple-choice',
			details : function() {
				var html = "A. standard form<br>B. word form<br>C. normal form<br>D. expanded form";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 15,
			time : '10:07',
			html : function(controllers) {
				var html = '<h2>What do we call in writing numbers in this manner? 75960 = Seventy Five Thousand, Nine Hundred Sixty</h2>';
				var choices = [
					'A. standard form', 
					'B. word form',
					'C. normal form',
					'D. expanded form'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. word form',
			question : 'What do we call in writing numbers in this manner? 75960 = Seventy Five Thousand, Nine Hundred Sixty',
			type : 'multiple-choice',
			details : function() {
				var html = "A. standard form<br>B. word form<br>C. normal form<br>D. expanded form";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 16,
			time : '10:35',
			html : function(controllers) {
				var html = '<h2>Which digit in 15678 has a PLACE VALUE of THOUSANDS?</h2>';
				var choices = [
					'A. 1', 
					'B. 5',
					'C. 6',
					'D. 8'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 5',
			question : 'Which digit in 15678 has a PLACE VALUE of THOUSANDS?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 1<br>B. 5<br>C. 6<br>D. 8";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 17,
			time : '11:01',
			html : function(controllers) {
				var html = '<h2>Which digit in 67589 has a PLACE VALUE of 10 000?</h2>';
				var choices = [
					'A. 6', 
					'B. 7',
					'C. 5',
					'D. 8'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 6',
			question : 'Which digit in 67589 has a PLACE VALUE of 10 000?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 6<br>B. 7<br>C. 5<br>D. 8";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 18,
			time : '11:26',
			html : function(controllers) {
				var html = '<h2>What is the PLACE VALUE of the underlined digit? 6<u>4</u>321</h2>';
				var choices = [
					'A. Ones', 
					'B. Tens',
					'C. Hundreds',
					'D. Thousands'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 3,
			answer_text : 'D. Thousands',
			question : 'What is the PLACE VALUE of the underlined digit? 6<u>4</u>321',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Ones<br>B. Tens<br>C. Hundreds<br>D. Thousands";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 19,
			time : '11:51',
			html : function(controllers) {
				var html = '<h2>How about the PLACE VALUE of the underlined digit in <u>5</u>10 024?</h2>';
				var choices = [
					'A. Hundreds', 
					'B. Ten Thousands',
					'C. Hundred Thousands',
					'D. Thousands'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. Hundred Thousands',
			question : 'How about the PLACE VALUE of the underlined digit in <u>5</u>10 024?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Hundreds<br>B. Ten Thousands<br>C. Hundred Thousands<br>D. Thousands";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 20,
			time : '12:18',
			html : function(controllers) {
				var html = '<h2>What is the VALUE of the underlined digit in <u>4</u>5040?</h2>';
				var choices = [
					'A. 400', 
					'B. 4000',
					'C. 40 000',
					'D. 400 000'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 40 000',
			question : 'What is the VALUE of the underlined digit in <u>4</u>5040?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 400<br>B. 4000<br>C. 40 000<br>D. 400 000";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
			end_session : 1,
		},
	],
};
