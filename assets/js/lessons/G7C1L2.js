var G7C1L2 = {
	title : 'Powers and Exponents',
	duration : '12:23',
		data : [
		{	quiz_number : 1,
			time : '00:51',
			html : function(controllers) {
				var html = '<img src="/assets/images/lessons/G7C1L2_table1.png">';
				html += '<h2>What do you observe on the number of text messages after a minute goes by?</h2>';
				var choices = [
					'a. The number of text messages doubles', 
					'b. The number of text messages triples',
					'c. The number of text messages remains the same',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. The number of text messages doubles',
			question : 'What do you observe on the number of text messages after a minute goes by?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L2_table1.png"></p>';
				html += "a. The number of text messages doubles<br>\
b. The number of text messages triples<br>\
c. The number of text messages remains the same";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2,
			time : '01:25',
			html : function(controllers) {
				var html = '<img src="/assets/images/lessons/G7C1L2_table1.png">';
				html += '<h2>How many text messages will be sent at 4 minutes?</h2>';
				var choices = [
					'a. 14', 
					'b. 16',
					'c. 18'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. 16',
			question : 'How many text messages will be sent at 4 minutes?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L2_table1.png"></p>';
				html += "a. 14<br>b. 16<br>c. 18";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '02:23',
			html : function(controllers) {
				var html = '<img src="/assets/images/lessons/G7C1L2_table2.png">';
				html += '<h2>What is the relationship of the number of 2\'s and the number of minutes?</h2>';
				var choices = [
					'a. minutes indicates number of times 2 is multiplied', 
					'b. minutes indicates the number of times 2 is added',
					'c. minutes have no relationship with number 2 '
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. minutes indicates number of times 2 is multiplied',
			question : 'What is the relationship of the number of 2\'s and the number of minutes?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L2_table2.png"></p>';
				html += "a. minutes indicates number of times 2 is multiplied<br>\
b. minutes indicates the number of times 2 is added<br>\
c. minutes have no relationship with number 2";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '03:20',
			html : function(controllers) {
				var html = '<h3>Imagine  how many text messages will be sent after 10 minutes.</h3>';
				html += '<h2>How would you express the number of text messages?</h2>';
				var choices = [
					'a. 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2', 
					'b. 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2',
					'c. 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2',
			question : 'How would you express the number of text messages?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>Imagine how many text messages will be sent after 10 minutes.</p>\
a. 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2<br>\
b. 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2<br>\
c. 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5,
			time : '04:20',
			html : function(controllers) {
				var html = '<h3 style="color:blue;">2 x 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2</h3><h2>What is the common factor in the expression above?</h2>';
				var choices = [
					'a. 2', 
					'b. 10',
					'c. 1'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. 2',
			question : 'What is the common factor in the expression above?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>2 x 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2</p>\
				a. 2<br>b. 10<br>c. 1";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 6,
			time : '04:44',
			html : function(controllers) {
				var html = '<h3 style="color:blue;">2 x 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2</h3><h2>How many times is 2 being multiplied?</h2>';
				var choices = [
					'a. 8', 
					'b. 9',
					'c. 10'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'c. 10',
			question : 'How many times is 2 being multiplied?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>2 x 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2</p>\
				a. 8<br>b. 9<br>c. 10";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time : '05:14',
			html : function(controllers) {
				var html = '<h3 style="color:blue;">2 x 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2</h3><h2>Which of the following is the exponent form of the expression?</h2>';
				var choices = [
					'a. 2<sup>9</sup>', 
					'b. 2<sup>8</sup>',
					'c. 2<sup>10</sup>'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'c. 2<sup>10</sup>',
			question : 'Which of the following is the exponent form of the expression?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>2 x 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2 x 2</p>\
				a. 2<sup>9</sup><br>b. 2<sup>8</sup><br>c. 2<sup>10</sup>";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '05:46',
			html : function(controllers) {
				var html = '<h2>2<sup>10</sup><br />What is the base and the exponent?</h2>';
				var choices = [
					'a. The base is 2 and the exponent is 10', 
					'b. The base is 10 and the exponent is 2',
					'c. The base is 2 and the exponent is 2'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. The base is 2 and the exponent is 10',
			question : 'What is the base and the exponent?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>2<sup>10</sup></p>\
a. The base is 2 and the exponent is 10<br>\
b. The base is 10 and the exponent is 2<br>\
c. The base is 2 and the exponent is 2";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '06:18',
			html : function(controllers) {
				var html = '<h2>How do you read 2<sup>10</sup>?</h2>';
				var choices = [
					'a. 2 times 10', 
					'b. 2 to the 10th power',
					'c. 2 plus 10'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. 2 to the 10th power',
			question : 'How do you read 2<sup>10</sup>?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. 2 times 10<br>b. 2 to the 10th power<br>c. 2 plus 10";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '07:15',
			html : function(controllers) {
				var html = '<h2>7<sup>5</sup><br />How do you read the notation above?</h2>';
				var choices = [
					'a. 5 times 5', 
					'b. 7 times 5',
					'c. 7 to the 5th power',
					'd. 5 to the 7th power'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'c. 7 to the 5th power',
			question : 'How do you read the notation above?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>7<sup>5</sup></p>\
a. 5 times 5<br>b. 7 times 5<br>c. 7 to the 5th power<br>d. 5 to the 7th power";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11,
			time : '07:40',
			html : function(controllers) {
				var html = '<h2>7<sup>5</sup><br /> What is the common factor or base?</h2>';
				var choices = [
					'a. 7', 
					'b. 5',
					'c. 6'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. 7',
			question : 'What is the common factor or base?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>7<sup>5</sup></p>a. 7<br>b. 5<br>c. 6";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 12,
			time : '08:01',
			html : function(controllers) {
				var html = '<h2>7<sup>5</sup><br />How about the exponent?</h2>';
				var choices = [
					'a. 7', 
					'b. 6',
					'c. 5'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'c. 5',
			question : 'How about the exponent?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>7<sup>5</sup></p>a. 7<br>b. 6<br>c. 5";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 13,
			time : '08:41',
			html : function(controllers) {
				var html = '<h2>7<sup>5</sup><br />Which of the following is the correct way to write the power of the same factor?</h2>';
				var choices = [
					'a. 7 x 7 x 7 x 7 x 7', 
					'b. 5 x 5 x 5 x 5 x 5 x 5 x 5',
					'c. 7 x 5'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. 7 x 7 x 7 x 7 x 7',
			question : 'Which of the following is the correct way to write the power of the same factor?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>7<sup>5</sup></p>\
a. 7 x 7 x 7 x 7 x 7<br>\
b. 5 x 5 x 5 x 5 x 5 x 5 x 5<br>\
c. 7 x 5";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 14,
			time : '09:22',
			html : function(controllers) {
				var html = '<h2>What is the standard form of 2<sup>5</sup></h2>';
				var choices = [
					'a. 2 x 2 x 2 x 2 x 2', 
					'b. 5 x 5',
					'c. 5 x 5 x 5 x 5 x 5'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. 2 x 2 x 2 x 2 x 2',
			question : 'What is the standard form of 2<sup>5</sup>?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. 2 x 2 x 2 x 2 x 2<br>b. 5 x 5<br>c. 5 x 5 x 5 x 5 x 5";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 15,
			time : '09:53',
			html : function(controllers) {
				var html = '<h2>Evaluate (1/2)<sup>2</sup></h2>';
				var choices = [
					'a. 1/2 x 2', 
					'b. 1/2 x 1',
					'c. 1/2 x 1/2'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'c. 1/2 x 1/2',
			question : 'Evaluate (1/2)<sup>2</sup>',
			type : 'multiple-choice',
			details : function() {
				var html = "a. 1/2 x 2<br>b. 1/2 x 1<br>c. 1/2 x 1/2";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 16,
			time : '10:30',
			html : function(controllers) {
				var html = '<h2>What is the exponential form of 3 x 3 x 3 x 3?</h2>';
				var choices = [
					'a. 4<sup>3</sup>', 
					'b. 3<sup>4</sup>',
					'c. 3<sup>3</sup>'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. 3<sup>4</sup>',
			question : 'What is the exponential form of 3 x 3 x 3 x 3?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. 4<sup>3</sup><br>b. 3<sup>4</sup><br>c. 3<sup>3</sup>";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 17,
			time : '11:06',
			html : function(controllers) {
				var html = '<h2>Which of the following is the exponential form of 2/3 x 2/3 x 2/3?</h2>';
				var choices = [
					'a. (2/3)<sup>3</sup>', 
					'b. (2<sup>3</sup>/3)',
					'c. (2/3<sup>3</sup>)'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. (2/3)<sup>3</sup>',
			question : 'Which of the following is the exponential form of 2/3 x 2/3 x 2/3?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. (2/3)<sup>3</sup><br>b. (2<sup>3</sup>/3)<br>c. (2/3<sup>3</sup>)";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
			end_session : 1,
		},
	],
};
