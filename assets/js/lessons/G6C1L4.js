var G6C1L4 = {
	title : 'Rounding Decimals',
	duration : '24:47',
	data : [
		{	quiz_number : 1,
			time : '00:40',
			html : function(controllers) {
				var html = '<h3>One day, Ben\'s mother asked Ben to buy a home theater system in an electronic store for \$117.50. His mother gave him \$120.</h3>';
				html += '<h2>How much does the home theater system cost?</h2>';
				var choices = [
					'a. $117.50', 
					'b. $120',
					'c. It\'s not mentioned in the problem'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. $117.50',
			question : 'How much does the home theater system cost?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>One day, Ben\'s mother asked Ben to buy a home theater system in \
an electronic store for \$117.50. His mother gave him \$120.</p>';
					html += 'a. $117.50<br>\
b. $120<br>\
c. It\'s not mentioned in the problem';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2,
			time : '01:06',
			html : function(controllers) {
				var html = '<h3>One day, Ben\'s mother asked Ben to buy a home theater system in an electronic store for \$117.50. His mother gave him \$120.</h3>';
				html += '<h2>How much did the mother give to Ben?</h2>';
				var choices = [
					'a. $117.50', 
					'b. $120',
					'c. It\'s not mentioned in the problem'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. $120',
			question : 'How much did the mother give to Ben?',
			type : 'multiple-choice',
			details : function() {
					var html = '<p>One day, Ben\'s mother asked Ben to buy a home theater system in \
an electronic store for \$117.50. His mother gave him \$120.</p>';
					html += 'a. $117.50<br>\
b. $120<br>\
c. It\'s not mentioned in the problem';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '01:40',
			html : function(controllers) {
				var html = '<h3>One day, Ben\'s mother asked Ben to buy a home theater system in an electronic store for \$117.50. His mother gave him \$120.</h3>';
					html += '<h2>What should be the BEST way to check if the money given by Ben\'s mother is enough?</h2>';
				var choices = [
					'a. If the amount is less than the home theater system\'s price.', 
					'b. If the amount is greater than the home theater system\'s price.',
					'c. If the amount is neither less than or greater than the home theater system\'s price.',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. If the amount is greater than the home theater system\'s price.',
			question : 'What should be the BEST way to check if the money given by Ben\'s mother is enough?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>One day, Ben\'s mother asked Ben to buy a home theater system in \
an electronic store for \$117.50. His mother gave him \$120.</p>';
					html += "a. If the amount is less than the home theater system\'s price.<br>\
b. If the amount is greater than the home theater system\'s price.<br>\
c. If the amount is neither less than or greater than the home theater system\'s price.";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '02:49',
			html : function(controllers) {
				var html = '<p><img src="/assets/images/lessons/G6C1L4_img1.png" width="100%"></p>';
				html += '<h2>What place value should we compare first?</h2>';
				var choices = [
					'a. hundreds', 
					'b. tens',
					'c. ones',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. hundreds',
			question : 'What place value should we compare first?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G6C1L4_img1.png" width="100%"></p>';
				html += "a. hundreds<br>b. tens<br>c. ones";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5,
			time : '03:13',
			html : function(controllers) {
				var html = '<p><img src="/assets/images/lessons/G6C1L4_img1.png" width="100%"></p>';
				html += '<h2>What can you say about the digits in the hundreds place?</h2>';
				var choices = [
					'a. the first digit is greater than the second digit', 
					'b. the first digit is less than second digit',
					'c. they are the same',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'c. they are the same',
			question : 'What can you say about the digits in the hundreds place?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G6C1L4_img1.png" width="100%"></p>';
				html += "a. the first digit is greater than the second digit<br>\
b. the first digit is less than second digit<br>\
c. they are the same";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 6,
			time : '03:33',
			html : function(controllers) {
				var html = '<p><img src="/assets/images/lessons/G6C1L4_img1.png" width="100%"></p>';
				html += '<h2>So, what place value should we compare next?</h2>';
				var choices = [
					'a. hundreds', 
					'b. tens',
					'c. ones'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. tens',
			question : 'So, what place value should we compare next?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G6C1L4_img1.png" width="100%"></p>';
				html += "a. hundreds<br>b. tens<br>c. ones"
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time : '03:59',
			html : function(controllers) {
				var html = '<p><img src="/assets/images/lessons/G6C1L4_img1.png" width="100%"></p>';
				html += '<h3>Which of the two digits in the tens place is greater, is it the first digit or the second digit?</h3>';
				var choices = [
					'a. first digit', 
					'b. second digit',
					'c. there\'s no difference between the two'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. second digit',
			question : 'Which of the two digits in the tens place is greater, is it the first digit or the second digit?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G6C1L4_img1.png" width="100%"></p>';
				html += "a. first digit<br>b. second digit<br>c. there's no difference between the two";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '05:06',
			html : function(controllers) {
				var html = '<h2>Will this amount be enough to buy it? Explain.</h2>';
				var choices = [
					'a. Yes, because $117.50 is greater than $120.', 
					'b. No, because $117.50 is less than $120.',
					'c. Yes, because $120 is greater than $117.50.'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'c. Yes, because $120 is greater than $117.50.',
			question : 'Will this amount be enough to buy it? Explain.',
			type : 'multiple-choice',
			details : function() {
				var html = "a. Yes, because $117.50 is greater than $120.<br>\
b. No, because $117.50 is less than $120.<br>\
c. Yes, because $120 is greater than $117.50.";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '06:56',
			html : function(controllers) {
				var html = '<p><img src="/assets/images/lessons/G6C1L4_img2.png" width="100%"></p>';
				html += '<h2>What have you observed with 25.3 when rounded to the nearest ones?</h2>';
				var choices = [
					'a. Its rounded value is reduced', 
					'b. Its rounded value is increased',
					'c. Its rounded value is kept the same'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. Its rounded value is reduced',
			question : 'What have you observed with 25.3 when rounded to the nearest ones?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G6C1L4_img2.png" width="100%"></p>';
				html += "a. Its rounded value is reduced<br>\
b. Its rounded value is increased<br>\
c. Its rounded value is kept the same";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '07:31',
			html : function(controllers) {
				var html = '<p><img src="/assets/images/lessons/G6C1L4_img3.png" width="100%"></p>';
				html += '<h2>With regards to 25.5, what have you observed as it\'s rounded to the nearest ones?</h2>';
				var choices = [
					'a. Its rounded value is reduced', 
					'b. Its rounded value is increased',
					'c. Its rounded value is kept the same'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. Its rounded value is increased',
			question : 'With regards to 25.5, what have you observed as it\'s rounded to the nearest ones?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G6C1L4_img3.png" width="100%"></p>';
				html += "a. Its rounded value is reduced<br>\
b. Its rounded value is increased<br>\
c. Its rounded value is kept the same";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11,
			time : '08:14',
			html : function(controllers) {
				var html = '<p><img src="/assets/images/lessons/G6C1L4_img4.png" width="100%"></p>';
				html += '<h2>What have you observed with 25.8 as it\'s rounded to the nearest ones?</h2>';
				var choices = [
					'a. Its rounded value is reduced', 
					'b. Its rounded value is increased',
					'c. Its rounded value is kept the same'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. Its rounded value is increased',
			question : 'What have you observed with 25.8 as it\'s rounded to the nearest ones?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G6C1L4_img4.png" width="100%"></p>';
				html += "a. Its rounded value is reduced<br>\
b. Its rounded value is increased<br>\
c. Its rounded value is kept the same";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 12,
			time : '10:14',
			html : function(controllers) {
				var html = '<p><img src="/assets/images/lessons/G6C1L4_img5.png" width="100%"></p>';
				html += '<h2>What happened to 34.4 when it was rounded down?</h2>';
				var choices = [
					'a. the rounded value is kept the same which is 34.4', 
					'b. the rounded value is increased and became 40',
					'c. the rounded value is decresed and became 30'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'c. the rounded value is decresed and became 30',
			question : 'What happened to 34.4 when it was rounded down?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G6C1L4_img5.png" width="100%"></p>';
				html += "a. the rounded value is kept the same which is 34.4<br>\
b. the rounded value is increased and became 40<br>\
c. the rounded value is decresed and became 30";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 13,
			time : '11:02',
			html : function(controllers) {
				var html = '<p><img src="/assets/images/lessons/G6C1L4_img6.png" width="100%"></p>';
				html += '<h2>When we round 37.5 and 35.8, what happen to these two numbers?</h2>';
				var choices = [
					'a. the rounded values are kept the same', 
					'b. the rounded values are increased and became 40',
					'c. the rounded values are decreased and became 30'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. the rounded values are increased and became 40',
			question : 'When we round 37.5 and 35.8, what happen to these two numbers?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G6C1L4_img6.png" width="100%"></p>';
				html += "a. the rounded values are kept the same<br>\
b. the rounded values are increased and became 40<br>\
c. the rounded values are decreased and became 30";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 14,
			time : '12:15',
			html : function(controllers) {
				var html = '<p><img src="/assets/images/lessons/G6C1L4_img6.png" width="100%"></p>';
				html += '<h2>When will we round up a number just like 37.5 and 35.8?</h2>';
				var choices = [
					'a. if the next digit to the right of the tens place is greater than or equal to 5', 
					'b. if the next digit to the right of the tens place is less than 5',
					'c. it depends on the learner whether he wants to round it up or not',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. if the next digit to the right of the tens place is greater than or equal to 5',
			question : 'When will we round up a number just like 37.5 and 35.8?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G6C1L4_img6.png" width="100%"></p>';
				html += "a. if the next digit to the right of the tens place is greater than or equal to 5<br>\
b. if the next digit to the right of the tens place is less than 5<br>\
c. it depends on the learner whether he wants to round it up or not";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 15,
			time : '12:52',
			html : function(controllers) {
				var html = '<p><img src="/assets/images/lessons/G6C1L4_img7.png" width="100%"></p>';
				html += '<h2>When will we round down a number like 34.4?</h2>';
				var choices = [
					'a. if the next digit to the right of the tens place is greater than or equal to 5', 
					'b. if the next digit to the right of the tens place is less than 5',
					'c. it depends on the learner whether he wants to round it up or not'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. if the next digit to the right of the tens place is less than 5',
			question : 'When will we round down a number like 34.4?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G6C1L4_img7.png" width="100%"></p>';
				html += "a. if the next digit to the right of the tens place is greater than or equal to 5<br>\
b. if the next digit to the right of the tens place is less than 5<br>\
c. it depends on the learner whether he wants to round it up or not";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 16,
			time : '13:21',
			html : function(controllers) {
				var html = '<h2>What is the round value of 345.6 to the nearest hundreds?</h2>';
				var choices = [
					'a. 350', 
					'b. 300',
					'c. 400',
					
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. 300',
			question : 'What is the round value of 345.6 to the nearest hundreds?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. 350<br>b. 300<br>c. 400";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 17,
			time : '14:07',
			html : function(controllers) {
				var html = '<h2><strong>345.6</strong> - Look at the next digit to the right of the hundreds place, is it "less than" or "greater than or equal to 5"?</h2>';
				var choices = [
					'a. less than', 
					'b. greater than or equal to',
					'c. either of the two',
					
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. less than',
			question : '<strong>345.6</strong> - Look at the next digit to the right of the hundreds place, is it "less than" or "greater than or equal to 5"?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. less than<br>b. greater than or equal to<br>c. either of the two";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 18,
			time : '15:07',
			html : function(controllers) {
				var html = '<h2>We look at the next digit to the right of the hundreds place because:</h2>';
				var choices = [
					'a. it will help us determine whether to round up or round down a number <br />since we should round a number to the nearest hundreds', 
					'b. it will eliminate the other digits and leave all the digits until the hundreds place',
					'c. it will distract the learner and confuse them',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. it will help us determine whether to round up or round down a number <br />since we should round a number to the nearest hundreds',
			question : 'We look at the next digit to the right of the hundreds place because:',
			type : 'multiple-choice',
			details : function() {
				var html = "a. it will help us determine whether to round up or round down a number <br />\
since we should round a number to the nearest hundreds<br>\
b. it will eliminate the other digits and leave all the digits until the hundreds place<br>\
c. it will distract the learner and confuse them";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 19,
			time : '15:41',
			html : function(controllers) {
				var html = '<h2>Round 134.674 to the nearest tenths</h2>';
				var choices = [
					'a. 135', 
					'b. 134.7',
					'c. 130',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. 134.7',
			question : 'Round 134.674 to the nearest tenths',
			type : 'multiple-choice',
			details : function() {
				var html = "a. 135<br>b. 134.7<br>c. 130";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 20,
			time : '16:01',
			html : function(controllers) {
				var html = '<h3>134.674</h3><h2>What is the digit in the tenths place?</h2>';
				var choices = [
					'a. 6', 
					'b. 7',
					'c. 4',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. 6',
			question : '134.674 - What is the digit in the tenths place?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. 6<br>b. 7<br>c. 4";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 21,
			time : '16:23',
			html : function(controllers) {
				var html = '<h3>134.674</h3><h2>What digit should we base for us to know whether to round up or round down?</h2>';
				var choices = [
					'a. 4', 
					'b. 7',
					'c. 3',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. 7',
			question : '134.674 - What digit should we base for us to know whether to round up or round down?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. 4<br>b. 7<br>c. 3";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 22,
			time : '17:24',
			html : function(controllers) {
				var html = '<h2>Round 29.21 to the nearest ones</h2>';
				var choices = [
					'a. 29', 
					'b. 20',
					'c. 30',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. 29',
			question : 'Round 29.21 to the nearest ones',
			type : 'multiple-choice',
			details : function() {
				var html = "a. 29<br>b. 20<br>c. 30";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 23,
			time : '18:02',
			html : function(controllers) {
				var html = '<h2>By rounding 74.5 to the nearest ones, it will become:</h2>';
				var choices = [
					'a. 80', 
					'b. 75',
					'c. 70',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. 75',
			question : 'By rounding 74.5 to the nearest ones, it will become:',
			type : 'multiple-choice',
			details : function() {
				var html = "a. 80<br>b. 75<br>c. 70";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 24,
			time : '18:50',
			html : function(controllers) {
				var html = '<h2>Round 1.239 to the nearest ones, 1.239 will become:</h2>';
				var choices = [
					'a. 1.24', 
					'b. 1',
					'c. 1.2',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. 1',
			question : 'Round 1.239 to the nearest ones, 1.239 will become:',
			type : 'multiple-choice',
			details : function() {
				var html = "a. 1.24<br>b. 1<br>c. 1.2";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 25,
			time : '19:49',
			html : function(controllers) {
				var html = '<h2>Which of the following BEST describes significant digits?</h2>';
				var choices = [
					'a. digits that starts with the first non-zero digit and ends with the last non-zero digit', 
					'b. digits being asked in every problem',
					'c. digits determined by its place value',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. digits that starts with the first non-zero digit and ends with the last non-zero digit',
			question : 'Which of the following BEST describes significant digits?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. digits that starts with the first non-zero digit and ends with the last non-zero digit<br>\
b. digits being asked in every problem<br>\
c. digits determined by its place value";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 26,
			time : '20:42',
			html : function(controllers) {
				var html = '<h3>Round 2.346 to 2 significant digits</h3><h2>In the decimal 2.346, what are the first two non-zero digits?</h2>';
				var choices = [
					'a. 2 and 3', 
					'b. 3 and 4',
					'c. 2 and 6',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. 2 and 3',
			question : 'Round 2.346 to 2 significant digits - In the decimal 2.346, what are the first two non-zero digits?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. 2 and 3<br>b. 3 and 4<br>c. 2 and 6";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 27,
			time : '21:06',
			html : function(controllers) {
				var html = '<h3>Round 2.346 to 2 significant digits</h3><h2>What digit should be based on when we round off 2.3?</h2>';
				var choices = [
					'a. 4', 
					'b. 6',
					'c. 3'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. 4',
			question : 'Round 2.346 to 2 significant digits - What digit should be based on when we round off 2.3?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. 4<br>b. 6<br>c. 3";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 28,
			time : '21:30',
			html : function(controllers) {
				var html = '<h3>Round 2.346 to 2 significant digits</h3><h2>Should we round up or round down?</h2>';
				var choices = [
					'a. round up', 
					'b. round down',
					'c. do both'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. round down',
			question : 'Round 2.346 to 2 significant digits - Should we round up or round down?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. round up<br>b. round down<br>c. do both";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 29,
			time : '22:09',
			html : function(controllers) {
				var html = '<h2>What is the rounded value of 0.016585 up to 3 significant digits?</h2>';
				var choices = [
					'a. 0.01', 
					'b. 0.0166',
					'c. 0.017'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. 0.0166',
			question : 'What is the rounded value of 0.016585 up to 3 significant digits?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. 0.01<br>b. 0.0166<br>c. 0.017";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 30,
			time : '22:55',
			html : function(controllers) {
				var html = '<h2>Why are the zeroes NOT included?</h2>';
				var choices = [
					'a. they serve as place holder in the decimal number', 
					'b. they cancel out the other digits',
					'c. they serve no purpose and they can be removed',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. they serve as place holder in the decimal number',
			question : 'Why are the zeroes NOT included?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. they serve as place holder in the decimal number<br>\
b. they cancel out the other digits<br>\
c. they serve no purpose and they can be removed";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 31,
			time : '23:37',
			html : function(controllers) {
				var html = '<h2>How many significant digits are there in 34.080?</h2>';
				var choices = [
					'a. 4', 
					'b. 5',
					'c. 3',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. 4',
			question : 'How many significant digits are there in 34.080?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. 4<br>b. 5<br>c. 3";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 32,
			time : '24:30',
			html : function(controllers) {
				var html = '<h2>How many significantl digits are there in 380.?</h2>';
				var choices = [
					'a. 2', 
					'b. 3',
					'c. none'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. 3',
			question : 'How many significantl digits are there in 380.?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. 2<br>b. 3<br>c. none";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
			end_session : 1,
		},
	],
};
