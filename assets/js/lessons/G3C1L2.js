var G3C1L2 = {
		title : "Compare Numbers To Hundred Thousands",
        duration : "18:27",
        data : [
                {		quiz_number : 1,
						time : '00:23',
                        html : function(controllers) {
                                var html = '<h2>In Grade 2, you learned to COMPARE NUMBERS up to how many digits?</h2>';
                                var choices = [
                                        'A. 1 digit', 
                                        'B. 2 digits',
                                        'C. 3 digits',
                                        'D. 4 digits'
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 2,
                        answer_text : 'C. 3 digits',
                        question : 'In Grade 2, you learned to COMPARE NUMBERS up to how many digits?',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. 1 digit<br>B. 2 digits<br>C. 3 digits<br>D. 4 digits";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 2,
						time : '00:50',
                        html : function(controllers) {
                                var html = '<h2>When comparing two numbers, what do you compare first?</h2>';
                                var choices = [
                                        'A. The tens place', 
                                        'B. The hundreds place',
                                        'C. The digit with highest PLACE VALUE',
                                        'D. The digit with lowest PLACE VALUE'
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 2,
                        answer_text : 'C. The digit with highest PLACE VALUE',
                        question : 'When comparing two numbers, what do you compare first?',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. The tens place<br>B. The hundreds place<br>C. The digit with highest PLACE VALUE<br>D. The digit with lowest PLACE VALUE";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 3,
						time : '01:23',
                        html : function(controllers) {
                                var html = '<h2>When comparing these numbers, say 587 and 924, which PLACE do you compare first?</h2>';
                                var choices = [
                                        'A. Ones', 
                                        'B. Tens',
                                        'C. Hundreds',
                                        'D. It doesn\'t matter'
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 2,
                        answer_text : 'C. Hundreds',
                        question : 'When comparing these numbers, say 587 and 924, which PLACE do you compare first?',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. Ones<br>B. Tens<br>C. Hundreds<br>D. It doesn't matter";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 4,
						time : '01:48',
                        html : function(controllers) {
                                var html = '<h2>How about these numbers? 53 or 84</h2>';
                                var choices = [
                                        'A. Ones', 
                                        'B. Tens',
                                        'C. Hundreds',
                                        'D. It doesn\'t matter'
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 1,
                        answer_text : 'B. Tens',
                        question : 'How about these numbers? 53 or 84',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. Ones<br>B. Tens<br>C. Hundreds<br>D. It doesn't matter";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 5,
						time : '02:46',
                        html : function(controllers) {
                                var html = '<h2>Which of these numbers is GREATER? 69 or 24</h2>';
                                var choices = [
                                        'A. 69', 
                                        'B. 24',
                                        'C. They are just equal',
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 0,
                        answer_text : 'A. 69',
                        question : 'Which of these numbers is GREATER? 69 or 24',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. 69<br>B. 24<br>C. They are just equal";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 6,
						time : '03:25',
                        html : function(controllers) {
                                var html = '<h2>Which of these numbers is LESS? 269 or 524</h2>';
                                var choices = [
                                        'A. 269', 
                                        'B. 524',
                                        'C. They are just equal',
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 0,
                        answer_text : 'A. 269',
                        question : 'Which of these numbers is LESS? 269 or 524',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. 269<br>B. 524<br>C. They are just equal";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 7,
						time : '03:48',
                        html : function(controllers) {
                                var html = '<h2>Fill in the square with the right SYMBOL. 84 ___ 59</h2>';
                                var choices = [
                                        'A. >', 
                                        'B. <',
                                        'C. =',
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 0,
                        answer_text : 'A. >',
                        question : 'Fill in the square with the right SYMBOL. 84 ___ 59',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. ><br>B. <<br>C. =";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 8,
						time : '04:39',
                        html : function(controllers) {
                                var html = '<h2>How about this? 912 ___ 925</h2>';
                                var choices = [
                                        'A. Compare the next higher PLACE digit', 
                                        'B. Compare the next lower PLACE digit',
                                        'C. They are just equal',
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 1,
                        answer_text : 'B. Compare the next lower PLACE digit',
                        question : 'How about this? 912 ___ 925',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. Compare the next higher PLACE digit<br>B. Compare the next lower PLACE digit<br>C. They are just equal";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 9,
						time : '05:10',
                        html : function(controllers) {
                                var html = '<h2>What is the proper way to compare these numbers? 912 or 925</h2>';
                                var choices = [
                                        'A. Compare the Ones Place', 
                                        'B. Compare the Tens Place',
                                        'C. Compare the Hundreds Place',
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 1,
                        answer_text : 'B. Compare the Tens Place',
                        question : 'What is the proper way to compare these numbers? 912 or 925',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. Compare the Ones Place<br>B. Compare the Tens Place<br>C. Compare the Hundreds Place";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 10,
						time : '05:27',
                        html : function(controllers) {
                                var html = '<h2>thus the correct symbol in the blank is? 912 ___ 925</h2>';
                                var choices = [
                                        'A. >', 
                                        'B. <',
                                        'C. =',
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 1,
                        answer_text : 'B. <',
                        question : 'thus the correct symbol in the blank is? 912 ___ 925',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. ><br>B. <<br>C. =";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 11,
						time : '06:19',
                        html : function(controllers) {
                                var html = '<h2>We will be comparing numbers based on its PLACE VALUE up to?</h2>';
                                var choices = [
                                        'A. Hundreds', 
                                        'B. Thousands',
                                        'C. Ten Thousands',
                                        'D. Hundred Thousands'
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 3,
                        answer_text : 'D. Hundred Thousands',
                        question : 'We will be comparing numbers based on its PLACE VALUE up to?',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. Hundreds<br>B. Thousands<br>C. Ten Thousands<br>D. Hundred Thousands";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 12,
						time : '07:21',
                        html : function(controllers) {
                                var html = '<h2>Comparing the numbers 723,393 and 735,132, which PLACE VALUE shall we compare first?</h2>';
                                var choices = [
                                        'A. Hundreds', 
                                        'B. Ten Thousands Place',
                                        'C. Hundred Thousands Place',
                                        'D. Thousands Place'
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 2,
                        answer_text : 'C. Hundred Thousands Place',
                        question : 'Comparing the numbers 723,393 and 735,132, which PLACE VALUE shall we compare first?',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. Hundreds<br>B. Ten Thousands Place<br>C. Hundred Thousands Place<br>D. Thousands Place";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 13,
						time : '08:03',
                        html : function(controllers) {
                                var html = '<h2>Comparing the digits at the "Hundred Thousands Place". Which is GREATER between 723,993 and 735,132?</h2>';
                                var choices = [
                                        'A. 723 993', 
                                        'B. 735 132',
                                        'C. They are just equal',
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 2,
                        answer_text : 'C. They are just equal',
                        question : 'Comparing the digits at the "Hundred Thousands Place". Which is GREATER between 723,993 and 735,132?',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. 723 993<br>B. 735 132<br>C. They are just equal";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 14,
						time : '08:25',
                        html : function(controllers) {
                                var html = '<h2>The value of the digit is?</h2>';
                                html += '<p><input type="text" id="answer-box" /></p>';
                                html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
                                return html;
                        },
                        answer : 700000,
                        answer_text : 700000,
                        question : 'The value of the digit is?',
                        type : 'input-box',
                        details : function() {
                                var html = "";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 15,
						time : '08:57',
                        html : function(controllers) {
                                var html = '<h2>Since the "Hundred Thousands Place" are just equal, the next step in comparing these numbers is</h2>';
                                var choices = [
                                        'A. Compare the Thousands Place', 
                                        'B. Compare the Hundreds Place',
                                        'C. Compare the Ten Thousands Place',
                                        'D. Compare the Tens',
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 2,
                        answer_text : 'C. Compare the Ten Thousands Place',
                        question : 'Since the "Hundred Thousands Place" are just equal, the next step in comparing these numbers is',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. Compare the Thousands Place<br>\
B. Compare the Hundreds Place<br>\
C. Compare the Ten Thousands Place<br>\
D. Compare the Tens";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 16,
						time : '09:38',
                        html : function(controllers) {
                                var html = '<h2>Which is GREATER between 723 993 and  735 132?</h2>';
                                var choices = [
                                        'A. 723 993', 
                                        'B. 735 132',
                                        'C. They are just equal',
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 1,
                        answer_text : 'B. 735 132',
                        question : 'Which is GREATER between 723 993 and  735 132?',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. 723 993<br>B. 735 132<br>C. They are just equal";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 17,
						time : '11:36',
                        html : function(controllers) {
                                var html = '<h2>That means we need to start in the PLACE VALUE of?</h2>';
                                html += '<p><select id="answer-box">\
									<option>ones</option>\
									<option>tens</option>\
									<option>hundreds</option>\
									<option>thousands</option>\
									<option>ten thousands</option>\
								</select></p>';
                                html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
                                return html;
                        },
                        answer : 'hundreds',
                        answer_text : 'hundreds',
                        question : 'That means we need to start in the PLACE VALUE of?',
                        type : 'input-box',
                        details : function() {
                                var html = "";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 18,
						time : '12:06',
                        html : function(controllers) {
                                var html = '<h2>Since 94 does not have a "Hundreds Place", so what do you think we shall do?</h2>';
                                var choices = [
                                        'A. Just compare the numbers in the Tens Place', 
                                        'B. Just compare the numbers in the Ones Place',
                                        'C. Add zero to the Hundreds Place',
                                        'D. None of the above',
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 2,
                        answer_text : 'C. Add zero to the Hundreds Place',
                        question : 'Since 94 does not have a "Hundreds Place", so what do you think we shall do?',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. Just compare the numbers in the Tens Place<br>\
B. Just compare the numbers in the Ones Place<br>\
C. Add zero to the Hundreds Place<br>\
D. None of the above";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 19,
						time : '12:58',
                        html : function(controllers) {
                                var html = '<h2>Therefore, 94 is ______________ 136.</h2>';
                                var choices = [
                                        'A. GREATER THAN', 
                                        'B. LESS THAN',
                                        'C. EQUAL TO',
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 1,
                        answer_text : 'B. LESS THAN',
                        question : 'Therefore, 94 is ______________ 136.',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. GREATER THAN<br>B. LESS THAN<br>C. EQUAL TO";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 20,
						time : '13:37',
                        html : function(controllers) {
                                var html = '<h2>When one compares two numbers of different digits. Which one _____________ is larger.</h2>';
                                var choices = [
                                        'A. The number that has more digits', 
                                        'B. The number that has less digits',
                                        'C. The number that has the next larger PLACE VALUE',
                                        'D. The number that has the next lower PLACE VALUE',
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 0,
                        answer_text : 'A. The number that has more digits',
                        question : 'When one compares two numbers of different digits. Which one _____________ is larger.',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. The number that has more digits<br>\
B. The number that has less digits<br>\
C. The number that has the next larger PLACE VALUE<br>\
D. The number that has the next lower PLACE VALUE";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 21,
						time : '14:02',
                        html : function(controllers) {
                                var html = '<h2>2640 > 2640</h2>';
                                var choices = [
                                        'A. True', 
                                        'B. False',
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 1,
                        answer_text : 'B. False',
                        question : '2640 > 2640',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. True<br>B. False";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 22,
						time : '14:14',
                        html : function(controllers) {
                                var html = '<h2>20834 < 20712</h2>';
                                var choices = [
                                        'A. True', 
                                        'B. False',
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 1,
                        answer_text : 'B. False',
                        question : '20834 < 20712',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. True<br>B. False";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 23,
						time : '14:26',
                        html : function(controllers) {
                                var html = '<h2>145 623 > 145 263</h2>';
                                var choices = [
                                        'A. True', 
                                        'B. False',
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 0,
                        answer_text : 'A. True',
                        question : '145 623 > 145 263',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. True<br>B. False";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 24,
						time : '14:41',
                        html : function(controllers) {
                                var html = '<h2>820 834 < 820 712</h2>';
                                var choices = [
                                        'A. True', 
                                        'B. False',
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 1,
                        answer_text : 'B. False',
                        question : '820 834 < 820 712',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. True<br>B. False";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 25,
						time : '14:52',
                        html : function(controllers) {
                                var html = '<h2>54 237 > 101 002</h2>';
                                var choices = [
                                        'A. True', 
                                        'B. False',
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 1,
                        answer_text : 'B. False',
                        question : '54 237 > 101 002',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. True<br>B. False";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 26,
						time : '15:07',
                        html : function(controllers) {
                                var html = '<h2>207 834 < 20 712</h2>';
                                var choices = [
                                        'A. True', 
                                        'B. False',
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 1,
                        answer_text : 'B. False',
                        question : '207 834 < 20 712',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. True<br>B. False";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 27,
						time : '15:27',
                        html : function(controllers) {
                                var html = '<h2>What is the right symbol to make the statement true?</h2> 245 732 ____ 245 722';
                                var choices = [
                                        'A. >', 
                                        'B. <',
                                        'C. ='
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 0,
                        answer_text : 'A. >',
                        question : 'What is the right symbol to make the statement true?</h2> 245 732 ____ 245 722',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. ><br>B. <<br>C. =";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 28,
						time : '15:39',
                        html : function(controllers) {
                                var html = '<h2>What is the right symbol to make the statement true?</h2> 924 322 ____ 924 322';
                                var choices = [
                                        'A. >', 
                                        'B. <',
                                        'C. ='
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 2,
                        answer_text : 'C. =',
                        question : 'What is the right symbol to make the statement true?</h2> 924 322 ____ 924 322',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. ><br>B. <<br>C. =";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 29,
						time : '15:56',
                        html : function(controllers) {
                                var html = '<h2>What is the right symbol to make the statement true?</h2> 500 005 ____ 500 050';
                                var choices = [
                                        'A. >', 
                                        'B. <',
                                        'C. ='
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 1,
                        answer_text : 'B. <',
                        question : 'What is the right symbol to make the statement true?</h2> 500 005 ____ 500 050',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. ><br>B. <<br>C. =";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 30,
						time : '16:08',
                        html : function(controllers) {
                                var html = '<h2>What is the right symbol to make the statement true?</h2> 804 408 ____ 804 082';
                                var choices = [
                                        'A. >', 
                                        'B. <',
                                        'C. ='
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 0,
                        answer_text : 'A. >',
                        question : 'What is the right symbol to make the statement true?</h2> 804 408 ____ 804 082',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. ><br>B. <<br>C. =";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 31,
						time : '16:24',
                        html : function(controllers) {
                                var html = '<h2>What is the right symbol to make the statement true?</h2> 802 005 ____ 23 999';
                                var choices = [
                                        'A. >', 
                                        'B. <',
                                        'C. ='
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 0,
                        answer_text : 'A. >',
                        question : 'What is the right symbol to make the statement true?</h2> 802 005 ____ 23 999',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. ><br>B. <<br>C. =";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                },
                {		quiz_number : 32,
						time : '16:37',
                        html : function(controllers) {
                                var html = '<h2>What is the right symbol to make the statement true?</h2> 9 999 ____ 10 000';
                                var choices = [
                                        'A. >', 
                                        'B. <',
                                        'C. ='
                                ];
                                html += controllers.button_choices(choices);
                                return html;
                        },
                        answer : 1,
                        answer_text : 'B. <',
                        question : 'What is the right symbol to make the statement true?</h2> 9 999 ____ 10 000',
                        type : 'multiple-choice',
                        details : function() {
                                var html = "A. ><br>B. <<br>C. =";
                                return html;
                        },
                        controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
                                controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
                        },
                        end_session : 1,
                },
		],
};
