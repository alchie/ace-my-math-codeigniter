var G7C2L1 = {
	title : 'Absolute Value',
	duration : '20:07',
	data : [
		{	quiz_number : 1,
			time : '00:39',
			html : function(controllers) {
				var html = '<h3>The bottom of a ramp is 5 feet below street level.\
				A value of -5 represents below street level. </h3>\
				<h2>What does a value of -7 represent?</h2>';
				var choices = [
					'A. 7 feet above street level', 
					'B. 7 feet below street level',
					'C. 7 feet street level',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 7 feet below street level',
			question : 'What does a value of -7 represent?',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "The bottom of a ramp is 5 feet below street level.\
A value of -5 represents below street level.<br><br>";
				html += "A. 7 feet above street level<br>\
B. 7 feet below street level<br>\
C. 7 feet street level";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},		
		{	quiz_number : 2,
			time : '01:13',
			html : function(controllers) {
				var html = '<p>The top deck of the ramp is 3 feet above street level.</p><h2>How can you represent 3 feet above street level?</h2>';
				var choices = [
					'A. +3', 
					'B. -3',
					'C. 1/3',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : '+3',
			question : 'How can you represent 3 feet above street level?',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "The top deck of the ramp is 3 feet above street level.<br><br>\
A. +3<br>\
B. -3<br>\
C. 1/3";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '02:23',
			html : function(controllers) {
				var html = '<p>Write an integer for the following situation.</p>\
				<h2>An average rainfall of 8 inches above normal?</h2>\
				<p>(Hint: answer may be "+" or "-")</p>';
				html += '<p><input type="text" id="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : [8, "+8"],
			answer_text : '8 / +8',
			question : 'Write an integer for the following situation.',
			type : 'input-box', // input-box
			details : function() {
				var html = 'An average rainfall of 8 inches above normal? Hint: answer may be "+" or "-"';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box_any('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '02:59',
			html : function(controllers) {
				var html = '<h2>How about an average temperature of 5 degrees below zero?</h2>\
				<p>(Hint: answer may be "+" or "-")</p>';
				html += '<p><input type="text" id="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : "-5",
			answer_text : '-5',
			question : 'How about an average temperature of 5 degrees below zero?',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = '(Hint: answer may be "+" or "-")';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5,
			time : '04:10',
			html : function(controllers) {
				var html = '<h2>How many units away is +4 from 0?</h2>';
				html += '<p><input type="text" id="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 4,
			answer_text : 4,
			question : 'How many units away is +4 from 0?',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 6,
			time : '04:33',
			html : function(controllers) {
				var html = '<h2>How about -4, what is the distance of -4 from 0?</h2>';
				html += '<p><input type="text" id="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 4,
			answer_text : 4,
			question : 'How about -4, what is the distance of -4 from 0?',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time : '05:40',
			html : function(controllers) {
				var html = '<h2>What is the absolute value of -6?</h2>';
				html += '<p><input type="text" id="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 6,
			answer_text : 6,
			question : 'What is the absolute value of -6?',
			type : 'input-box', 
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '06:29',
			html : function(controllers) {
				var html = '<h2>Evaluate |-8| - |-5|<br>Which of the following is the first step?</h2>';
				var choices = [
					'A. Subtract -8 and -5', 
					'B. Add -8 and -5',
					'C. Find the absolute values of -8 and -5',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. Find the absolute values of -8 and -5',
			question : 'Which of the following is the first step?',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "Evaluate |-8| - |-5|<br><br>";
				html += "A. Subtract -8 and -5<br>\
B. Add -8 and -5<br>\
C. Find the absolute values of -8 and -5";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '06:52',
			html : function(controllers) {
				var html = '<h2>Find the absolute values of -8 and -5</h2>';
				html += '<p>|-8| = <input type="text" class="answer-box" maxlength="1" /></p>';
				html += '<p>|-5| = <input type="text" class="answer-box" maxlength="1" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : [8,5],
			answer_text : '8 and 5',
			question : 'Find the absolute values of -8 and -5',
			type : 'input-box', // input-box
			details : function() {
				var html = "";
				html += '|-8| = _______<br>';
				html += '|-5| = _______';
				return html; 
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '07:27',
			html : function(controllers) {
				var html = '<h2>Evaluate |-8| - |-5|</h2>';
				var choices = [
					'A. 13', 
					'B. -3',
					'C. 3',
					'D. -13',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 3',
			question : 'Evaluate |-8| - |-5|',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "A. 13<br>\
B. -3<br>\
C. 3<br>\
D. -13";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11,
			time : '08:22',
			html : function(controllers) {
				var html = '<h2>Evaluate |-10| &divide; 2 x |5|<br>What is the first order of operation?</h2>';
				var choices = [
					'A. Find the absolute values of -10 and 5', 
					'B. Divide -10 by 2',
					'C. Multiply 2 by 5',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. Find the absolute values of -10 and 5',
			question : 'What is the first order of operation?',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "Evaluate |-10| &divide; 2 x |5|<br><br>\
A. Find the absolute values of -10 and 5<br>\
B. Divide -10 by 2<br>\
C. Multiply 2 by 5";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 12,
			time : '08:52',
			html : function(controllers) {
				var html = '<h2>Find the absolute values of -10 and 5</h2>';
				html += '<p>|-10| = <input type="text" class="answer-box" maxlength="2" /></p>';
				html += '<p>|-5| = <input type="text" class="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : [10,5],
			answer_text : '10 and 5',
			question : ' Find the absolute values of -10 and 5',
			type : 'input-box', // input-box
			details : function() {
				var html = "";
				html += '|-10| = _______<br>';
				html += '|-5| = _______';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);

			},
		},
		{	quiz_number : 13,
			time : '09:36',
			html : function(controllers) {
				var html = '<h2>Evaluate |-10| &divide; 2 x |5|<br>Which of the following is the second order of operation?</h2>';
				var choices = [
					'A. Divide 10 by 2', 
					'B. Multiply 10 by 5',
					'C. Multiply 2 by 5',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'Divide 10 by 2',
			question : 'Which of the following is the second order of operation?',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "Evaluate |-10| &divide; 2 x |5|<br><br>A. Divide 10 by 2<br>\
B. Multiply 10 by 5<br>\
C. Multiply 2 by 5";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {

				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 14,
			time : '10:15',
			html : function(controllers) {
				var html = '<h2>What is the value of the expression?</h2>';
				html += '<h2>|-10| &divide 2 x |5|</h2>';
				html += '<p><input type="text" id="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 25,
			answer_text : 25,
			question : 'What is the value of the expression?',
			type : 'input-box', // input-box
			details : function() {
				var html = "|-10| &divide 2 x |5|";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				
			},
		},
		{	quiz_number : 15,
			time : '11:19',
			html : function(controllers) {
				var html = '<h2>Evaluate |-5| x |-10| + 20 - |-40| &divide; 8<br>Which of the following is the first order of operation?</h2>';
				var choices = [
					'A. Multiply', 
					'B. Divide',
					'C. Add',
					'D. Subtract',
					'E. Evaluate numbers inside the grouping symbols'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 4,
			answer_text : 'E. Evaluate numbers inside the grouping symbols',
			question : 'Which of the following is the first order of operation?',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "Evaluate |-5| x |-10| + 20 - |-40| &divide; 8";
				html += "<br><br>A. Multiply<br>\
B. Divide<br>\
C. Add<br>\
D. Subtract<br>\
E. Evaluate numbers inside the grouping symbols";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {

				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 16,
			time : '11:43',
			html : function(controllers) {
				var html = '<h2>Find the absolute values of the following numbers.</h2>';
				html += '<p>|-5| = <input type="text" class="answer-box" maxlength="2" /></p>';
				html += '<p>|-10| = <input type="text" class="answer-box" maxlength="2" /></p>';
				html += '<p>|-40| = <input type="text" class="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : [5,10,40],
			answer_text : '5, 10 and 40',
			question : 'Find the absolute values of the following numbers.',
			type : 'input-box', // input-box
			details : function() {
				var html = "|-5| = _______<br>|-10| = _______ <br>|-40| = _______";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);

			},
		},
		{	quiz_number : 17,
			time : '12:41',
			html : function(controllers) {
				var html = '<p>After evaluating the numbers inside the grouping symbols, the expression becomes 5 x 10 + 20 - 40 &divide; 8</p>';
				html += '<h2>What is the second order of operation?</h2>';
				var choices = [
					'A. Add', 
					'B. Subtract', 
					'C. Multiply',
					'D. Divide',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. Multiply',
			question : 'What is the second order of operation?',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "After evaluating the numbers inside the grouping symbols, <br>the expression becomes 5 x 10 + 20 - 40 &divide; 8<br><br>";
				html += "A. Add<br>\
B. Subtract<br>\
C. Multiply<br>\
D. Divide";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {

				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 18,
			time : '13:05',
			html : function(controllers) {
				var html = '<h2>Evaluate 5 x 10 + 20 - 40 &divide; 8</h2>';
				html += '<p><input type="text" id="answer-box" maxlength="2" /> + 20 - 40 &divide; 8</p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 50,
			answer_text : 50,
			question : 'Evaluate 5 x 10 + 20 -40 &divide; 8',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "_______  + 20 - 40 &divide; 8";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);	
			},
		},
		{	quiz_number : 19,
			time : '13:40',
			html : function(controllers) {
				var html = '<h2>After multiplying, the expression is <br>50 + 20 - 40 &divide; 8<br>What is the 3rd order of operation?</h2>';
				var choices = [
					'A. Addition', 
					'B. Subtraction',
					'C. Division',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. Division',
			question : 'What is the 3rd order of operation?',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "After multiplying, the expression is 50 + 20 - 40 &divide; 8<br><br>";
				html += "A. Addition<br>\
B. Subtraction<br>\
C. Division";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {

				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 20,
			time : '14:05',
			html : function(controllers) {
				var html = '<h2>Evaluate 50 + 20 - 40 &divide; 8</h2>';
				html += '<p>50 + 20 - <input type="text" id="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 5,
			answer_text : 5,
			question : 'Evaluate 50 + 20 - 40 &divide; 8',
			type : 'input-box', // input-box
			details : function() {
				var html = "50 + 20 - _______";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);	
			},
		},
		{	quiz_number : 21,
			time : '14:33',
			html : function(controllers) {
				var html = '<h2>After division, expression becomes <br>50 + 20 - 5<br>What is the value of the expression <br>50 + 20 - 5?</h2>';
				html += '<p><input type="text" id="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 65,
			answer_text : 65,
			question : 'What is the value of the expression 50 + 20 - 5?',
			type : 'input-box', // input-box
			details : function() {
				var html = "After division, expression becomes 50 + 20 - 5";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);				
			},
		},
		{	quiz_number : 22,
			time : '16:58',
			html : function(controllers) {
				var html = '<h2>Evaluate |3|<sup>2</sup> + 5 x |-2| - 10<br>What is the first order of operation?</h2>';
				var choices = [
					'A. Find the absolute values', 
					'B. Evaluate the powers',
					'C. Multiply',
					'D. Add'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. Find the absolute values',
			question : 'What is the first order of operation?',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "Evaluate |3|<sup>2</sup> + 5 x |-2| - 10<br><br>";
				html += "A. Find the absolute values<br>\
B. Evaluate the powers<br>\
C. Multiply<br>\
D. Add";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {

				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 23,
			time : '17:28',
			html : function(controllers) {
				var html = '<h2>Find the absolute values of <br>|3| and |-2|</h2>';
				html += '<p>|3| = <input type="text" class="answer-box" maxlength="2" /></p>';
				html += '<p>|-2| = <input type="text" class="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : [3,2],
			answer_text : '3 and 2',
			question : 'Find the absolute values of |3| and |-2|',
			type : 'input-box', // input-box
			details : function() {
				var html = "|3| = _______<br>|-2| = _______";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);

			},
		},
		{	quiz_number : 24,
			time : '18:07',
			html : function(controllers) {
				var html = '<h2>After finding the absolute value, what would be the second order of operation? <br> 3<sup>2</sup> + 5 x 2 - 10</h2>';
				var choices = [
					'A. Multipy', 
					'B. Add',
					'C. Evaluate the power',
					'D. Subtract'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. Evaluate the power',
			question : 'After finding the absolute value, what would be the second order of operation?',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "3<sup>2</sup> + 5 x 2 - 10<br><br>\
A. Multipy<br>\
B. Add<br>\
C. Evaluate the power<br>\
D. Subtract";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {

				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 25,
			time : '18:40',
			html : function(controllers) {
				var html = '<h2>What is the value of 9 + 5 x 2 - 10?</h2>';
				var choices = [
					'A. 14', 
					'B. 9',
					'C. 7',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 9',
			question : 'What is the value of 9 + 5 x 2 - 10?',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "A. 14<br>\
B. 9<br>\
C. 7";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 26,
			time : '19:30',
			html : function(controllers) {
				var html = '<h2>What do you call the numbers that have the same distance from zero on a number line?</h2>';
				var choices = [
					'A. Zero Values', 
					'B. Greater Values',
					'C. Absolute Values',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. Absolute Values',
			question : 'What do you call the numbers that have the same distance from zero on a number line?',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "A. Zero Values<br>\
B. Greater Values<br>\
C. Absolute Values";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {

				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
			end_session : 1,
		},
		
	],
};
