var G4C1L2 = {
	title : 'Numbers through Millions',
	duration : '17:30',
	data : [
		{	quiz_number : 1,
			time : '04:40',
			html : function(controllers) {
				var html = '<h2>How do you read this number? <br><strong>594 341 921</strong></h2>';
				var choices = [
					'A. five hundred ninety-four million, three hundred forty-one thousand, nine hundred twenty-one', 
					'B. five hundred ninety-four million, three hundred forty-one thousand, nine hundred twelve',
					'C. five hundred ninety-four thousand, three hundred forty-one thousand, nine hundred twelve'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. five hundred ninety-four million, three hundred forty-one thousand, nine hundred twenty-one',
			question : 'How do you read this number? <br><strong>594 341 921</strong>',
			type : 'multiple-choice',
			details : function() {
				var html = "A. five hundred ninety-four million, three hundred forty-one thousand, nine hundred twenty-one<br>\
B. five hundred ninety-four million, three hundred forty-one thousand, nine hundred twelve<br>\
C. five hundred ninety-four thousand, three hundred forty-one thousand, nine hundred twelve";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2,
			time : '04:59', 
			html : function(controllers) {
				var html = '<h2>What is the place value of the underlined digit of the number? <br><strong>59<u>4</u> 341 921</strong></h2>';
				html += '<p><label>Place Value</label><select class="answer-box">';
					html += '<option>ones</option>';
					html += '<option>tens</option>';
					html += '<option>hundreds</option>';
					html += '<option>thousands</option>';
					html += '<option>ten thousands</option>';
					html += '<option>hundred thousands</option>';
					html += '<option>millions</option>';
					html += '<option>ten millions</option>';
					html += '<option>hundred millions</option>';
				html += '</select></p>';
				html += '<p><label>Value</label><input type="text" class="answer-box" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : ['millions',4000000],
			answer_text : 'millions and 4000000',
			question : 'What is the place value of the underlined digit of the number? <br><strong>59<u>4</u> 341 921</strong>',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '05:46',
			html : function(controllers) {
				var html = '<h2>How do you read this number? <br><strong>674 941 105</strong></h2>';
				var choices = [
					'A. six hundred seventy-four billion, nine hundred forty-one thousand, one hundred five', 
					'B. six hundred seventy-four thousand, nine hundred forty-one thousand, one hundred five',
					'C. six hundred seventy-four million, nine hundred forty-one thousand, one hundred five'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. six hundred seventy-four million, nine hundred forty-one thousand, one hundred five',
			question : 'How do you read this number? <br><strong>674 941 105</strong>',
			type : 'multiple-choice',
			details : function() {
				var html = "A. six hundred seventy-four billion, nine hundred forty-one thousand, one hundred five<br>\
B. six hundred seventy-four thousand, nine hundred forty-one thousand, one hundred five<br>\
C. six hundred seventy-four million, nine hundred forty-one thousand, one hundred five";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '06:05', 
			html : function(controllers) {
				var html = '<h2>What is the place value of the underlined digit of the number? <br><strong>6<u>7</u>4 941 105</strong></h2>';
				html += '<p><label>Place Value</label><select class="answer-box">';
					html += '<option>ones</option>';
					html += '<option>tens</option>';
					html += '<option>hundreds</option>';
					html += '<option>thousands</option>';
					html += '<option>ten thousands</option>';
					html += '<option>hundred thousands</option>';
					html += '<option>millions</option>';
					html += '<option>ten millions</option>';
					html += '<option>hundred millions</option>';
				html += '</select></p>';
				html += '<p><label>Value</label><input type="text" class="answer-box" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : ['ten millions',70000000],
			answer_text : 'ten millions and 70000000',
			question : 'What is the place value of the underlined digit of the number? <br><strong>6<u>7</u>4 941 105</strong>',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5,
			time : '06:53',
			html : function(controllers) {
				var html = '<h2>How do you read this number? <br><strong>910 416 748</strong></h2>';
				var choices = [
					'A. ninety one million, four hundred sixteen thousand, seven hundred forty-eight',
					'B. nine hundred ten million, four hundred sixteen thousand, seven hundred forty-eight',
					'C. nine hundred ten thousand, four hundred sixteen thousand, seven hundred forty-eight'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. nine hundred ten million, four hundred sixteen thousand, seven hundred forty-eight',
			question : 'How do you read this number? <br><strong>910 416 748</strong>',
			type : 'multiple-choice',
			details : function() {
				var html = "A. ninety one million, four hundred sixteen thousand, seven hundred forty-eight<br>\
B. nine hundred ten million, four hundred sixteen thousand, seven hundred forty-eight<br>\
C. nine hundred ten thousand, four hundred sixteen thousand, seven hundred forty-eight";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 6,
			time : '07:13', 
			html : function(controllers) {
				var html = '<h2>What is the place value of the underlined digit of the number? <br><strong><u>9</u>10 416 748</strong></h2>';
				html += '<p><label>Place Value</label><select class="answer-box">';
					html += '<option>ones</option>';
					html += '<option>tens</option>';
					html += '<option>hundreds</option>';
					html += '<option>thousands</option>';
					html += '<option>ten thousands</option>';
					html += '<option>hundred thousands</option>';
					html += '<option>millions</option>';
					html += '<option>ten millions</option>';
					html += '<option>hundred millions</option>';
				html += '</select></p>';
				html += '<p><label>Value</label><input type="text" class="answer-box" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : ['hundred millions',900000000],
			answer_text : 'hundred millions and 900000000',
			question : 'What is the place value of the underlined digit of the number? <br><strong><u>9</u>10 416 748</strong>',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time : '08:00',
			html : function(controllers) {
				var html = '<h2>How do you read this number? <br><strong>12 479 428</strong></h2>';
				var choices = [
					'A. twelve million, four hundred seventy-nine, four hundred twenty-eight',
					'B. twelve thousand, four hundred seventy-nine, thousand, four hundred twenty-eight',
					'C. twelve million, four hundred seventy-nine thousand, four hundred twenty-eight'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. twelve million, four hundred seventy-nine thousand, four hundred twenty-eight',
			question : 'How do you read this number? <br><strong>12 479 428</strong>',
			type : 'multiple-choice',
			details : function() {
				var html = "A. twelve million, four hundred seventy-nine, four hundred twenty-eight<br>\
B. twelve thousand, four hundred seventy-nine, thousand, four hundred twenty-eight<br>\
C. twelve million, four hundred seventy-nine thousand, four hundred twenty-eight";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '08:20', 
			html : function(controllers)  {
				var html = '<h2>What is the place value of the underlined digit of the number? <br><strong><u>1</u>2 479 428</strong></h2>';
				html += '<p><label>Place Value</label><select class="answer-box autofocus">';
					html += '<option>ones</option>';
					html += '<option>tens</option>';
					html += '<option>hundreds</option>';
					html += '<option>thousands</option>';
					html += '<option>ten thousands</option>';
					html += '<option>hundred thousands</option>';
					html += '<option>millions</option>';
					html += '<option>ten millions</option>';
					html += '<option>hundred millions</option>';
				html += '</select></p>';
				html += '<p><label>Value</label><input type="text" class="answer-box" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : ['ten millions',10000000],
			answer_text : 'ten millions and 10000000',
			question : 'What is the place value of the underlined digit of the number? <br><strong><u>1</u>2 479 428</strong>',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '09:08',
			html : function(controllers) {
				var html = '<h2>How do you read this number? <br><strong>6 783 184</strong></h2>';
				var choices = [
					'A. six million, seven hundred eighty-three thousand, one hundred eighty-four',
					'B. six hundred thousand, seven hundred eighty-three thousand, one hundred eighty-four',
					'C. six thousand, seven hundred eighty-three thousand, one hundred eighty-four'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. six million, seven hundred eighty-three thousand, one hundred eighty-four',
			question : 'How do you read this number? <br><strong>6 783 184</strong>',
			type : 'multiple-choice',
			details : function() {
				var html = "A. six million, seven hundred eighty-three thousand, one hundred eighty-four<br>\
B. six hundred thousand, seven hundred eighty-three thousand, one hundred eighty-four<br>\
C. six thousand, seven hundred eighty-three thousand, one hundred eighty-four";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '09:28', 
			html : function(controllers)  {
				var html = '<h2>What is the place value of the underlined digit of the number? <br><strong><u>6</u> 783 184</strong></h2>';
				html += '<p><label>Place Value</label><select class="answer-box">';
					html += '<option>ones</option>';
					html += '<option>tens</option>';
					html += '<option>hundreds</option>';
					html += '<option>thousands</option>';
					html += '<option>ten thousands</option>';
					html += '<option>hundred thousands</option>';
					html += '<option>millions</option>';
					html += '<option>ten millions</option>';
					html += '<option>hundred millions</option>';
				html += '</select></p>';
				html += '<p><label>Value</label><input type="text" class="answer-box" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : ['millions',6000000],
			answer_text : 'millions and 6000000',
			question : 'What is the place value of the underlined digit of the number? <br><strong><u>6</u> 783 184</strong>',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11,
			time : '10:04',
			html : function(controllers) {
				var html = '<h2><strong>Five hundred thirty-eight million, seven hundred eighty-two thousand, six hundred fifteen</strong> in standard form is:</h2>';
				var choices = [
					'A. 538 782 600',
					'B. 538 782 615',
					'C. 5 3087 802 615',
					'D. 5 387 820 615',
					'E. 538 782 516'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 538 782 615',
			question : '<strong>Five hundred thirty-eight million, seven hundred eighty-two thousand, six hundred fifteen</strong> in standard form is:',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 538 782 600<br>B. 538 782 615<br>C. 5 3087 802 615<br>D. 5 387 820 615<br>E. 538 782 516";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 12,
			time : '10:43',
			html : function(controllers) {
				var html = '<h2><strong>10 000 000 + 2 000 000 + 300 000 + 10 000 + 9 000 + 200 + 90 + 8</strong> in standard form is what?</h2>';
				var choices = [
					'A. 12 319 298',
					'B. 120 319 298',
					'C. 12 913 892',
					'D. 21 482 298'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 12 319 298',
			question : '<strong>10 000 000 + 2 000 000 + 300 000 + 10 000 + 9 000 + 200 + 90 + 8</strong> in standard form is what?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 12 319 298<br>B. 120 319 298<br>C. 12 913 892<br>D. 21 482 298";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 13,
			time : '11:21',
			html : function(controllers) {
				var html = '<h2><strong>seventeen million, four hundred eighty-nine thousand, five hundred eight</strong> in standard form is what?</h2>';
				var choices = [
					'A. 170 489 508',
					'B. 17 489 508',
					'C. 17 489 580',
					'D. 170 489 588'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 17 489 508',
			question : '<strong>seventeen million, four hundred eighty-nine thousand, five hundred eight</strong> in standard form is what?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 170 489 508<br>B. 17 489 508<br>C. 17 489 580<br>D. 170 489 588";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 14,
			time : '12:03',
			html : function(controllers) {
				var html = '<h2><strong>100 000 000 + 20 000 000 + 900 000 + 80 000 + 7 000 + 300 + 40 + 5</strong> in standard form is what? Be Carefull!</h2>';
				var choices = [
					'A. 12 987 345',
					'B. 1 230 456 789',
					'C. 120 987 345',
					'D. 1 239 870 345'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 120 987 345',
			question : '<strong>100 000 000 + 20 000 000 + 900 000 + 80 000 + 7 000 + 300 + 40 + 5</strong> in standard form is what? Be Carefull!',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 12 987 345<br>B. 1 230 456 789<br>C. 120 987 345<br>D. 1 239 870 345";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 15,
			time : '13:20',
			html : function(controllers) {
				var html = '<h2>So what does it mean when the millions place is missing?</h2>';
				var choices = [
					'A. It means we have to move everything up by 1 place value',
					'B. It means we have 0 000 000 for the missing term',
					'C. It means we have to move everything down by 1 place value'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. It means we have 0 000 000 for the missing term',
			question : 'So what does it mean when the millions place is missing?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. It means we have to move everything up by 1 place value<br>\
B. It means we have 0 000 000 for the missing term<br>\
C. It means we have to move everything down by 1 place value";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 16,
			time : '13:47',
			html : function(controllers) {
				var html = '<h2><strong>seven hundred million, fifty thousand</strong> in standard form is what?</h2>';
				var choices = [
					'A. 700 050 000',
					'B. 70 050 000',
					'C. 750 000 000',
					'D. 700 500 00'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 700 050 000',
			question : '<strong>seven hundred million, fifty thousand</strong> in standard form is what?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 700 050 000<br>B. 70 050 000<br>C. 750 000 000<br>D. 700 500 00";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 17,
			time : '14:40',
			html : function(controllers) {
				var html = '<h2><strong>942 348 748</strong> when written in words is:</h2>';
				var choices = [
					'A. Nine hundred forty-two million, three hundred forty-eight thousand, seven hundred forty-eight',
					'B. Nine hundred forty-two billion, two hundred forty-eight thousand, seven hundred forty',
					'C. Nine hundred forty-two thousand, three hundred forty-eight thousand, seventy-eight',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. Nine hundred forty-two million, three hundred forty-eight thousand, seven hundred forty-eight',
			question : '<strong>942 348 748</strong> when written in words is:',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Nine hundred forty-two million, three hundred forty-eight thousand, seven hundred forty-eight<br>\
B. Nine hundred forty-two billion, two hundred forty-eight thousand, seven hundred forty<br>\
C. Nine hundred forty-two thousand, three hundred forty-eight thousand, seventy-eight";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 18,
			time : '15:21',
			html : function(controllers) {
				var html = '<h2><strong>100 248 098</strong> when written in words is:</h2>';
				var choices = [
					'A. One hundred million, two hundred forty-eight thousand, nine hundred eight',
					'B. One hundred million, two hundred thousand, ninety-eight',
					'C. One hundred million, two hundred forty-eight thousand, ninety-eight',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. One hundred million, two hundred forty-eight thousand, ninety-eight',
			question : '<strong>100 248 098</strong> when written in words is:',
			type : 'multiple-choice',
			details : function() {
				var html = "A. One hundred million, two hundred forty-eight thousand, nine hundred eight<br>\
B. One hundred million, two hundred thousand, ninety-eight<br>\
C. One hundred million, two hundred forty-eight thousand, ninety-eight";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 19,
			time : '16:02',
			html : function(controllers) {
				var html = '<h2><strong>75 092 843</strong> when written in words is:</h2>';
				var choices = [
					'A. Seventy million, ninety-two thousand, eight hundred forty-three',
					'B. Seventy-five million, ninety-two thousand, eight hundred forty-three',
					'C. Seventy-five million, ninety-two hundred thousand, eight hundred forty-three',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. Seventy-five million, ninety-two thousand, eight hundred forty-three',
			question : '<strong>75 092 843</strong> when written in words is:',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Seventy million, ninety-two thousand, eight hundred forty-three<br>\
B. Seventy-five million, ninety-two thousand, eight hundred forty-three<br>\
C. Seventy-five million, ninety-two hundred thousand, eight hundred forty-three";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 20,
			time : '16:40',
			html : function(controllers) {
				var html = '<h2><strong>850 100 009</strong> when written in words is:</h2>';
				var choices = [
					'A. Eight hundred million, five hundred thousand, one hundred nine',
					'B. Eight hundred fifty million, one hundred thousand, nine',
					'C. Eight hundred fifty million, one hundred thousand, ninety',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. Eight hundred fifty million, one hundred thousand, nine',
			question : '<strong>850 100 009</strong> when written in words is:',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Eight hundred million, five hundred thousand, one hundred nine<br>\
B. Eight hundred fifty million, one hundred thousand, nine<br>\
C. Eight hundred fifty million, one hundred thousand, ninety";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 21,
			time : '17:11',
			html : function(controllers) {
				var html = '<h2><strong>400 000 020</strong> when written in words is:</h2>';
				var choices = [
					'A. Four hundred million, twenty',
					'B. Four hundred million, two',
					'C. Forty million, twenty',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. Four hundred million, twenty',
			question : '<strong>400 000 020</strong> when written in words is:',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Four hundred million, twenty<br>\
B. Four hundred million, two<br>\
C. Forty million, twenty";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
			end_session : 1,
		},
	],
};
