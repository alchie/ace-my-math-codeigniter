var G2C3L1 = {
	title : 'Adding Tens',
	duration : '16:28',
	data : [
		{	quiz_number : 1,
			time : '00:23',
			html : function(controllers) {
				var html = '<h2>3 + 5 = <input type="text" id="answer-box" maxlength="2" /></h2>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 8,
			answer_text : 8,
			question : '3 + 5 = _____ ?',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2,
			time : '00:43',
			html : function(controllers) {
				var html = '<h2>30 + 50 = <input type="text" id="answer-box" maxlength="2" /></h2>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 80,
			answer_text : 80,
			question : '30 + 50 = _____ ?',
			type : 'input-box', 
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '01:42',
			html : function(controllers) {
				var html = '<h2>7 tens + 2 tens = <input type="text" id="answer-box" maxlength="2" /> tens</h2>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 9,
			answer_text : 9,
			question : '7 tens + 2 tens = _____ tens',
			type : 'input-box', 
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '02:00',
			html : function(controllers) {
				var html = '<h2>7 tens is equal to what number?</h2>';
				html += '<p><input type="text" id="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 70,
			answer_text : 70,
			question : '7 tens is equal to what number?',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5,
			time : '02:13',
			html : function(controllers) {
				var html = '<h2>How about 2 tens?</h2>';
				html += '<p><input type="text" id="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 20,
			answer_text : 20,
			question : 'How about 2 tens?',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 6,
			time : '02:38',
			html : function(controllers) {
				var html = '<h2>70 + 20 = <input type="text" id="answer-box" maxlength="2" /></h2>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 90,
			answer_text : 90,
			question : '70 + 20 = _____',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time : '03:26',
			html : function(controllers) {
				var html = '<h2>40 + 30 = <input type="text" id="answer-box" maxlength="2" /> ?</h2>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 70,
			answer_text : 70,
			question : '40 + 30 = _____ ?',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '03:54',
			html : function(controllers) {
				var html = '<h2>20 + 20 = <input type="text" id="answer-box" maxlength="2" /> ?</h2>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 40,
			answer_text : 40,
			question : '20 + 20 = _____ ?',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '04:44',
			html : function(controllers) {
				var html = '<h2>Gina collected 30 candies after trick-or-treating. Mom gave her additional 10 candies. How much candies does Gina have now?</h2>';
				var choices = [
					'A. 20 candies', 
					'B. 30 candies',
					'C. 40 candies',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 40 candies',
			question : 'How much candies does Gina have now?',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "Gina collected 30 candies after trick-or-treating. Mom gave her additional 10 candies.<br><br>A. 20 candies<br>B. 30 candies<br>C. 40 candies";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '05:52',
			html : function(controllers) {
				var html = '<h2>What is the sum of 15 + 4?</h2>';
				html += '<p><input type="text" id="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 19,
			answer_text : 19,
			question : 'What is the sum of 15 + 4?',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11,
			time : '06:52',
			html : function(controllers) {
				var html = '<h2>15 + 40 = <input type="text" id="answer-box" maxlength="2" /> ?</h2>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 55,
			answer_text : 55,
			question : '15 + 40 = _____ ?',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 12,
			time : '07:21',
			html : function(controllers) {
				var html = '<h2>What is the next number after 15 if we count by ones 10 times?</h2>';
				html += '<img style="float:right;width:70%" src="/assets/images/lessons/G2C3L1_matrix1.png">';
				html += '<p><input type="text" id="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				
				return html;
			},
			answer : 25,
			answer_text : 25,
			question : 'What is the next number after 15 if we count by ones 10 times?',
			type : 'input-box',
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C3L1_matrix1.png">';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 13,
			time : '08:16',
			html : function(controllers) {
				var html = '<h2>How many tens is 40? <input type="text" id="answer-box" maxlength="2" /> tens</h2>';
				html += '<img style="float:right;width:70%;" src="/assets/images/lessons/G2C3L1_matrix1.png">';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				
				return html;
			},
			answer : 4,
			answer_text : 4,
			question : 'How many tens is 40? _____ tens',
			type : 'input-box',
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C3L1_matrix1.png">';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 14,
			time : '09:56',
			html : function(controllers) {
				var html = '<h2>If we add 3 + 30, How many levels will we go down after 3?</h2>';
				html += '<img style="float:right;width:70%" src="/assets/images/lessons/G2C3L1_matrix1.png">';
				var choices = [
					'A. 3 times', 
					'B. 2 times',
					'C. 1 time',
				];
				html += controllers.button_choices(choices);
				
				return html;
			},
			answer : 0,
			answer_text : 'A. 3 times',
			question : 'If we add 3 + 30, How many levels will we go down after 3?',
			type : 'multiple-choice', 
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C3L1_matrix1.png">';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 15,
			time : '10:51',
			html : function(controllers) {
				var html = '<h2>How many tens do we need to count to find the sum of 70 + 27?</h2>';
				html += '<img style="float:right;width:70%" src="/assets/images/lessons/G2C3L1_matrix1.png">';
				html += '<p><input type="text" id="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				
				return html;
			},
			answer : 7,
			answer_text : 7,
			question : 'How many tens do we need to count to find the sum of 70 + 27?',
			type : 'input-box',
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C3L1_matrix1.png">';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 16,
			time : '11:16',
			html : function(controllers) {
				var html = '<h2>What is the sum of 70 + 27?</h2>';
				html += '<img style="float:right;width:70%;" src="/assets/images/lessons/G2C3L1_matrix1.png">';
				html += '<p><input type="text" id="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				
				return html;
			},
			answer : 97,
			answer_text : 97,
			question : 'What is the sum of 70 + 27?',
			type : 'input-box',
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C3L1_matrix1.png">';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 17,
			time : '12:45',
			html : function(controllers) {
				var html = '<h3>During a fundraising activity, \
				Glenda Sold 43 servings of lemonade while John sold \
				30 servings of lemonade. How many servings of lemonade \
				did they sell?</h3><h2>43 + 30 = _____<h2>';
				var choices = [
					'A. 63', 
					'B. 73',
					'C. 83',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 73',
			question : 'How many servings of lemonade \
				did they sell?',
			type : 'multiple-choice',
			details : function() {
				var html = "During a fundraising activity, \
				Glenda Sold 43 servings of lemonade while John sold \
				30 servings of lemonade. <br><br>43 + 30 = _____";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 18,
			time : '14:00',
			html : function(controllers) {
				var html = '<h2>How many tens are there in 50?</h2>';
				html += '<p><input type="text" id="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 5,
			answer_text : 5,
			question : 'How many tens are there in 50?',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 19,
			time : '14:14',
			html : function(controllers) {
				var html = '<h2>30 + 60 = <input type="text" id="answer-box" maxlength="2" />?</h2>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 90,
			answer_text : 90,
			question : '30 + 60 = _____ ?',
			type : 'input-box', 
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				
			},
		},
		{	quiz_number : 20,
			time : '15:16',
			html : function(controllers) {
				var html = '<h2>36 + 50 = <input type="text" id="answer-box" maxlength="2" />?</h2>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 86,
			answer_text : 86,
			question : '36 + 50 = _____?',
			type : 'input-box', 
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
			end_session : 1,
		},
	],
};
