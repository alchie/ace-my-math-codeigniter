var G2C2L4 = {
	title : "Repeated Addition",
	duration : "11:34",
	data : [
		{	quiz_number : 1,
			time : '00:17',
			html : function(controllers) {
				var html = '<h2>How many balls can you see in the picture?</h2>';
				html += '<p><input type="text" id="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				html += '<img src="/assets/images/lessons/G2C2L4_balls1.png">';
				return html;
			},
			answer : 12,
			answer_text : 12,
			question : 'How many balls can you see in the picture?',
			type : 'input-box', 
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C2L4_balls1.png">';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2,
			time : '00:38',
			html : function(controllers) {
				var html = '<h2>How many groups of balls can you see?</h2>';
				var choices = [
					'A. 4', 
					'B. 3',
					'C. 2',
				];
				html += controllers.button_choices(choices);
				html += '<img src="/assets/images/lessons/G2C2L4_balls1.png">';
				return html;
			},
			answer : 1,
			answer_text : 'B. 3',
			question : 'How many groups of balls can you see?',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C2L4_balls1.png">';
				html += "<br><br>A. 4<br>B. 3<br>C. 2<br>";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '01:03',
			html : function(controllers) {
				var html = '<h2>How many balls are there in each group?</h2>';
				html += '<p><img src="/assets/images/lessons/G2C2L4_balls1.png"></p>';
				var choices = [
					'A. 4', 
					'B. 3',
					'C. 2',
				];
				html += controllers.button_choices(choices);
				
				return html;
			},
			answer : 0,
			answer_text : 'A. 4',
			question : 'How many balls are there in each group?',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C2L4_balls1.png">';
				html += "<br><br>A. 4<br>B. 3<br>C. 2<br>";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '01:46',
			html : function(controllers) {
				var html = '<h2>How many times did we add 4?</h2>';
				html += '<p><img src="/assets/images/lessons/G2C2L4_balls1.png"></p>';
				html += '<p><input type="text" id="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				
				return html;
			},
			answer : 3,
			answer_text : 3,
			question : 'How many times did we add 4?',
			type : 'input-box', // input-box
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C2L4_balls1.png">';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5,
			time : '02:16',
			html : function(controllers) {
				var html = '<h2>How many group of two\'s can you see?</h2>';
				html += '<p><img src="/assets/images/lessons/G2C2L4_box1.png"></p>';
				html += '<p><input type="text" id="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 4,
			answer_text : 4,
			question : 'How many group of two\'s can you see?',
			type : 'input-box', 
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C2L4_box1.png">';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 6,
			time : '02:50',
			html : function(controllers) {
				var html = '<h2>What number sentence can we write from the grouping made?</h2>';
				html += '<p><img src="/assets/images/lessons/G2C2L4_box1.png"></p>';
				var choices = [
					'A. 2 + 2 + 2 + 2 = 8', 
					'B. 4 + 4 = 8',
					'C. 2 + 6 = 8',
				];
				html += controllers.button_choices(choices);
				
				return html;
			},
			answer : 0,
			answer_text : 'A. 2 + 2 + 2 + 2 = 8',
			question : 'What number sentence can we write from the grouping made?',
			type : 'multiple-choice', 
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C2L4_box1.png">';
				html += "<br><br>A. 2 + 2 + 2 + 2 = 8<br>B. 4 + 4 = 8<br>C. 2 + 6 = 8";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time : '03:42',
			html : function(controllers) {
				var html = '<h2>In the image shown, find the missing number.</h2>';
				html += '<p><img src="/assets/images/lessons/G2C2L4_crayons1.png"></p>';
				html += '<p>There are <input type="text" class="answer-box" maxlength="2" /> groups of <input type="text" class="answer-box" maxlength="2" /> crayons</p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : [3,5],
			answer_text : '3 and 5',
			question : 'In the image shown, find the missing number.',
			type : 'input-box', 
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C2L4_crayons1.png">';
				html += '<p>There are _____ groups of _____ crayons</p>';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '04:28',
			html : function(controllers) {
				var html = '<h2>Using repeated addition, write a number sentence to find the total number of pencils shown.</h2>';
				html += '<p><img src="/assets/images/lessons/G2C2L4_crayons1.png"></p>';
				var choices = [
					'A. 3 + 3 + 3 + 3 + 3 = 15', 
					'B. 5 + 5 + 5 = 15',
					'C. 10 + 5  = 15',
				];
				html += controllers.button_choices(choices);
				
				return html;
			},
			answer : 1,
			answer_text : 'B. 5 + 5 + 5 = 15',
			question : 'Using repeated addition, write a number sentence to find the total number of pencils shown.',
			type : 'multiple-choice',
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C2L4_crayons1.png">';
				html += "<p>A. 3 + 3 + 3 + 3 + 3 = 15<br>B. 5 + 5 + 5 = 15<br>C. 10 + 5  = 15</p>";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '05:17',
			html : function(controllers) {
				var html = '<h2>Now, write an addition sentence to show the number of objects in each group and find the total.</h2>';
				html += '<p><img src="/assets/images/lessons/G2C2L4_strawberry1.png"></p>';
				html += '<p><input style="width:60px" type="text" class="answer-box" maxlength="2" /> +\
				<input style="width:60px" type="text" class="answer-box" maxlength="2" /> +\
				<input style="width:60px" type="text" class="answer-box" maxlength="2" /> +\
				<input style="width:60px" type="text" class="answer-box" maxlength="2" /> +\
				<input style="width:60px" type="text" class="answer-box" maxlength="2" /> =\
				<input style="width:60px" type="text" class="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : [2,2,2,2,2,10],
			answer_text : '2 + 2 + 2 + 2 + 2 = 10',
			question : 'Now, write an addition sentence to show the number of objects in each group and find the total.',
			type : 'input-box',
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C2L4_strawberry1.png"><br><br>_____ + _____ + _____ + _____ + _____ = _____';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '05:36',
			html : function(controllers) {
				var html = '<h2>How many two\'s did we make?</h2>';
				html += '<p><img src="/assets/images/lessons/G2C2L4_strawberry1.png"></p>';
				html += '<p><input type="text" class="answer-box" maxlength="2" /> twos\
				 = <input type="text" class="answer-box" maxlength="2" /></p>';
				 html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : [5,10],
			answer_text : '5 twos = 10',
			question : 'How many two\'s did we make?',
			type : 'input-box',
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C2L4_strawberry1.png"><br><br> _____ twos = _____';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11,
			time : '06:16',
			html : function(controllers) {
				var html = '<h2>Fill in the missing numbers in the addition sentence.</h2>';
				html += '<p><img src="/assets/images/lessons/G2C2L4_sacks1.png"></p>';
				html += '<p><input style="width:60px" type="text" class="answer-box" maxlength="2" /> +\
				<input style="width:60px" type="text" class="answer-box" maxlength="2" /> +\
				<input style="width:60px" type="text" class="answer-box" maxlength="2" /> +\
				<input style="width:60px" type="text" class="answer-box" maxlength="2" /> +\
				<input style="width:60px" type="text" class="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : [4,4,4,4,4],
			answer_text : '4 + 4 + 4 + 4 + 4',
			question : 'Fill in the missing numbers in the addition sentence.',
			type : 'input-box', 
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C2L4_sacks1.png">';
				html += '<p>_____ + _____ + _____ + _____ + _____ </p>';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 12,
			time : '06:40',
			html : function(controllers) {
				var html = '<img src="/assets/images/lessons/G2C2L4_sacks1.png">';
				html += '<h2>5 groups of <input type="text" id="answer-box" maxlength="2" /></h2>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 4,
			answer_text : 4,
			question : '5 groups of _____',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C2L4_sacks1.png">';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 13,
			time : '06:55',
			html : function(controllers) {
				var html = '<img src="/assets/images/lessons/G2C2L4_sacks1.png">';
				html += '<h2>5 groups of 4 = <input type="text" id="answer-box" maxlength="2" /></h2>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 20,
			answer_text : 20,
			question : '5 groups of 4 = _____',
			type : 'input-box', 
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C2L4_sacks1.png">';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 14,
			time : '07:35',
			html : function(controllers) {
				var html = '<h2><img style="float:right;" src="/assets/images/lessons/G2C2L4_markers1.png">Markers come in packs of 2. If Jessie has 6 packs of markers, how many markers does she have in all?</h2>';
				var choices = [
					'A. 2', 
					'B. 6',
					'C. 12',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 12',
			question : 'Markers come in packs of 2. If Jessie has 6 packs of markers, how many markers does she have in all?',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = '<img class="float:right;" src="/assets/images/lessons/G2C2L4_markers1.png">';
				html += '<p>A. 2<br>B. 6<br>C. 12</p>';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 15,
			time : '09:06',
			html : function(controllers) {
				var html = '<h2>3 + 3 + 3 + 3 + 3 + 3 = 18<br>How many groups of 3\'s are present?</h2>';
				html += '<p><input type="text" id="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 6,
			answer_text : 6,
			question : 'How many groups of 3\'s are present?',
			type : 'input-box', // input-box
			details : function() {
				var html = "3 + 3 + 3 + 3 + 3 + 3 = 18";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
			end_session : 1,
		},
	],
};
