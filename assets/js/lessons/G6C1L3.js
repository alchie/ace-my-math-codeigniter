var G6C1L3 = {
		title : 'Ordering decimals',
		duration : '16:05',
		data : [
		{	quiz_number : 1,
			time : '01:58',
			html : function(controllers) {
				var html = '<p><img src="/assets/images/lessons/G6C1L3_img1.png" width="100%"></p>';
				html += '<h2>What is the role of zeroes?</h2>';
				var choices = [
					'a. they serve as place holders', 
					'b. they cancel out the other digits',
					'c. they serve no purpose, and they can be removed',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. they serve as place holders',
			question : 'What is the role of zeroes?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G6C1L3_img1.png" width="100%"></p>';
				html += "a. they serve as place holders<br>\
b. they cancel out the other digits<br>\
c. they serve no purpose, and they can be removed";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2,
			time : '02:29',
			html : function(controllers) {
				var html = '<p><img src="/assets/images/lessons/G6C1L3_img1.png" width="100%"></p>';
				html += '<h2>What should we do?</h2>';
				var choices = [
					'a. erase the other digits', 
					'b. add zeroes to the empty places',
					'c. distribute the digits for a number to have equal number digits',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. add zeroes to the empty places',
			question : 'What should we do?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G6C1L3_img1.png" width="100%"></p>';
				html += "a. erase the other digits<br>\
b. add zeroes to the empty places<br>\
c. distribute the digits for a number to have equal number digits";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '03:02',
			html : function(controllers) {
				var html = '<p><img src="/assets/images/lessons/G6C1L3_img2.png" width="100%"></p>';
				html += '<h2>What place value should we compare first?</h2>';
				var choices = [
					'a. ones', 
					'b. tenths',
					'c. hundredths'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. ones',
			question : 'What place value should we compare first?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G6C1L3_img2.png" width="100%"></p>';
				html += "a. ones<br>b. tenths<br>c. hundredths";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '03:32',
			html : function(controllers) {
				var html = '<p><img src="/assets/images/lessons/G6C1L3_img2.png" width="100%"></p>';
				html += '<h2>Which of those decimal numbers is the greatest?</h2>';
				var choices = [
					'a. 0.402', 
					'b. 0.42',
					'c. 4.02'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'c. 4.02',
			question : 'Which of those decimal numbers is the greatest?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G6C1L3_img2.png" width="100%"></p>';
				html += "a. 0.402<br>b. 0.42<br>c. 4.02";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5,
			time : '04:08',
			html : function(controllers) {
				var html = '<p><img src="/assets/images/lessons/G6C1L3_img2.png" width="100%"></p>';
				html += '<h2>What have you observe with these two numbers? 0.402 and 0.42</h2>';
				var choices = [
					'a. they have the same number of digits', 
					'b. they are equal',
					'c. they have the same ones and tenths digit'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'c. they have the same ones and tenths digit',
			question : 'What have you observe with these two numbers? 0.402 and 0.42',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G6C1L3_img2.png" width="100%"></p>';
				html += "a. they have the same number of digits<br>b. they are equal<br>\
c. they have the same ones and tenths digit";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 6,
			time : '04:32',
			html : function(controllers) {
				var html = '<p><img src="/assets/images/lessons/G6C1L3_img2.png" width="100%"></p>';
				html += '<h2>What should we compare next?</h2>';
				var choices = [
					'a. the hundredths digit', 
					'b. the thousandths digit',
					'c. no need to compare because they are the same'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. the hundredths digit',
			question : 'What should we compare next?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G6C1L3_img2.png" width="100%"></p>';
				html += "a. the hundredths digit<br>b. the thousandths digit<br>c. no need to compare because they are the same";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time : '05:04',
			html : function(controllers) {
				var html = '<p><img src="/assets/images/lessons/G6C1L3_img2.png" width="100%"></p>';
				html += '<h2>Which is greater, 0.402 or 0.42</h2>';
				var choices = [
					'a. 0.402', 
					'b. 0.42',
					'c. they are the same',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. 0.42',
			question : 'Which is greater, 0.402 or 0.42',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G6C1L3_img2.png" width="100%"></p>';
				html += "a. 0.402<br>b. 0.42<br>c. they are the same";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '06:31',
			html : function(controllers) {
				var html = '<h2>Which of them is CORRECT? Is it Agnes or Adam?</h2>';
				var choices = [
					'a. Adam', 
					'b. Agnes',
					'c. They are both correct.',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. Adam',
			question : 'Which of them is CORRECT? Is it Agnes or Adam?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. Adam<br>b. Agnes<br>c. They are both correct.";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '07:53',
			html : function(controllers) {
				var html = '<ol>';
				html += '<li>Compare the digits using the whole number part at first</li>';
				html += '<li>Place all the digits in a place value chart</li>';
				html += '<li>If the digits in the whole number part are equal, move to the digits after the decimal point</li>';
				html += '<li>Fill in all empty places with zeroes</li>';
				html += '</ol>';
				html += '<h2>The correct arrangement of steps in ordering of decimals is __________.</h2>';
				var choices = [
					'a. 1,2,3,4', 
					'b. 2,4,3,1',
					'c. 2,4,1,3'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'c. 2,4,1,3',
			question : 'The correct arrangement of steps in ordering of decimals is __________.',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>1. Compare the digits using the whole number part at first.<br>\
2. Place all the digits in a place value chart<br>\
3. If the digits in the whole number part are equal, move to the digits after the decimal point.<br>\
4. Fill in all empty places with zeroes</p>"; 
				html += "a. 1,2,3,4<br>b. 2,4,3,1<br>c. 2,4,1,3";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '10:10',
			html : function(controllers) {
				var html = '<h2>Is 1.56 greater than 1.056?</h2>';
				var choices = [
					'a. Yes', 
					'b. No',
					'c. It depends'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. Yes',
			question : 'Is 1.56 greater than 1.056?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. Yes<br>b. No<br>c. It depends";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11,
			time : '10:30',
			html : function(controllers) {
				var html = '<h2>Is 1.056 greater than 0.8?</h2>';
				var choices = [
					'a. Yes', 
					'b. No',
					'c. It depends'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. Yes',
			question : 'Is 1.056 greater than 0.8?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. Yes<br>b. No<br>c. It depends";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 12,
			time : '10:51',
			html : function(controllers) {
				var html = '<h2>Is 1.56 greater than 0.8?</h2>';
				var choices = [
					'a. Yes', 
					'b. No',
					'c. It depends',
					
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. Yes',
			question : 'Is 1.56 greater than 0.8?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. Yes<br>b. No<br>c. It depends";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 13,
			time : '12:01',
			html : function(controllers) {
				var html = '<h2>Which decimal numbers below are arranged in ASCENDING order?</h2>';
				var choices = [
					'a. 1.56, 1.056, 0.8', 
					'b. 0.8, 1.056, 1.56',
					'c. 1.056, 0.8, 1.56'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. 0.8, 1.056, 1.56',
			question : 'Which decimal numbers below are arranged in ASCENDING order?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. 1.56, 1.056, 0.8<br>b. 0.8, 1.056, 1.56<br>c. 1.056, 0.8, 1.56";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 14,
			time : '13:04',
			html : function(controllers) {
				var html = '<h2><strong style="font-size:80%">Jerry (110.6),  Amy (110.06),  John (106.1)</strong> <br> In which order will these pupils be in the line from shortest to tallest?</h2>';
				var choices = [
					'a. Jerry, John, Amy', 
					'b. Amy, John, Jerry',
					'c. John, Amy, Jerry'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'c. John, Amy, Jerry',
			question : '<strong>Jerry (110.6), Amy (110.06), John (106.1)</strong> - In which order will these pupils be in the line from shortest to tallest?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. Jerry, John, Amy<br>b. Amy, John, Jerry<br>c. John, Amy, Jerry";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 15,
			time : '14:26',
			html : function(controllers) {
				var html = '<h2>Comparing their whole number part, which of the three pupils has the least value?</h2>';
				var choices = [
					'a. Jerry (110.6)', 
					'b. Amy (110.06)',
					'c. John (106.1)',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'c. John (106.1)',
			question : 'Comparing their whole number part, which of the three pupils has the least value?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. Jerry (110.6)<br>b. Amy (110.06)<br>c. John (106.1)";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 16,
			time : '14:54',
			html : function(controllers) {
				var html = '<h2>Who has greater value in the whole number part between Jerry and Amy?</h2>';
				var choices = [
					'a. Jerry (110.6)', 
					'b. Amy (110.06)',
					'c. Both have the same value in the whole number part'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'c. Both have the same value in the whole number part',
			question : 'Who has greater value in the whole number part between Jerry and Amy?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. Jerry (110.6)<br>b. Amy (110.06)<br>c. Both have the same value in the whole number part";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 17,
			time : '15:31',
			html : function(controllers) {
				var html = '<h2>Look at the digits after the decimal point starting from the tenths place, which of the two has the greater value?</h2>';
				var choices = [
					'a. Jerry (110.6)', 
					'b. Amy (110.06)',
					'c. They both have the same value'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. Jerry (110.6)',
			question : 'Look at the digits after the decimal point starting from the tenths place, which of the two has the greater value?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. Jerry (110.6)<br>b. Amy (110.06)<br>c. They both have the same value";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
			end_session : 1,
		},
	],
};
