var G5C1L1 = {
	title : 'Decimal Place Value System',
	duration : '11:21',
	data : [
		{	quiz_number : 1,
			time : '01:42',
			html : function(controllers) { 	
				var html = '<h2>The DECIMAL PLACE VALUE SYSTEM, therefore,</h2>';
				var choices = [
					'A. is a system of counting', 
					'B. is in base 10, using the ten Hindu-Arabic Numerals',
					'C. identifies the value of a digit depending on its position in a number',
					'D. is all of the above'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 3,
			answer_text : 'D. is all of the above',
			question : 'The DECIMAL PLACE VALUE SYSTEM, therefore,',
			type : 'multiple-choice',
			details : function() {
				var html = "A. is a system of counting<br>\
B. is in base 10, using the ten Hindu-Arabic Numerals<br>\
C. identifies the value of a digit depending on its position in a number<br>\
D. is all of the above";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) { 	controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2,
			time : '03:40',
			html : function(controllers) {
				var html = '<h2>This time, the first wheel will tick \'0\' again. What do you think will happen?</h2>';
				html += '<img src="/assets/images/lessons/G5C1L1_wheel1.png" style="width:250px;float:right">';
				var choices = [
					'A. third wheel must come out', 
					'B. The 2nd wheel moves 1 step backward',
					'C. The 2nd wheel moves 1 step forward',
					'D. None of the above'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. The 2nd wheel moves 1 step forward',
			question : 'This time, the first wheel will tick \'0\' again. What do you think will happen?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G5C1L1_wheel1.png"></p>';
				html += "A. third wheel must come out<br>\
B. The 2nd wheel moves 1 step backward<br>\
C. The 2nd wheel moves 1 step forward<br>\
D. None of the above";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) { 	controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '04:37',
			html : function(controllers) {
				var html = '<h2>To show the next higher number, what should happen next?</h2>';
				var choices = [
					'A 3rd wheel must come out', 
					'The 2nd wheel moves 1 step backward',
					'The 2nd wheel moves 1 step forward',
					'The 1st wheel moves 1 step backward',
					'The 1st wheel moves 1 step forward'
				];
				for( i in choices ) { 		
					html += '<p><label><input class="g5c1l1q1b" type="checkbox" data-index="'+i+'" id="g5c1l1q1b'+i+'"> ' + choices[i] + '</label></p>';
				}
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : [0,2,4],
			answer_text : 'A 3rd wheel must come out<br>The 2nd wheel moves 1 step forward<br>The 1st wheel moves 1 step forward',
			question : 'To show the next higher number, what should happen next?',
			type : 'multiple-choice',
			details : function() {
				var html = "__ A 3rd wheel must come out<br>\
__ The 2nd wheel moves 1 step backward<br>\
__ The 2nd wheel moves 1 step forward<br>\
__ The 1st wheel moves 1 step backward<br>\
__ The 1st wheel moves 1 step forward";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) { 	controllers.checkbox_controls('g5c1l1q1b', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '05:48',
			html : function(controllers) {
				var html = '<h2>Before the wheel to the left moves, the previous wheel should have</h2>';
				var choices = [
					'A. moved 10 steps forward', 
					'B. moved 1 step forward',
					'C. moved 10 steps backward',
					'D. moved 1 step  backward'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. moved 10 steps forward',
			question : 'Before the wheel to the left moves, the previous wheel should have',
			type : 'multiple-choice',
			details : function() {
				var html = "A. moved 10 steps forward<br>\
B. moved 1 step forward<br>\
C. moved 10 steps backward<br>\
D. moved 1 step  backward";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) { 	controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5,
			time : '06:19',
			html : function(controllers) {
				var html = '<h2>This leads us to another interesting feature of the decimal place value system. Which one is true?</h2>';
				var choices = [
					'A. The wheel to the left has a value which is 10 times that of the previous wheel.', 
					'B. The wheel to the left has a value equal to that of the previous wheel.',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. The wheel to the left has a value which is 10 times that of the previous wheel.',
			question : 'This leads us to another interesting feature of the decimal place value system. Which one is true?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. The wheel to the left has a value which is 10 times that of the previous wheel.<br>\
B. The wheel to the left has a value equal to that of the previous wheel.";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) { 	controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 6,
			time : '07:00',
			html : function(controllers) {
				var html = '<h2>We can assign the 1st wheel to count the number of</h2>';
				var choices = [
					'A. Ones', 
					'B. Tens',
					'C. Hundreds',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. Ones',
			question : 'We can assign the 1st wheel to count the number of',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Ones<br>B. Tens<br>C. Hundreds";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) { 	controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time : '07:21',
			html : function(controllers) {
				var html = '<h2>We can assign the 2nd wheel to count the number of</h2>';
				var choices = [
					'A. Ones', 
					'B. Tens',
					'C. Hundreds',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. Tens',
			question : 'We can assign the 2nd wheel to count the number of',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Ones<br>B. Tens<br>C. Hundreds";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) { 	controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '07:43',
			html : function(controllers) {
				var html = '<h2>We can assign the 3rd wheel to count the number of</h2>';
				var choices = [
					'A. Ones', 
					'B. Tens',
					'C. Hundreds',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. Hundreds',
			question : 'We can assign the 3rd wheel to count the number of',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Ones<br>B. Tens<br>C. Hundreds";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) { 	controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '08:40',
			html : function(controllers) {
				var html = '<h2>What is the value of <strong>2</strong> in the number <br><strong>3 219</strong>?</h2>';
				var choices = [
					'A. One', 
					'B. Ten',
					'C. Hundred',
					'D. Thousand',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. Hundred',
			question : 'What is the value of <strong>2</strong> in the number <br><strong>3 219</strong>?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. One<br>B. Ten<br>C. Hundred<br>D. Thousand";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) { 	controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '09:02',
			html : function(controllers) {
				var html = '<h2>We can say that <strong>3 219</strong> has</h2>';
				html += '<div class="input-box"><input class="answer-box input-index-0 autofocus" maxlength="1" data-index="0"> <span class="input-label">thousands</span></div>';
				html += '<div class="input-box"><input class="answer-box input-index-1" maxlength="1" data-index="1"> <span class="input-label">hundreds</span></div>';
				html += '<div class="input-box"><input class="answer-box input-index-2" maxlength="1" data-index="2"> <span class="input-label">tens</span></div>';
				html += '<div class="input-box"><input class="answer-box input-index-3" maxlength="1" data-index="3"> <span class="input-label">ones</span></div>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm input-index-4" type="button">Submit</button>';
				return html;
			},
			answer : [3,2,1,9],
			answer_text : '3,2,1, and 9',
			question : 'We can say that <strong>3 219</strong> has',
			type : 'input-box',
			details : function() {
				var html = "_____ thousands<br>_____ hundreds<br>_____ tens<br>_____ ones";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) { 	controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11,
			time : '09:26',
			html : function(controllers) {
				var html = '<h2><strong>3 219</strong> can be written in expanded form as:</h2>';
				
				html += '<div class="input-box" style="float:left">( <input class="answer-box input-index-0 autofocus" maxlength="1" data-index="0" style="width:25px"> <span class="input-label"> x 1000</span> ) +</div>';
				html += '<div class="input-box" style="float:left">( <input class="answer-box input-index-1" maxlength="1" data-index="1" style="width:25px"> <span class="input-label">x 100</span> ) +</div>';
				html += '<div class="input-box" style="float:left">( <input class="answer-box input-index-2" maxlength="1" data-index="2" style="width:25px"> <span class="input-label">x 10</span> ) +</div>';
				html += '<div class="input-box" style="float:left">( <input class="answer-box input-index-3" maxlength="1" data-index="3" style="width:25px"> <span class="input-label">x 1</span> )</div>';
				html += '<span class="clearfix"></span>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm input-index-4" type="button">Submit</button>';
				
				return html;
			},
			answer : [3,2,1,9],
			answer_text : '3,2,1, and 9',
			question : '<strong>3 219</strong> can be written in expanded form as:',
			type : 'input-box',
			details : function() {
				var html = "( __ x 1000 ) +( __ x 100 ) +( __ x 10 ) +( __ x 1 )";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) { 	controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 12,
			time : '10:01',
			html : function(controllers) {
				var html = '<h2><strong>2, 834, 106</strong> has</h2>';
				html += '<div class="input-box"><input class="answer-box input-index-0 autofocus" maxlength="1" data-index="0"> <span class="input-label">millions</span></div>';
				html += '<div class="input-box"><input class="answer-box input-index-1" maxlength="1" data-index="1"> <span class="input-label">hundred thousands</span></div>';
				html += '<div class="input-box"><input class="answer-box input-index-2" maxlength="1" data-index="2"> <span class="input-label">ten thousands</span></div>';
				html += '<div class="input-box"><input class="answer-box input-index-3" maxlength="1" data-index="3"> <span class="input-label">thousands</span></div>';
				html += '<div class="input-box"><input class="answer-box input-index-4" maxlength="1" data-index="4"> <span class="input-label">hundreds</span></div>';
				html += '<div class="input-box"><input class="answer-box input-index-5" maxlength="1" data-index="5"> <span class="input-label">tens</span></div>';
				html += '<div class="input-box"><input class="answer-box input-index-6" maxlength="1" data-index="6"> <span class="input-label">ones</span></div>';
				
				html += '<button id="submit-answer" class="btn btn-default btn-sm input-index-7" type="button">Submit</button>';
				return html;
			},
			answer : [2,8,3,4,1,0,6],
			answer_text : '2, 8, 3, 4, 1, 0 and 6',
			question : '<strong>2, 834, 106</strong> has',
			type : 'input-box',
			details : function() {
				var html = "_____ millions<br>\
_____ hundred thousands<br>\
_____ ten thousands<br>\
_____ thousands<br>\
_____ hundreds<br>\
_____ tens<br>\
_____ ones";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) { 	
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 13,
			time : '10:23',
			html : function(controllers) {
				var html = '<h2><strong>2, 834, 106</strong> can be written in expanded form as:</h2>';
				
				html += '<div class="input-box" style="float:left">( <input class="answer-box input-index-0 autofocus" maxlength="1" data-index="0" style="width:25px"> <span class="input-label"> x 1,000,000</span> ) +</div>';
				html += '<div class="input-box" style="float:left">( <span class="input-label"> 8 x </span> <input class="answer-box input-index-1" maxlength="7" data-index="1" style="width:65px"> ) +</div>';
				html += '<div class="input-box" style="float:left">( <input class="answer-box input-index-2" maxlength="1" data-index="2" style="width:25px"> <span class="input-label"> x 10,000</span> ) +</div>';
				html += '<div class="input-box" style="float:left">( <span class="input-label"> 4 x </span> <input class="answer-box input-index-3" maxlength="5" data-index="3" style="width:50px"> ) +</div>';
				html += '<div class="input-box" style="float:left">( <input class="answer-box input-index-4" maxlength="1" data-index="4" style="width:25px"> <span class="input-label">x 100</span> ) +</div>';
				html += '<div class="input-box" style="float:left">( <span class="input-label"> 0 x </span> <input class="answer-box input-index-5" maxlength="2" data-index="5" style="width:33px"> ) +</div>';
				html += '<div class="input-box" style="float:left">( <input class="answer-box input-index-6" maxlength="1" data-index="6" style="width:25px"> <span class="input-label"> x 1</span> )</div>';
				html += '<span class="clearfix"></span>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm input-index-7" type="button">Submit</button>';
				
				return html;
			},
			answer : [2,100000,3,1000,1,10,6],
			answer_text : '2,100000,3,1000,1,10, and 6',
			question : '<strong>2, 834, 106</strong> can be written in expanded form as:',
			type : 'input-box',
			details : function() {
				var html = "( __ x 1,000,000 ) +( 8 x __ ) +( __ x 10,000 ) +( 4 x __ ) +( __ x 100 ) +( 0 x __ ) +( __ x 1 )";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) { 	
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
			end_session : 1,
		},
	],
};
