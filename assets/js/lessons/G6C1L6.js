var G6C1L6 = {
	title : 'Subtracting Decimals',
	duration : '24:47',
	data : [
		{	quiz_number : 1,
			time : '00:24',
			html : function(controllers) {
				var html = '<h2>5 minus 2 equals _______</h2>';
				var choices = [
					'A. 7', 
					'B. 3',
					'C. 2',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 3',
			question : '5 minus 2 equals _______',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 7<br>B. 3<br>C. 2";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2,
			time : '00:47',
			html : function(controllers) {
				var html = '<h2>35 minus 12 is _______</h2>';
				var choices = [
					'A. 13', 
					'B. 47',
					'C. 23',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 23',
			question : '35 minus 12 is _______',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 13<br>B. 47<br>C. 23";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '01:12',
			html : function(controllers) {
				var html = '<h2>So, 143 minus 98 is equal to how much?</h2>';
				var choices = [
					'A. 45', 
					'B. 54',
					'C. 65',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 45',
			question : 'So, 143 minus 98 is equal to how much?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 45<br>B. 54<br>C. 65";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '02:08',
			html : function(controllers) {
				var html = '<h3 style="text-align:center; font-weight:bold; color:blue"><span style="color:red">5</span> - 2 = 3 &nbsp;&nbsp;&nbsp; <span style="color:red">35</span> - 12 = 23 &nbsp;&nbsp;&nbsp; <span style="color:red">143</span> - 98 = 45</h3>';
				html += '<h2>5, 35, 143 are what we call _________.</h2>';
				var choices = [
					'A. subtrahend', 
					'B. difference',
					'C. minuend',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. minuend',
			question : '5, 35, 143 are what we call _________.',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><span style="color:red">5</span> - 2 = 3 &nbsp;&nbsp;&nbsp; <span style="color:red">35</span> - 12 = 23 &nbsp;&nbsp;&nbsp; <span style="color:red">143</span> - 98 = 45</p>';
				html += "A. subtrahend<br>B. difference<br>C. minuend";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5,
			time : '02:36',
			html : function(controllers) {
				var html = '<h3 style="text-align:center; font-weight:bold; color:blue">5 - <span style="color:red">2</span> = 3 &nbsp;&nbsp;&nbsp; 35 - <span style="color:red">12</span> = 23 &nbsp;&nbsp;&nbsp; 143 - <span style="color:red">98</span> = 45</h3>';
				html += '<h2>How about 2, 12, and 98?</h2>';
				var choices = [
					'A. subtrahend', 
					'B. difference',
					'C. minuend',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. subtrahend',
			question : 'How about 2, 12, and 98?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>5 - <span style="color:red">2</span> = 3 &nbsp;&nbsp;&nbsp; 35 - <span style="color:red">12</span> = 23 &nbsp;&nbsp;&nbsp; 143 - <span style="color:red">98</span> = 45</p>';
				html += "A. subtrahend<br>B. difference<br>C. minuend";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 6,
			time : '03:02',
			html : function(controllers) {
				var html = '<h3 style="text-align:center; font-weight:bold; color:blue">5 - 2 = <span style="color:red">3</span> &nbsp;&nbsp;&nbsp; 35 - 12 = <span style="color:red">23</span> &nbsp;&nbsp;&nbsp; 143 - 98 = <span style="color:red">45</span></h3>';
				html += '<h2>And 3, 23, and 45 are what we call _________.</h2>';
				var choices = [
					'A. subtrahend', 
					'B. difference',
					'C. minuend',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. difference',
			question : 'And 3, 23, and 45 are what we call _________.',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>5 - 2 = <span style="color:red">3</span> &nbsp;&nbsp;&nbsp; 35 - 12 = <span style="color:red">23</span> &nbsp;&nbsp;&nbsp; 143 - 98 = <span style="color:red">45</span></p>';
				html += "A. subtrahend<br>B. difference<br>C. minuend";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time : '03:42',
			html : function(controllers) {
				var html = '<h2>To get the ________, we will subtract the _________ from the _________.</h2>';
				var choices = [
					'A. difference, subtrahend, minuend', 
					'B. minuend, subtrahend, difference',
					'C. difference, minuend, subtrahend',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. difference, subtrahend, minuend',
			question : 'To get the ________, we will subtract the _________ from the _________.',
			type : 'multiple-choice',
			details : function() {
				var html = "A. difference, subtrahend, minuend<br>\
B. minuend, subtrahend, difference<br>\
C. difference, minuend, subtrahend";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '04:39',
			html : function(controllers) {
				var html = '<h2>Which terms in the table also describe subtraction?</h2>';
				var choices = [
					'A. minus, less, difference, sum decrease, more', 
					'B. deduct, take away, decrease, difference, less, minus',
					'C. take away, deduct, decrease, difference, minus, more',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. deduct, take away, decrease, difference, less, minus',
			question : 'Which terms in the table also describe subtraction?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. minus, less, difference, sum decrease, more<br>\
B. deduct, take away, decrease, difference, less, minus<br>\
C. take away, deduct, decrease, difference, minus, more";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '05:26',
			html : function(controllers) {
				var html = '<div style="position:absolute;font-size:50px;right:35px;line-height: 1.1;top: 100px;color: blue;">&nbsp;7.5<br><u>-1.2</u><br>&nbsp;6.3</div>';
				html += '<h2>Which among the numbers is the minuend?</h2>';
				var choices = [
					'A. 6.3', 
					'B. 1.2',
					'C. 7.5',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 7.5',
			question : 'Which among the numbers is the minuend?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>&nbsp;7.5<br><u>-1.2</u><br>&nbsp;6.3</p>';
				html += "A. 6.3<br>B. 1.2<br>C. 7.5";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '05:48',
			html : function(controllers) {
				var html = '<div style="position:absolute;font-size:50px;right:35px;line-height: 1.1;top: 100px;color: blue;">&nbsp;7.5<br><u>-1.2</u><br>&nbsp;6.3</div>';
				html += '<h2>Which is the subtrahend?</h2>';
				var choices = [
					'A. 6.3', 
					'B. 1.2',
					'C. 7.5',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 1.2',
			question : 'Which is the subtrahend?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>&nbsp;7.5<br><u>-1.2</u><br>&nbsp;6.3</p>';
				html += "A. 6.3<br>B. 1.2<br>C. 7.5";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11,
			time : '06:09',
			html : function(controllers) {
				var html = '<div style="position:absolute;font-size:50px;right:35px;line-height: 1.1;top: 100px;color: blue;">&nbsp;7.5<br><u>-1.2</u><br>&nbsp;6.3</div>';
				html += '<h2>Therefore, the difference is __________.</h2>';
				var choices = [
					'A. 6.3', 
					'B. 1.2',
					'C. 7.5',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 6.3',
			question : 'Therefore, the difference is __________.',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>&nbsp;7.5<br><u>-1.2</u><br>&nbsp;6.3</p>';
				html += "A. 6.3<br>B. 1.2<br>C. 7.5";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 12,
			time : '06:39',
			html : function(controllers) {
				var html = '<div style="position:absolute;font-size:50px;right:35px;line-height: 1.1;top: 100px;color: blue;"><span style="color:red">&nbsp;7.5</span><br><u>-1.2</u><br>&nbsp;6.3</div>';
				html += '<h2>Why is it 7.5 is the minuend?</h2>';
				var choices = [
					'A. Because it is where the other number is subtracted', 
					'B. Because it is the number to be subtracted',
					'C. Because it is the result after subtracting two numbers',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. Because it is where the other number is subtracted',
			question : 'Why is it 7.5 is the minuend?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><span style="color:red">&nbsp;7.5</span><br><u>-1.2</u><br>&nbsp;6.3</p>';
				html += "A. Because it is where the other number is subtracted<br>\
B. Because it is the number to be subtracted<br>\
C. Because it is the result after subtracting two numbers";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 13,
			time : '07:15',
			html : function(controllers) {
				var html = '<div style="position:absolute;font-size:50px;right:35px;line-height: 1.1;top: 100px;color: blue;">&nbsp;7.5<br><u style="color:red">-1.2</u><br>&nbsp;6.3</div>';
				html +=  '<h2>Why is it that 1.2 is the subtrahend?</h2>';
				var choices = [
					'A. Because it is where the other number is subtracted', 
					'B. Because it is the number to be subtracted',
					'C. Because it is the result after subtracting two numbers',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. Because it is the number to be subtracted',
			question : 'Why is it that 1.2 is the subtrahend?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>&nbsp;7.5<br><u style="color:red">-1.2</u><br>&nbsp;6.3</p>';
				html +=  "A. Because it is where the other number is subtracted<br>\
B. Because it is the number to be subtracted<br>\
C. Because it is the result after subtracting two numbers";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 14,
			time : '07:57',
			html : function(controllers) {
				var html = '<div style="position:absolute;font-size:50px;right:35px;line-height: 1.1;top: 100px;color: blue;"><span style="color:red">&nbsp;7.5</span><br><u>-1.2</u><br>&nbsp;6.3</div>';
				html += '<h2>How many digits are there in 7.5?</h2>';
				var choices = [
					'A. 2', 
					'B. 3',
					'C. 1',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 2',
			question : 'How many digits are there in 7.5?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><span style="color:red">&nbsp;7.5</span><br><u>-1.2</u><br>&nbsp;6.3</p>';
				html += "A. 2<br>B. 3<br>C. 1";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 15,
			time : '08:26',
			html : function(controllers) {
				var html = '<div style="position:absolute;font-size:50px;right:35px;line-height: 1.1;top: 100px;color: blue;">&nbsp;7.5<br><u style="color:red">-1.2</u><br>&nbsp;6.3</div>';
				html += '<h2>In 1.2, there are _______ digits.</h2>';
				var choices = [
					'A. 2', 
					'B. 3',
					'C. 1',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 2',
			question : 'In 1.2, there are _______ digits.',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>&nbsp;7.5<br><u style="color:red">-1.2</u><br>&nbsp;6.3</p>';
				html += "A. 2<br>B. 3<br>C. 1";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 16,
			time : '09:02',
			html : function(controllers) {
				var html = '<div style="position:absolute;font-size:50px;right:35px;line-height: 1.1;top: 100px;color: blue;">&nbsp;7.5<br><u>-1.2</u><br>&nbsp;6.3</div>';
				html += '<h2>How are they lined up?</h2>';
				var choices = [
					'A. They are lined up according to their place value.', 
					'B. They are lined up according to given',
					'C. No specific rule can be applied',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. They are lined up according to their place value.',
			question : 'How are they lined up?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>&nbsp;7.5<br><u>-1.2</u><br>&nbsp;6.3</p>';
				html += "A. They are lined up according to their place value.<br>\
B. They are lined up according to given<br>\
C. No specific rule can be applied";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 17,
			time : '09:34',
			html : function(controllers) {
				var html = '<div style="position:absolute;font-size:50px;right:35px;line-height: 1.1;top: 100px;color: blue;">&nbsp;7.<span style="color:red">5</span><br><u>-1.2</u><br>&nbsp;6.3</div>';
				html +=  '<h2>What digit is subtracted from 5?</h2>';
				var choices = [
					'A. 7', 
					'B. 1',
					'C. 2',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 2',
			question : 'What digit is subtracted from 5?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>&nbsp;7.<span style="color:red">5</span><br><u>-1.2</u><br>&nbsp;6.3</p>';
				html +=  "A. 7<br>B. 1<br>C. 2";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 18,
			time : '10:07',
			html : function(controllers) {
				var html = '<div style="position:absolute;font-size:50px;right:35px;line-height: 1.1;top: 100px;color: blue;">&nbsp;7.5<br><u>-<span style="color:red">1</span>.2</u><br>&nbsp;6.3</div>';
				html += '<h2>1 is subtracted from ______.</h2>';
				var choices = [
					'A. 5', 
					'B. 7',
					'C. 2',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 7',
			question : '1 is subtracted from ______.',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>&nbsp;7.5<br><u>-<span style="color:red">1</span>.2</u><br>&nbsp;6.3</p>';
				html += "A. 5<br>B. 7<br>C. 2";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 19,
			time : '10:30',
			html : function(controllers) {
				var html = '<div style="position:absolute;font-size:50px;right:35px;line-height: 1.1;top: 100px;color: blue;">&nbsp;7.5<br><u>-1.2</u><br>&nbsp;6.3</div>';
				html += '<h2>In what direction should we start subtracting?</h2>';
				var choices = [
					'A. from left to right', 
					'B. from right to left',
					'C. any direction will do',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. from right to left',
			question : 'In what direction should we start subtracting?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>&nbsp;7.5<br><u>-1.2</u><br>&nbsp;6.3</p>';
				html += "A. from left to right<br>B. from right to left<br>C. any direction will do";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},		
		{	quiz_number : 20,
			time : '11:27',
			html : function(controllers) {
				var html = '<div style="margin-top:50px;font-size:50px;line-height: 1.1;color: blue;text-align:center">&nbsp;3.5<br><u>-2.3</u><br>';
				html += '<input disabled maxlength="1" type="text" style="width: 35px;font-size: 50px;line-height: 1;padding: 0;">.';
				html += '<input class="answer-box first autofocus" maxlength="1" type="text" style="width: 35px;font-size: 50px;line-height: 1;padding: 0;"></div>';
				html += '<br><center><input id="submit-answer" type="button" value="Submit"><center>';
				return html;
			},
			answer : [2],
			answer_text : 2,
			question : '3.<span style="color:red">5</span><u>-2.<span style="color:red">3</span></u> = __.<span style="color:red">__</span>',
			type : 'input-box',
			details : function() {
				var html = '<p>&nbsp;3.<span style="color:red">5</span><br><u>-2.<span style="color:red">3</span></u><br>&nbsp;__.<span style="color:red">__</span></p>';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},		
		{	quiz_number : 21,
			time : '11:40',
			html : function(controllers) {
				var html = '<div style="margin-top:50px;font-size:50px;line-height: 1.1;color: blue;text-align:center">&nbsp;3.5<br><u>-2.3</u><br>';
				html += '<input class="answer-box autofocus" maxlength="1" type="text" style="width: 35px;font-size: 50px;line-height: 1;padding: 0;">.';
				html += '<input disabled value="2" maxlength="1" type="text" style="width: 35px;font-size: 50px;line-height: 1;padding: 0;"></div>';
				html += '<br><center><input id="submit-answer" type="button" value="Submit"><center>';
				return html;
			},
			answer : [1],
			answer_text : 1,
			question : '<span style="color:red">3</span>.5<u>-<span style="color:red">2</span>.3</u> = <span style="color:red">__</span>.2',
			type : 'input-box',
			details : function() {
				var html = '<p>&nbsp;<span style="color:red">3</span>.5<br><u>-<span style="color:red">2</span>.3</u><br>&nbsp;<span style="color:red">__</span>.2</p>';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},		
		{	quiz_number : 22,
			time : '11:55',
			html : function(controllers) {
				var html = '<div style="margin-top:50px;font-size:50px;line-height: 1.1;color: blue;text-align:center">&nbsp;3.5 - 2.3 = ';
				html += '<input class="answer-box autofocus" maxlength="3" type="text" style="width: 80px;font-size: 50px;line-height: 1;padding: 0;"></div>';
				
				html += '<br><center><input id="submit-answer" type="button" value="Submit"><center>';
				return html;
			},
			answer : [1.2],
			answer_text : '1.2',
			question : '3.5 - 2.3 = ___',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},		
		{	quiz_number : 23,
			time : '12:46',
			html : function(controllers) {
				var html = '<h2><center><span style="color:blue">9.2 - 7.18</span> = _______</center>How many digits are there in 9.2?</h2>';
				var choices = [
					'A. 1', 
					'B. 2',
					'C. 3',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 2',
			question : 'How many digits are there in 9.2?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p><span style=\"color:red\">9.2</span> - 7.18 = _______ </p>A. 1<br>B. 2<br>C. 3";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 24,
			time : '13:15',
			html : function(controllers) {
				var html = '<h2><center><span style="color:blue">9.2 - 7.18</span> = _______</center>In 7.18, how many digits are there?</h2>';
				var choices = [
					'A. 1', 
					'B. 2',
					'C. 3',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 3',
			question : 'In 7.18, how many digits are there?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>9.2 - <span style=\"color:red\">7.18</span> = _______ </p>A. 1<br>B. 2<br>C. 3";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 25,
			time : '13:52',
			html : function(controllers) {
				var html = '<h2><center><span style="color:blue">9.2 - 7.18</span> = _______</center>What is the empty place value that should be filled in with zero in 9.2?</h2>';
				var choices = [
					'A. tenths', 
					'B. tens',
					'C. hundredths',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. hundredths',
			question : 'What is the empty place value that should be filled in with zero in 9.2?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>9.2 - 7.18 = ______</p>A. tenths<br>B. tens<br>C. hundredths";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 26,
			time : '14:45',
			html : function(controllers) {
				var html = '<div style="position:absolute;font-size:50px;right:35px;line-height: 1.1;top: 130px;color: blue;">&nbsp;9.2<u>0</u><br><u>-7.18</u></div>';
				html += '<h2>What should we do to remove 8 hundredths from 0 hundredths?</h2>';
				var choices = [
					'A. erase the rightmost digits', 
					'B. use "20 - 8" instead of "0 - 8"',
					'C. borrow one tenths from 2 tenths and add it to zero hundredths',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. borrow one tenths from 2 tenths and add it to zero hundredths',
			question : 'What should we do to remove 8 hundredths from 0 hundredths?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>&nbsp;9.2<u>0</u><br><u>-7.18</u></p>';
				html += 'A. erase the rightmost digits<br>\
B. use "20 - 8" instead of "0 - 8"<br>\
C. borrow one tenths from 2 tenths and add it to zero hundredths';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},		
		{	quiz_number : 27,
			time : '15:32',
			html : function(controllers) {
				var html = '<div style="position:relative;width: 130px;margin: 50px auto 0;font-size:50px;line-height: 1.1;color: blue;text-align:center">';
				html += '<span style="font-size:14px;color:red;position:absolute;right:12px;top: -7px;">10</span>';
				html += '<span style="font-size:14px;color:blue;position:absolute;right:55px;top: -7px;">1</span>';
				html += '<span style="font-size:70px;color:rgba(134, 134, 134,0.8);position:absolute;right:45px;top: -10px;">/</span>';
				html += '<span style="padding:0 5px">9</span>.<span style="padding:0 5px">2</span><span style="padding:0 5px">0</span>';
				html += '<span style="position:absolute;left: -40px;top: 50px;">-</span>';
				html += '<br><span style="padding:0 5px">7</span>.<span style="padding:0 5px">1</span><span style="padding:0 5px;color:red">8</span><br>';
				html += '<span style="position: absolute;margin-top: -55px;padding: 0;height: 10px;">______</span>';
				html += '<input disabled maxlength="1" type="text" style="width: 35px;font-size: 50px;line-height: 1;padding: 0;">.';
				html += '<input disabled maxlength="1" type="text" style="width: 35px;font-size: 50px;line-height: 1;padding: 0;margin-right:5px;">';
				html += '<input class="answer-box autofocus" maxlength="1" type="text" style="width: 35px;font-size: 50px;line-height: 1;padding: 0;"></div>';
				html += '<br><center><input id="submit-answer" type="button" value="Submit"><center>';
				return html;
			},
			answer : [2],
			answer_text : 2,
			question : '9.2<span style="color:red">0</span> - 7.1<span style="color:red">8</span> = ____',
			type : 'input-box',
			details : function() {
				var html = '';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},		
		{	quiz_number : 28,
			time : '15:47',
			html : function(controllers) {
				var html = '<div style="position:relative;width: 130px;margin: 50px auto 0;font-size:50px;line-height: 1.1;color: blue;text-align:center">';
				html += '<span style="font-size:14px;color:blue;position:absolute;right:12px;top: -7px;">10</span>';
				html += '<span style="font-size:14px;color:red;position:absolute;right:55px;top: -7px;">1</span>';
				html += '<span style="font-size:70px;color:rgba(134, 134, 134,0.8);position:absolute;right:45px;top: -10px;">/</span>';
				html += '<span style="padding:0 5px">9</span>.<span style="padding:0 5px">2</span><span style="padding:0 5px">0</span>';
				html += '<span style="position:absolute;left: -40px;top: 50px;">-</span>';
				html += '<br><span style="padding:0 5px">7</span>.<span style="padding:0 5px;color:red">1</span><span style="padding:0 5px;">8</span><br>';
				html += '<span style="position: absolute;margin-top: -55px;padding: 0;height: 10px;">______</span>';
				html += '<input disabled maxlength="1" type="text" style="width: 35px;font-size: 50px;line-height: 1;padding: 0;">.';
				html += '<input class="answer-box autofocus" maxlength="1" type="text" style="width: 35px;font-size: 50px;line-height: 1;padding: 0;margin-right:5px;">';
				html += '<input disabled value="2" maxlength="1" type="text" style="width: 35px;font-size: 50px;line-height: 1;padding: 0;"></div>';
				html += '<br><center><input id="submit-answer" type="button" value="Submit"><center>';
				return html;
			},
			answer : [0],
			answer_text : '0',
			question : '9.<span style="color:red">2</span>0 - 7.<span style="color:red">1</span>8 = _.<span style="color:red">_</span>2',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},		
		{	quiz_number : 29,
			time : '16:03',
			html : function(controllers) {
				var html = '<div style="position:relative;width: 130px;margin: 50px auto 0;font-size:50px;line-height: 1.1;color: blue;text-align:center">';
				html += '<span style="font-size:14px;color:blue;position:absolute;right:12px;top: -7px;">10</span>';
				html += '<span style="font-size:14px;color:blue;position:absolute;right:55px;top: -7px;">1</span>';
				html += '<span style="font-size:70px;color:rgba(134, 134, 134,0.8);position:absolute;right:45px;top: -10px;">/</span>';
				html += '<span style="padding:0 5px;color:red">9</span>.<span style="padding:0 5px">2</span><span style="padding:0 5px">0</span>';
				html += '<span style="position:absolute;left: -40px;top: 50px;">-</span>';
				html += '<br><span style="padding:0 5px;color:red">7</span>.<span style="padding:0 5px;">1</span><span style="padding:0 5px;">8</span><br>';
				html += '<span style="position: absolute;margin-top: -55px;padding: 0;height: 10px;">______</span>';
				html += '<input class="answer-box autofocus" maxlength="1" type="text" style="width: 35px;font-size: 50px;line-height: 1;padding: 0;">.';
				html += '<input disabled value="0" maxlength="1" type="text" style="width: 35px;font-size: 50px;line-height: 1;padding: 0;margin-right:5px;">';
				html += '<input disabled value="2" maxlength="1" type="text" style="width: 35px;font-size: 50px;line-height: 1;padding: 0;"></div>';
				html += '<br><center><input id="submit-answer" type="button" value="Submit"><center>';
				return html;
			},
			answer : [2],
			answer_text : '2',
			question : '<span style="color:red">9</span>.20 - <span style="color:red">7</span>.18 = <span style="color:red">_</span>.02',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},		
		{	quiz_number : 31,
			time : '16:46',
			html : function(controllers) {
				var html = '<h2>What is the difference between 20 and 2.8?</h2>';
				var choices = [
					'A. 22.8', 
					'B. 0.8',
					'C. 17.2',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 17.2',
			question : 'What is the difference between 20 and 2.8?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 22.8<br>B. 0.8<br>C. 17.2";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 32,
			time : '17:56',
			html : function(controllers) {
				var html = '<div style="position:absolute;font-size:50px;right:35px;line-height: 1.1;top: 130px;color: blue;">&nbsp;20.<u>0</u><br>-<u>0</u>2.8<br><span style="line-height: 0;margin-top: -19px;position: absolute;">______</span></div>';
				html += '<h2>We have 2 zeroes in the minuend, where can we borrow additional value?</h2>';
				var choices = [
					'A. 2 tens', 
					'B. 0 tens',
					'C. 2 ones',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 2 tens',
			question : 'We have 2 zeroes in the minuend, where can we borrow additional value?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>&nbsp;20.<span style="color:red">0</span><br><u>-<span style="color:red">0</span>2.8</u></p>';
				html += "A. 2 tens<br>B. 0 tens<br>C. 2 ones";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 33,
			time : '18:19',
			html : function(controllers) {
				var html = '<div style="position:absolute;font-size:50px;right:35px;line-height: 1.1;top: 130px;color: blue;">&nbsp;20.<u>0</u><br>-<u>0</u>2.8<br><span style="line-height: 0;margin-top: -19px;position: absolute;">______</span></div>';
				html += '<h2>The digit which will borrow from 2 tens will be ________.</h2>';
				var choices = [
					'A. 0 ones', 
					'B. 0 tens',
					'C. 2 ones',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 0 ones',
			question : 'The digit which will borrow from 2 tens will be ________.',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>&nbsp;20.<span style="color:red">0</span><br><u>-<span style="color:red">0</span>2.8</u></p>';
				html += "A. 0 ones<br>B. 0 tens<br>C. 2 ones";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 34,
			time : '18:53',
			html : function(controllers) {
				var html = '<div style="position:absolute;font-size:50px;right:35px;line-height: 1.1;top: 180px;color: blue;">&nbsp;20.<u style="color:red">0</u><br>-<u>0</u>2.8<br><span style="line-height: 0;margin-top: -19px;position: absolute;">______</span></div>';
				html += '<h2>Where should 0 tenths borrow digits?</h2>';
				var choices = [
					'A. In 2 tens', 
					'B. In 0 ones which will now become 10 ones after <br>borrowing 1 tens from 2 tens',
					'C. In the subtrahend',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. In 0 ones which will now become 10 ones after borrowing 1 tens from 2 tens',
			question : 'Where should 0 tenths borrow digits?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p>&nbsp;20.<span style="color:red">0</span><br><u>-02.8</u></p>';
html += "A. In 2 tens<br>\
B. In 0 ones which will now become 10 ones after borrowing 1 tens from 2 tens<br>\
C. In the subtrahend";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 35,
			time : '20:26',
			html : function(controllers) {
				var html = '<h2>Arthor bought 100 pounds of cotton candy to sell during the circus. On day one of the circus, he sold 21.66 pounds of cotton candy. How much cotton candy was left?</h2>';
				var choices = [
					'A. 121.66 pounds', 
					'B. 78.34 pounds',
					'C. 21.56 pounds',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 78.34 pounds',
			question : 'How much cotton candy was left?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>Arthor bought 100 pounds of cotton candy to sell during the circus. On day one of the circus, he sold 21.66 pounds of cotton candy. </p>";
				html += "A. 121.66 pounds<br>B. 78.34 pounds<br>C. 21.56 pounds";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 36,
			time : '22:25',
			html : function(controllers) {
				var html = '<ol>';
				html += '<li>Add zeroes to empty places</li>';
				html += '<li>Subtract the subtrahend from the minuend starting from right to left</li>';
				html += '<li>Line up the decimal numbers as well as the decimal point</li>';
				html += '<li>If digits can\'t be removed because the minuend\'s digit is less than the subtrahend\'s digit, borrow from the digits to its left.</li>';
				html += '</ol>';
				html += '<h2>Which of the following are arranged from first step until the last step?</h2>';
				var choices = [
					'A. 1,2,3,4', 
					'B. 2,3,1,4',
					'C. 3,1,2,4',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 3,1,2,4',
			question : 'Which of the following are arranged from first step until the last step?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><ol>';
				html += '<li>Add zeroes to empty places</li>';
				html += '<li>Subtract the subtrahend from the minuend starting from right to left</li>';
				html += '<li>Line up the decimal numbers as well as the decimal point</li>';
				html += '<li>If digits can\'t be removed because the minuend\'s digit is less than the subtrahend\'s digit, borrow from the digits to its left.</li>';
				html += '</ol></p>';
				html += 'A. 1,2,3,4<br>B. 2,3,1,4<br>C. 3,1,2,4';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
			end_session : 1,
		},
	],
};
