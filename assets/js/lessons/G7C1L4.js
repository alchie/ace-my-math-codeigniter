var G7C1L4 = {
	title : 'Order of Operations',
	duration : '17:51',
	data : [
		{	quiz_number : 1,
			time : '00:41',
			html : function(controllers) {
				var html = '<h2>Which of the following is the first step?</h2>';
				var choices = [
					'A. Evaluate all powers', 
					'B. Evaluate the expression inside the grouping symbols',
					'C. Multiply and divide in order from left to right',
					'D. Add and subtract in order from left to right'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. Evaluate the expression inside the grouping symbols',
			question : 'Which of the following is the first step?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Evaluate all powers<br>\
B. Evaluate the expression inside the grouping symbols<br>\
C. Multiply and divide in order from left to right<br>\
D. Add and subtract in order from left to right";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2,
			time : '01:03',
			html : function(controllers) {
				var html = '<h2>Which of the following are grouping symbols? Check all that apply.</h2>';
				var choices = [
					'< >', 
					'/ /',
					'( )',
					'[ ]',
					'{ }'
				];
				html += controllers.checkbox_choices(choices);
				html += '<p><button class="btn btn-default btn-sm" id="submit-answer">Submit</button></p>';
				return html;
			},
			answer : [2,3,4],
			answer_text : '( ), [ ], { }',
			question : 'Which of the following are grouping symbols? Check all that apply.',
			type : 'multiple-choice',
			details : function() {
				var html = "__ < ><br>\
__ / /<br>\
__ ( )<br>\
__ [ ]<br>\
__ { }";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.checkbox_controls('checkbox-choices', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '01:42',
			html : function(controllers) {
				var html = '<h2>How about the second step?</h2>';
				var choices = [
					'A. Evaluate all powers', 
					'B. Evaluate the expression inside the grouping symbols',
					'C. Multiply and divide in order from left to right',
					'D. Add and subtract in order from left to right'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. Evaluate all powers',
			question : 'How about the second step?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Evaluate all powers<br>\
B. Evaluate the expression inside the grouping symbols<br>\
C. Multiply and divide in order from left to right<br>\
D. Add and subtract in order from left to right";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '02:26',
			html : function(controllers) {
				var html = '<h2>What is the 3<sup>rd</sup> step in the Order of Operation?</h2>';
				var choices = [
					'A. Evaluate all powers', 
					'B. Evaluate the expression inside the grouping symbols',
					'C. Multiply and divide in order from left to right',
					'D. Add and subtract in order from left to right'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. Multiply and divide in order from left to right',
			question : 'What is the 3<sup>rd</sup> step in the Order of Operation?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Evaluate all powers<br>\
B. Evaluate the expression inside the grouping symbols<br>\
C. Multiply and divide in order from left to right<br>\
D. Add and subtract in order from left to right";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5,
			time : '03:30',
			html : function(controllers) {
				var html = '<h2>Evaluate 5 + (12 - 3)<br>Which of the following should be done first?</h2>';
				var choices = [
					'A. 5 + 12', 
					'B. 5 + 3',
					'C. 12 - 3',
					'D. 5 - 3'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 12 - 3',
			question : 'Which of the following should be done first?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>Evaluate 5 + (12 - 3)</p>A. 5 + 12<br>\
B. 5 + 3<br>\
C. 12 - 3<br>\
D. 5 - 3";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 6,
			time : '04:12',
			html : function(controllers) {
				var html = '<h2>Which of the following is the second step in 5 + (12 - 3)?</h2>';
				var choices = [
					'A. 5 + 9', 
					'B. 5 + 12',
					'C. 5 + 15',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 5 + 9',
			question : 'Which of the following is the second step in 5 + (12 - 3)?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 5 + 9<br>\
B. 5 + 12<br>\
C. 5 + 15";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time : '04:51',
			html : function(controllers) {
				var html = '<h2>What is the value of the expression 5 + (12 - 3)?</h2>';
				var choices = [
					'A. 10', 
					'B. 9',
					'C. 14',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 14',
			question : 'What is the value of the expression 5 + (12 - 3)?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 10<br>B. 9<br>C. 14";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '05:28',
			html : function(controllers) {
				var html = '<h2>Evaluate 8 - 3 x 2 + 7<br />Which of the following operations should be done first?</h2>';
				var choices = [
					'A. Addition', 
					'B. Subtraction',
					'C. Multiplication',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. Multiplication',
			question : 'Which of the following operations should be done first?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>Evaluate 8 - 3 x 2 + 7</p>A. Addition<br>B. Subtraction<br>C. Multiplication";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '06:04',
			html : function(controllers) {
				var html = '<h2>8 - 3 x 2 + 7<br />Which is the first step?</h2>';
				var choices = [
					'A. 3 x 2', 
					'B. 2 + 7',
					'C. 8 - 3',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 3 x 2',
			question : 'Which is the first step?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>8 - 3 x 2 + 7</p>A. 3 x 2<br>B. 2 + 7<br>C. 8 - 3";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '06:30',
			html : function(controllers) {
				var html = '<h2> 8 - 3 x 2 + 7</h2><h3>Find the product of 3 and 2.</h3>';
				html += '<h3>8 - <input type="text" id="answer-box" style="width:50px;text-align:center" maxlength="2"> + 7<h3>'
				html += '<p><button class="btn btn-default btn-sm" id="submit-answer">Submit</button></p>';
				return html;
			},
			answer : 6,
			answer_text : 6,
			question : 'Find the product of 3 and 2.',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>8 - ___ + 7</p>";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11,
			time : '06:59',
			html : function(controllers) {
				var html = '<h2>8 - 6 + 7<br>Which is the first order of the operation?</h2>';
				var choices = [
					'A. 8 - 6', 
					'B. 6 + 7',
					'C. 8 + 6',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 8 - 6',
			question : 'Which is the first order of the operation?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>8 - 6 + 7</p>A. 8 - 6<br>B. 6 + 7<br>C. 8 + 6";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 12,
			time : '07:31',
			html : function(controllers) {
				var html = '<h2>What is the second order of operation of 8 - 6 + 7?</h2>';
				var choices = [
					'A. add 6', 
					'B. add 7',
					'C. add 8',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. add 7',
			question : 'What is the second order of operation of 8 - 6 + 7?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. add 6<br>B. add 7<br>C. add 8";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 13,
			time : '07:59',
			html : function(controllers) {
				var html = '<h2>What is the value of 8 - 3 x 2 + 7?</h2>';
				var choices = [
					'A. 7', 
					'B. 9',
					'C. 11',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 9',
			question : 'What is the value of 8 - 3 x 2 + 7?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 7<br>B. 9<br>C. 11";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 14,
			time : '08:48',
			html : function(controllers) {
				var html = '<h2>Evaluate 5 x 3<sup>2</sup> - 7<br>What is the first step?</h2>';
				var choices = [
					'A. Find the value of 3<sup>2</sup>', 
					'B. Multiply 5 x 3<sup>2</sup>',
					'C. Subtract 7 from 5',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. Find the value of 3<sup>2</sup>',
			question : 'What is the first step?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>Evaluate 5 x 3<sup>2</sup> - 7</p>\
A. Find the value of 3<sup>2</sup><br>\
B. Multiply 5 x 3<sup>2</sup><br>\
C. Subtract 7 from 5";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 15,
			time : '09:24',
			html : function(controllers) {
				var html = '<h2>5 x 3<sup>2</sup> - 7</h2><h3>Find the value of 3<sup>2</sup>.</h3>';
				html += '<h3>= 5 x <input type="text" id="answer-box" style="width:50px;text-align:center" maxlength="2"> - 7<h3>'
				html += '<p><button class="btn btn-default btn-sm" id="submit-answer">Submit</button></p>';
				return html;
			},
			answer : 9,
			answer_text : 9,
			question : 'Find the value of 3<sup>2</sup>.',
			type : 'input-box',
			details : function() {
				var html = "<p>= 5 x ___ - 7</p>";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 16,
			time : '10:03',
			html : function(controllers) {
				var html = '<h2>5 x 9 - 7<br>Which is correct?</h2>';
				var choices = [
					'A. 45 - 7', 
					'B. 5 x 2',
					'C. 2 x 9',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 45 - 7',
			question : 'Which is correct?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>5 x 9 - 7</p>A. 45 - 7<br>B. 5 x 2<br>C. 2 x 9";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 17,
			time : '10:32',
			html : function(controllers) {
				var html = '<h2>What is the value of 5 x 9 - 7?</h2>';
				var choices = [
					'A. 38', 
					'B. 39',
					'C. 40',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 38',
			question : 'What is the value of 5 x 9 - 7?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 38<br>B. 39<br>C. 40";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 18,
			time : '11:37',
			html : function(controllers) {
				var html = '<h2>Evaluate 14 + 3(7 - 2)<br>Which is the first step?</h2>';
				var choices = [
					'A. 3 x 7', 
					'B. 14 + 3',
					'C. 7 - 2',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 7 - 2',
			question : 'Which is the first step?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>Evaluate 14 + 3(7 - 2)</p>A. 3 x 7<br>B. 14 + 3<br>C. 7 - 2";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 19,
			time : '11:55',
			html : function(controllers) {
				var html = '<h2>14 + 3(7 - 2)</h2><h3> Evaluate numbers inside the parenthesis</h3>';
				html += '<h3>14 + 3(<input type="text" id="answer-box" style="width:50px;text-align:center" maxlength="2">)<h3>'
				html += '<p><button class="btn btn-default btn-sm" id="submit-answer">Submit</button></p>';
				
				return html;
			},
			answer : 5,
			answer_text : 5,
			question : 'Evaluate numbers inside the parenthesis. 14 + 3(7 - 2)',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>14 + 3(___)</p>";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 20,
			time : '12:30',
			html : function(controllers) {
				var html = '<h2>Lets go to the second step, we got this expression: 14 + 3(5)<br>What would be the first order of operation?</h2>';
				var choices = [
					'A. 14 + 3', 
					'B. 3 x 5',
					'C. 14 x 5',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 3 x 5',
			question : 'What would be the first order of operation?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>Lets go to the second step, we got this expression: 14 + 3(5)</p>\
A. 14 + 3<br>B. 3 x 5<br>C. 14 x 5";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 21,
			time : '13:05',
			html : function(controllers) {
				var html = '<h2>What is the value of 14 + 3(5)?</h2>';
				var choices = [
					'A. 30', 
					'B. 29',
					'C. 28',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 29',
			question : 'What is the value of 14 + 3(5)?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 30<br>B. 29<br>C. 28";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 22,
			time : '13:55',
			html : function(controllers) {
				var html = '<h2>Solve (20-10)<sup>2</sup> + 10 x 2 - 6 &divide; 3 using the order of operations.<br> Which of the following comes first?</h2>';
				var choices = [
					'A. Evaluate the numbers inside the parenthesis', 
					'B. Evaluate powers',
					'C. Add',
					'D. Multiply'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. Evaluate the numbers inside the parenthesis',
			question : 'Which of the following comes first?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>Solve (20-10)<sup>2</sup> + 10 x 2 - 6 &divide; 3 using the order of operations.</p>\
A. Evaluate the numbers inside the parenthesis<br>\
B. Evaluate powers<br>\
C. Add<br>\
D. Multiply";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 23,
			time : '14:15',
			html : function(controllers) {
				var html = '<h3>Evaluate numbers inside the parenthesis</h3><h2>(20-10)<sup>2</sup> + 10 x 2 - 6 &divide; 3</h2>';
				html += '<h3>(<input type="text" id="answer-box" style="width:50px;text-align:center" maxlength="2">)<sup>2</sup> + 10 x 2 - 6 &divide; 3<h3>'
				html += '<p><button class="btn btn-default btn-sm" id="submit-answer">Submit</button></p>';
				
				return html;
			},
			answer : 10,
			answer_text : '',
			question : 'Evaluate numbers inside the parenthesis: (20-10)<sup>2</sup> + 10 x 2 - 6 &divide; 3',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>(___)<sup>2</sup> + 10 x 2 - 6 &divide; 3";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 24,
			time : '15:01',
			html : function(controllers) {
				var html = '<h2>10<sup>2</sup> + 10 x 2 - 6 &divide; 3<br>Which of the following is the second step?</h2>';
				var choices = [
					'A. Multiply from left to right', 
					'B. Add',
					'C. Subtract',
					'D. Evaluate powers'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 3,
			answer_text : 'D. Evaluate powers',
			question : 'Which of the following is the second step?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>10<sup>2</sup> + 10 x 2 - 6 &divide; 3</p>A. Multiply from left to right<br>\
B. Add<br>\
C. Subtract<br>\
D. Evaluate powers";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 25,
			time : '15:16',
			html : function(controllers) {
				var html = '<h3>Evaluate the powers.</h3><h2>10<sup>2</sup> + 10 x 2 - 6 &divide; 3</h2>';
				html += '<h3><input type="text" id="answer-box" style="width:50px;text-align:center" maxlength="4"> + 10 x 2 - 6 &divide; 3<h3>'
				html += '<p><button class="btn btn-default btn-sm" id="submit-answer">Submit</button></p>';
				
				return html;
			},
			answer : 100,
			answer_text : 100,
			question : 'Evaluate the powers. 10<sup>2</sup> + 10 x 2 - 6 &divide; 3',
			type : 'multiple-choice',
			details : function() {
				var html = "___ + 10 x 2 - 6 &divide; 3";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box',$, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 26,
			time : '15:52',
			html : function(controllers) {
				var html = '<h2>100 + 10 x 2 - 6 &divide; 3<br>After evaluating powers, which operation comes next?</h2>';
				var choices = [
					'A. Addition', 
					'B. Multiplication',
					'C. Division',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. Multiplication',
			question : 'After evaluating powers, which operation comes next?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>100 + 10 x 2 - 6 &divide; 3</p>A. Addition<br>B. Multiplication<br>C. Division";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 27,
			time : '16:21',
			html : function(controllers) {
				var html = '<h3>Perform Multipication</h3><h2>100 + 10 x 2 - 6 &divide; 3</h2>';
				html += '<h3>100 + <input type="text" id="answer-box" style="width:50px;text-align:center" maxlength="3"> - 6 &divide; 3<h3>'
				html += '<p><button class="btn btn-default btn-sm" id="submit-answer">Submit</button></p>';
				
				return html;
			},
			answer : 20,
			answer_text : 20,
			question : 'Perform Multipication: 100 + 10 x 2 - 6 &divide; 3',
			type : 'multiple-choice',
			details : function() {
				var html = "100 + ___ - 6 &divide; 3";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box',$, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 28,
			time : '16:46',
			html : function(controllers) {
				var html = '<h2>100 + 20 - 6 &divide; 3<br>After multiplication, what will be the next operation?</h2>';
				var choices = [
					'A. Addition', 
					'B. Multiplication',
					'C. Division',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. Division',
			question : 'After multiplication, what will be the next operation?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>100 + 20 - 6 &divide; 3</p>A. Addition<br>B. Multiplication<br>C. Division";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 29,
			time : '17:12',
			html : function(controllers) {
				var html = '<h3>Perform the division</h3><h2>100 + 20 - 6 &divide; 3</h2>';
				html += '<h3>100 + 20 - <input type="text" id="answer-box" style="width:50px;text-align:center" maxlength="2"><h3>'
				html += '<p><button class="btn btn-default btn-sm" id="submit-answer">Submit</button></p>';
				
				return html;
			},
			answer : 2,
			answer_text : 2,
			question : 'Perform the division: 100 + 20 - 6 &divide; 3',
			type : 'multiple-choice',
			details : function() {
				var html = "100 + 20 - ___";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box',$, jwp, data, correct_callback, wrong_callback);
			},
			end_session : 1,
		},
	],
};
