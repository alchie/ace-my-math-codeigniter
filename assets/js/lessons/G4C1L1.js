var G4C1L1 = {
	title : 'Numbers through Billions',
	duration : '12:26',
	data : [
		{	quiz_number : 1,
			time : '01:55',
			html : function(controllers) {
				var html = '<h2>How do you read this number? 2 983</h2>';
				var choices = [
					'A. two thousand, three hundred ninety-eight', 
					'B. nine thousand, two hundred thirty-eight',
					'C. two thousand, nine hundred eighty-three',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. two thousand, nine hundred eighty-three',
			question : 'How do you read this number? 2 983',
			type : 'multiple-choice',
			details : function() {
				var html = "A. two thousand, three hundred ninety-eight<br>B. nine thousand, two hundred thirty-eight<br>C. two thousand, nine hundred eighty-three";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2,
			time : '02:28',
			html : function(controllers) {
				var html = '<h2>How do you read this number? 9 761</h2>';
				var choices = [
					'A. nine thousand, seven hundred sixty-one', 
					'B. nine thousand, two hundred thirty-eight',
					'C. nine thousand, sixty-one',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. nine thousand, seven hundred sixty-one',
			question : 'How do you read this number? 9 761',
			type : 'multiple-choice',
			details : function() {
				var html = "A. nine thousand, seven hundred sixty-one<br>B. nine thousand, two hundred thirty-eight<br>C. nine thousand, sixty-one";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '02:57',
			html : function(controllers) {
				var html = '<h2>How do you read this number? 10 000</h2>';
				var choices = [
					'A. ten thousand', 
					'B. ten hundred',
					'C. one thousand'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. ten thousand',
			question : 'How do you read this number? 10 000',
			type : 'multiple-choice',
			details : function() {
				var html = "A. ten thousand<br>B. ten hundred<br>C. one thousand";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '03:30',
			html : function(controllers) {
				var html = '<h2>How do you read this number? 14 075</h2>';
				var choices = [
					'A. fourteen thousand, seven hundred and five', 
					'B. fourteen thousand, seventy-five',
					'C. fourteen thousand, seven hundred fifty'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'A. fourteen thousand, seven hundred and five',
			question : 'How do you read this number? 14 075',
			type : 'multiple-choice',
			details : function() {
				var html = "A. fourteen thousand, seven hundred and five<br>B. fourteen thousand, seventy-five<br>C. fourteen thousand, seven hundred fifty";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5,
			time : '04:05',
			html : function(controllers) {
				var html = '<h2>How do you read this number? 94 654</h2>';
				var choices = [
					'A. ninety-four thousand, six hundred fifty-four', 
					'B. ninety-four thousand, six hundred four',
					'C. ninety-four thousand, six hundred forty-five',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. ninety-four thousand, six hundred fifty-four',
			question : 'How do you read this number? 94 654',
			type : 'multiple-choice',
			details : function() {
				var html = "A. ninety-four thousand, six hundred fifty-four<br>B. ninety-four thousand, six hundred four<br>C. ninety-four thousand, six hundred forty-five";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 6,
			time : '04:27',
			html : function(controllers) {
				var html = '<h2>six thousand, four hundred thirty-eight</h2>';
				html += '<p><input type="text" id="answer-box" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 6438,
			answer_text : 6438,
			question : 'six thousand, four hundred thirty-eight',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time : '04:45',
			html : function(controllers) {
				var html = '<h2>Nine thousand, eight hundred fifty-two</h2>';
				html += '<p><input type="text" id="answer-box" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 9852,
			answer_text : 9852,
			question : 'Nine thousand, eight hundred fifty-two',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '05:01',
			html : function(controllers) {
				var html = '<h2>Ten Thousand, one</h2>';
				html += '<p><input type="text" id="answer-box" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 10001,
			answer_text : 10001,
			question : 'Ten Thousand, one',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '05:18',
			html : function(controllers) {
				var html = '<h2>Twenty thousand, eight hundred thirty-eight</h2>';
				html += '<p><input type="text" id="answer-box" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 20838,
			answer_text : 20838,
			question : 'Twenty thousand, eight hundred thirty-eight',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '05:36',
			html : function(controllers) {
				var html = '<h2>Eight-six thousand, four hundred forty-nine</h2>';
				html += '<p><input type="text" id="answer-box" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 86449,
			answer_text : 86449,
			question : 'Eight-six thousand, four hundred forty-nine',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11,
			time : '08:37',
			html : function(controllers) {
				var html = '<h2>Three hundred thirty-three thousand, two hundred twenty-two</h2>';
				html += '<p><input type="text" id="answer-box" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 333222,
			answer_text : 333222,
			question : 'Three hundred thirty-three thousand, two hundred twenty-two',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 12,
			time : '08:56',
			html : function(controllers) {
				var html = '<h2>Nine hundred ninety-nine thousand, nine hundred ninety-nine</h2>';
				html += '<p><input type="text" id="answer-box" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 999999,
			answer_text : 999999,
			question : 'Nine hundred ninety-nine thousand, nine hundred ninety-nine',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 13,
			time : '09:17',
			html : function(controllers) {
				var html = '<h2>Six hundred fifty-three thousand, eight hundred twelve</h2>';
				html += '<p><input type="text" id="answer-box" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 653812,
			answer_text : 653812,
			question : 'Six hundred fifty-three thousand, eight hundred twelve',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 14,
			time : '09:36',
			html : function(controllers) {
				var html = '<h2>Four hundred ninety-six thousand and eight</h2>';
				html += '<p><input type="text" id="answer-box" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 496008,
			answer_text : 496008,
			question : 'Four hundred ninety-six thousand and eight',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 15,
			time : '09:53',
			html : function(controllers) {
				var html = '<h2>Five hundred thousand</h2>';
				html += '<p><input type="text" id="answer-box" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 500000,
			answer_text : 500000,
			question : 'Five hundred thousand',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 16,
			time : '10:51',
			html : function(controllers) {
				var html = '<h2>What is the place value and the value of the first underlined digit? 2<u>9</u>6, 174</h2>';
				html += '<p><label>Place Value</label><select class="answer-box">';
					html += '<option>ones</option>';
					html += '<option>tens</option>';
					html += '<option>hundreds</option>';
					html += '<option>thousands</option>';
					html += '<option>ten thousands</option>';
					html += '<option>hundred thousands</option>';
				html += '</select></p>';
				html += '<p><label>Value</label><input type="text" class="answer-box" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : ['ten thousands',90000],
			answer_text : 'ten thousands and 90000',
			question : 'What is the place value and the value of the first underlined digit? 2<u>9</u>6, 174',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 17,
			time : '11:10',
			html : function(controllers) {
				var html = '<h2>What is the place value and the value of the first underlined digit? 31<u>9</u>, 000</h2>';
				html += '<p><label>Place Value</label><select class="answer-box">';
					html += '<option>ones</option>';
					html += '<option>tens</option>';
					html += '<option>hundreds</option>';
					html += '<option>thousands</option>';
					html += '<option>ten thousands</option>';
					html += '<option>hundred thousands</option>';
				html += '</select></p>';
				html += '<p><label>Value</label><input type="text" class="answer-box" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : ['thousands',9000],
			answer_text : 'thousands and 9000',
			question : 'What is the place value and the value of the first underlined digit? 31<u>9</u>, 000',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 18,
			time : '11:27',
			html : function(controllers) {
				var html = '<h2>What is the place value and the value of the first underlined digit?  319, 0<u>0</u>0</h2>';
				html += '<p><label>Place Value</label><select class="answer-box">';
					html += '<option>ones</option>';
					html += '<option>tens</option>';
					html += '<option>hundreds</option>';
					html += '<option>thousands</option>';
					html += '<option>ten thousands</option>';
					html += '<option>hundred thousands</option>';
				html += '</select></p>';
				html += '<p><label>Value</label><input type="text" class="answer-box" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : ['tens',[0,"00"]],
			answer_text : 'tens and 0 or 00',
			question : 'What is the place value and the value of the first underlined digit?  319, 0<u>0</u>0',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 19,
			time : '11:43',
			html : function(controllers) {
				var html = '<h2>What is the place value and the value of the first underlined digit? <u>7</u>65, 161</h2>';
				html += '<p><label>Place Value</label><select class="answer-box">';
					html += '<option>ones</option>';
					html += '<option>tens</option>';
					html += '<option>hundreds</option>';
					html += '<option>thousands</option>';
					html += '<option>ten thousands</option>';
					html += '<option>hundred thousands</option>';
				html += '</select></p>';
				html += '<p><label>Value</label><input type="text" class="answer-box" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : ['hundred thousands',700000],
			answer_text : 'hundred thousands and 700000',
			question : 'What is the place value and the value of the first underlined digit? <u>7</u>65, 161',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 20,
			time : '12:04',
			html : function(controllers) {
				var html = '<h2>What is the place value and the value of the first underlined digit? <u>9</u>17, 324</h2>';
				html += '<p><label>Place Value</label><select class="answer-box">';
					html += '<option>ones</option>';
					html += '<option>tens</option>';
					html += '<option>hundreds</option>';
					html += '<option>thousands</option>';
					html += '<option>ten thousands</option>';
					html += '<option>hundred thousands</option>';
				html += '</select></p>';
				html += '<p><label>Value</label><input type="text" class="answer-box" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : ['hundred thousands',900000],
			answer_text : 'hundred thousands and 900000',
			question : 'What is the place value and the value of the first underlined digit? <u>9</u>17, 324',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
			end_session : 1,
		},
	],
};
