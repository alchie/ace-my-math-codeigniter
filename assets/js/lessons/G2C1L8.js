var G2C1L8 = {
	title : 'Subtract All and Subtract Zero',
	duration : '11:04',
	data : [
		{	quiz_number : 1,
			time : '00:55',
			html : function(controllers) {
				var html = '<img src="/assets/images/lessons/G2C1L7_numberline.png" width="100%"><h2>11 - 7 is?</h2>';
				var choices = [
					'A. 11', 
					'B. 7',
					'C. 4',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 4',
			question : '11 - 7 is?',
			type : 'multiple-choice',
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C1L7_numberline.png" width="100%">';
				html += '<br><br>A. 11<br>B. 7<br>C. 4';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2,
			time : '01:48',
			html : function(controllers) {
				var html = '<h2>9 oranges - 4 oranges = _____ oranges.</h2>';
				html += '<img src="/assets/images/lessons/G2C1L8_apples.png" style="float:right;">';
				var choices = [
					'A. 5', 
					'B. 4',
					'C. 9',
				];
				html += controllers.button_choices(choices);
				
				return html;
			},
			answer : 0,
			answer_text : 'A. 5',
			question : '9 oranges - 4 oranges = _____ oranges.',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G2C1L8_apples.png"></p>';
				html += 'A. 5<br>B. 4<br>C. 9';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '03:33',
			html : function(controllers) {
				var html = '<h3>While strolling in the park, Gina saw six beetles resting on the leaf of a plant. Despite the strong wind, none of the beetles flew away. How many beetles are on the leaf?</h3>';
				html += '<img src="/assets/images/lessons/G2C1L8_bettles.png" style="float:right;">';
				var choices = [
					'A. 6', 
					'B. 0',
					'C. 1',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : '',
			question : 'How many beetles are on the leaf?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>While strolling in the park, Gina saw six beetles resting on the leaf of a plant. Despite the strong wind, none of the beetles flew away.</p>";
				html += '<p><img src="/assets/images/lessons/G2C1L8_bettles.png"></p>';
				html += 'A. 6<br>B. 0<br>C. 1';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '04:12',
			html : function(controllers) {
				var html = '<h2>6 - 0 = 6<br> Now, what did you notice with the small number, is it a zero?</h2>';
				var choices = [
					'A. Yes', 
					'B. No',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. Yes',
			question : '6 - 0 = 6 Now, what did you notice with the small number, is it a zero?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Yes<br>B. No";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5,
			time : '05:12',
			html : function(controllers) {
				var html = '<h2>What is 8 - 0?</h2>';
				var choices = [
					'A. 0', 
					'B. 8',
					'C. 1',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 8',
			question : 'What is 8 - 0?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 0<br>B. 8<br>C. 1";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 6,
			time : '06:12',
			html : function(controllers) {
				var html = '<h2>There were eight lemons on the counter. Jim was so thirsty that he used all the lemons to make a lemonade. How many lemons were left?</h2>';
				html += '<img src="/assets/images/lessons/G2C1L8_lemons.png" style="float:right">';
				var choices = [
					'A. 8', 
					'B. 0',
					'C. 1',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 0',
			question : 'How many lemons were left?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G2C1L8_lemons.png"></p>';
				html += "<p>There were eight lemons on the counter. Jim was so thirsty that he used all the lemons to make a lemonade.</p>";
				html += "<p>A. 8<br>B. 0<br>C. 1</p>";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time : '06:54',
			html : function(controllers) {
				var html = '<h2><u>8</u> - <u>8</u> = 0<br>Now, what did you notice with the two underlined numbers?</h2>';
				var choices = [
					'A. They are the same.', 
					'B. They are not the same.',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. They are the same.',
			question : '<u>8</u> - <u>8</u> = 0 Now, what did you notice with the two underlined numbers?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. They are the same.<br>B. They are not the same.";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '07:47',
			html : function(controllers) {
				var html = '<h2>Having 6 - 6, the difference is?</h2>';
				html += '<h3> <input type="text" class="autofocus" id="answer-box" style="width:50px;text-align:center" maxlength="2"><h3>'
				html += '<p><button class="btn btn-default btn-sm" id="submit-answer">Submit</button></p>';
				return html;
			},
			answer : 0,
			answer_text : 0,
			question : 'Having 6 - 6, the difference is?',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '08:31',
			html : function(controllers) {
				var html = '<h2>In SUBTRACT ZERO strategy, a number minus ZERO always equals to what number?</h2>';
				var choices = [
					'A. The same number', 
					'B. The other number',
					'C. Zero',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. The same number',
			question : 'In SUBTRACT ZERO strategy, a number minus ZERO always equals to what number?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. The same number<br>B. The other number<br>C. Zero";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '09:10',
			html : function(controllers) {
				var html = '<h2>In SUBTRACT ALL strategy, a number minus the SAME number always equals to what number?</h2>';
				var choices = [
					'A. The same number', 
					'B. The other number',
					'C. Zero',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. Zero',
			question : 'In SUBTRACT ALL strategy, a number minus the SAME number always equals to what number?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. The same number<br>B. The other number<br>C. Zero";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11,
			time : '09:39',
			html : function(controllers) {
				var html = '<h2>Mike caught five fishes. He let all of them go. How many fishes does Mike have?</h2>';
				html += '<img src="/assets/images/lessons/G2C1L8_fishes.png" style="float:right;">';
				html += '<h3> <input type="text" class="autofocus" id="answer-box" style="width:50px;text-align:center" maxlength="2"><h3>'
				html += '<p><button class="btn btn-default btn-sm" id="submit-answer">Submit</button></p>';
				return html;
			},
			answer : 0,
			answer_text : 0,
			question : 'Mike caught five fishes. He let all of them go. How many fishes does Mike have?',
			type : 'multiple-choice',
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C1L8_fishes.png">'
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 12,
			time : '10:09',
			html : function(controllers) {
				var html = '<h2>Three birds are on a tree. None flew away. How many birds were left on the tree?</h2>';
				html += '<img src="/assets/images/lessons/G2C1L8_birds.png" style="float:right;">';
				html += '<h3> <input type="text" class="autofocus" id="answer-box" style="width:50px;text-align:center" maxlength="2"><h3>'
				html += '<p><button class="btn btn-default btn-sm" id="submit-answer">Submit</button></p>';
				return html;
			},
			answer : 3,
			answer_text : 3,
			question : 'Three birds are on a tree. None flew away. How many birds were left on the tree?',
			type : 'multiple-choice',
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C1L8_birds.png">';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
			end_session : 1,
		},
	],
};
