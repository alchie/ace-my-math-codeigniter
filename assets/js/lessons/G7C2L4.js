var G7C2L4 = {
	title : 'Adding Integers',
	duration : '11:32',
	data : [
		{	quiz_number : 1,
			time : '00:32',
			html : function(controllers) {
				var html = '<img src="/assets/images/lessons/G7C2L4_numberline1.png">';
				html += '<h2>Using a number line, what do you call the numbers that are found on the left of 0?</h2>';
				var choices = [
					'A. Positive Numbers', 
					'B. Negative Numbers',
					'C. Plus Numbers',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. Negative Numbers',
			question : 'Using a number line, what do you call the numbers that are found on the left of 0?',
			type : 'multiple-choice', 
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C2L4_numberline1.png"></p>';
				html += "A. Positive Numbers<br>B. Negative Numbers<br>C. Plus Numbers";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2,
			time : '01:04',
			html : function(controllers) {
				var html = '<img src="/assets/images/lessons/G7C2L4_numberline1.png">';
				html += '<h2>How about the numbers found on the right side of zero?</h2>';
				var choices = [
					'A. Positive Numbers', 
					'B. Negative Numbers',
					'C. Plus Numbers',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. Positive Numbers',
			question : 'How about the numbers found on the right side of zero?',
			type : 'multiple-choice', 
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C2L4_numberline1.png"></p>';
				html += "A. Positive Numbers<br>B. Negative Numbers<br>C. Plus Numbers";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '01:29',
			html : function(controllers) {
				var html = '<h2>What is the sum of 5 and 6?</h2>';
				html += '<p>5 + 6 = <input type="text" id="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 11,
			answer_text : 11,
			question : 'What is the sum of 5 and 6?',
			type : 'input-box',
			details : function() {
				var html = "5 + 6 = ____";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '02:33',
			html : function(controllers) {
				var html = '<h2>How many positive (+) ballons and negative (-) weights are there?</h2>';
				html += '<img style="float:right;width:200px;" src="/assets/images/lessons/G7C2L4_img1.png">';
				html += '<p><input type="text" class="answer-box" maxlength="2" /> Positive Balloons</p>';
				html += '<p><input type="text" class="answer-box" maxlength="2" /> Negative Weights</p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : [5,5],
			answer_text : '5 , 5',
			question : 'How many positive (+) ballons and negative (-) weights are there?',
			type : 'input-box',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C2L4_img1.png"></p>';
				html += "____ Positive Balloons<br>\
				____ Negative Weights";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5,
			time : '02:49',
			html : function(controllers) {
				var html = '<img style="float:right" src="/assets/images/lessons/G7C2L4_img1.png">\
				<h2>Express the number of balloons as an integer:</h2>';
				html += '<p>balloons = <input type="text" id="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : [5,'+5'],
			answer_text : '5 or +5',
			question : 'Express the number of balloons as an integer:',
			type : 'input-box',
			details : function() {
				var html = '<img src="/assets/images/lessons/G7C2L4_img1.png"><br><br>balloons = ____';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box_any('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 6,
			time : '03:11',
			html : function(controllers) {
				var html = '<img style="float:right" src="/assets/images/lessons/G7C2L4_img1.png">\
				<h2>What value expresses the number of weights as integers?</h2>';
				var choices = [
					'A. 5', 
					'B. -5',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. -5',
			question : 'What value expresses the number of weights as integers?',
			type : 'multiple-choice',
			details : function() {
				var html = '<img src="/assets/images/lessons/G7C2L4_img1.png"><br><br>A. 5<br>B. -5';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time : '04:51',
			html : function(controllers) {
				var html = '<img style="float:right" src="/assets/images/lessons/G7C2L4_img2.png">\
				<h2>What is the sum of 1 balloon and 1 weight?</h2>';
				var choices = [
					'A. 2', 
					'B. -2',
					'C. 0',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 0',
			question : 'What is the sum of 1 balloon and 1 weight?',
			type : 'multiple-choice', 
			details : function() {
				var html = '<img src="/assets/images/lessons/G7C2L4_img2.png"><br><br>A. 2<br>B. -2<br>C. 0';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '05:28',
			html : function(controllers) {
				var html = '<img style="float:right" src="/assets/images/lessons/G7C2L4_img1.png">\
				<h2>What will happen to the basket if we add more balloons?</h2>';
				var choices = [
					'A. The basket is pulled upward', 
					'B. The basket is pulled downward',
					'C. The basket will not move',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. The basket is pulled upward',
			question : 'What will happen to the basket if we add more balloons?',
			type : 'multiple-choice',
			details : function() {
				var html = '<img src="/assets/images/lessons/G7C2L4_img1.png"><br><br>\
A. The basket is pulled upward<br>\
B. The basket is pulled downward<br>\
C. The basket will not move';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '06:05',
			html : function(controllers) {
				var html = '<h2>How many pairs of balloon and weight can we make from 7 balloons and 5 weights?</h2>\
				<img style="float:right" src="/assets/images/lessons/G7C2L4_img3.png">';
				var choices = [
					'A. 2', 
					'B. 5',
					'C. 12',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 5',
			question : 'How many pairs of balloon and weight can we make from 7 balloons and 5 weights?',
			type : 'multiple-choice', 
			details : function() {
				var html = '<img src="/assets/images/lessons/G7C2L4_img3.png"><br><br>\
A. 2<br>B. 5<br>C. 12';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '07:20',
			html : function(controllers) {
				var html = '<img style="float:right" src="/assets/images/lessons/G7C2L4_img1.png">\
				<h2>Suppose we add weights to the basket, what will happen to the basket?</h2>';
				var choices = [
					'A. The basket gets pulled upward', 
					'B. The basket gets pulled downward',
					'C. The basket remains at its position',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. The basket gets pulled downward',
			question : 'Suppose we add weights to the basket, what will happen to the basket?',
			type : 'multiple-choice',
			details : function() {
				var html = '<img src="/assets/images/lessons/G7C2L4_img1.png"><br><br>\
A. The basket gets pulled upward<br>\
B. The basket gets pulled downward<br>\
C. The basket remains at its position';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11,
			time : '07:57',
			html : function(controllers) {
				var html = '<h2>How many pairs of balloon and weight can we make from 5 balloons and 7 weights?</h2>\
				<img style="float:right" src="/assets/images/lessons/G7C2L4_img4.png">';
				var choices = [
					'A. 5', 
					'B. 12',
					'C. -2',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 5',
			question : 'How many pairs of balloon and weight can we make from 5 balloons and 7 weights?',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C2L4_img4.png"></p>\
A. 5<br>B. 12<br>C. -2';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 12,
			time : '09:48',
			html : function(controllers) {
				var html = '<p><img src="/assets/images/lessons/G7C2L4_img5.png"></p>';
				html += '<h2>-13 + (-12) = <input type="text" id="answer-box" maxlength="3" /></h2>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : '-25',
			answer_text : '-25',
			question : '-13 + (-12) = ____',
			type : 'input-box',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C2L4_img5.png"></p>';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 13,
			time : '10:10',
			html : function(controllers) {
				var html = '<img style="float:right;" src="/assets/images/lessons/G7C2L4_img6.png">';
				html += '<h2>What is the sum of 9 weights and 20 balloons?</h2>';
				html += '<p>-9 + 20 = <input type="text" id="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 11,
			answer_text : 11,
			question : 'What is the sum of 9 weights and 20 balloons?',
			type : 'input-box',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C2L4_img6.png"></p>';
				html += "-9 + 20 = ___";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 14,
			time : '11:00',
			html : function(controllers) {
				var html = '<h2>Find 8 + (-32)</h2>';
				html += '<p>8 + (-32) = <input type="text" id="answer-box" maxlength="3" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : '-24',
			answer_text : '-24',
			question : 'Find 8 + (-32)',
			type : 'input-box',
			details : function() {
				var html = "8 + (-32) = ___";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
			end_session : 1,
		},
	],
};
