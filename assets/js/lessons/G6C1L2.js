var G6C1L2 = {
	title : 'Comparing decimals',
	duration : '12:50',
	data : [
		{	quiz_number : 1,
			time : '00:28',
			html : function(controllers) {
				var html = '<h2>What does this symbol <strong>&lt;</strong> mean?</h2>';
				var choices = [
					'A. less than', 
					'B. greater than',
					'C. equal to'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. less than',
			question : 'What does this symbol <strong>&lt;</strong> mean?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. less than<br>B. greater than<br>C. equal to";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2,
			time : '00:48',
			html : function(controllers) {
				var html = '<h2>How about this symbol <strong>&gt;</strong>?</h2>';
				var choices = [
					'A. less than', 
					'B. greater than',
					'C. equal to'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. greater than',
			question : 'How about this symbol <strong>&gt;</strong>?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. less than<br>B. greater than<br>C. equal to";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '01:09',
			html : function(controllers) {
				var html = '<h2>This symbol <strong>=</strong> means ________________</h2>';
				var choices = [
					'A. less than', 
					'B. greater than',
					'C. equal to'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. equal to',
			question : 'This symbol <strong>=</strong> means ________________',
			type : 'multiple-choice',
			details : function() {
				var html = "A. less than<br>B. greater than<br>C. equal to";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '02:09',
			html : function(controllers) {
				var html = '<h2>Which part of the numbers should we compare first? <br><center><strong>10.3 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;100.3</strong></center></h2>';
				var choices = [
					'A. whole number part', 
					'B. numbers after the decimal point',
					'C. both parts'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. whole number part',
			question : 'Which part of the numbers should we compare first? <strong>10.3 &nbsp;&nbsp; 100.3</strong>',
			type : 'multiple-choice',
			details : function() {
				var html = "A. whole number part<br>B. numbers after the decimal point<br>C. both parts";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5,
			time : '02:46',
			html : function(controllers) {
				var html = '<h2>For the whole number part, which is greater, the tens or the hundreds? <br><center><strong>10.3 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;100.3</strong></center></h2>';
				var choices = [
					'A. tens', 
					'B. hundreds',
					'C. none of the two'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. hundreds',
			question : 'For the whole number part, which is greater, the tens or the hundreds? <strong>10.3 &nbsp;&nbsp; 100.3</strong></center>',
			type : 'multiple-choice',
			details : function() {
				var html = "A. tens<br>B. hundreds<br>C. none of the two";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 6,
			time : '03:31',
			html : function(controllers) {
				var html = '<h2>When can we compare the numbers after the decimal point? <br><center><strong>10.3 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;100.3</strong></center></h2>';
				var choices = [
					'A. after comparing the numbers in the whole number part', 
					'B. when the numbers in the whole number part are the same',
					'C. whenever you like to'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. when the numbers in the whole number part are the same',
			question : 'When can we compare the numbers after the decimal point? <strong>10.3 &nbsp;&nbsp; 100.3</strong>',
			type : 'multiple-choice',
			details : function() {
				var html = "A. after comparing the numbers in the whole number part<br>\
B. when the numbers in the whole number part are the same<br>\
C. whenever you like to";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time :  '03:58',
			html : function(controllers) {
				var html = '<h2>Let\'s focus on the numbers after the decimal point, which is greater, tenths or hundredths? </h2>';
				var choices = [
					'A. tenths', 
					'B. hundredths',
					'C. none of the above'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. tenths',
			question : 'Let\'s focus on the numbers after the decimal point, which is greater, tenths or hundredths?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. tenths<br>B. hundredths<br>C. none of the above";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '05:10',
			html : function(controllers) {
				var html = '<h2>So, which is greater, is it 10.3 or 100.3?<br><center><strong>10.3 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;100.3</strong></center></h2>';
				var choices = [
					'A. 10.3', 
					'B. 100.3',
					'C. can\'t be determined'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 100.3',
			question : 'So, which is greater, is it 10.3 or 100.3? <strong>10.3 &nbsp;&nbsp; 100.3</strong></center>',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 10.3<br>B. 100.3<br>C. can't be determined";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '06:06',
			html : function(controllers) {
				var html = '<h2><strong>20.5</strong> is ________ <strong>20.5</strong></h2>';
				var choices = [
					'A. <', 
					'B. >',
					'C. ='
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. =',
			question : '<strong>20.5</strong> is ________ <strong>20.5</strong>',
			type : 'multiple-choice',
			details : function() {
				var html = "A. <<br>B. ><br>C. =";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '06:51',
			html : function(controllers) {
				var html = '<h2>How about these numbers? What can you say about the two?<br><strong>30.2</strong> is ________ <strong>20.3</strong></h2>';
				var choices = [
					'A. <', 
					'B. >',
					'C. ='
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. >',
			question : 'How about these numbers? What can you say about the two?<br><strong>30.2</strong> is ________ <strong>20.3</strong>',
			type : 'multiple-choice',
			details : function() {
				var html = "A. <<br>B. ><br>C. =";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11,
			time : '07:34',
			html : function(controllers) {
				var html = '<h2><strong>21.05</strong> is ________ <strong>21.5</strong></h2>';
				var choices = [
					'A. <', 
					'B. >',
					'C. ='
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. <',
			question : '<strong>21.05</strong> is ________ <strong>21.5</strong>',
			type : 'multiple-choice',
			details : function() {
				var html = "A. <<br>B. ><br>C. =";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 12,
			time : '08:59',
			html : function(controllers) {
				var html = '<h2>Who swim slower?</h2>';
				var choices = [
					'A. Emily 47.50 seconds', 
					'B. Cielo 47.12 seconds',
					'C. Both of them',
					'D. None of them'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. Emily 47.50 seconds',
			question : 'Who swim slower?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Emily 47.50 seconds<br>B. Cielo 47.12 seconds<br>C. Both of them<br>D. None of them";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 13,
			time : '09:46',
			html : function(controllers) {
				var html = '<img src="/assets/images/lessons/G6C1L2_matrix1.png" width="100%">\
				<h2>Which of the following holds TRUE for the numbers in the whole number part?</h2>';
				var choices = [
					'A. The whole numbers parts are the same.', 
					'B. The first number\'s whole number part is greater than the second one',
					'C. The second number\'s tens and ones are greater than the first one.'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. The whole numbers parts are the same.',
			question : 'Which of the following holds TRUE for the numbers in the whole number part?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G6C1L2_matrix1.png" width="100%"></p>';
				html += "A. The whole numbers parts are the same.<br>\
B. The first number\'s whole number part is greater than the second one<br>\
C. The second number\'s tens and ones are greater than the first one.";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 14,
			time : '10:26',
			html : function(controllers) {
				var html = '<img src="/assets/images/lessons/G6C1L2_matrix1.png" width="100%">\
				<h2>What do we compare next?</h2>';
				var choices = [
					'A. The hundredths place', 
					'B. The tenth place',
					'C. The ones place',
					'D. We are done comparing'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. The tenth place',
			question : 'What do we compare next?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G6C1L2_matrix1.png" width="100%"></p>';
				html += "A. The hundredths place<br>B. The tenth place<br>C. The ones place<br>D. We are done comparing";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 15,
			time : '11:05',
			html : function(controllers) {
				var html = '<img src="/assets/images/lessons/G6C1L2_matrix1.png" width="100%">\
				<h2>For the tenths place, we have 5 and 1, which of the two is greater?</h2>';
				var choices = [
					'A. 5', 
					'B. 1',
					'C. None'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 5',
			question : 'For the tenths place, we have 5 and 1, which of the two is greater?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G6C1L2_matrix1.png" width="100%"></p>';
				html += "A. 5<br>B. 1<br>C. None";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 16,
			time : '11:43',
			html : function(controllers) {
				var html = '<img src="/assets/images/lessons/G6C1L2_matrix1.png" width="100%">\
				<h2>So, which of the two numbers is greater, is it 47.50 or 47.12?</h2>';
				var choices = [
					'A. 47.50', 
					'B. 47.12',
					'C. None'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 47.50',
			question : 'So, which of the two numbers is greater, is it 47.50 or 47.12?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G6C1L2_matrix1.png" width="100%"></p>';
				html += "A. 47.50<br>B. 47.12<br>C. None";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
			end_session : 1,
		},
	],
};

