var G6C1L8 = {
	title : 'Dividing decimals',
	duration : '26:35',
	data : [
		{	quiz_number : 1,
			time : '00:26',
			html : function(controllers) {
				var html = '<h2>63 divided by 7 is equal to how much?</h2>';
				var choices = [
					'A. 7', 
					'B. 9',
					'C. 8',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 9',
			question : '63 divided by 7 is equal to how much?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 7<br>B. 9<br>C. 8";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2,
			time : '01:11',
			html : function(controllers) {
				var html = '<h2>Which of the following digits is the dividend?</h2>';
				html += '<span style="font-size:40px;font-weight:bold;float:right;">63 &divide; 7 = 9</span>';
				var choices = [
					'A. 63', 
					'B. 7',
					'C. 9',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 63',
			question : 'Which of the following digits is the dividend?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>63 &divide; 7 = 9</p>A. 63<br>B. 7<br>C. 9";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '01:33',
			html : function(controllers) {
				var html = '<h2>Which is the divisor?</h2>';
				html += '<span style="font-size:40px;font-weight:bold;float:right;">63 &divide; 7 = 9</span>';
				var choices = [
					'A. 63', 
					'B. 9',
					'C. 7',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 7',
			question : 'Which is the divisor?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>63 &divide; 7 = 9</p>A. 63<br>B. 9<br>C. 7";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '01:55',
			html : function(controllers) {
				var html = '<h2>Which is the quotient?</h2>';
				html += '<span style="font-size:40px;font-weight:bold;float:right;">63 &divide; 7 = 9</span>';
				var choices = [
					'A. 9', 
					'B. 63',
					'C. 7',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 9',
			question : 'Which is the quotient?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>63 &divide; 7 = 9</p>A. 9<br>B. 63<br>C. 7";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},		
		{	quiz_number : 5,
			time : '02:23',
			html : function(controllers) {
				var html = '<h2>100 divided by 25 is equal to how much?</h2>';
				var choices = [
					'A. 4', 
					'B. 5',
					'C. 6',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 4',
			question : '100 divided by 25 is equal to how much?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 4<br>B. 5<br>C. 6";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},		
		{	quiz_number : 6,
			time : '03:15',
			html : function(controllers) {
				var html = '<h2>Find the quotient of 462 divided by 3.</h2>';
				var choices = [
					'A. 145', 
					'B. 154',
					'C. 135',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 154',
			question : 'Find the quotient of 462 divided by 3.',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 145<br>B. 154<br>C. 135";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time : '07:08',
			html : function(controllers) {
				var html = '<h2>Which of the following is the correct multiplication form of the division?</h2>';
				html += '<span style="font-size:40px;font-weight:bold;float:right;">28.8 &divide; 24</span>';
				var choices = [
					'A. ___ x 28.8 = 24', 
					'B. ___ x 24 = 28.8',
					'C. 24 x 28.8 = ___',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. ___ x 24 = 28.8',
			question : 'Which of the following is the correct multiplication form of the division?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>28.8 &divide; 24</p>A. ___ x 28.8 = 24<br>B. ___ x 24 = 28.8<br>C. 24 x 28.8 = ___";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '07:49',
			html : function(controllers) {
				var html = '<h2>Which of the decimal numbers below would give us 28.8 as the product given that the other factor is 24?</h2>';
				html += '<span style="font-size:40px;font-weight:bold;float:right;">___ x 24 = 28.8</span>';
				var choices = [
					'A. 1.2', 
					'B. 12',
					'C. 0.12',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 1.2',
			question : 'Which of the decimal numbers below would give us 28.8 as the product given that the other factor is 24?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>___ x 24 = 28.8</p>A. 1.2<br>B. 12<br>C. 0.12";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '09:43',
			html : function(controllers) {
				var html = '<h2>What\'s missing in the quotient?</h2>';
				html += '<img style="float:right;" src="/assets/images/lessons/G6C1L8_image1.png">';
				var choices = [
					'A. some numbers', 
					'B. decimal point',
					'C. decimal places',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. decimal point',
			question : 'What\'s missing in the quotient?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img style="float:right;" src="/assets/images/lessons/G6C1L8_image1.png"></p>';
					html += "A. some numbers<br>B. decimal point<br>C. decimal places";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '10:07',
			html : function(controllers) {
				var html = '<h2>Where do you think we should put the decimal point in the quotient?</h2>';
				html += '<img style="float:right;" src="/assets/images/lessons/G6C1L8_image1.png">';
				var choices = [
					'A. before 1', 
					'B. between 1 and 2',
					'C. after 2',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. between 1 and 2',
			question : 'Where do you think we should put the decimal point in the quotient?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img style="float:right;" src="/assets/images/lessons/G6C1L8_image1.png"></p>';
					html += "A. before 1<br>B. between 1 and 2<br>C. after 2";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11,
			time : '10:41',
			html : function(controllers) {
				var html = '<h2>What will be our basis to know where to put the decimal point of the quotient?</h2>';
				html += '<img style="float:right;" src="/assets/images/lessons/G6C1L8_image1.png">';
				var choices = [
					'A. the decimal point of the dividend', 
					'B. the decimal point of the divisor',
					'C. the number of digits',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. the decimal point of the dividend',
			question : 'What will be our basis to know where to put the decimal point of the quotient?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img style="float:right;" src="/assets/images/lessons/G6C1L8_image1.png"></p>';
					html += "A. the decimal point of the dividend<br>B. the decimal point of the divisor<br>C. the number of digits";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},		
		{	quiz_number : 12,
			time : '11:39',
			html : function(controllers) {
				var html = '<h2>1000 &divide; 5 = ____</h2>';
				var choices = [
					'A. 200', 
					'B. 20',
					'C. 2',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 200',
			question : '1000 &divide; 5 = ____',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 200<br>B. 20<br>C. 2";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},		
		{	quiz_number : 13,
			time : '12:00',
			html : function(controllers) {
				var html = '<h2>100 &divide; 0.5 = ____</h2>';
				var choices = [
					'A. 200', 
					'B. 20',
					'C. 2',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 200',
			question : '100 &divide; 0.5 = ____',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 200<br>B. 20<br>C. 2";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},		
		{	quiz_number : 14,
			time : '12:37',
			html : function(controllers) {
				var html = '<h2>In 1000 &divide; 5, which is the dividend?</h2>';
				var choices = [
					'A. 5', 
					'B. 1000',
					'C. 1000 and 5',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 1000',
			question : 'In 1000 &divide; 5, which is the dividend?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 5<br>B. 1000<br>C. 1000 and 5";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},		
		{	quiz_number : 15,
			time : '13:02',
			html : function(controllers) {
				var html = '<h2>How about in 100 &divide; 0.5, which is the dividend?</h2>';
				var choices = [
					'A. 100', 
					'B. 0.5',
					'C. 100 and 0.5',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 100',
			question : 'How about in 100 &divide; 0.5, which is the dividend?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 100<br>B. 0.5<br>C. 100 and 0.5";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},		
		{	quiz_number : 16,
			time : '13:34',
			html : function(controllers) {
				var html = '<h2>By how many times as great is 1000 compare to 100?</h2>';
				var choices = [
					'A. 1000 is twice 100', 
					'B. 1000 is five times 100',
					'C. 1000 is ten times 100',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 1000 is ten times 100',
			question : 'By how many times as great is 1000 compare to 100?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 1000 is twice 100<br>\
B. 1000 is five times 100<br>\
C. 1000 is ten times 100";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},		
		{	quiz_number : 17,
			time : '14:18',
			html : function(controllers) {
				var html = '<h2>By how many times as great is 5 compare to 0.5?</h2>';
				var choices = [
					'A. 5 is ten times 0.5', 
					'B. 5 is a hundred times 0.5',
					'C. 5 is a thousand times 0.5',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 5 is ten times 0.5',
			question : 'By how many times as great is 5 compare to 0.5?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 5 is ten times 0.5<br>\
B. 5 is a hundred times 0.5<br>\
C. 5 is a thousand times 0.5";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},		
		{	quiz_number : 18,
			time : '15:26',
			html : function(controllers) {
				var html = '<h2>How do we make the divisors as whole numbers?</h2>';
				html += '<span style="font-size:40px;font-weight:bold;float:right;">100 &divide; 0.5</span>';
				var choices = [
					'A. multiply the divisor by the base 10 until you get a whole number', 
					'B. move the decimal point to the left',
					'C. divide the divisor with multiples of 10 until you get a whole number',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. multiply the divisor by the base 10 until you get a whole number',
			question : 'How do we make the divisors as whole numbers?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>100 &divide; 0.5</p>A. multiply the divisor by the base 10 until you get a whole number<br>\
B. move the decimal point to the left<br>\
C. divide the divisor with multiples of 10 until you get a whole number";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},		
		{	quiz_number : 19,
			time : '15:57',
			html : function(controllers) {
				var html = '<h2>Should we do the same to the dividend?</h2>';
				html += '<span style="font-size:40px;font-weight:bold;float:right;">100 &divide; 0.5</span>';
				var choices = [
					'A. Yes', 
					'B. No',
					
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. Yes',
			question : 'Should we do the same to the dividend?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>100 &divide; 0.5</p>A. Yes<br>B. No";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 20,
			time : '16:28',
			html : function(controllers) {
				var html = '<h2>What happen to the decimal point as we multiply both dividend and divisor by 10?</h2>';
				html += '<span style="font-size:40px;font-weight:bold;float:right;">100 &divide; 0.5</span>';
				var choices = [
					'A. it is still in its place', 
					'B. it is moved to the right',
					'C. it is moved to the left',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. it is moved to the right',
			question : 'What happen to the decimal point as we multiply both dividend and divisor by 10?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>100 &divide; 0.5</p>A. it is still in its place<br>B. it is moved to the right<br>C. it is moved to the left";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 21,
			time : '17:23',
			html : function(controllers) {
				var html = '<h2>What have you observe in the divisor?</h2>';
				html += '<span style="font-size:40px;font-weight:bold;float:right;">6.4 &divide; 0.4</span>';
				var choices = [
					'A. It\'s a whole number', 
					'B. It\'s a decimal number',
					
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. It\'s a decimal number',
			question : 'What have you observe in the divisor?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>6.4 &divide; 0.4</p>A. It's a whole number<br>B. It's a decimal number";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 22,
			time : '17:48',
			html : function(controllers) {
				var html = '<h2>What should we do with the decimal divisor for easy dividing?</h2>';
				html += '<span style="font-size:40px;font-weight:bold;float:right;">6.4 &divide; 0.4</span>';
				var choices = [
					'A. Delete the digits after the decimal point', 
					'B. Round it to the nearest ones',
					'C. Make it a whole number',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. Make it a whole number',
			question : 'What should we do with the decimal divisor for easy dividing?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>6.4 &divide; 0.4</p>A. Delete the digits after the decimal point<br>\
B. Round it to the nearest ones<br>\
C. Make it a whole number";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 23,
			time : '18:15',
			html : function(controllers) {
				var html = '<h2>How are we going to make the decimal number to a whole number?</h2>';
				html += '<span style="font-size:40px;font-weight:bold;float:right;">6.4 &divide; 0.4</span>';
				var choices = [
					'A. multiply the dividend and divisor by 10', 
					'B. round to the nearest digit',
					'C. add zero to the decimal numbers',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. multiply the dividend and divisor by 10',
			question : 'How are we going to make the decimal number to a whole number?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>6.4 &divide; 0.4</p>A. multiply the dividend and divisor by 10<br>\
B. round to the nearest digit<br>\
C. add zero to the decimal numbers";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 24,
			time : '18:39',
			html : function(controllers) {
				var html = '<h2>In what direction should we move the decimal point?</h2>';
				html += '<span style="font-size:40px;font-weight:bold;float:right;">6.4 &divide; 0.4</span>';
				var choices = [
					'A. To the left', 
					'B. To the right',
					'C. Either left or right',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. To the right',
			question : 'In what direction should we move the decimal point?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>6.4 &divide; 0.4</p>A. To the left<br>B. To the right<br>C. Either left or right";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 25,
			time : '18:59',
			html : function(controllers) {
				var html = '<h2>So, what will the divisor look like now?</h2>';
				html += '<span style="font-size:40px;font-weight:bold;float:right;">6.4 &divide; 0.4</span>';
				var choices = [
					'A. 6', 
					'B. 64',
					'C. 4',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 4',
			question : 'So, what will the divisor look like now?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>6.4 &divide; 0.4</p>A. 6<br>B. 64<br>C. 4";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 26,
			time : '19:36',
			html : function(controllers) {
				var html = '<h2>How about 6.4 after the decimal point is moved?</h2>';
				html += '<span style="font-size:40px;font-weight:bold;float:right;">6.4 &divide; 0.4</span>';
				var choices = [
					'A. 6', 
					'B. 64',
					'C. 4',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 64',
			question : 'How about 6.4 after the decimal point is moved?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>6.4 &divide; 0.4</p>A. 6<br>B. 64<br>C. 4";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 27,
			time : '20:02',
			html : function(controllers) {
				var html = '<h2>After moving the decimal points, what should we do next?</h2>';
				html += '<span style="font-size:40px;font-weight:bold;float:right;">6.4 &divide; 0.4</span>';
				var choices = [
					'A. add zeroes to empty places', 
					'B. divide using long division',
					'C. change 64 and 4 back to its original form',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. divide using long division',
			question : 'After moving the decimal points, what should we do next?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>6.4 &divide; 0.4</p>A. add zeroes to empty places<br>\
B. divide using long division<br>\
C. change 64 and 4 back to its original form";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 28,
			time : '21:54',
			html : function(controllers) {
				var html = '<h2>6.4 &divide; 0.4 equals to how much?</h2>';
				var choices = [
					'A. 16', 
					'B. 1.6',
					'C. 0.16',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 16',
			question : '6.4 &divide; 0.4 equals to how much?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 16<br>B. 1.6<br>C. 0.16";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 29,
			time : '23:13',
			html : function(controllers) {
				var html = '<p>A store owner has 9.45 kilograms of candy. If she puts the candy into 9 jars, how much candy will each jar contain?</p>';
				html += '<h2>In order for us to determine how many kilograms of candy in each jar, which of these mathematical sentences can be used?</h2>';
				var choices = [
					'A. weight of candy in each jar = number of jars &divide; weight of candies', 
					'B. weight of candy in each jar = weight of candies &divide; 1 jar',
					'C. weight of candy in each jar = total weight of candies &divide; total number of jars',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. weight of candy in each jar = total weight of candies &divide; total number of jars',
			question : 'In order for us to determine how many kilograms of candy in each jar, which of these mathematical sentences can be used?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>A store owner has 9.45 kilograms of candy. If she puts the candy into 9 jars, how much candy will each jar contain?</p>\
A. weight of candy in each jar = number of jars &divide; weight of candies<br>\
B. weight of candy in each jar = weight of candies &divide; 1 jar<br>\
C. weight of candy in each jar = total weight of candies &divide; total number of jars";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 30,
			time : '23:49',
			html : function(controllers) {
				var html = '<h2>What is 9.45 kg divided by 9 jars?</h2>';
				var choices = [
					'A. 1.5', 
					'B. 15',
					'C. 1.05',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 1.05',
			question : 'What is 9.45 kg divided by 9 jars?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 1.5<br>B. 15<br>C. 1.05";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
			end_session : 1,
		},
	],
};
