var G2C1L1 = {
	title : 'Addition Properties',
	duration : '16:18',
	data : [
		{	quiz_number : 1, 
			time : '00:17',
			html : function(controllers) {
				var html = '<h2>How many oranges can you see in the picture?</h2>';
				html += '<img style="float:right" src="/assets/images/lessons/G2C1L1_orange1.png">';
				var choices = [
					'A. three', 
					'B. four',
					'C. five',
					'D. six'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. four',
			question : 'How many oranges can you see in the picture?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G2C1L1_orange1.png"></p>';
				html += "A. three<br>B. four<br>C. five<br>D. six";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2, 
			time : '00:41',
			html : function(controllers) {
				var html = '<h2>Mommy bought two more oranges, how many oranges are there now?</h2>';
				html += '<img style="float:right" src="/assets/images/lessons/G2C1L1_orange2.png">';
				var choices = [
					'A. three', 
					'B. four',
					'C. five',
					'D. six'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 3,
			answer_text : 'D. six',
			question : 'Mommy bought two more oranges, how many oranges are there now?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p><img src=\"/assets/images/lessons/G2C1L1_orange2.png\"></p>\
A. three<br>\
B. four<br>\
C. five<br>\
D. six";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3, 
			time : '01:05',
			html : function(controllers) {
				var html = '<h2>Write a number sentence that shows how many oranges are now present. </h2>';
				html += '<p><input type="text" class="answer-box">';
				html += ' + <input type="text" class="answer-box">';
				html += ' = <input type="text" class="answer-box"></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : [4,2,6],
			answer_text : '4 + 2 = 6',
			question : 'Write a number sentence that shows how many oranges are now present.',
			type : 'input-box',
			details : function() {
				var html = "______ + ______ = ______";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4, 
			time : '01:32',
			html : function(controllers) {
				var html = '<h2>What is the name of this symbol + ?</h2>';
				var choices = [
					'A. minus', 
					'B. plus',
					'C. equals',
					'D. less',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. plus',
			question : 'What is the name of this symbol + ?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. minus<br>\
B. plus<br>\
C. equals<br>\
D. less";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5, 
			time : '01:53',
			html : function(controllers) {
				var html = '<h2>What does a plus sign do? It _______ numbers. </h2>';
				var choices = [
					'A. adds', 
					'B. subtracts',
					'C. divides',
					'D. multiplies',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. adds',
			question : 'What does a plus sign do? It _______ numbers.',
			type : 'multiple-choice',
			details : function() {
				var html = "A. adds<br>\
B. subtracts<br>\
C. divides<br>\
D. multiplies";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 6, 
			time : '03:31',
			html : function(controllers) {
				var html = '<h2>Given 6 + 2 = 8, What do we call the 6 and 2 numbers?</h2>';
				var choices = [
					'A. add', 
					'B. sum',
					'C. plus',
					'D. addends',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 3,
			answer_text : 'D. addends',
			question : 'Given 6 + 2 = 8, What do we call the 6 and 2 numbers?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. add<br>\
B. sum<br>\
C. plus<br>\
D. addends";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7, 
			time : '03:55',
			html : function(controllers) {
				var html = '<h2>Given 6 + 2 = 8, What do we call the result of the addends which is 8?</h2>';
				var choices = [
					'A. add', 
					'B. sum',
					'C. plus',
					'D. addends',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. sum',
			question : 'Given 6 + 2 = 8, What do we call the result of the addends which is 8?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. add<br>\
B. sum<br>\
C. plus<br>\
D. addends";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8, 
			time : '04:22',
			html : function(controllers) {
				var html = '<h2>Given 6 + 2 = 8, What do we call the operation in which one number is added to another number?</h2>';
				var choices = [
					'A. subtraction', 
					'B. multiplication',
					'C. division',
					'D. addition',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 3,
			answer_text : 'D. addition',
			question : 'Given 6 + 2 = 8, What do we call the operation in which one number is added to another number?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. subtraction<br>\
B. multiplication<br>\
C. division<br>\
D. addition";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9, 
			time : '04:56',
			html : function(controllers) {
				var html = '<h2>4 + 3 is equivalent to _________ ?</h2>';
				var choices = [
					'A. 4 + 4', 
					'B. 5 + 3',
					'C. 3 + 4',
					'D. 3 + 3',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 3 + 4',
			question : '4 + 3 is equivalent to _________ ?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 4 + 4<br>\
B. 5 + 3<br>\
C. 3 + 4<br>\
D. 3 + 3";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10, 
			time : '05:39',
			html : function(controllers) {
				var html = '<h2>Why is the sum the same when you add 2+3 or 3+2? What is the addition properties involved?</h2>';
				var choices = [
					'A. Commutative Property', 
					'B. Associative Property',
					'C. Distributive Property',
					'D. Cumulative Property',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. Commutative Property',
			question : 'Why is the sum the same when you add 2+3 or 3+2? What is the addition properties involved?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Commutative Property<br>\
B. Associative Property<br>\
C. Distributive Property<br>\
D. Cumulative Property";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11, 
			time : '06:31',
			html : function(controllers) {
				var html = '<h2>Now, what happens when one addend is a zero number?</h2>';
				var choices = [
					'A. When you add zero addends to a number, the sum is always the same as the number itself.', 
					'B. When you add zero addends to a number, the sum is always zero.',
					'C. When you add zero addends to a number, the sum is different as the other number.',
					'D. When you add zero addends to a number, the sum is not the same as the number itself.'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. When you add zero addends to a number, the sum is always the same as the number itself.',
			question : 'Now, what happens when one addend is a zero number?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. When you add zero addends to a number, the sum is always the same as the number itself.<br>\
B. When you add zero addends to a number, the sum is always zero.<br>\
C. When you add zero addends to a number, the sum is different as the other number.<br>\
D. When you add zero addends to a number, the sum is not the same as the number itself.";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 12, 
			time : '07:25',
			html : function(controllers) {
				var html = '<h2>4 + 0 = 4 is an example of what property of addition?</h2>';
				var choices = [
					'A. Commutative Property',
					'B. Zero Property',
					'C. Distributive Property',
					'D. Associative Property'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. Zero Property',
			question : '4 + 0 = 4 is an example of what property of addition?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Commutative Property<br>\
B. Zero Property<br>\
C. Distributive Property<br>\
D. Associative Property";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 13, 
			time : '08:21',
			html : function(controllers) {
				var html = '<h2>7 + 0 is equivalent to _______ ?</h2>';
				var choices = [
					'A. 7 + 1',
					'B. 1 + 7',
					'C. 0 + 7',
					'D. 0 + 0'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 0 + 7',
			question : '7 + 0 is equivalent to _______ ?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 7 + 1<br>\
B. 1 + 7<br>\
C. 0 + 7<br>\
D. 0 + 0";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 14, 
			time : '08:54',
			html : function(controllers) {
				var html = '<h2>7 + 0 or 0 + 7 is equal to _______ ?</h2>';
				var choices = [
					'A . 7',
					'B. 8',
					'C. 9',
					'D. 10'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A . 7',
			question : '7 + 0 or 0 + 7 is equal to _______ ?',
			type : 'multiple-choice',
			details : function() {
				var html = "A . 7<br>\
B. 8<br>\
C. 9<br>\
D. 10";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 15, 
			time : '09:22',
			html : function(controllers) {
				var html = '<h2>You can add numbers in any order. The sum will be the same.</h2>';
				var choices = [
					'A. True',
					'B. False'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. True',
			question : 'You can add numbers in any order. The sum will be the same.',
			type : 'multiple-choice',
			details : function() {
				var html = "A. True<br>\
B. False";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 16, 
			time : '09:54',
			html : function(controllers) {
				var html = '<h2> When you add zero addends to a number, the sum is always the number itself.</h2>';
				var choices = [
					'A. True', 
					'B. False'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. True',
			question : 'When you add zero addends to a number, the sum is always the number itself.',
			type : 'multiple-choice',
			details : function() {
				var html = "A. True<br>\
B. False";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 17, 
			time : '10:34',
			html : function(controllers) {
				var html = '<h2>Using equation 4 + 7 = 11, 4 and 7 are called ______ ? </h2>';
				var choices = [
					'A. addends',
					'B. addition',
					'C. sum',
					'D. additives'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. addends',
			question : 'Using equation 4 + 7 = 11, 4 and 7 are called ______ ?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. addends<br>\
B. addition<br>\
C. sum<br>\
D. additives";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 18, 
			time : '10:55',
			html : function(controllers) {
				var html = '<h2>Using equation 4 + 7 = 11, 11 is the ______ ?</h2>';
				var choices = [
					'A. addition',
					'B. sum',
					'C. addends',
					'D. answer'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. sum',
			question : 'Using equation 4 + 7 = 11, 11 is the ______ ?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. addition<br>\
B. sum<br>\
C. addends<br>\
D. answer";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 19, 
			time : '11:18',
			html : function(controllers) {
				var html = '<h2>4 + 7 = 11 is an example of _______________ equation?</h2>';
				var choices = [
					'A. addition',
					'B. subtraction',
					'C. multiplication',
					'D. division'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. addition',
			question : '4 + 7 = 11 is an example of _______________ equation?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. addition<br>\
B. subtraction<br>\
C. multiplication<br>\
D. division";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 20, 
			time : '11:46',
			html : function(controllers) {
				var html = '<h2>4 + 7 can also be written as _________ using the Commutative Property of Addition.</h2>';
				var choices = [
					'A. 5 + 6',
					'B. 3 + 8',
					'C. 7 + 4',
					'D. 10 + 1'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 7 + 4',
			question : '4 + 7 can also be written as _________ using the Commutative Property of Addition.',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 5 + 6<br>\
B. 3 + 8<br>\
C. 7 + 4<br>\
D. 10 + 1";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 21, 
			time : '12:39',
			html : function(controllers) {
				var html = '<h2>Which of the following number sentence is the same with the equation 4 + 7 = 11 using Commutative Property of Addition?</h2>';
				var choices = [
					'A. 5 + 6 = 11',
					'B. 3 + 8 = 11',
					'C. 7 + 4 = 11',
					'D. 10 + 1 = 11'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 7 + 4 = 11',
			question : 'Which of the following number sentence is the same with the equation 4 + 7 = 11 using Commutative Property of Addition?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 5 + 6 = 11<br>\
B. 3 + 8 = 11<br>\
C. 7 + 4 = 11<br>\
D. 10 + 1 = 11";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 22, 
			time : '13:26',
			html : function(controllers) {
				var html = '<h2>The equation 15 + 0 = 15 uses what property of addition?</h2>';
				var choices = [
					'A. Commutative Property', 
					'B. Zero Property',
					'C. Distributive Property',
					'D. Associative Property'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. Zero Property',
			question : 'The equation 15 + 0 = 15 uses what property of addition?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Commutative Property<br>\
B. Zero Property<br>\
C. Distributive Property<br>\
D. Associative Property";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 23, 
			time : '14:03',
			html : function(controllers) {
				var html = '<h2>Which of the following number sentence is the same with the 9 + 0 = 9 equation using the Commutative Property of Addition? </h2>';
				var choices = [
					'A. 5 + 4 = 9',
					'B. 0 + 9 = 9',
					'C. 7 + 2 = 9',
					'D. 6 + 3 = 9',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 0 + 9 = 9',
			question : 'Which of the following number sentence is the same with the 9 + 0 = 9 equation using the Commutative Property of Addition? ',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 5 + 4 = 9<br>\
B. 0 + 9 = 9<br>\
C. 7 + 2 = 9<br>\
D. 6 + 3 = 9";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 24, 
			time : '14:45',
			html : function(controllers) {
				var html = '<h2>Is 6 + 5 = 5 + 6 the same?</h2>';
				var choices = [
					'A. Yes', 
					'B. No',
					'C. I\'m not sure',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. Yes',
			question : 'Is 6 + 5 = 5 + 6 the same?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Yes<br>\
B. No<br>\
C. I'm not sure";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 25, 
			time : '15:18',
			html : function(controllers) {
				var html = '<h2>Is 4 + 11 = 15 using the Commutative Property of Addition the same as 6 + 9 = 15?</h2>';
				var choices = [
					'A. Yes', 
					'B. No',
					'C. I\'m not sure',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. No',
			question : 'Is 4 + 11 = 15 using the Commutative Property of Addition the same as 6 + 9 = 15?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Yes<br>\
B. No<br>\
C. I'm not sure";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 26, 
			time : '15:54',
			html : function(controllers) {
				var html = '<h2>Is 0 + 5 = 5 using Zero Property of Addition the same with 4 + 1 = 5?</h2>';
				var choices = [
					'A. Yes', 
					'B. No',
					'C. I\'m not sure',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. No',
			question : 'Is 0 + 5 = 5 using Zero Property of Addition the same with 4 + 1 = 5?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Yes<br>\
B. No<br>\
C. I'm not sure";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
			end_session : 1,
		},
	],
};
