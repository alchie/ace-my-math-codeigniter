var G4C1L4 = {
	title : 'Round Off Through Thousands',
	duration : '15:57',
	data : [
		{	quiz_number : 1,
			time : '01:00',
			html : function(controllers) {
				var html = '<h2>Which of the following numbers do you think is closer to 30?</h2>';
				var choices = [
					'A. 21', 
					'B. 23',
					'C. 29',
					'D. 24'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 29',
			question : 'Which of the following numbers do you think is closer to 30?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 21<br>B. 23<br>C. 29<br>D. 24";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2,
			time : '01:20',
			html : function(controllers) {
				var html = '<h2>So, 29 is rounded off to which round number?</h2>';
				var choices = [
					'A. 20', 
					'B. 30'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 30',
			question : 'So, 29 is rounded off to which round number?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 20<br>B. 30";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '01:43',
			html : function(controllers) {
				var html = '<h2>Which of the following numbers do you think is closer to 20?</h2>';
				var choices = [
					'A. 28', 
					'B. 22',
					'C. 29',
					'D. 27'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 22',
			question : 'Which of the following numbers do you think is closer to 20?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 28<br>B. 22<br>C. 29<br>D. 27";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '02:03',
			html : function(controllers) {
				var html = '<h2>So, 22 is rounded off to which round number?</h2>';
				var choices = [
					'A. 20', 
					'B. 30',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 20',
			question : 'So, 22 is rounded off to which round number?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 20<br>B. 30";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5,
			time : '02:26',
			html : function(controllers) {
				var html = '<h2>25 is in the middle of 20 and 30. Where do you think we can round off this number to?</h2>';
				var choices = [
					'A. 20', 
					'B. 30',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 30',
			question : '25 is in the middle of 20 and 30. Where do you think we can round off this number to?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 20<br>B. 30";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 6,
			time : '02:52',
			html : function(controllers) {
				var html = '<h2>If any digit to the right of the rounding place is 5 or greater, What should we do with the rounding place?</h2>';
				var choices = [
					'A. Round up (add 1)', 
					'B. Round down (retain digit)',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. Round up (add 1)',
			question : 'If any digit to the right of the rounding place is 5 or greater, What should we do with the rounding place?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Round up (add 1)<br>B. Round down (retain digit)";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time : '03:19',
			html : function(controllers) {
				var html = '<h2>How about if the digit to the right is less than 5. What should we do with the rounding place?</h2>';
				var choices = [
					'A. Round up (add 1)', 
					'B. Round down (retain the digit)',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. Round down (retain the digit)',
			question : 'How about if the digit to the right is less than 5. What should we do with the rounding place?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Round up (add 1)<br>B. Round down (retain the digit)";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '03:49',
			html : function(controllers) {
				var html = '<h2>After rounding up or rounding down, what should we do with all the digits to the right of the rounding place?</h2>';
				var choices = [
					'A. Change all to zeroes', 
					'B. Retain the digit',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. Change all to zeroes',
			question : 'After rounding up or rounding down, what should we do with all the digits to the right of the rounding place?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Change all to zeroes<br>B. Retain the digit";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '04:26',
			html : function(controllers) {
				var html = '<h2>When 94 is rounded off to the nearest tens, it is ____.</h2>';
				var choices = [
					'A. 100', 
					'B. 90',
					'C. 80',
					'D. 95'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 90',
			question : 'When 94 is rounded off to the nearest tens, it is ____.',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 100<br>B. 90<br>C. 80<br>D. 95";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '04:50',
			html : function(controllers) {
				var html = '<h2>When 86 is rounded off to the nearest tens, it is ____.</h2>';
				var choices = [
					'A. 100', 
					'B. 90',
					'C. 80',
					'D. 65'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 90',
			question : 'When 86 is rounded off to the nearest tens, it is ____.',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 100<br>B. 90<br>C. 80<br>D. 65";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11,
			time : '05:13',
			html : function(controllers) {
				var html = '<h2>When 46 is rounded off to the nearest tens, it is ____.</h2>';
				var choices = [
					'A. 50', 
					'B. 40',
					'C. 45',
					'D. 60'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 50',
			question : 'When 46 is rounded off to the nearest tens, it is ____.',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 50<br>B. 40<br>C. 45<br>D. 60";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 12,
			time : '05:46',
			html : function(controllers) {
				var html = '<h2>When 374 is rounded off to the nearest hundreds, it is ____.</h2>';
				var choices = [
					'A. 300', 
					'B. 400',
					'C. 350',
					'D. 370'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 400',
			question : 'When 374 is rounded off to the nearest hundreds, it is ____.',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 300<br>B. 400<br>C. 350<br>D. 370";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 13,
			time : '06:13',
			html : function(controllers) {
				var html = '<h2>When 944 is rounded off to the nearest hundreds, it is ____.</h2>';
				var choices = [
					'A. 900', 
					'B. 100',
					'C. 1000',
					'D. 950'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 900',
			question : 'When 944 is rounded off to the nearest hundreds, it is ____.',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 900<br>B. 100<br>C. 1000<br>D. 950";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 14,
			time : '06:41',
			html : function(controllers) {
				var html = '<h2>How do you round of 261 to the nearest hundreds?</h2>';
				var choices = [
					'A. 200', 
					'B. 300',
					'C. 250',
					'D. 600'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 300',
			question : 'How do you round of 261 to the nearest hundreds?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 200<br>B. 300<br>C. 250<br>D. 600";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 15,
			time : '07:09',
			html : function(controllers) {
				var html = '<h2>When 674 is rounded off to the nearest hundreds, it is ____.</h2>';
				var choices = [
					'A. 600', 
					'B. 650',
					'C. 700',
					'D. 670'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 700',
			question : 'When 674 is rounded off to the nearest hundreds, it is ____.',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 600<br>B. 650<br>C. 700<br>D. 670";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 16,
			time : '07:37',
			html : function(controllers) {
				var html = '<h2>When 294 is rounded off to the nearest hundreds, it is ____.</h2>';
				var choices = [
					'A. 290', 
					'B. 250',
					'C. 200',
					'D. 300'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 3,
			answer_text : 'D. 300',
			question : 'When 294 is rounded off to the nearest hundreds, it is ____.',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 290<br>B. 250<br>C. 200<br>D. 300";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 17,
			time : '08:04',
			html : function(controllers) {
				var html = '<span style="margin-left: 73px;">';
				for( i=20; i < 31; i++ ) {
					
					html += '<span style="margin-right: 16px;"><span style="font-size: 18px;padding:2px;';
					if( i == 30 ) {
						html += 'border:2px solid red;border-radius:20px;';
					}
					html += '">' + i + '</span><span style="border-right:3px solid blue;height:50px;position:absolute;margin: 25px -15px;"></span></span>';
				}
				html += '</span><img src="/assets/images/lessons/G4C1L4_lines.png">';
				html += '<h2>Now using the number line, which other number is rounded off to 30?</h2>';
				var choices = [
					'A. 21', 
					'B. 22',
					'C. 24',
					'D. 26'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 3,
			answer_text : 'D. 26',
			question : 'Now using the number line, which other number is rounded off to 30?',
			type : 'multiple-choice',
			details : function() {
				var html = '<span style="margin-left: 73px;">';
				for( i=20; i < 31; i++ ) {
					
					html += '<span style="margin-right: 16px;"><span style="font-size: 18px;padding:2px;';
					if( i == 30 ) {
						html += 'border:2px solid red;border-radius:20px;';
					}
					html += '">' + i + '</span><span style="border-right:3px solid blue;height:50px;position:absolute;margin: 25px -15px;"></span></span>';
				}
				html += '</span><img src="/assets/images/lessons/G4C1L4_lines.png">';
				html += 'A. 21<br>B. 22<br>C. 24<br>D. 26';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 18,
			time : '08:29',
			html : function(controllers) {
				var html = '<span style="margin-left: 70px;">';
				for( i=20; i < 31; i++ ) {
					
					html += '<span style="margin-right: 16px;"><span style="font-size: 18px;padding:2px;';
					if( i == 20 ) {
						html += 'border:2px solid red;border-radius:20px;';
					}
					html += '">' + i + '</span><span style="border-right:3px solid blue;height:50px;position:absolute;margin: 25px -15px;"></span></span>';
				}
				html += '</span><img src="/assets/images/lessons/G4C1L4_lines.png">';
				html += '<h2>Using the number line again, which other number is rounded off to 20?</h2>';
				var choices = [
					'A. 27', 
					'B. 29',
					'C. 24',
					'D. 26'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 24',
			question : 'Using the number line again, which other number is rounded off to 20?',
			type : 'multiple-choice',
			details : function() {
				var html = '<span style="margin-left: 70px;">';
				for( i=20; i < 31; i++ ) {
					
					html += '<span style="margin-right: 16px;"><span style="font-size: 18px;padding:2px;';
					if( i == 20 ) {
						html += 'border:2px solid red;border-radius:20px;';
					}
					html += '">' + i + '</span><span style="border-right:3px solid blue;height:50px;position:absolute;margin: 25px -15px;"></span></span>';
				}
				html += '</span><img src="/assets/images/lessons/G4C1L4_lines.png">';
				html += 'A. 27<br>B. 29<br>C. 24<br>D. 26';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 19,
			time : '08:53',
			html : function(controllers) {
				var html = '<span style="margin-left: 40px;">';
				for( i=90; i < 200; ) {
					i = i + 10;
					html += '<span style="margin-right: 16px;position:relative;"><span style="font-size: 14px;padding:2px;';
					if( i == 200 ) {
						html += 'border:2px solid red;border-radius:20px;';
					}
					html += '">' + i + '</span><span style="border-right:3px solid blue;height:40px;position:absolute;margin: 25px -15px;"></span></span>';
				}
				html += '</span><img src="/assets/images/lessons/G4C1L4_lines.png">';
				html += '<h2>Which number is rounded off to 200?</h2>';
				var choices = [
					'A. 130', 
					'B. 140',
					'C. 170',
					'D. 110'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 170',
			question : 'Which number is rounded off to 200?',
			type : 'multiple-choice',
			details : function() {
				var html = '<span style="margin-left: 40px;">';
				for( i=90; i < 200; ) {
					i = i + 10;
					html += '<span style="margin-right: 16px;position:relative;"><span style="font-size: 14px;padding:2px;';
					if( i == 200 ) {
						html += 'border:2px solid red;border-radius:20px;';
					}
					html += '">' + i + '</span><span style="border-right:3px solid blue;height:40px;position:absolute;margin: 25px -15px;"></span></span>';
				}
				html += '</span><img src="/assets/images/lessons/G4C1L4_lines.png">';
				html += 'A. 130<br>B. 140<br>C. 170<br>D. 110';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 20,
			time : '09:20',
			html : function(controllers) {
				var html = '<span style="margin-left: 40px;">';
				for( i=90; i < 200; ) {
					i = i + 10;
					html += '<span style="margin-right: 16px;position:relative;"><span style="font-size: 14px;padding:2px;';
					if( i == 100 ) {
						html += 'border:2px solid red;border-radius:20px;';
					}
					html += '">' + i + '</span><span style="border-right:3px solid blue;height:40px;position:absolute;margin: 25px -15px;"></span></span>';
				}
				html += '</span><img src="/assets/images/lessons/G4C1L4_lines.png">';
				html += '<h2>Using the number line, which number is rounded off to 100?</h2>';
				var choices = [
					'A. 130', 
					'B. 150',
					'C. 170',
					'D. 190'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 130',
			question : 'Using the number line, which number is rounded off to 100?',
			type : 'multiple-choice',
			details : function() {
				var html = '<span style="margin-left: 40px;">';
				for( i=90; i < 200; ) {
					i = i + 10;
					html += '<span style="margin-right: 16px;position:relative;"><span style="font-size: 14px;padding:2px;';
					if( i == 100 ) {
						html += 'border:2px solid red;border-radius:20px;';
					}
					html += '">' + i + '</span><span style="border-right:3px solid blue;height:40px;position:absolute;margin: 25px -15px;"></span></span>';
				}
				html += '</span><img src="/assets/images/lessons/G4C1L4_lines.png">';
				html += 'A. 130<br>B. 150<br>C. 170<br>D. 190';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 21,
			time : '09:50',
			html : function(controllers) {
				var html = '<span style="margin-left: 30px;">';
				for( i=900; i < 2000; ) {
					i = i + 100;
					html += '<span style="margin-right: 10px;position:relative;"><span style="font-size: 14px;padding:2px;';
					if( i == 2000 ) {
						html += 'border:2px solid red;border-radius:20px;';
					}
					html += '">' + i + '</span><span style="border-right:3px solid blue;height:40px;position:absolute;margin: 25px -18px;"></span></span>';
				}
				html += '</span><img src="/assets/images/lessons/G4C1L4_lines.png">';
				html += '<h2>Using the number line, which number is rounded off to 2000?</h2>';
				var choices = [
					'A. 1300', 
					'B. 1200',
					'C. 1600',
					'D. 1100'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 1600',
			question : 'Using the number line, which number is rounded off to 2000?',
			type : 'multiple-choice',
			details : function() {
				var html = '<span style="margin-left: 30px;">';
				for( i=900; i < 2000; ) {
					i = i + 100;
					html += '<span style="margin-right: 10px;position:relative;"><span style="font-size: 14px;padding:2px;';
					if( i == 2000 ) {
						html += 'border:2px solid red;border-radius:20px;';
					}
					html += '">' + i + '</span><span style="border-right:3px solid blue;height:40px;position:absolute;margin: 25px -18px;"></span></span>';
				}
				html += '</span><img src="/assets/images/lessons/G4C1L4_lines.png">';
				html += 'A. 1300<br>B. 1200<br>C. 1600<br>D. 1100';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 22,
			time : '10:20',
			html : function(controllers) {
				var html = '<span style="margin-left: 30px;">';
				for( i=900; i < 2000; ) {
					i = i + 100;
					html += '<span style="margin-right: 10px;position:relative;"><span style="font-size: 14px;padding:2px;';
					if( i == 1000 ) {
						html += 'border:2px solid red;border-radius:20px;';
					}
					html += '">' + i + '</span><span style="border-right:3px solid blue;height:40px;position:absolute;margin: 25px -18px;"></span></span>';
				}
				html += '</span><img src="/assets/images/lessons/G4C1L4_lines.png">';
				html += '<h2>Using the number line, which number is rounded off to 1000?</h2>';
				var choices = [
					'A. 1100', 
					'B. 1700',
					'C. 1600',
					'D. 1900'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 1100',
			question : 'Using the number line, which number is rounded off to 1000?',
			type : 'multiple-choice',
			details : function() {
				var html = '<span style="margin-left: 30px;">';
				for( i=900; i < 2000; ) {
					i = i + 100;
					html += '<span style="margin-right: 10px;position:relative;"><span style="font-size: 14px;padding:2px;';
					if( i == 1000 ) {
						html += 'border:2px solid red;border-radius:20px;';
					}
					html += '">' + i + '</span><span style="border-right:3px solid blue;height:40px;position:absolute;margin: 25px -18px;"></span></span>';
				}
				html += '</span><img src="/assets/images/lessons/G4C1L4_lines.png">';
				html += 'A. 1100<br>B. 1700<br>C. 1600<br>D. 1900';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 23,
			time : '10:58',
			html : function(controllers) {
				var html = '<h2>What is 9432 when rounded off to the nearest thousands?</h2>';
				var choices = [
					'A. 4000', 
					'B. 1000',
					'C. 9500',
					'D. 9000'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 3,
			answer_text : 'D. 9000',
			question : 'What is 9432 when rounded off to the nearest thousands?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 4000<br>B. 1000<br>C. 9500<br>D. 9000";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 24,
			time : '11:32',
			html : function(controllers) {
				var html = '<h2>What is 4998 when rounded off to the nearest thousands?</h2>';
				var choices = [
					'A. 4000', 
					'B. 4500',
					'C. 5000',
					'D. 9000'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 5000',
			question : 'What is 4998 when rounded off to the nearest thousands?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 4000<br>B. 4500<br>C. 5000<br>D. 9000";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 25,
			time : '12:03',
			html : function(controllers) {
				var html = '<h2>What is 6500 when rounded off to the nearest thousands?</h2>';
				var choices = [
					'A. 7000', 
					'B. 6500',
					'C. 6000',
					'D. 5000'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 7000',
			question : 'What is 6500 when rounded off to the nearest thousands?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 7000<br>B. 6500<br>C. 6000<br>D. 5000";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 26,
			time : '12:37',
			html : function(controllers) {
				var html = '<h2>What is 1515 when rounded off to the nearest thousands?</h2>';
				var choices = [
					'A. 1000', 
					'B. 1500',
					'C. 2000',
					'D. 1520'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 2000',
			question : 'What is 1515 when rounded off to the nearest thousands?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 1000<br>B. 1500<br>C. 2000<br>D. 1520";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 27,
			time : '13:09',
			html : function(controllers) {
				var html = '<h2>What is 3210 when rounded off to the nearest thousands?</h2>';
				var choices = [
					'A. 3000', 
					'B. 3200',
					'C. 4000',
					'D. 2000'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 3000',
			question : 'What is 3210 when rounded off to the nearest thousands?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 3000<br>B. 3200<br>C. 4000<br>D. 2000";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 28,
			time : '13:40',
			html : function(controllers) {
				var html = '<table class="table table-hover table-bordered">\
				<thead>\
				<tr>\
					<th rowspan="2" style="vertical-align:middle;text-align: center;">Exact Number</th>\
					<th colspan="3" style="text-align: center;">Round Off to the Nearest</th>\
				</tr>\
				<tr>\
					<th style="text-align: center;">Ten</th>\
					<th style="text-align: center;">Hundred</th>\
					<th style="text-align: center;">Thousand</th>\
				</tr>\
				</thead>\
				<tbody>\
					<tr>\
						<td style="text-align: center;width:20%;font-size:20px;"><strong>1 984</strong></td>\
						<td><input class="answer-box autofocus input-index-0" data-index="0" type="text" style="text-align: center;width:100%" /></td>\
						<td><input class="answer-box input-index-1" data-index="1" type="text" style="text-align: center;width:100%" /></td>\
						<td><input class="answer-box input-index-2" data-index="2" type="text" style="text-align: center;width:100%" /></td>\
					</tr>\
					<tr>\
						<td style="text-align: center;width:20%;font-size:20px;"><strong>7 659</strong></td>\
						<td><input class="answer-box input-index-3" data-index="3" type="text" style="text-align: center;width:100%" /></td>\
						<td><input class="answer-box input-index-4" data-index="4" type="text" style="text-align: center;width:100%" /></td>\
						<td><input class="answer-box input-index-5" data-index="5" type="text" style="text-align: center;width:100%" /></td>\
					</tr>\
					<tr>\
						<td style="text-align: center;width:20%;font-size:20px;"><strong>8 412</strong></td>\
						<td><input class="answer-box input-index-6" data-index="6" type="text" style="text-align: center;width:100%" /></td>\
						<td><input class="answer-box input-index-7" data-index="7" type="text" style="text-align: center;width:100%" /></td>\
						<td><input class="answer-box input-index-8" data-index="8" type="text" style="text-align: center;width:100%" /></td>\
					</tr>\
					<tr>\
						<td style="text-align: center;width:20%;font-size:20px;"><strong>5 971</strong></td>\
						<td><input class="answer-box input-index-9" data-index="9" type="text" style="text-align: center;width:100%" /></td>\
						<td><input class="answer-box input-index-10" data-index="10" type="text" style="text-align: center;width:100%" /></td>\
						<td><input class="answer-box input-index-11" data-index="11" type="text" style="text-align: center;width:100%" /></td>\
					</tr>\
					<tr>\
						<td style="text-align: center;width:20%;font-size:20px;"><strong>9 465</strong></td>\
						<td><input class="answer-box input-index-12" data-index="12" type="text" style="text-align: center;width:100%" /></td>\
						<td><input class="answer-box input-index-13" data-index="13" type="text" style="text-align: center;width:100%" /></td>\
						<td><input class="answer-box input-index-14" data-index="14" type="text" style="text-align: center;width:100%" /></td>\
					</tr>\
				</tbody>\
				</table>';
				html += '<center><button id="submit-answer" class="btn btn-default btn-sm" type="button" style="margin-top:15px;">Check Answers</button></center>';
				return html;
			},
			answer : [1980,2000,2000,7660,7700,8000,8410,8400,8000,5970,6000,6000,9470,9500,9000],
			answer_text : '1984 = 1980, 2000, 2000<br>\
7659 = 7660, 7700, 8000<br>\
8412 = 8410, 8400, 8000<br>\
5971 = 5970, 6000, 6000<br>\
9465 = 9470, 9500, 9000',
			question : 'Round Off to the Nearest',
			type : 'multiple-choice',
			details : function() {
				var html = "Exact Number = Ten, Hundred, Thousand<br>\
1984 = ____, ____, ____ <br>\
7659 = ____, ____, ____ <br>\
8412 = ____, ____, ____ <br>\
5971 = ____, ____, ____ <br>\
9465 = ____, ____, ____ ";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				var check_answers = function() {
						var all_correct = true;
						$('.answer-box').each(function() {
							var index = $(this).attr("data-index"),
							user_answer = $(this).val();
							if( user_answer.length > 0 ) {
								if( controllers.sanitize(user_answer) == controllers.sanitize(data.answer[index]) ) {
									$(this).parent().css('background-color', 'green');
								} else {
									all_correct = false;
									$(this).parent().css('background-color', 'red');
								}
							} else {
								all_correct	 = false;
							}
						});
						if( all_correct ) {
							correct_callback();
						}
					};
				$('#submit-answer').click( check_answers );
				$('.autofocus').focus();
				$(".answer-box").keyup(function(e) { 
					var code = e.which, 
						max = $(this).attr("maxlength"), 
						index = $(this).attr("data-index"), 
						count = $(this).val().length;
					
					if(code==13){ 
						e.preventDefault(); 
						check_answers(); 
						if( index == 14 ) { index = -1; }
						$( '.input-index-' + (parseInt(index) + 1 )).focus().select();
					} 
					if(code==9){ 
						e.preventDefault(); 
						check_answers(); 
					} 
				}).click(function(){
					$(this).select();
				});
			},
			end_session : 1,
		},
	],
};
