var G7C1L9 = {
	title : 'Algebra: Arithmetic Sequences',
	duration : '16:39',
	data : [
		{	quiz_number : 1,
			time : '00:37',
			html : function(controllers) {
				var html = '<h2>How many squares do you see in figure 1?</h2>';
				html += '<img src="/assets/images/lessons/G7C1L9_figure1.png" style="width:60%;float:right;">';
				var choices = [
					'A. 2', 
					'B. 3',
					'C. 4',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 4',
			question : 'How many squares do you see in figure 1?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L9_figure1.png"></p>';
				html += "A. 2<br>B. 3<br>C. 4";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2,
			time : '01:05',
			html : function(controllers) {
				var html = '<h2>How about in figure 2, how many squares are there?</h2>';
				html += '<img src="/assets/images/lessons/G7C1L9_figure1.png" style="width:60%;float:right;">';
				var choices = [
					'A. 6', 
					'B. 8',
					'C. 10',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 8',
			question : 'How about in figure 2, how many squares are there?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L9_figure1.png"></p>';
				html += "A. 6<br>B. 8<br>C. 10";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '',//'01:32',
			html : function(controllers) {
				var html = '<h2>In figure 3, how many small squares are there?</h2>';
				html += '<img src="/assets/images/lessons/G7C1L9_figure1.png" style="width:60%;float:right;">';
				var choices = [
					'A. 12', 
					'B. 14',
					'C. 16',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 12',
			question : 'In figure 3, how many small squares are there?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L9_figure1.png"></p>';
				html += "A. 12<br>B. 14<br>C. 16";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '02:12',
			html : function(controllers) {
				var html = '<h2>As we move from figure 1 to figure 2, and figure 2 to figure 3, what pattern do you see?</h2>';
				html += '<img src="/assets/images/lessons/G7C1L9_figure1.png" style="width:55%;right:0;position:absolute;">';
				var choices = [
					'A. the number of squares is added by 4', 
					'B. the number of squares is added by 5',
					'C. the number of squares is added by 6',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. the number of squares is added by 4',
			question : 'As we move from figure 1 to figure 2, and figure 2 to figure 3, what pattern do you see?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L9_figure1.png"></p>';
				html += "A. the number of squares is added by 4<br>\
B. the number of squares is added by 5<br>\
C. the number of squares is added by 6";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5,
			time : '02:46',
			html : function(controllers) {
				var html = '<h2>Suppose the pattern continues. Complete the table to find the number of squares needed to make each figure.</h2>';
				html += '<table class="table table-bordered table-striped">\
				<thead>\
				<tr align="center" style="font-size:16px;font-weight:bold;background-color:#fe388b;">\
					<th style="padding:2px;text-align:center;vertical-align:middle;">Figure</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">1</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">2</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">3</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">4</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">5</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">6</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">7</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">8</th>\
				</tr>\
				</thead>\
				<tbody>\
					<tr align="center" style="font-size:16px;font-weight:bold;background-color:#ffebf4;">\
						<td>Squares needed</td>\
						<td>4</td>\
						<td>8</td>\
						<td>12</td>\
						<td><input data-index="0" class="answer-box autofocus input-index-0" maxlength="2" type="text" style="width: 35px;font-size: 20px;line-height: 1;padding: 0;text-align:center;"></td>\
						<td><input data-index="1" class="answer-box input-index-1" maxlength="2" type="text" style="width: 35px;font-size: 20px;line-height: 1;padding: 0;text-align:center;"></td>\
						<td><input data-index="2" class="answer-box  input-index-2" maxlength="2" type="text" style="width: 35px;font-size: 20px;line-height: 1;padding: 0;text-align:center;"></td>\
						<td><input data-index="3" class="answer-box  input-index-3" maxlength="2" type="text" style="width: 35px;font-size: 20px;line-height: 1;padding: 0;text-align:center;"></td>\
						<td><input data-index="4" class="answer-box  input-index-4" maxlength="2" type="text" style="width: 35px;font-size: 20px;line-height: 1;padding: 0;text-align:center;"></td>\
					</tr>\
				</tbody>\
				</table>\
				';
				html += '<br><center><input id="submit-answer" type="button" value="Submit"><center>';
				return html;
			},
			answer : [16,20,24,28,32],
			answer_text : '16,20,24,28,32',
			question : 'Suppose the pattern continues. Complete the table to find the number of squares needed to make each figure.',
			type : 'input-box',
			details : function() {
				var html = '<table class="table table-bordered table-striped">\
				<thead>\
				<tr align="center" style="font-size:16px;font-weight:bold;background-color:#fe388b;">\
					<th style="padding:2px;text-align:center;vertical-align:middle;">Figure</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">1</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">2</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">3</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">4</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">5</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">6</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">7</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">8</th>\
				</tr>\
				</thead>\
				<tbody>\
					<tr align="center" style="font-size:16px;font-weight:bold;background-color:#ffebf4;">\
						<td>Squares needed</td>\
						<td>4</td>\
						<td>8</td>\
						<td>12</td>\
						<td>___</td>\
						<td>___</td>\
						<td>___</td>\
						<td>___</td>\
						<td>___</td>\
						</tr>\
				</tbody>\
				</table>\
				';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				var check_answer = function(){
					$('.answer-box').each(function(i){
						if( controllers.sanitize($(this).val()) == controllers.sanitize(data.answer[i]) ) {
							$(this).parent().css('background-color', 'green');
						} else {
							$(this).parent().css('background-color', 'red');
						}
					});
				};
				$('#submit-answer').click( check_answer );
				$('.answer-box').keyup(function(e){
					var code = e.which; 
					if(code==13){ e.preventDefault(); check_answer(); } 
				});
				
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 6,
			time : '03:44',
			html : function(controllers) {
				var html = '<img src="/assets/images/lessons/G7C1L9_image2.png">';
				html += '<h2>How many squares would you need to make the 10<sup>th</sup> figure?</h2>';
				var choices = [
					'A. 40', 
					'B. 44',
					'C. 46',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 40',
			question : 'How many squares would you need to make the 10<sup>th</sup> figure?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L9_image2.png"></p>';
				html += "A. 40<br>B. 44<br>C. 46";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time : '05:13',
			html : function(controllers) {
				var html = '<h3><strong>8,11,14,17,20 ...</strong></h3>';
				html += '<h2>How do we find the values in each term?</h2>';
				var choices = [
					'A. by adding 3 to the previous term', 
					'B. by adding 2 to the previous term',
					'C. by adding 4 to the previous term',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. by adding 3 to the previous term',
			question : 'How do we find the values in each term?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>8,11,14,17,20 ...</p>A. by adding 3 to the previous term<br>\
B. by adding 2 to the previous term<br>\
C. by adding 4 to the previous term";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '06:23',
			html : function(controllers) {
				var html = '<h3>What would be the values of the 6<sup>th</sup> term and 7<sup>th</sup> term?</h3>';
				html += '<table class="table table-bordered table-striped">\
				<thead>\
				<tr align="center" style="font-size:14px;font-weight:bold;background-color:#fe388b;">\
					<th style="padding:2px;text-align:center;vertical-align:middle;">Figure</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">Value</th>\
				</tr>\
				</thead>\
				<tbody>\
					<tr align="center">\
						<td style="padding:2px;">1st</td>\
						<td style="padding:2px;">8</td>\
					</tr>\
					<tr align="center">\
						<td style="padding:2px;">2nd</td>\
						<td style="padding:2px;">11</td>\
					</tr>\
					<tr align="center">\
						<td style="padding:2px;">3rd</td>\
						<td style="padding:2px;">14</td>\
					</tr>\
					<tr align="center">\
						<td style="padding:2px;">4th</td>\
						<td style="padding:2px;">17</td>\
					</tr>\
					<tr align="center">\
						<td style="padding:2px;">5th</td>\
						<td style="padding:2px;">20</td>\
					</tr>\
					<tr align="center">\
						<td style="padding:2px;">6th</td>\
						<td style="padding:2px;"><input data-index="0" class="answer-box autofocus input-index-0" maxlength="2" type="text" style="width: 55px;font-size: 20px;line-height: 1;padding: 0;text-align:center;"></td>\
					</tr>\
					<tr align="center">\
						<td style="padding:2px;">7th</td>\
						<td style="padding:2px;"><input data-index="1" class="answer-box input-index-1" maxlength="2" type="text" style="width: 55px;font-size: 20px;line-height: 1;padding: 0;text-align:center;"></td>\
					</tr>\
				</tbody>\
				</table>\
				';
				html += '<br><center><input id="submit-answer" type="button" value="Submit"><center>';
				return html;
			},
			answer : [23,26],
			answer_text : '23,26',
			question : 'What would be the values of the 6<sup>th</sup> term and 7<sup>th</sup> term?',
			type : 'input-box',
			details : function() {
				var html = '<table class="table table-bordered table-striped">\
				<thead>\
				<tr align="center" style="font-size:14px;font-weight:bold;background-color:#fe388b;">\
					<th style="padding:2px;text-align:center;vertical-align:middle;">Figure</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">Value</th>\
				</tr>\
				</thead>\
				<tbody>\
					<tr align="center">\
						<td style="padding:2px;">1st</td>\
						<td style="padding:2px;">8</td>\
					</tr>\
					<tr align="center">\
						<td style="padding:2px;">2nd</td>\
						<td style="padding:2px;">11</td>\
					</tr>\
					<tr align="center">\
						<td style="padding:2px;">3rd</td>\
						<td style="padding:2px;">14</td>\
					</tr>\
					<tr align="center">\
						<td style="padding:2px;">4th</td>\
						<td style="padding:2px;">17</td>\
					</tr>\
					<tr align="center">\
						<td style="padding:2px;">5th</td>\
						<td style="padding:2px;">20</td>\
					</tr>\
					<tr align="center">\
						<td style="padding:2px;">6th</td>\
						<td style="padding:2px;">___</td>\
					</tr>\
					<tr align="center">\
						<td style="padding:2px;">7th</td>\
						<td style="padding:2px;">___</td>\
					</tr>\
				</tbody>\
				</table>\
				';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				var check_answer = function(){
					$('.answer-box').each(function(i){
						if( controllers.sanitize($(this).val()) == controllers.sanitize(data.answer[i]) ) {
							$(this).parent().css('background-color', 'green');
						} else {
							$(this).parent().css('background-color', 'red');
						}
					});
				};
				$('#submit-answer').click( check_answer );
				$('.answer-box').keyup(function(e){
					var code = e.which; 
					if(code==13){ e.preventDefault(); check_answer(); } 
				});
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '07:29',
			html : function(controllers) {
				var html = '<h3><strong>0, 13, 26, 39, ...</strong></h3>';
				html += '<h2>How do you describe the relationship between the terms in arithmetic sequence?</h2>';
				var choices = [
					'A. Each term is found by adding 12 to the previous term', 
					'B. Each term is found by adding 13 to the previous term',
					'C. Each term is found by adding 14 to the previous term',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. Each term is found by adding 13 to the previous term',
			question : 'How do you describe the relationship between the terms in arithmetic sequence?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>0, 13, 26, 39, ...</p>\
A. Each term is found by adding 12 to the previous term<br>\
B. Each term is found by adding 13 to the previous term<br>\
C. Each term is found by adding 14 to the previous term";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '07:51',
			html : function(controllers) {
				var html = '<h3>Find the values of the 5th, 6th and 7th terms.</h3>';
				html += '<table class="table table-bordered table-striped">\
				<tbody>\
					<tr align="center" style="font-size:16px;font-weight:bold;background-color:#ffebf4;">\
						<td>0</td>\
						<td>13</td>\
						<td>26</td>\
						<td>39</td>\
						<td><input data-index="0" class="answer-box autofocus input-index-0" maxlength="2" type="text" style="width: 35px;font-size: 20px;line-height: 1;padding: 0;text-align:center;"></td>\
						<td><input data-index="1" class="answer-box input-index-1" maxlength="2" type="text" style="width: 35px;font-size: 20px;line-height: 1;padding: 0;text-align:center;"></td>\
						<td><input data-index="2" class="answer-box  input-index-2" maxlength="2" type="text" style="width: 35px;font-size: 20px;line-height: 1;padding: 0;text-align:center;"></td>\
					</tr>\
				</tbody>\
				</table>\
				';
				html += '<br><center><input id="submit-answer" type="button" value="Submit"><center>';
				return html;
			},
			answer : [52,65,78],
			answer_text : '52,65,78',
			question : 'Find the values of the 5th, 6th and 7th terms.',
			type : 'input-box',
			details : function() {
				var html = '<table class="table table-bordered table-striped">\
				<tbody>\
					<tr align="center" style="font-size:16px;font-weight:bold;background-color:#ffebf4;">\
						<td>0</td>\
						<td>13</td>\
						<td>26</td>\
						<td>39</td>\
						<td>___</td>\
						<td>___</td>\
						<td>___</td>\
					</tr>\
				</tbody>\
				</table>\
				';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				var check_answer = function(){
					$('.answer-box').each(function(i){
						if( controllers.sanitize($(this).val()) == controllers.sanitize(data.answer[i]) ) {
							$(this).parent().css('background-color', 'green');
						} else {
							$(this).parent().css('background-color', 'red');
						}
					});
				};
				$('#submit-answer').click( check_answer );
				$('.answer-box').keyup(function(e){
					var code = e.which; 
					if(code==13){ e.preventDefault(); check_answer(); } 
				});
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11,
			time : '09:33',
			html : function(controllers) {
				var html = '<img src="/assets/images/lessons/G7C1L9_image3.png">';
				html += '<h2>What do you observe in the table?</h2>';
				var choices = [
					'A. As the position number increases by 1, the value of term increases by 2', 
					'B. As the position number increases by 2, the value of term increases by 1',
					'C. As the position number increases by 2, the value of term increases by 2',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. As the position number increases by 1, the value of term increases by 2',
			question : 'What do you observe in the table?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L9_image3.png"></p>';
				html += "A. As the position number increases by 1, the value of term increases by 2<br>\
B. As the position number increases by 2, the value of term increases by 1<br>\
C. As the position number increases by 2, the value of term increases by 2";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 12,
			time : '10:19',
			html : function(controllers) {
				var html = '<h2>If n represents the position in the sequence, which of the following expression is correct in finding the value of the term?</h2>';
				html += '<img src="/assets/images/lessons/G7C1L9_image3.png" style="float:right;">';
				var choices = [
					'A. n - 1', 
					'B. n + 2',
					'C. n x 2',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. n x 2',
			question : 'If n represents the position in the sequence, which of the following expression is correct in finding the value of the term?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L9_image3.png"></p>';
				html += "A. n - 1<br>B. n + 2<br>C. n x 2";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 13,
			time : '10:48',
			html : function(controllers) {
				var html = '<h2>What is the value of 6<sup>th</sup> term?</h2>';
				html += '<img src="/assets/images/lessons/G7C1L9_image3.png" style="float:right;">';
				var choices = [
					'A. 10', 
					'B. 12',
					'C. 14',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 12',
			question : 'What is the value of 6<sup>th</sup> term?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L9_image3.png"></p>';
				html += "A. 10<br>B. 12<br>C. 14";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 14,
			time : '11:42',
			html : function(controllers) {
				var html = '<h2>Fill in the table to display the sequence.</h2>\
				<img src="/assets/images/lessons/G7C1L9_image4.png" style="float:right">';
				html += '<table class="table table-bordered table-striped" style="width:200px">\
				<thead>\
				<tr align="center" style="font-size:14px;font-weight:bold;background-color:#fe388b;">\
					<th style="padding:2px;text-align:center;vertical-align:middle;">Week</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">Boxes</th>\
				</tr>\
				</thead>\
				<tbody>\
					<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">1</td>\
						<td style="padding:2px;"><input data-index="0" class="answer-box autofocus input-index-0" maxlength="2" type="text" style="width: 55px;font-size: 20px;line-height: 1;padding: 0;text-align:center;"></td>\
					</tr>\
					<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">2</td>\
						<td style="padding:2px;"><input data-index="1" class="answer-box input-index-1" maxlength="2" type="text" style="width: 55px;font-size: 20px;line-height: 1;padding: 0;text-align:center;"></td>\
					</tr>\
					<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">3</td>\
						<td style="padding:2px;"><input data-index="2" class="answer-box input-index-2" maxlength="2" type="text" style="width: 55px;font-size: 20px;line-height: 1;padding: 0;text-align:center;"></td>\
					</tr>\
				</tbody>\
				</table>\
				';
				html += '<br><center><input id="submit-answer" type="button" value="Submit"><center>';
				return html;
			},
			answer : [5,10,15],
			answer_text : '5,10,15',
			question : 'Fill in the table to display the sequence.',
			type : 'input-box',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L9_image4.png"></p>';
				html += '<table class="table table-bordered table-striped" style="width:200px">\
				<thead>\
				<tr align="center" style="font-size:14px;font-weight:bold;background-color:#fe388b;">\
					<th style="padding:2px;text-align:center;vertical-align:middle;">Week</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">Boxes</th>\
				</tr>\
				</thead>\
				<tbody>\
					<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">1</td>\
						<td style="padding:2px;">___</td>\
					</tr>\
					<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">2</td>\
						<td style="padding:2px;">___</td>\
					</tr>\
					<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">3</td>\
						<td style="padding:2px;">___</td>\
					</tr>\
				</tbody>\
				</table>\
				';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				var check_answer = function(){
					$('.answer-box').each(function(i){
						if( controllers.sanitize($(this).val()) == controllers.sanitize(data.answer[i]) ) {
							$(this).parent().css('background-color', 'green');
						} else {
							$(this).parent().css('background-color', 'red');
						}
					});
				};
				$('#submit-answer').click( check_answer );
				$('.answer-box').keyup(function(e){
					var code = e.which; 
					if(code==13){ e.preventDefault(); check_answer(); } 
				});
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 15,
			time : '12:23',
			html : function(controllers) {
				var html = '<h2>What do you observe in the number of boxes as weeks pass by?</h2>\
				<img src="/assets/images/lessons/G7C1L9_image5.png" style="float:right;width:250px;">';
				var choices = [
					'A. Each boxes is 5 more than its week number', 
					'B. Each boxes is 5 times its week number',
					'C. Each boxes is 5 less than its week number',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. Each boxes is 5 times its week number',
			question : 'What do you observe in the number of boxes as weeks pass by?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L9_image5.png"></p>';
				html += "A. Each boxes is 5 more than its week number<br>\
B. Each boxes is 5 times its week number<br>\
C. Each boxes is 5 less than its week number";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 16,
			time : '12:45',
			html : function(controllers) {
				var html = '<h2>If n is number of weeks, write the expression to find the n-th boxes.</h2>';
				html += '<table class="table table-bordered table-striped">\
				<thead>\
				<tr align="center" style="font-size:14px;font-weight:bold;background-color:#fe388b;">\
					<th style="padding:2px;text-align:center;vertical-align:middle;">Week</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">Operation</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">Boxes</th>\
				</tr>\
				</thead>\
				<tbody>\
				<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">1</td>\
						<td style="padding:2px;">1x5</td>\
						<td style="padding:2px;">5</td>\
				</tr>\
				<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">2</td>\
						<td style="padding:2px;">2x5</td>\
						<td style="padding:2px;">10</td>\
				</tr>\
				<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">3</td>\
						<td style="padding:2px;">3x5</td>\
						<td style="padding:2px;">15</td>\
				</tr>\
					<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">n</td>\
						<td style="padding:2px;" class="noticeBox"><input data-index="0" class="answer-box autofocus input-index-0" maxlength="1" type="text" style="width: 35px;font-size: 20px;line-height: 1;padding: 0;text-align:center;"> x <input data-index="1" class="answer-box input-index-1" maxlength="1" type="text" style="width: 35px;font-size: 20px;line-height: 1;padding: 0;text-align:center;"></td>\
						<td>5n</td>\
					</tr>\
				</tbody>\
				</table>\
				';
				html += '<br><center><input id="submit-answer" type="button" value="Submit"><center>';
				return html;
			},
			answer : ['n',5],
			answer_text : 'n x 5',
			question : 'If n is number of weeks, write the expression to find the n-th boxes.',
			type : 'multiple-choice',
			details : function() {
				var html = '<table class="table table-bordered table-striped">\
				<thead>\
				<tr align="center" style="font-size:14px;font-weight:bold;background-color:#fe388b;">\
					<th style="padding:2px;text-align:center;vertical-align:middle;">Week</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">Operation</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">Boxes</th>\
				</tr>\
				</thead>\
				<tbody>\
				<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">1</td>\
						<td style="padding:2px;">1x5</td>\
						<td style="padding:2px;">5</td>\
				</tr>\
				<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">2</td>\
						<td style="padding:2px;">2x5</td>\
						<td style="padding:2px;">10</td>\
				</tr>\
				<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">3</td>\
						<td style="padding:2px;">3x5</td>\
						<td style="padding:2px;">15</td>\
				</tr>\
					<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">n</td>\
						<td style="padding:2px;" class="noticeBox">___ x ___</td>\
						<td>5n</td>\
					</tr>\
				</tbody>\
				</table>\
				';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.interchangeable_input_box('answer-box', $, jwp, data, function(){
						correct_callback();
						$('.noticeBox').css('background-color', 'green');
					}, function(){
						wrong_callback();
						$('.noticeBox').css('background-color', 'red');
					});
			},
		},
		{	quiz_number : 17,
			time : '13:15',
			html : function(controllers) {
				var html = '<table class="table table-bordered table-striped">\
				<thead>\
				<tr align="center" style="font-size:14px;font-weight:bold;background-color:#fe388b;">\
					<th style="padding:2px;text-align:center;vertical-align:middle;">Week</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">Operation</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">Boxes</th>\
				</tr>\
				</thead>\
				<tbody>\
				<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">1</td>\
						<td style="padding:2px;">1x5</td>\
						<td style="padding:2px;">5</td>\
				</tr>\
				<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">2</td>\
						<td style="padding:2px;">2x5</td>\
						<td style="padding:2px;">10</td>\
				</tr>\
				<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">3</td>\
						<td style="padding:2px;">3x5</td>\
						<td style="padding:2px;">15</td>\
				</tr>\
					<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">n</td>\
						<td style="padding:2px;">n x 5</td>\
						<td style="padding:2px;">5n</td>\
					</tr>\
				</tbody>\
				</table>\
				';
				html += '<h2>What is the total number of boxes sold at the end of 100<sup>th</sup> week?</h2>';
				var choices = [
					'A. 50', 
					'B. 500',
					'C. 5000',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 500',
			question : 'What is the total number of boxes sold at the end of 100<sup>th</sup> week?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><table class="table table-bordered table-striped">\
				<thead>\
				<tr align="center" style="font-size:14px;font-weight:bold;background-color:#fe388b;">\
					<th style="padding:2px;text-align:center;vertical-align:middle;">Week</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">Operation</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">Boxes</th>\
				</tr>\
				</thead>\
				<tbody>\
				<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">1</td>\
						<td style="padding:2px;">1x5</td>\
						<td style="padding:2px;">5</td>\
				</tr>\
				<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">2</td>\
						<td style="padding:2px;">2x5</td>\
						<td style="padding:2px;">10</td>\
				</tr>\
				<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">3</td>\
						<td style="padding:2px;">3x5</td>\
						<td style="padding:2px;">15</td>\
				</tr>\
					<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">n</td>\
						<td style="padding:2px;">n x 5</td>\
						<td style="padding:2px;">5n</td>\
					</tr>\
				</tbody>\
				</table>\
				</p>';
				html += "A. 50<br>B. 500<br>C. 5000";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 18,
			time : '14:06',
			html : function(controllers) {
				var html = '<h2>Fill in the table to display the sequence.</h2>\
				<img src="/assets/images/lessons/G7C1L9_image6.png" style="float:left;margin-right:20px;"><table class="table table-bordered table-striped" style="width:180px">\
				<thead>\
				<tr align="center" style="font-size:14px;font-weight:bold;background-color:#fe388b;">\
					<th width="50%" style="padding:2px;text-align:center;vertical-align:middle;">Figure</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">Number of Circles</th>\
				</tr>\
				</thead>\
				<tbody>\
				<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">1</td>\
						<td style="padding:2px;"><input data-index="0" class="answer-box autofocus input-index-0" maxlength="1" type="text" style="width: 35px;font-size: 20px;line-height: 1;padding: 0;text-align:center;"></td>\
				</tr>\
				<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">2</td>\
						<td style="padding:2px;"><input data-index="1" class="answer-box input-index-1" maxlength="1" type="text" style="width: 35px;font-size: 20px;line-height: 1;padding: 0;text-align:center;"></td>\
				</tr>\
				<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">3</td>\
						<td style="padding:2px;"><input data-index="2" class="answer-box input-index-2" maxlength="1" type="text" style="width: 35px;font-size: 20px;line-height: 1;padding: 0;text-align:center;"></td>\
				</tr>\
				</tbody>\
				</table>\
				<br><center><input id="submit-answer" type="button" value="Submit"><center>';
				return html;
			},
			answer : [3,6,9],
			answer_text : '3,6,9',
			question : 'Fill in the table to display the sequence.',
			type : 'input-box',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L9_image6.png"></p>\
				<table class="table table-bordered table-striped" style="width:180px">\
				<thead>\
				<tr align="center" style="font-size:14px;font-weight:bold;background-color:#fe388b;">\
					<th width="50%" style="padding:2px;text-align:center;vertical-align:middle;">Figure</th>\
					<th style="padding:2px;text-align:center;vertical-align:middle;">Number of Circles</th>\
				</tr>\
				</thead>\
				<tbody>\
				<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">1</td>\
						<td style="padding:2px;">___</td>\
				</tr>\
				<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">2</td>\
						<td style="padding:2px;">___</td>\
				</tr>\
				<tr align="center" style="font-size:14px;font-weight:bold;">\
						<td style="padding:2px;">3</td>\
						<td style="padding:2px;">___</td>\
				</tr>\
				</tbody>\
				</table>';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				var check_answer = function(){
					$('.answer-box').each(function(i){
						if( controllers.sanitize($(this).val()) == controllers.sanitize(data.answer[i]) ) {
							$(this).parent().css('background-color', 'green');
						} else {
							$(this).parent().css('background-color', 'red');
						}
					});
				};
				$('#submit-answer').click( check_answer );
				$('.answer-box').keyup(function(e){
					var code = e.which; 
					if(code==13){ e.preventDefault(); check_answer(); } 
				});
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 19,
			time : '14:47',
			html : function(controllers) {
				var html = '<h2>What do you observe in the number of circles as the figure progresses?</h2>\
				<img src="/assets/images/lessons/G7C1L9_image7.png" style="float:right;">';
				var choices = [
					'A. Number of circles is three times its figure number', 
					'B. Number of circles is twice its figure number',
					'C. Number of circles is four times its figure number',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. Number of circles is three times its figure number',
			question : 'What do you observe in the number of circles as the figure progresses?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L9_image7.png"></p>';
				html += "A. Number of circles is three times its figure number<br>\
B. Number of circles is twice its figure number<br>\
C. Number of circles is four times its figure number";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 20,
			time : '15:19',
			html : function(controllers) {
				var html = '<h2>If x represents the figure number, which of the following expressions represents the number of circles?</h2>\
				<img src="/assets/images/lessons/G7C1L9_image7.png" style="float:right;">';
				var choices = [
					'A. 2x', 
					'B. 3x',
					'C. 4x',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 3x',
			question : 'If x represents the figure number, which of the following expressions represents the number of circles?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L9_image7.png"></p>';
				html += "A. 2x<br>B. 3x<br>C. 4x";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 21,
			time : '15:48',
			html : function(controllers) {
				var html = '<h2>How many circles are there in the 50th figure? 3x</h2>\
				<img src="/assets/images/lessons/G7C1L9_image6.png" style="float:right;">';
				var choices = [
					'A. 100', 
					'B. 125',
					'C. 150',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 150',
			question : 'How many circles are there in the 50th figure? 3x',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G7C1L9_image6.png"></p>';
				html += "A. 100<br>B. 125<br>C. 150";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
			end_session : 1,
		},
	],
};
