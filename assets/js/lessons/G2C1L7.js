var G2C1L7 = {
	title : 'Count Back to Subtract',
	duration : '14:42',
	data : [
		{	quiz_number : 1,
			time : '00:34',
			html : function(controllers) {
				var html = '<img style="width:100%" src="/assets/images/lessons/G2C1L7_img0.png">\
				<h2>Jo has ten boxes, and he wants to take away six from them. How many boxes will be left?</h2>';
				var choices = [
					'A. 10', 
					'B. 6',
					'C. 4',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 4',
			question : 'Jo has ten boxes, and he wants to take away six from them. How many boxes will be left?',
			type : 'multiple-choice',
			details : function() {
				var html = '<img style="width:100%" src="/assets/images/lessons/G2C1L7_img0.png"><br><br>A. 10<br>B. 6<br>C. 4';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2,
			time : '00:58',
			html : function(controllers) {
				var html = '<img style="width:100%" src="/assets/images/lessons/G2C1L7_img0.png">\
				<h2>We are going to mark X\'s on the boxes that Jo takes away, how many boxes should we mark?</h2>';
				html += '<input id="answer-box" class="autofocus" maxlength="2" type="text" style="width: 100px;font-size: 50px;line-height: 1;padding: 0;text-align:center;margin: 0 auto 15px;display:block;"></div>';
				html += '<center><button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button></center>';
				return html;
			},
			answer : 6,
			answer_text : 6,
			question : 'We are going to mark X\'s on the boxes that Jo takes away, how many boxes should we mark?',
			type : 'input-box',
			details : function() {
				var html = '<img style="width:100%" src="/assets/images/lessons/G2C1L7_img0.png">';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '01:59',
			html : function(controllers) {
				var html = '<img style="width:100%" src="/assets/images/lessons/G2C1L7_img1.png">\
				<h2>Dina has five Ping-Pong balls.<br>John removed the two Ping-Pong balls, how many are left?</h2>';
				var choices = [
					'A. 5', 
					'B. 3',
					'C. 2',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 3',
			question : 'Dina has five Ping-Pong balls.<br>John removed the two Ping-Pong balls, how many are left?',
			type : 'multiple-choice',
			details : function() {
				var html = "<img style=\"width:100%\" src=\"/assets/images/lessons/G2C1L7_img1.png\"><br><br>A. 5<br>B. 3<br>C. 2";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '02:41',
			html : function(controllers) {
				var html = '<h2>What did we do in the previous examples?</h2>';
				var choices = [
					'A. Add', 
					'B. Subtract',
					'C. Multiply',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. Subtract',
			question : 'What did we do in the previous examples?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Add<br>B. Subtract<br>C. Multiply";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5,
			time : '03:26',
			html : function(controllers) {
				var html = '<h2>If Jesse takes away 4 bananas from 8 bananas, how many bananas will be left?</h2>\
				<img style="float:right" src="/assets/images/lessons/G2C1L7_img2.png">';
				var choices = [
					'A. 4', 
					'B. 8',
					'C. 0',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 4',
			question : 'If Jesse takes away 4 bananas from 8 bananas, how many bananas will be left?',
			type : 'multiple-choice',
			details : function() {
				var html = "<img src=\"/assets/images/lessons/G2C1L7_img2.png\"><br><br>A. 4<br>B. 8<br>C. 0";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 6,
			time : '04:06',
			html : function(controllers) {
				var html = '<h2>How do we write 8 take away 1 equals 7 as a subtraction sentence?</h2>\
				<img style="float:right" src="/assets/images/lessons/G2C1L7_img3.png">';
				var choices = [
					'A. 8 - 1 = 7', 
					'B. 8 - 7 = 1',
					'C. 7 - 8 = 1',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 8 - 1 = 7',
			question : 'How do we write 8 take away 1 equals 7 as a subtraction sentence?',
			type : 'multiple-choice',
			details : function() {
				var html = "<img src=\"/assets/images/lessons/G2C1L7_img3.png\"><br><br>A. 8 - 1 = 7<br>B. 8 - 7 = 1<br>C. 7 - 8 = 1";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time : '04:43',
			html : function(controllers) {
				var html = '<h2>Write the subtraction sentence shown in the picture.</h2>';
				html += "<p>";
				html += '<img src="/assets/images/lessons/G2C1L7_car2.png" width="90px" style="padding:10px 10px">';
				html += '<img src="/assets/images/lessons/G2C1L7_car2.png" width="90px" style="padding:10px 10px">';
				html += '<img src="/assets/images/lessons/G2C1L7_car1.png" width="90px" style="padding:10px 10px">';
				html += '<img src="/assets/images/lessons/G2C1L7_car1.png" width="90px" style="padding:10px 10px">';
				html += '<img src="/assets/images/lessons/G2C1L7_car1.png" width="90px" style="padding:10px 10px">';
				html += '<img src="/assets/images/lessons/G2C1L7_car1.png" width="90px" style="padding:10px 10px">';
				html += "</p>";
				var choices = [
					'A. 6 - 4 = 2', 
					'B. 6 - 2 = 4',
					'C. 4 - 2 = 2',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 6 - 2 = 4',
			question : 'Write the subtraction sentence shown in the picture.',
			type : 'multiple-choice',
			details : function() {
				var html = "";
				html += "<p>";
				html += '<img src="/assets/images/lessons/G2C1L7_car2.png" width="90px" style="padding:10px 10px">';
				html += '<img src="/assets/images/lessons/G2C1L7_car2.png" width="90px" style="padding:10px 10px">';
				html += '<img src="/assets/images/lessons/G2C1L7_car1.png" width="90px" style="padding:10px 10px">';
				html += '<img src="/assets/images/lessons/G2C1L7_car1.png" width="90px" style="padding:10px 10px">';
				html += '<img src="/assets/images/lessons/G2C1L7_car1.png" width="90px" style="padding:10px 10px">';
				html += '<img src="/assets/images/lessons/G2C1L7_car1.png" width="90px" style="padding:10px 10px">';
				html += "</p>";
				html += '<p>A. 6 - 4 = 2<br>B. 6 - 2 = 4<br>C. 4 - 2 = 2<p>';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '05:16',
			html : function(controllers) {
				var html = '<h2>Now, what do we call the answer after we have subtract the two numbers?</h2>';
				var choices = [
					'A. Subtrahend', 
					'B. Minuend',
					'C. Difference',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. Difference',
			question : 'Now, what do we call the answer after we have subtract the two numbers?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Subtrahend<br>B. Minuend<br>C. Difference";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '05:51',
			html : function(controllers) {
				var html = '<h2>Find the difference of 9 apples and 3 apples.</h2>\
				<img style="float:right" src="/assets/images/lessons/G2C1L7_img4.png">';
				var choices = [
					'A. 9', 
					'B. 3',
					'C. 6',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 6',
			question : 'Find the difference of 9 apples and 3 apples.',
			type : 'multiple-choice',
			details : function() {
				var html = "<img src=\"/assets/images/lessons/G2C1L7_img4.png\"><br><br>A. 9<br>B. 3<br>C. 6";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '06:41',
			html : function(controllers) {
				var html = '<h2>Let\'s solve, 9 pencils less 5 pencils is _____.</h2>\
				<img style="float:right" src=\"/assets/images/lessons/G2C1L7_img5.png\">';
				var choices = [
					'A. 5', 
					'B. 4',
					'C. 3',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 4',
			question : 'Let\'s solve, 9 pencils less 5 pencils is _____.',
			type : 'multiple-choice',
			details : function() {
				var html = '<img src=\"/assets/images/lessons/G2C1L7_img5.png\"><br><br>A. 5<br>B. 4<br>C. 3';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11,
			time : '08:17',
			html : function(controllers) {
				var html = '<h2>Find the difference of 17 - 8 using count back to subtract strategy.</h2>';
				html += '<img src="/assets/images/lessons/G2C1L7_numberline.png" width="558px" style="padding:10px 10px">';
				var choices = [
					'A. 11', 
					'B. 10',
					'C. 9',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 9',
			question : 'Find the difference of 17 - 8 using count back to subtract strategy.',
			type : 'multiple-choice',
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C1L7_numberline.png" width="558px" style="padding:10px 10px">';
				html += '<br>A. 11<br>B. 10<br>C. 9';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 12,
			time : '09:01',
			html : function(controllers) {
				var html = '<h2>What is the answer of subtract 13 - 6 using count back?</h2>';
				html += '<img src="/assets/images/lessons/G2C1L7_numberline.png" width="558px" style="padding:10px 10px">';
				var choices = [
					'A. 13', 
					'B. 6',
					'C. 7',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 7',
			question : 'What is the answer of subtract 13 - 6 using count back?',
			type : 'multiple-choice',
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C1L7_numberline.png" width="558px" style="padding:10px 10px">';
				html += "<br><br>A. 13<br>B. 6<br>C. 7";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 13,
			time : '09:53',
			html : function(controllers) {
				var html = '<h3>Gina has twelve candies. She gave away five candies to Mary. Use a number line to count back how many candies were left.<br>What number are we going to start counting from?</h3>';
				html += '<img src="/assets/images/lessons/G2C1L7_numberline.png" width="558px" style="padding:10px 10px">';
				var choices = [
					'A. 12', 
					'B. 5',
					'C. 7',
				];
				html += controllers.button_choices(choices);
				
				return html;
			},
			answer : 0,
			answer_text : 'A. 12',
			question : 'What number are we going to start counting from?',
			type : 'multiple-choice',
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C1L7_numberline.png" width="558px" style="padding:10px 10px">';
				html += '<p>Gina has twelve candies. She gave away five candies to Mary. Use a number line to count back how many candies were left.</p>';
				html += '<p>A. 12<br>B. 5<br>C. 7</p>';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 14,
			time : '10:19',
			html : function(controllers) {
				var html = '<h3>Gina has twelve candies. She gave away five candies to Mary. Use a number line to count back how many candies were left.<br>How many numbers are we going to count back?</h3>';
				html += '<img src="/assets/images/lessons/G2C1L7_numberline.png" width="558px" style="padding:10px 10px">';
				var choices = [
					'A. 12', 
					'B. 5',
					'C. 7',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 5',
			question : 'How many numbers are we going to count back?',
			type : 'multiple-choice',
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C1L7_numberline.png" width="558px" style="padding:10px 10px">';
				html += "<p>Gina has twelve candies. She gave away five candies to Mary. Use a number line to count back how many candies were left.</p>";
				html += '<p>A. 12<br>B. 5<br>C. 7</p>';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 15,
			time : '10:46',
			html : function(controllers) {
				var html = '<h3>Gina has twelve candies. She gave away five candies to Mary. Use a number line to count back how many candies were left.<br>How many candies were left to Gina?</h3>';
				html += '<img src="/assets/images/lessons/G2C1L7_numberline.png" width="558px" style="padding:10px 10px">';
				var choices = [
					'A. 5', 
					'B. 6',
					'C. 7',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 7',
			question : 'How many candies were left to Gina?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>Gina has twelve candies. She gave away five candies to Mary. \
				Use a number line to count back how many candies were left.</p>";
				html += '<img src="/assets/images/lessons/G2C1L7_numberline.png" width="558px" style="padding:10px 10px">';
				html += 'A. 5<br>B. 6<br>C. 7';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 16,
			time :  '11:27',
			html : function(controllers) {
				var html = '<img src="/assets/images/lessons/G2C1L7_numberline.png" width="558px" style="padding:10px 10px">';
				html += '<h2>Which is the big number if I have <br><strong>11 - 6?</strong></h2>';
				var choices = [
					'A. 5', 
					'B. 6',
					'C. 11',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 11',
			question : 'Which is the big number if I have 11 - 6?',
			type : 'multiple-choice',
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C1L7_numberline.png" width="558px" style="padding:10px 10px">';
				html += "A. 5<br>B. 6<br>C. 11";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 17,
			time : '12:00',
			html : function(controllers) {
				var html = '<h2>Which is the small number if I have <br><strong>6 - 5</strong>?</h2>';
				html += '<img src="/assets/images/lessons/G2C1L7_numberline.png" width="558px" style="padding:10px 10px">';
				var choices = [
					'A. 5', 
					'B. 6',
					'C. 1',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 5',
			question : 'Which is the small number if I have 6 - 5?',
			type : 'multiple-choice',
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C1L7_numberline.png" width="558px" style="padding:10px 10px">';
				html += 'A. 5<br>B. 6<br>C. 1';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 18,
			time : '12:34',
			html : function(controllers) {
				var html = '<h2>Which is the difference if I have <br><strong>6 - 1 = 5</strong>?</h2>';
				html += '<img src="/assets/images/lessons/G2C1L7_numberline.png" width="558px" style="padding:10px 10px">';
				var choices = [
					'A. 6', 
					'B. 5',
					'C. 1',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 5',
			question : 'Which is the difference if I have 6 - 1 = 5?',
			type : 'multiple-choice',
			details : function() {
				var html = '<img src="/assets/images/lessons/G2C1L7_numberline.png" width="558px" style="padding:10px 10px">';
				html += 'A. 6<br>B. 5<br>C. 1';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 19,
			time : '13:17',
			html : function(controllers) {
				var html = '<h2>Ricky bought 9 marbles in the store. While he was playing, he lost 3 marbles. How many marbles were left?</h2>';
				html += '<img src="/assets/images/lessons/G2C1L7_numberline.png" width="558px" style="padding:10px 10px">';
				var choices = [
					'A. 5', 
					'B. 6',
					'C. 9',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 6',
			question : 'How many marbles were left?',
			type : 'multiple-choice',
			details : function() {
				var html = "Ricky bought 9 marbles in the store. While he was playing, he lost 3 marbles.";
				html += '<img src="/assets/images/lessons/G2C1L7_numberline.png" width="558px" style="padding:10px 10px">';
				html += 'A. 5<br>B. 6<br>C. 9';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
			end_session : 1,
		},
	],
};
