var G2C1L3 = {
	data : [
		{	quiz_number : 1,
			time : '00:32',
			html : function(controllers) {
				var html = '<h2>How many boys can you see?</h2>';
				html += '<img src="/assets/images/lessons/G2C1L3_kids1.png" style="float:right" />';
				var choices = [
					'A. 3', 
					'B. 4',
					'C. 5',
					'D. 6'
				];
				html += controllers.button_choices(choices);
				
				return html;
			},
			answer : 0,
			answer_text : 'A. 3',
			question : 'How many boys can you see?',
			type : 'multiple-choice',
			details : function() {
				var html = "<img src=\"/assets/images/lessons/G2C1L3_kids1.png\" style=\"float:right\" />A. 3<br>\
B. 4<br>\
C. 5<br>\
D. 6";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2,
			time : '00:54',
			html : function(controllers) {
				var html = '<h2>How many girls are there?</h2>';
				html += '<img src="/assets/images/lessons/G2C1L3_kids1.png" style="float:right" />';
				var choices = [
					'A. 3', 
					'B. 4',
					'C. 5',
					'D. 6'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 3',
			question : 'How many girls are there?',
			type : 'multiple-choice',
			details : function() {
				var html = "<img src=\"/assets/images/lessons/G2C1L3_kids1.png\" style=\"float:right\" />A. 3<br>\
B. 4<br>\
C. 5<br>\
D. 6";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '01:18',
			html : function(controllers) {
				var html = '<h2>How many kids are there in all?</h2>';
				html += '<img src="/assets/images/lessons/G2C1L3_kids1.png" style="float:right" />';
				var choices = [
					'A. 3', 
					'B. 4',
					'C. 5',
					'D. 6'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 3,
			answer_text : 'D. 6',
			question : 'How many kids are there in all?',
			type : 'multiple-choice',
			details : function() {
				var html = "<img src=\"/assets/images/lessons/G2C1L3_kids1.png\" style=\"float:right\" />A. 3<br>\
B. 4<br>\
C. 5<br>\
D. 6";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '01:45',
			html : function(controllers) {
				var html = '<h2>Write the addition number sentence of the kids playing in the park.</h2>';
				var choices = [
					'A. three plus three equals six', 
					'B. three + three = six ',
					'C. 3 plus 3 equals 6',
					'D. 3 + 3 = 6'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 3,
			answer_text : 'D. 3 + 3 = 6',
			question : 'Write the addition number sentence of the kids playing in the park.',
			type : 'multiple-choice',
			details : function() {
				var html = "A. three plus three equals six<br>\
B. three + three = six<br>\
C. 3 plus 3 equals 6<br>\
D. 3 + 3 = 6";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5,
			time : '02:26',
			html : function(controllers) {
				var html = '<h2>In <span style="background-color:#CCC;padding:0px 5px">3</span> + <span style="background-color:#CCC;padding:0px 5px">3</span> = 6, what do you call the highlighted numbers?</h2>';
				var choices = [
					'A. add', 
					'B. sum',
					'C. plus',
					'D. addends'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 3,
			answer_text : 'D. addends',
			question : 'In <u>3</u> + <u>3</u> = 6, what do you call the highlighted numbers?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. add<br>\
B. sum<br>\
C. plus<br>\
D. addends";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 6,
			time : '03:00',
			html : function(controllers) {
				var html = '<h2>Are the addends in the addition sentence 3 + 3 = 6 the same?</h2>';
				var choices = [
					'A. Yes', 
					'B. No',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. Yes',
			question : 'Are the addends in the addition sentence 3 + 3 = 6 the same?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Yes<br>\
B. No";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time : '04:18',
			html : function(controllers) {
				var html = '<h2>If I have 5 marbles in my left hand and also 5 marbles in my right hand, what double  fact is that?</h2>';
				html += '<img src="/assets/images/lessons/G2C1L3_marbles1.png" style="float:right">';
				var choices = [
					'A. double 5', 
					'B. double 10',
					'C. double 55',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. double 5',
			question : 'If I have 5 marbles in my left hand and also 5 marbles in my right hand, what double  fact is that?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G2C1L3_marbles1.png"></p>';
				html += "A. double 5<br>\
B. double 10<br>\
C. double 55";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '05:05',
			html : function(controllers) {
				var html = '<h2>So if I have double 7, what is the sum?</h2>';
				var choices = [
					'A. 7', 
					'B. 14',
					'C. 17',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 14',
			question : 'So if I have double 7, what is the sum?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 7<br>\
B. 14<br>\
C. 17";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '05:45',
			html : function(controllers) {
				var html = '<h2>Looking at the addition number sentence 4 + 4 = 8, what double fact do we have?</h2>';
				var choices = [
					'A. double 8', 
					'B. double 2',
					'C. double 4',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. double 4',
			question : 'Looking at the addition number sentence 4 + 4 = 8, what double fact do we have?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. double 8<br>\
B. double 2<br>\
C. double 4";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '06:18',
			html : function(controllers) {
				var html = '<h2>1 + 1 = DOUBLE <input type="text" id="answer-box" maxlength="1" /> </h2>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 1,
			answer_text : 1,
			question : '1 + 1 = DOUBLE ______',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11,
			time : '06:40',
			html : function(controllers) {
				var html = '<h2>2 + 2 = DOUBLE <input type="text" id="answer-box" maxlength="1" /> </h2>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 2,
			answer_text : 2,
			question : '2 + 2 = DOUBLE ______',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 12,
			time : '07:02',
			html : function(controllers) {
				var html = '<h2>DOUBLE 3 is equal to?</h2>';
				html += '<input type="text" id="answer-box" maxlength="1" />';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 6,
			answer_text : 6,
			question : 'DOUBLE 3 is equal to?',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 13,
			time : '07:23',
			html : function(controllers) {
				var html = '<h2>DOUBLE 4 is equal to?</h2>';
				html += '<input type="text" id="answer-box" maxlength="1" />';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 8,
			answer_text : 8,
			question : 'DOUBLE 4 is equal to?',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 14,
			time : '07:44',
			html : function(controllers) {
				var html = '<h2>5 + 5 = 10 is DOUBLE <input type="text" id="answer-box" maxlength="1" /> </h2>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 5,
			answer_text : 5,
			question : '5 + 5 = 10 is DOUBLE ______',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 15,
			time : '08:05',
			html : function(controllers) {
				var html = '<h2>6 + 6 = 12 is DOUBLE <input type="text" id="answer-box" maxlength="1" /> </h2>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 6,
			answer_text : 6,
			question : '6 + 6 = 12 is DOUBLE ______',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 16,
			time : '08:26',
			html : function(controllers) {
				var html = '<h2>DOUBLE 7 is equal to?</h2>';
				html += '<input type="text" id="answer-box" maxlength="2" />';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 14,
			answer_text : 14,
			question : 'DOUBLE 7 is equal to?',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 17,
			time : '08:46',
			html : function(controllers) {
				var html = '<h2>DOUBLE 8 is equal to?</h2>';
				html += '<input type="text" id="answer-box" maxlength="2" />';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 16,
			answer_text : 16,
			question : 'DOUBLE 8 is equal to?',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 18,
			time : '09:08',
			html : function(controllers) {
				var html = '<h2>9 + 9 = DOUBLE <input type="text" id="answer-box" maxlength="1" /> </h2>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 9,
			answer_text : 9,
			question : '9 + 9 = DOUBLE ______',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 19,
			time : '09:31',
			html : function(controllers) {
				var html = '<h2>10 + 10 = DOUBLE <input type="text" id="answer-box" maxlength="2" /> </h2>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 10,
			answer_text : 10,
			question : '10 + 10 = DOUBLE ______',
			type : 'input-box',
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 20,
			time : '10:51',
			html : function(controllers) {
				var html = '<h2>Since Ismael join the group, how many boys are there now?</h2>';
				html += '<img src="/assets/images/lessons/G2C1L3_kids2.png" style="float:right" />';
				var choices = [
					'A. 3', 
					'B. 4',
					'C. 5',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 4',
			question : 'Since Ismael join the group, how many boys are there now?',
			type : 'multiple-choice',
			details : function() {
				var html = "<img src=\"/assets/images/lessons/G2C1L3_kids2.png\" style=\"float:right\" />A. 3<br>\
B. 4<br>\
C. 5";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 21,
			time : '11:19',
			html : function(controllers) {
				var html = '<h2>Now how many kids are there playing in all?</h2>';
				html += '<img src="/assets/images/lessons/G2C1L3_kids2.png" style="float:right" />';
				var choices = [
					'A. 5', 
					'B. 6',
					'C. 7',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 7',
			question : 'Now how many kids are there playing in all?',
			type : 'multiple-choice',
			details : function() {
				var html = "<img src=\"/assets/images/lessons/G2C1L3_kids2.png\" style=\"float:right\" />A. 5<br>\
B. 6<br>\
C. 7";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 22,
			time : '12:16',
			html : function(controllers) {
				var html = '<h2>What is the smaller addend?</h2>';
				var choices = [
					'A. 3', 
					'B. 4',
					
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 3',
			question : 'What is the smaller addend?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 3<br>\
B. 4";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 23,
			time : '13:15',
			html : function(controllers) {
				var html = '<h2>Now, what did you notice about the addends 3 and 4?</h2>';
				var choices = [
					'A. The addends are far from each other.', 
					'B. The addends are near each other.',
					'C. The addends are the same.',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. The addends are near each other.',
			question : 'Now, what did you notice about the addends 3 and 4?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. The addends are far from each other.<br>\
B. The addends are near each other.<br>\
C. The addends are the same.";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 24,
			time : '16:24',
			html : function(controllers) {
				var html = '<h2>Find the sum of 8 and 7.<br> What double fact do you use to get the sum?</h2>';
				var choices = [
					'A. double 8', 
					'B. double 7',
					'C. double 9',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. double 7',
			question : 'Find the sum of 8 and 7. What double fact do you use to get the sum?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. double 8<br>\
B. double 7<br>\
C. double 9";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 25,
			time : '17:04',
			html : function(controllers) {
				var html = '<h2>Find the sum of 8 and 7.<br> How do we solve 8 + 7 and get its sum using your previous answer?</h2>';
				var choices = [
					'A. double 7 + 1 more', 
					'B. double 8 + 1 more',
					'C. double 9 + 1 more',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. double 7 + 1 more',
			question : 'Find the sum of 8 and 7. How do we solve 8 + 7 and get its sum using your previous answer?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. double 7 + 1 more<br>\
B. double 8 + 1 more<br>\
C. double 9 + 1 more";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 26,
			time : '17:55',
			html : function(controllers) {
				var html = '<h2>9 + 8 = ______ can also be solved by:</h2>';
				var choices = [
					'A. double 9 + 1 more', 
					'B. double 8 + 1 more',
					'C. double 7 + 1 more',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. double 8 + 1 more',
			question : '9 + 8 = ______ can also be solved by:',
			type : 'multiple-choice',
			details : function() {
				var html = "A. double 9 + 1 more<br>\
B. double 8 + 1 more<br>\
C. double 7 + 1 more";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 27,
			time : '18:47',
			html : function(controllers) {
				var html = '<h2>DOUBLE 8 plus 1 more is equal to?</h2>';
				var choices = [
					'A. 16 + 1 = 17', 
					'B. 18 + 1 = 19',
					'C. 88 + 1 = 89',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 16 + 1 = 17',
			question : 'DOUBLE 8 plus 1 more is equal to?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 16 + 1 = 17<br>\
B. 18 + 1 = 19<br>\
C. 88 + 1 = 89";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 28,
			time : '19:36',
			html : function(controllers) {
				var html = '<h2>How do we know that the addends we are adding use near doubles facts of addition?</h2>';
				var choices = [
					'A. If the addends are the same', 
					'B. If the addends near each other',
					'C. If the addends are far from each other',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. If the addends near each other',
			question : 'How do we know that the addends we are adding use near doubles facts of addition?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. If the addends are the same<br>\
B. If the addends near each other<br>\
C. If the addends are far from each other";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 29,
			time : '20:14',
			html : function(controllers) {
				var html = '<h2>4 + 4 = 8, is an example of what addition fact?</h2>';
				var choices = [
					'A. Near Doubles', 
					'B. Doubles',
					'C. Identity',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. Doubles',
			question : '4 + 4 = 8, is an example of what addition fact?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Near Doubles<br>\
B. Doubles<br>\
C. Identity";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 30,
			time : '21:00',
			html : function(controllers) {
				var html = '<h2>9 + 8 = 17, is an example of using what fact of addition?</h2>';
				var choices = [
					'A. Near Doubles', 
					'B. Doubles',
					'C. Identity',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. Near Doubles',
			question : '9 + 8 = 17, is an example of using what fact of addition?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Near Doubles<br>\
B. Doubles<br>\
C. Identity";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 31,
			time : '21:54',
			html : function(controllers) {
				var html = '<h2>How are you going to add 5 + 4 using the near doubles fact of addition?</h2>';
				var choices = [
					'A. double 5 + 1 more', 
					'B. double 4 + 1 more',
					'C. double 6 + 1 more',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. double 4 + 1 more',
			question : 'How are you going to add 5 + 4 using the near doubles fact of addition?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. double 5 + 1 more<br>\
B. double 4 + 1 more<br>\
C. double 6 + 1 more";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
			end_session : 1,
		},
	],
};
