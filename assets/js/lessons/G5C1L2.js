var G5C1L2 = {
	title : 'Understanding the decimal place value system (rational numbers)',
	duration : '12:54',
	data : [
		{	quiz_number : 1,
			time : '00:37',
			html : function(controllers) {
				var html = '<img style="width:100%" src="/assets/images/lessons/G5C1L2_img1.png">';
				html += '<h2>What does it mean?</h2>';
				var choices = [
					'A. You multiply 10 to get the place value of the one to the left', 
					'B. You multiply 10 to get the place value of the one to the right',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. You multiply 10 to get the place value of the one to the left',
			question : 'What does it mean?',
			type : 'multiple-choice', 
			details : function() {
				var html = '<img src="/assets/images/lessons/G5C1L2_img1.png">';
				html += "<br><br>A. You multiply 10 to get the place value of the one to the left<br>\
B. You multiply 10 to get the place value of the one to the right";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2,
			time : '01:19',
			html : function(controllers) {
				var html = '<h2>What should we do in order to get the place value of the one to the right?</h2>';
				var choices = [
					'A. We multiply by 10', 
					'B. We divide by 10',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. We divide by 10',
			question : 'What should we do in order to get the place value of the one to the right?',
			type : 'multiple-choice', 
			details : function() {
				var html = "A. We multiply by 10<br>\
B. We divide by 10";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '01:44',
			html : function(controllers) {
				var html = '<h2>What is 100 divided by 10?</h2>';
				var choices = [
					'A. 1000', 
					'B. 100',
					'C. 10',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 10',
			question : 'What is 100 divided by 10?',
			type : 'multiple-choice', 
			details : function() {
				var html = "A. 1000<br>\
B. 100<br>\
C. 10";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '02:11',
			html : function(controllers) {
				var html = '<h2>What is 10 divided by 10?</h2>';
				var choices = [
					'A. 100', 
					'B. 10',
					'C. 1',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 1',
			question : 'What is 10 divided by 10?',
			type : 'multiple-choice', 
			details : function() {
				var html = "A. 100<br>B. 10<br>C. 1";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5,
			time : '02:40',
			html : function(controllers) {
				var html = '<h2>What is 1 divided by 10?</h2>';
				var choices = [
					'A. 10', 
					'B. 1',
					'C. 0.1',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 0.1',
			question : 'What is 1 divided by 10?',
			type : 'multiple-choice', 
			details : function() {
				var html = "A. 10<br>B. 1<br>C. 0.1";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 6,
			time : '03:22',
			html : function(controllers) {
				var html = '<p><img src="/assets/images/lessons/G5C1L2_table0.png"></p>';
				html += '<h2>What is 0.1 &divide; 10?</h2>';
				var choices = [
					'A. 0.1', 
					'B. 0.01',
					'C. 0.001',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 0.01',
			question : 'What is 0.1 &divide; 10?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G5C1L2_table0.png"></p>';
				html += "A. 0.1<br>B. 0.01<br>C. 0.001";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time : '',//'04:03',
			html : function(controllers) {
				var html = '<p><img src="/assets/images/lessons/G5C1L2_table0.png"></p>';
				html += '<h2>What is 0.01 &divide; 10?</h2>';
				var choices = [
					'A. 0.1', 
					'B. 0.01',
					'C. 0.001',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 0.001',
			question : 'What is 0.01 &divide; 10?',
			type : 'multiple-choice', 
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G5C1L2_table0.png"></p>';
				html += "A. 0.1<br>B. 0.01<br>C. 0.001";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '04:40',
			html : function(controllers) {
				var html = '<h2>Without using a calculator, what should be the value in the blank?</h2>';
				html += '<p><img style="width:100%" src="/assets/images/lessons/G5C1L2_table1.png"></p>';
				var choices = [
					'A. 1.0000', 
					'B. 0.0001',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 0.0001',
			question : 'Without using a calculator, what should be the value in the blank?',
			type : 'multiple-choice', 
			details : function() {
				var html = '<img style="width:100%" src="/assets/images/lessons/G5C1L2_table1.png">';
				html += "<br><br>A. 1.0000<br>B. 0.0001";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '05:19',
			html : function(controllers) {
				var html = '<h2>Without using a calculator, what should be the value in the blank?</h2>';
				html += '<p><img style="width:100%" src="/assets/images/lessons/G5C1L2_table2.png"></p>';
				var choices = [
					'A. 0.00001', 
					'B. 0.000001',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 0.00001',
			question : 'Without using a calculator, what should be the value in the blank?',
			type : 'multiple-choice', 
			details : function() {
				var html = '<img style="width:100%" src="/assets/images/lessons/G5C1L2_table2.png">';
				html += "<br><br>A. 0.00001<br>B. 0.000001";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '05:55',
			html : function(controllers) {
				var html = '<h2>Without using a calculator, what should be the value in the blank?</h2>';
				html += '<p><img style="width:100%" src="/assets/images/lessons/G5C1L2_table3.png"></p>';
				var choices = [
					'A. 0.000001', 
					'B. 0.0001',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 0.000001',
			question : 'Without using a calculator, what should be the value in the blank?',
			type : 'multiple-choice', 
			details : function() {
				var html = '<img style="width:100%" src="/assets/images/lessons/G5C1L2_table3.png">';
				html += "<br><br>A. 0.000001<br>B. 0.0001";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11,
			time : '06:42',
			html : function(controllers) {
				var html = '<img style="width:100%" src="/assets/images/lessons/G5C1L2_table4.png">';
				html += '<h2>What did you notice in the values after the ones place value?</h2>';
				var choices = [
					'A. The values have dots', 
					'B. The values are bigger',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. The values have dots',
			question : 'What did you notice in the values after the ones place value?',
			type : 'multiple-choice', 
			details : function() {
				var html = '<img style="width:100%" src="/assets/images/lessons/G5C1L2_table4.png">';
				html += "<br><br>A. The values have dots<br>B. The values are bigger";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 12,
			time : '08:25',
			html : function(controllers) {
				var html = '<h2>What do you call the place value to the right of tenths?</h2>\
				<img style="width:70%;float:right" src="/assets/images/lessons/G5C1L2_table5.png">';
				var choices = [
					'A. Hundreds', 
					'B. Hundredths',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. Hundredths',
			question : 'What do you call the place value to the right of tenths?',
			type : 'multiple-choice', 
			details : function() {
				var html = '<img style="width:100%" src="/assets/images/lessons/G5C1L2_table5.png"><br><br>\
				A. Hundreds<br>B. Hundredths';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 13,
			time : '09:00',
			html : function(controllers) {
				var html = '<h2>What do you call the place value of the right of hundredths?</h2>\
				<img style="width:70%;float:right" src="/assets/images/lessons/G5C1L2_table6.png">';
				var choices = [
					'A. Thousands', 
					'B. Thousandths',
					'C. Ten Thousandths',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. Thousandths',
			question : 'What do you call the place value of the right of hundredths?',
			type : 'multiple-choice', 
			details : function() {
				var html = '<img style="width:100%" src="/assets/images/lessons/G5C1L2_table6.png"><br><br>\
				A. Thousands<br>B. Thousandths<br>C. Ten Thousandths';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 14,
			time : '10:15',
			html : function(controllers) {
				var html = '<h2><strong>1,982.34765</strong><br>What number is in the ten thousandths place?</h2>';
				var choices = [
					'A. 3', 
					'B. 4',
					'C. 5',
					'D. 6',
					'E. 7'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 3,
			answer_text : 'D. 6',
			question : 'What number is in the ten thousandths place?',
			type : 'multiple-choice', 
			details : function() {
				var html = "1,982.34765<br><br>A. 3<br>B. 4<br>C. 5<br>D. 6<br>E. 7";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 15,
			time : '10:57',
			html : function(controllers) {
				var html = '<h2><strong>1,982.34765</strong><br>What number is in the hundred thousandths place?</h2>';
				var choices = [
					'A. 3', 
					'B. 4',
					'C. 5',
					'D. 6',
					'E. 7'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 5',
			question : 'What number is in the hundred thousandths place?',
			type : 'multiple-choice', 
			details : function() {
				var html = "1,982.34765<br><br>A. 3<br>B. 4<br>C. 5<br>D. 6<br>E. 7";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 16,
			time : '11:40',
			html : function(controllers) {
				var html = '<h2>1,982.34765 can also be expressed in expanded form. Fill in the blanks.</h2>';
				html += '<div style="font-size: 25px;"><p>(<input style="font-size: 25px;width:35px" type="text" class="answer-box" maxlength="1" />x1,000)+\
				(<input style="font-size: 25px;width:35px" type="text" class="answer-box" maxlength="1" />x100) +\
				(<input style="font-size: 25px;width:35px" type="text" class="answer-box" maxlength="1" />x10) +\
				(2x<input style="font-size: 25px;width:35px" type="text" class="answer-box" maxlength="1" />)<br> +\
				(<input style="font-size: 25px;width:35px" type="text" class="answer-box" maxlength="1" />x0.1) +\
				(<input style="font-size: 25px;width:35px" type="text" class="answer-box" maxlength="1" />x0.01) +\
				(<input style="font-size: 25px;width:35px" type="text" class="answer-box" maxlength="1" />x0.001) +\
				(<input style="font-size: 25px;width:35px" type="text" class="answer-box" maxlength="1" />x0.0001) +\
				(<input style="font-size: 25px;width:35px" type="text" class="answer-box" maxlength="1" />x0.00001)</p></div>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : [1,9,8,1,3,4,7,6,5],
			answer_text : '1, 9, 8, 1, 3, 4, 7, 6 & 5',
			question : '1,982.34765 can also be expressed in expanded form. Fill in the blanks.',
			type : 'input-box',
			details : function() {
				var html = "(___x1,000)+(___x100)+(___x10)+(2x___)+(___x0.1)+(___x0.01)+(___x0.001)+(___x0.0001)+(___x0.00001)";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
			end_session : 1,
		},
	],
};
