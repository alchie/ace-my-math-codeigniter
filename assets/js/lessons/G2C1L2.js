var G2C1L2 = {
	title : 'Count On to Add',
	duration : '19:04',
	data : [
		{	quiz_number : 1,
			time : '00:26',
			html : function(controllers) {
				var html = '<img src="/assets/images/lessons/g2c1l2_1stpond.jpg" width="40%">';
				html += '<h2>How many fishes are there in the first pond?</h2>';
				html += '<p><input type="text" id="answer-box" maxlength="1" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 8,
			answer_text : 8,
			question : 'How many fishes are there in the first pond?',
			type : 'input-box',
			details : function() {
				var html = '<img src="/assets/images/lessons/g2c1l2_1stpond.jpg" width="40%">';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2,
			time : '00:37',
			html : function(controllers) {
				var html = '<img src="/assets/images/lessons/g2c1l2_2ndpond.jpg" width="40%">';
				html += '<h2>How about the second pond?</h2>';
				html += '<p><input type="text" id="answer-box" maxlength="1" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 5,
			answer_text : 5,
			question : 'How about the second pond?',
			type : 'input-box',
			details : function() {
				var html = '<img src="/assets/images/lessons/g2c1l2_2ndpond.jpg" width="40%">';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '01:04',
			html : function(controllers) {
				var html = '<img src="/assets/images/lessons/g2c1l2_2ndpond.jpg" width="40%" style="float:right"><img src="/assets/images/lessons/g2c1l2_1stpond.jpg" width="40%">';
				html += '<h3>One day, Mr. Hundry Alligator came by and he wants to eat all the fishes in the pond with more fishes in it. Where would he go?</h3>';
				var choices = [
					'A. In the first pond', 
					'B. In the second pond',
					
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. In the first pond',
			question : 'One day, Mr. Hundry Alligator came by and he wants to eat all the fishes in the pond with more fishes in it. Where would he go?',
			type : 'multiple-choice',
			details : function() {
				var html = '<img src="/assets/images/lessons/g2c1l2_2ndpond.jpg" width="40%" style="float:right">\
<img src="/assets/images/lessons/g2c1l2_1stpond.jpg" width="40%"><br><br>\
A. In the first pond<br>\
B. In the second pond';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '01:41',
			html : function(controllers) {
				var html = '<img src="/assets/images/lessons/g2c1l2_2ndpond.jpg" width="40%" style="float:right"><img src="/assets/images/lessons/g2c1l2_1stpond.jpg" width="40%">';
				html += '<h2>Where would his mouth open between the two ponds with fishes?</h2>';
				var choices = [
					'First pond', 
					'Second pond',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'First pond',
			question : 'Where would his mouth open between the two ponds with fishes?',
			type : 'multiple-choice',
			details : function() {
				var html = '<img src="/assets/images/lessons/g2c1l2_2ndpond.jpg" width="40%" style="float:right">\
<img src="/assets/images/lessons/g2c1l2_1stpond.jpg" width="40%"><br><br>\
First pond<br>\
Second pond';
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5,
			time : '02:38',
			html : function(controllers) {
				var html = '<h2>How is this statement read? <br><strong> 1 < 7 </strong></h2>';
				var choices = [
					'A. One is greater than seven', 
					'B. One is less than seven',
					'C. One is equal to seven',
					'D. I\'m not sure'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. One is less than seven',
			question : 'How is this statement read? 1 < 7 ',
			type : 'multiple-choice',
			details : function() {
				var html = "A. One is greater than seven<br>\
B. One is less than seven<br>\
C. One is equal to seven<br>\
D. I'm not sure";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 6,
			time : '03:14',
			html : function(controllers) {
				var html = '<h2>Is <strong>8 < 5</strong> a correct statement?</h2>';
				var choices = [
					'A. Yes', 
					'B. No',
					'C. Maybe',
					'D. I\'m not sure'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. No',
			question : 'Is 8 < 5 a correct statement?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Yes<br>\
B. No<br>\
C. Maybe<br>\
D. I'm not sure";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time : '03:58',
			html : function(controllers) {
				var html = '<h2>Twelve is greater than five is written as ______:</h2>';
				var choices = [
					'A. 12 < 5', 
					'B. 5 > 12',
					'C. 12 > 5',
					'D. 5 < 12'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 12 > 5',
			question : 'Twelve is greater than five is written as ______:',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 12 < 5<br>\
B. 5 > 12<br>\
C. 12 > 5<br>\
D. 5 < 12";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '04:30',
			html : function(controllers) {
				var html = '<h2>Is 17 > 20 a correct statement?</h2>';
				var choices = [
					'A. Yes', 
					'B. No',
					'C. Maybe',
					'D. I\'m not sure'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. No',
			question : 'Is 17 > 20 a correct statement?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Yes<br>\
B. No<br>\
C. Maybe<br>\
D. I\'m not sure";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '05:13',
			html : function(controllers) {
				var html = '<h2>The <strong>=</strong> symbol is used to check if the two numbers are ______ one another.</h2>';
				var choices = [
					'A. less than', 
					'B. greater than',
					'C. not equal',
					'D. equal to'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 3,
			answer_text : 'D. equal to',
			question : 'The = symbol is used to check if the two numbers are ______ one another.',
			type : 'multiple-choice',
			details : function() {
				var html = "A. less than<br>\
B. greater than<br>\
C. not equal<br>\
D. equal to";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '06:00',
			html : function(controllers) {
				var html = '<h2>What can you say about this statement, <strong>9 = 19</strong>?</h2>';
				var choices = [
					'A. The statement is correct. The two numbers are equal to one another.', 
					'B. The statement is correct. The two numbers are equal because each has the same number 9.',
					'C. The statement is incorrect. The two numbers are not equal.',
					'D. The statement in incorrect. Nine is greater than nineteen.'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. The statement is incorrect. The two numbers are not equal.',
			question : 'What can you say about this statement, 9 = 19?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. The statement is correct. The two numbers are equal to one another.<br>\
B. The statement is correct. The two numbers are equal because each has the same number 9.<br>\
C. The statement is incorrect. The two numbers are not equal.<br>\
D. The statement in incorrect. Nine is greater than nineteen.";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11,
			time : '06:46',
			html : function(controllers) {
				var html = '<h2>Can you still remember the word <em>addends</em>? What does it do?</h2>';
				var choices = [
					'A. Addend is the result when you add two numbers.', 
					'B. The numbers you add together are called addends.',
					'C. Addend is the same as the sum.',
					'D. I don\'t know the answer.'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. The numbers you add together are called addends.',
			question : 'Can you still remember the word "addends"? What does it do?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Addend is the result when you add two numbers.<br>\
B. The numbers you add together are called addends.<br>\
C. Addend is the same as the sum.<br>\
D. I don\'t know the answer.";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 12,
			time : '07:26',
			html : function(controllers) {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 11px;position:relative;">';
					html += '<span style="font-size: 14px;padding:2px;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				html += '<h2>What is the greater addend? <strong>4 + 6</strong></h2>';
				var choices = [
					'A. 4', 
					'B. 6',
					'C. None of the above',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 6',
			question : 'What is the greater addend? 4 + 6',
			type : 'multiple-choice',
			details : function() {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 11px;position:relative;">';
					html += '<span style="font-size: 14px;padding:2px;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				html += "<br><br>A. 4<br>\
B. 6<br>\
C. None of the above";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 13,
			time : '08:14',
			html : function(controllers) {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 11px;position:relative;">';
					html += '<span style="font-size: 14px;padding:2px;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				html += '<h2>So, we start from 6 and then count on 4. What number did we stop after counting on 4?</h2>';
				var choices = [
					'A. 9', 
					'B. 10',
					'C. 11',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 10',
			question : 'So, we start from 6 and then count on 4. What number did we stop after counting on 4?',
			type : 'multiple-choice',
			details : function() {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 11px;position:relative;">';
					html += '<span style="font-size: 14px;padding:2px;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				html += "<br><br>A. 9<br>\
B. 10<br>\
C. 11";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 14,
			time : '09:18',
			html : function(controllers) {
				var html = '<h2>To make the counting technique easier, you should always find the _____ addend first.</h2>';
				var choices = [
					'A. Less', 
					'B. Greater',
					'C. Equal',
					'D. I\'m not sure'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. Greater',
			question : 'To make the counting technique easier, you should always find the _____ addend first.',
			type : 'multiple-choice',
			details : function() {
				var html = "A. Less<br>\
B. Greater<br>\
C. Equal<br>\
D. I\'m not sure";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 15,
			time : '09:52',
			html : function(controllers) {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 11px;position:relative;">';
					html += '<span style="font-size: 14px;padding:2px;';
					if( i == 9 ) {
						html += 'border:2px solid red;border-radius:20px;';
					}
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				html += '<h2>What happens if I have 9 + 3, and then start counting <strong>AT 9</strong> instead of <strong>FROM 9</strong>? What is the result?</h2>';
				var choices = [
					'A. 11', 
					'B. 12',
					'C. 13',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 11',
			question : 'What happens if I have 9 + 3, and then start counting AT 9 instead of FROM 9? What is the result?',
			type : 'multiple-choice',
			details : function() {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 11px;position:relative;">';
					html += '<span style="font-size: 14px;padding:2px;';
					if( i == 9 ) {
						html += 'border:2px solid red;border-radius:20px;';
					}
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				html += "<br><br>A. 11<br>\
B. 12<br>\
C. 13";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 16,
			time : '11:17',
			html : function(controllers) {
				var html = '<h2>17 + 2 = 19 can also be written as ______.</h2>';
				var choices = [
					'A. 19 = 17 + 2', 
					'B. 19 + 2 = 17',
					'C. 19 + 17 = 2',
					'D. 19 = 19'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 19 = 17 + 2',
			question : '17 + 2 = 19 can also be written as ______.',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 19 = 17 + 2<br>\
B. 19 + 2 = 17<br>\
C. 19 + 17 = 2<br>\
D. 19 = 19";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 17,
			time : '11:47',
			html : function(controllers) {
				var html = '<h2>What is the greater addend if I have 4 and 14?</h2>';
				var choices = [
					'A. 4', 
					'B. 14',
					'C. 13',
					'D. 15'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 14',
			question : 'What is the greater addend if I have 4 and 14?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 4<br>\
B. 14<br>\
C. 13<br>\
D. 15";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 18,
			time : '12:18',
			html : function(controllers) {
				var html = '<h2>If I have to add 4 and 14 using count on to add technique, what number should I start counting from first?</h2>';
				var choices = [
					'A. 4', 
					'B. 14',
					'C. 13',
					'D. 15'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 14',
			question : 'If I have to add 4 and 14 using count on to add technique, what number should I start counting from first?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 4<br>\
B. 14<br>\
C. 13<br>\
D. 15";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 19,
			time : '12:48',
			html : function(controllers) {
				var html = '<h2>What is the sum if I add 14 and 4 using count on to add technique?</h2>';
				var choices = [
					'A. 14', 
					'B. 16',
					'C. 17',
					'D. 18'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 3,
			answer_text : 'D. 18',
			question : 'What is the sum if I add 14 and 4 using count on to add technique?',
			type : 'multiple-choice',
			details : function() {
				var html = "A. 14<br>\
B. 16<br>\
C. 17<br>\
D. 18";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 20,
			time : '13:38',
			html : function(controllers) {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 11px;position:relative;">';
					html += '<span style="font-size: 14px;padding:2px;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				html += '<h2>If I have <strong>5 + 3</strong>, what is the greater addend?</h2>';
				var choices = [
					'A. 3', 
					'B. 5',
					'C. 8',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'B. 5',
			question : 'If I have 5 + 3, what is the greater addend?',
			type : 'multiple-choice',
			details : function() {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 11px;position:relative;">';
					html += '<span style="font-size: 14px;padding:2px;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				html += "<br><br>A. 3<br>\
B. 5<br>\
C. 8";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 21,
			time : '14:00',
			html : function(controllers) {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 11px;position:relative;">';
					html += '<span style="font-size: 14px;padding:2px;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				html += '<h2>If I have <strong>5 + 3</strong>, how many numbers should I count on to get the sum?</h2>';
				var choices = [
					'A. 3', 
					'B. 5',
					'C. 8',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'A. 3',
			question : 'If I have 5 + 3, how many numbers should I count on to get the sum?',
			type : 'multiple-choice',
			details : function() {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 11px;position:relative;">';
					html += '<span style="font-size: 14px;padding:2px;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				html += "<br><br>A. 3<br>\
B. 5<br>\
C. 8";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 22,
			time : '14:26',
			html : function(controllers) {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 11px;position:relative;">';
					html += '<span style="font-size: 14px;padding:2px;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				html +=  '<h2>If I have <strong>5 + 3</strong>, using count on to add technique, where did the counting stop?</h2>';
				var choices = [
					'A. 7', 
					'B. 5',
					'C. 8',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'C. 8',
			question : 'If I have 5 + 3, using count on to add technique, where did the counting stop?',
			type : 'multiple-choice',
			details : function() {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 11px;position:relative;">';
					html += '<span style="font-size: 14px;padding:2px;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				html += "<br><br>A. 7<br>\
B. 5<br>\
C. 8";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 23,
			time : '14:44',
			html : function(controllers) {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 9px;position:relative;">';
					html += '<span class="numberLine" data-number="'+i+'" style="font-size: 14px;padding:2px;cursor:pointer;border:1px solid #FFF;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				html += '<h2>Adding 9 and 10, what number should I start counting from?</h2>';
				html += '<p><input type="text" id="answer-box" maxlength="5" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 10,
			answer_text : 10,
			question : 'Adding 9 and 10, what number should I start counting from?',
			type : 'input-box',
			details : function() {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 9px;position:relative;">';
					html += '<span class="numberLine" data-number="'+i+'" style="font-size: 14px;padding:2px;cursor:pointer;border:1px solid #FFF;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				$('.numberLine').hover(function(){
					$(this).css('border', '1px solid #000');
				}, function(){
					$(this).css('border', '1px solid #FFF');
				}).click(function(){
					$('.numberLine').css('background-color', 'transparent');
					var number = $(this).attr('data-number');
					$('#answer-box').val(number);
					if( number == data.answer ) {
						$(this).css('background-color', 'green');
						correct_callback();
						$('#answer-box, #submit-answer').prop('disabled', true);
					} else {
						$(this).css('background-color', 'red');
						wrong_callback();
					}
				});
			},
		},
		{	quiz_number : 24,
			time : '14:57',
			html : function(controllers) {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 9px;position:relative;">';
					html += '<span data-number="'+i+'" style="font-size: 14px;padding:2px;cursor:pointer;border:1px solid #FFF;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				html += '<h2>Adding 9 and 10, how many numbers should I count on to get the sum?</h2>';
				html += '<p><input type="text" id="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 9,
			answer_text : 9,
			question : 'Adding 9 and 10, how many numbers should I count on to get the sum?',
			type : 'input-box',
			details : function() {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 9px;position:relative;">';
					html += '<span data-number="'+i+'" style="font-size: 14px;padding:2px;cursor:pointer;border:1px solid #FFF;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 25,
			time : '15:13',
			html : function(controllers) {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 9px;position:relative;">';
					html += '<span class="numberLine" data-number="'+i+'" style="font-size: 14px;padding:2px;cursor:pointer;border:1px solid #FFF;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				html += '<h2>Adding 9 and 10, using count on to add technique, where did the counting stop?</h2>';
				html += '<p><input type="text" id="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 19,
			answer_text : 19,
			question : 'Adding 9 and 10, using count on to add technique, where did the counting stop?',
			type : 'input-box',
			details : function() {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 9px;position:relative;">';
					html += '<span class="numberLine" data-number="'+i+'" style="font-size: 14px;padding:2px;cursor:pointer;border:1px solid #FFF;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				$('.numberLine').hover(function(){
					$(this).css('border', '1px solid #000');
				}, function(){
					$(this).css('border', '1px solid #FFF');
				}).click(function(){
					$('.numberLine').css('background-color', 'transparent');
					var number = $(this).attr('data-number');
					$('#answer-box').val(number);
					if( number == data.answer ) {
						$(this).css('background-color', 'green');
						correct_callback();
						$('#answer-box, #submit-answer').prop('disabled', true);
					} else {
						$(this).css('background-color', 'red');
						wrong_callback();
					}
				});
			},
		},
		{	quiz_number : 26,
			time : '15:29',
			html : function(controllers) {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 11px;position:relative;">';
					html += '<span style="font-size: 14px;padding:2px;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				html += '<h2>2 + 12 = <input type="text" id="answer-box" maxlength="2" /> </h2>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 14,
			answer_text : 14,
			question : '2 + 12 = _____',
			type : 'input-box',
			details : function() {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 11px;position:relative;">';
					html += '<span style="font-size: 14px;padding:2px;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 27,
			time : '16:02',
			html : function(controllers) {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 9px;position:relative;">';
					html += '<span class="numberLine" data-number="'+i+'" style="font-size: 14px;padding:2px;cursor:pointer;border:1px solid #FFF;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				html += '<h2>______ = 6 + 11</h2>';
				html += "<p>Click on the number that is the greater addend?</p>";
				return html;
			},
			answer : 11,
			answer_text : 11,
			question : '______ = 6 + 11 Click on the number that is the greater addend?',
			type : 'multiple-choice',
			details : function() {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 9px;position:relative;">';
					html += '<span class="numberLine" data-number="'+i+'" style="font-size: 14px;padding:2px;cursor:pointer;border:1px solid #FFF;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				$('.numberLine').hover(function(){
					$(this).css('border', '1px solid #000');
				}, function(){
					$(this).css('border', '1px solid #FFF');
				}).click(function(){
					$('.numberLine').css('background-color', 'transparent');
					var number = $(this).attr('data-number');
					$('#answer-box').val(number);
					if( number == data.answer ) {
						$(this).css('background-color', 'green');
						correct_callback();
						$('#answer-box, #submit-answer').prop('disabled', true);
					} else {
						$(this).css('background-color', 'red');
						wrong_callback();
					}
				});
			},
		},
		{	quiz_number : 28,
			time : '16:18',
			html : function(controllers) {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 9px;position:relative;">';
					html += '<span data-number="'+i+'" style="font-size: 14px;padding:2px;cursor:pointer;border:1px solid #FFF;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				html += '<h2>______ = 6 + 11<br>How many numbers should I count on to get the sum starting from the greater addend?</h2>';
				html += '<p><input type="text" id="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 6,
			answer_text : 6,
			question : '______ = 6 + 11 How many numbers should I count on to get the sum starting from the greater addend?',
			type : 'input-box',
			details : function() {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 9px;position:relative;">';
					html += '<span data-number="'+i+'" style="font-size: 14px;padding:2px;cursor:pointer;border:1px solid #FFF;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 29,
			time : '16:37',
			html : function(controllers) {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 9px;position:relative;">';
					html += '<span class="numberLine" data-selected="0" data-number="'+i+'" style="font-size: 14px;padding:2px;cursor:pointer;border:1px solid #FFF;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				html += '<h2>______ = 6 + 11</h2>';
				html += "<p>Now, using your previous answers, let\'s start counting. Press the numbers consecutively from the greater addend to start counting on.</p>";
				return html;
			},
			answer : 12,
			answer_text : "12-17",
			question : '______ = 6 + 11 Now, using your previous answers, let\'s start counting. Press the numbers consecutively from the greater addend to start counting on.',
			type : 'input-box',
			details : function() {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 9px;position:relative;">';
					html += '<span class="numberLine" data-selected="0" data-number="'+i+'" style="font-size: 14px;padding:2px;cursor:pointer;border:1px solid #FFF;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				$('.numberLine').hover(function(){
					$(this).css('border', '1px solid #000');
				}, function(){
					$(this).css('border', '1px solid #FFF');
				}).click(function(){
					$('.numberLine').css('background-color', 'transparent');
					var number = $(this).attr('data-number');
					var selected = parseInt( $(this).attr('data-selected') );
					if( selected == 1 ) {
						return;
					}
					if( number == data.answer) {
						if( number < 17 ) {
							data['answer'] = parseInt(number) + 1;
						}
						$(this).attr('data-selected', 1);
						$(this).css('background-color', 'green');
						$(this).removeClass('numberLine').click(function(){
							
						});
						if( number == 17 ) {
							correct_callback();
							$('.numberLine').attr('data-selected', 1);
						}
					} else {
						$(this).css('background-color', 'red');
						wrong_callback();
					}
				});
			},
		},
		{	quiz_number : 30,
			time : '16:44',
			html : function(controllers) {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 9px;position:relative;">';
					html += '<span data-number="'+i+'" style="font-size: 14px;padding:2px;cursor:pointer;border:1px solid #FFF;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				html += '<h2>______ = 6 + 11<br>';
				html += "Where did the counting stop?</h2>";
				html += '<p><input type="text" id="answer-box" maxlength="2" /></p>';
				html += '<button id="submit-answer" class="btn btn-default btn-sm" type="button">Submit</button>';
				return html;
			},
			answer : 17,
			answer_text : 17,
			question : '______ = 6 + 11 Where did the counting stop?',
			type : 'input-box',
			details : function() {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 9px;position:relative;">';
					html += '<span data-number="'+i+'" style="font-size: 14px;padding:2px;cursor:pointer;border:1px solid #FFF;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 31,
			time : '17:10',
			html : function(controllers) {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 9px;position:relative;">';
					html += '<span class="numberLine" data-number="'+i+'" style="font-size: 14px;padding:2px;cursor:pointer;border:1px solid #FFF;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				html += '<h2>______ = 13 + 7. How do we get the sum?</h2><br><p>Please press the greater addend</p>';
				return html;
			},
			answer : 13,
			answer_text : 13,
			question : '______ = 13 + 7. How do we get the sum? Please press the greater addend',
			type : 'multiple-choice',
			details : function() {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 9px;position:relative;">';
					html += '<span class="numberLine" data-number="'+i+'" style="font-size: 14px;padding:2px;cursor:pointer;border:1px solid #FFF;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				$('.numberLine').hover(function(){
					$(this).css('border', '1px solid #000');
				}, function(){
					$(this).css('border', '1px solid #FFF');
				}).click(function(){
					$('.numberLine').css('background-color', 'transparent');
					var number = $(this).attr('data-number');
					$('#answer-box').val(number);
					if( number == data.answer ) {
						$(this).css('background-color', 'green');
						correct_callback();
						$('#answer-box, #submit-answer').prop('disabled', true);
					} else {
						$(this).css('background-color', 'red');
						wrong_callback();
					}
				});
			},
		},
		{	quiz_number : 32,
			time : '17:23',
			html : function(controllers) {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 9px;position:relative;">';
					html += '<span class="numberLine" data-number="'+i+'" style="font-size: 14px;padding:2px;cursor:pointer;border:1px solid #FFF;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				html += '<h2>______ = 13 + 7. How do we get the sum?</h2><br><p>Please press the sum using count to add technique?</p>';
				return html;
			},
			answer : 20,
			answer_text : 20,
			question : '______ = 13 + 7. How do we get the sum? Please press the sum using count to add technique?',
			type : 'multiple-choice',
			details : function() {
				var html = '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-left: 10px solid black; width:0; height:0;float:right;"></div>';
				html += '<div style="border-top: 10px solid transparent;border-bottom: 10px solid transparent;border-right: 10px solid black; width:0; height:0;"></div>';
				html += '<div style="border-bottom:3px solid #000; width:99%; display:block; margin:-11px auto 0;"></div>';
				for(i=0;i<21;i++) {
					html += '<span style="margin-right: 9px;position:relative;">';
					html += '<span class="numberLine" data-number="'+i+'" style="font-size: 14px;padding:2px;cursor:pointer;border:1px solid #FFF;';
					html += '">' + i + '</span>';
					if( i != 0 && i != 20 ) {
						html += '<span style="border-right:3px solid #000;height:10px;position:absolute;margin:-2px 0 0 -8px;"></span>';
					}
					html += '</span>';
				}
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				$('.numberLine').hover(function(){
					$(this).css('border', '1px solid #000');
				}, function(){
					$(this).css('border', '1px solid #FFF');
				}).click(function(){
					$('.numberLine').css('background-color', 'transparent');
					var number = $(this).attr('data-number');
					$('#answer-box').val(number);
					if( number == data.answer ) {
						$(this).css('background-color', 'green');
						correct_callback();
						$('#answer-box, #submit-answer').prop('disabled', true);
					} else {
						$(this).css('background-color', 'red');
						wrong_callback();
					}
				});
			},
		},
		{	quiz_number : 33,
			time : '18:38',
			html : function(controllers) {
				var html = '<p><strong>When we use this technique, why do you think that we should start at the greater addend rather than the smaller addend? Choose from the following statements.</strong></p>';
				html += '<ol>';
					html += '<li>Because if we start at the smaller addend, the counting could be longer and takes more time to get the sum.</li>';
					html += '<li>Because if we start at the greater addend, the counting is shorter and takes less time to get the sum.</li>';
				html += '</ol>';
				var choices = [
					'A. Only statement 1 is correct.', 
					'B. Only statement 2 is correct.',
					'C. Statement 1 and 2 are both correct.',
					'D. Statement 1 and 2 are both incorrect.',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2, 
			answer_text : 'C. Statement 1 and 2 are both correct.',
			question : 'When we use this technique, why do you think that we should start at the greater addend rather than the smaller addend? Choose from the following statements.',
			type : 'multiple-choice',
			details : function() {
				var html = '<ol>';
					html += '<li>Because if we start at the smaller addend, the counting could be longer and takes more time to get the sum.</li>';
					html += '<li>Because if we start at the greater addend, the counting is shorter and takes less time to get the sum.</li>';
				html += '</ol><br>';
				html += "A. Only statement 1 is correct.<br>\
B. Only statement 2 is correct.<br>\
C. Statement 1 and 2 are both correct.<br>\
D. Statement 1 and 2 are both incorrect.";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
			end_session : 1,
		},
	],
};
