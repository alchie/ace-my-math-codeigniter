var G6C1L5 = {
	title : 'Adding Decimals',
	duration : '10:00',
	data : [
		{	quiz_number : 1,
			time : '00:27',
			html : function(controllers) {
				var html = '<h2>Supposed my aunt gave me 2 apples, and my mom gave me 5 oranges. How many fruits do I have all in all?</h2>';
				var choices = [
					'a. 3', 
					'b. 7',
					'c. 6'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. 7',
			question : 'Supposed my aunt gave me 2 apples, and my mom gave me 5 oranges. How many fruits do I have all in all?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. 3<br>b. 7<br>c. 6";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2,
			time : '00:54',
			html : function(controllers) {
				var html = '<h2>If I have 24 pencils and 20 color pens, what is the total number of pencils and color pens?</h2>';
				var choices = [
					'a. 24', 
					'b. 44',
					'c. 46'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. 44',
			question : 'If I have 24 pencils and 20 color pens, what is the total number of pencils and color pens?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. 24<br>b. 44<br>c. 46";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '01:29',
			html : function(controllers) {
				var html = '<h2>Giselle has $321 as her birthday gift. How much does she have all in all if she also has savings of $346?</h2>';
				var choices = [
					'a. $667', 
					'b. $25',
					'c. $567'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. $667',
			question : 'Giselle has $321 as her birthday gift. How much does she have all in all if she also has savings of $346?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. $667<br>b. $25<br>c. $567";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '01:56',
			html : function(controllers) {
				var html = '<h2>How did you add the numbers from the problem above?</h2>';
				var choices = [
					'a. just add directly', 
					'b. no rule is needed when adding',
					'c. line up numbers according to their place value'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'c. line up numbers according to their place value',
			question : 'How did you add the numbers from the problem above?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. just add directly<br>\
b. no rule is needed when adding<br>\
c. line up numbers according to their place value";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5,
			time : '02:28',
			html : function(controllers) {
				var html = '<h2>Add 1.5 to 2.3</h2>';
				var choices = [
					'a. 3.8', 
					'b. 2.45',
					'c. 1.73'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. 3.8',
			question : 'Add 1.5 to 2.3',
			type : 'multiple-choice',
			details : function() {
				var html = "a. 3.8<br>b. 2.45<br>c. 1.73";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 6,
			time : '04:14',
			html : function(controllers) {
				var html = '<h2>Which of the two decimal numbers have more digits?</h2>';
				var choices = [
					'a. 3.25', 
					'b. 0.075',
					'c. both decimal numbers'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. 0.075',
			question : 'Which of the two decimal numbers have more digits?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. 3.25<br>b. 0.075<br>c. both decimal numbers";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time : '05:07',
			html : function(controllers) {
				var html = '<h2>What should be added to 3.25 so that the number of digits will be the same as the number of digits in 0.075?</h2>';
				var choices = [
					'a. zero', 
					'b. any number',
					'c. don\'t add any number but delete a number from 0.075'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. zero',
			question : 'What should be added to 3.25 so that the number of digits will be the same as the number of digits in 0.075?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. zero<br>b. any number<br>c. don\'t add any number but delete a number from 0.075";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '05:43',
			html : function(controllers) {
				var html = '<h2>The sum of 3.25 and 0.075 is ______ .</h2>';
				var choices = [
					'a. 4', 
					'b. 3.325',
					'c. can\'t be determined'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. 3.325',
			question : 'The sum of 3.25 and 0.075 is ______ .',
			type : 'multiple-choice',
			details : function() {
				var html = "a. 4<br>b. 3.325<br>c. can't be determined";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '06:48',
			html : function(controllers) {
				var html = '<h2>Add 6.57, 1.4332 and 5.</h2>';
				var choices = [
					'a. 13', 
					'b. 13.0032',
					'c. 13.00032'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. 13.0032',
			question : 'Add 6.57, 1.4332 and 5.',
			type : 'multiple-choice',
			details : function() {
				var html = "a. 13<br>b. 13.0032<br>c. 13.00032";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '07:53',
			html : function(controllers) {
				var html = '<h2>"5" is lined up with "6" and "1" because _________.</h2>';
				html += '<img src="/assets/images/lessons/G6C1L5_img1.png" style="float:right">';
				var choices = [
					'a. it should always be lined up with the last digit to the left', 
					'b. that\'s one of the rules in addition',
					'c. it is a ones digit and should be aligned with the <br>other ones digit and can be written as 5.0'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'c. it is a ones digit and should be aligned with the other ones digit and can be written as 5.0',
			question : '"5" is lined up with "6" and "1" because _________.',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G6C1L5_img1.png"></p>';
					html += "a. it should always be lined up with the last digit to the left<br>\
b. that\'s one of the rules in addition<br>\
c. it is a ones digit and should be aligned with the other ones digit and can be written as 5.0";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11,
			time : '08:58',
			html : function(controllers) {
				var html = '<h2>Alberta wanted to buy the following items: <br> A DVD player for $47.65, a DVD holder for $20.95 and a personal stereo for $22.85. Will Alberta be able to buy all three items if she has $90 with her?</h2>';
				var choices = [
					'a. Yes, because the sum of all the items is $9.145', 
					'b. No, because the sum of all the items is $91.45',
					'c. It depends because it lacks some information'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. No, because the sum of all the items is $91.45',
			question : 'Will Alberta be able to buy all three items if she has $90 with her?',
			type : 'multiple-choice',
			details : function() {
				var html = "<p>Alberta wanted to buy the following items: <br> A DVD player for $47.65, <br>a DVD holder for $20.95 and <br>a personal stereo for $22.85.</p>";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
			end_session : 1,
		},
	],
};