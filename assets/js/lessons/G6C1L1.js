var G6C1L1 = {
		title : 'Place Value of Decimal Number',
		duration : '06:47',
		data : [
		{	quiz_number : 1,
			time : '00:44',
			html : function(controllers) {
				var html = '<h2>Which month did Angie has saved the most? Is it on March, April, or May?</h2>';
				html += '<img style="float:right" src="/assets/images/lessons/G6C1L1_cal1.png">';
				var choices = [
					'a. March', 
					'b. April',
					'c. May'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'c. May',
			question : 'Which month did Angie has saved the most? Is it on March, April, or May?',
			type : 'multiple-choice',
			details : function() {
				var html = '<p><img src="/assets/images/lessons/G6C1L1_cal1.png"></p>';
				html += "a. March<br>b. April<br>c. May";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2,
			time : '01:48',
			html : function(controllers) {
				var html = '<h2>Which of the following did Angie buy? (Cheapest)</h2>';
				var choices = [
					'a. candy ($2.05)', 
					'b. chocolate ($20.5)',
					'c. shake ($205)'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. candy ($2.05)',
			question : 'Which of the following did Angie buy? (Cheapest)',
			type : 'multiple-choice',
			details : function() {
				var html = "a. candy ($2.05)<br>b. chocolate ($20.5)<br>c. shake ($205)";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '02:33',
			html : function(controllers) {
				var html = '<h2>How are these three numbers different?</h3><h2>$20.5 $2.05 $205.</h2>';
				var choices = [
					'a. First one is twice as big as the second one',
					'b. Their decimal points are in different places',
					'c. They are all the same'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. Their decimal points are in different places',
			question : 'How are these three numbers different? $20.5 $2.05 $205.',
			type : 'multiple-choice',
			details : function() {
				var html = "a. First one is twice as big as the second one<br>\
				b. Their decimal points are in different places<br>\
				c. They are all the same";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '03:28',
			html : function(controllers) {
				var html = '<h2>What is the place value of 2 in $20.5?</h2>';
				var choices = [
					'a. ones', 
					'b. tens',
					'c. tenths'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. tens',
			question : 'What is the place value of 2 in $20.5?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. ones<br>b. tens<br>c. tenths";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5,
			time : '03:51',
			html : function(controllers) {
				var html = '<h2>What is the place value of 0 in $20.5?</h2>';
				var choices = [
					'a. ones', 
					'b. tens',
					'c. tenths'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. ones',
			question : 'What is the place value of 0 in $20.5?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. ones<br>b. tens<br>c. tenths";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 6,
			time : '04:10',
			html : function(controllers) {
				var html = '<h2>What is the place value of 5? $20.5</h2>';
				var choices = [
					'a. ones', 
					'b. tens',
					'c. tenths'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'b. tens',
			question : 'What is the place value of 5? $20.5',
			type : 'multiple-choice',
			details : function() {
				var html = "a. ones<br>b. tens<br>c. tenths";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time : '04:36',
			html : function(controllers) {
				var html = '<h2>So, we read the value of chocolate as? $20.5</h2>';
				var choices = [
					'a. 2 ones and 5 tenths', 
					'b. 2 hundreds and 5 ones',
					'c. 2 tens and 5 tenths'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'c. 2 tens and 5 tenths',
			question : 'So, we read the value of chocolate as? $20.5',
			type : 'multiple-choice',
			details : function() {
				var html = "a. 2 ones and 5 tenths<br>b. 2 hundreds and 5 ones<br>c. 2 tens and 5 tenths";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '05:26',
			html : function(controllers) {
				var html = '<h2>In $205, what is the place value of 2?</h2>';
				var choices = [
					'a. tens', 
					'b. hundredths',
					'c. hundreds'
				]; 
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : 'c. hundreds',
			question : 'In $205, what is the place value of 2?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. tens<br>b. hundredths<br>c. hundreds";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '05:48',
			html : function(controllers) {
				var html = '<h2>What number is in the ones place value of $205?</h2>';
				var choices = [
					'a. 5', 
					'b. 0',
					'c. 2'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 0,
			answer_text : 'a. 5',
			question : 'What number is in the ones place value of $205?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. 5<br>b. 0<br>c. 2";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '06:13',
			html : function(controllers) {
				var html = '<h2>We read $205 value as?</h2>';
				var choices = [
					'a. 2 tens and 5 ones', 
					'b. 2 hundreds and 5 ones',
					'c. 2 thousands and 5 tens'
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 1,
			answer_text : 'b. 2 hundreds and 5 ones',
			question : 'We read $205 value as?',
			type : 'multiple-choice',
			details : function() {
				var html = "a. 2 tens and 5 ones<br>b. 2 hundreds and 5 ones<br>c. 2 thousands and 5 tens";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
			end_session : 1,
		},
	],
};
