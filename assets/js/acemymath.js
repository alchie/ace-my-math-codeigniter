(function($) {

// PRELOADING AUDIO
$(new Audio()).prop({ src : '/assets/media/incorrect.mp3',autoplay: false});
	
var aMethods = {
		jwplayer : '',
		chapter_link_action : function() { 
			$('.collapse.in').collapse('toggle'); 
			$('#chapter-' + $(this).attr('data-id')).collapse('toggle'); 
			$('.list-grid-'+$(this).attr('data-level')+'-'+$(this).attr('data-chapter')).each(function(){
				if($(this).css('display') == 'none') {
					$(this).css('display', 'block');
				} else {
					$(this).css('display', 'none');
				}
			});
			aMethods.loadChapterLessons( $(this).attr('data-level'), $(this).attr('data-chapter') );
		},
		display_level_chapters : function( data ) {
			var self = this;
			if( data.results.length > 0 ) {
				var panelGroup = $('<div />').attr('id', 'level-chapter-' + data.level_id).attr('data-', data.level_id).css('display', 'none');
					panelGroup.addClass('panel-group level-chapter').appendTo('#main-content');
				for( i in data.results ) {
					var chapter = data.results[i];
					var panel = $('<div />').addClass('panel panel-default').appendTo(panelGroup);
					var panelHeading = $('<div />').addClass('panel-heading').appendTo(panel);
					
					var buttonGrid = $('<a />').addClass('pull-right').html('<span class="glyphicon glyphicon-th"></span>').appendTo(panelHeading);
					buttonGrid.attr('href', 'javascript:void(0);');
					buttonGrid.css('margin-left', '5px');
					buttonGrid.css('display', 'none');
					buttonGrid.attr('data-chapter', chapter.chapter_id).attr('data-level', chapter.level_id)
					buttonGrid.addClass('list-grid-icon list-grid-'+chapter.level_id+'-'+chapter.chapter_id);
					buttonGrid.click(function(){
						var chapter = $(this).attr('data-chapter'), level = $(this).attr('data-level');
						$('#lessons-group-'+level+'-'+chapter).removeClass('list-group').addClass('grid-group row');
						$('#lessons-group-'+level+'-'+chapter+' .lesson-item').removeClass('list-group-item').addClass('grid-group-item col-sm-3 col-md-3');
					});
					
					var buttonList = $('<a />').addClass('pull-right').html('<span class="glyphicon glyphicon-align-justify"></span>').appendTo(panelHeading);
					buttonList.attr('href', 'javascript:void(0);');
					buttonList.css('display', 'none');
					buttonList.attr('data-chapter', chapter.chapter_id).attr('data-level', chapter.level_id)
					buttonList.addClass('list-grid-icon list-grid-'+chapter.level_id+'-'+chapter.chapter_id);
					buttonList.click(function(){
						var chapter = $(this).attr('data-chapter'), level = $(this).attr('data-level');
						$('#lessons-group-'+level+'-'+chapter).addClass('list-group').removeClass('grid-group row');
						$('#lessons-group-'+level+'-'+chapter+' .lesson-item').addClass('list-group-item').removeClass('grid-group-item col-sm-3 col-md-3');
					});
					var panelTitle = $('<h4 />').addClass('panel-title').appendTo(panelHeading);
					var panelLink = $('<a />').attr('data-id', chapter.level_chapter_id)
						panelLink.attr('data-chapter', chapter.chapter_id).attr('data-level', chapter.level_id)
						panelLink.attr('href', 'javascript:void(0);')
						panelLink.html('<strong>'+chapter.chapter_name+'</strong>: '+chapter.chapter_title)
						panelLink.click(aMethods.chapter_link_action);
						panelLink.appendTo(panelTitle);
					var panelCollapse = $('<div />').attr('id', 'chapter-' + chapter.level_chapter_id)
						panelCollapse.addClass('panel-collapse collapse').appendTo(panel);
					var panelBody = $('<div />').attr('id', 'panel-body-'+chapter.level_id+'-' + chapter.chapter_id);
						panelBody.addClass('panel-body').appendTo(panelCollapse);
					var ajaxLoader = $('<center><img src="'+baseURL+'assets/img/bx_loader.gif"></center>').appendTo(panelBody);
				}
				$('#ajax-loader').slideUp(function() {
					$('#level-chapter-'+ data.level_id).slideDown();
				});
			}
		},
		display_chapter_lessons : function(data) {
			var self = this;
			if( data.results.length > 0 ) {
				var listGroup = $('<div />').attr('id', 'lessons-group-'+data.level_id+'-'+data.chapter_id).addClass('list-group').appendTo($('#panel-body-'+data.level_id+'-'+data.chapter_id).empty());
				for(i in data.results ) {
					var lesson = data.results[i];
					var item_container = $('<div />').addClass('list-group-item lesson-item');
					item_container.appendTo( listGroup );
					var listItem = $('<div />').addClass('lesson-item-container');
						listItem.html('<span class="lesson-item-title"><strong>Lesson '+lesson.lesson_number+'</strong>: ' + lesson.lesson_title + '</span>')
						listItem.appendTo( item_container );
						
					var listItemStar = $('<span />').addClass('lesson-item-star glyphicon glyphicon-star-empty').prependTo( listItem );
					var listItemButtons = $('<span />').addClass('lesson-item-buttons').prependTo( listItem );
					var videoLink = $('<a />').addClass('btn btn-xs').attr('href', baseURL+'lesson/'+lesson.lesson_id+'-'+lesson.lesson_slug+'/video').appendTo(listItemButtons);
					var videoIcon = $('<span />').addClass('glyphicon glyphicon-film').appendTo(videoLink);
					var exercisesLink = $('<a />').addClass('btn btn-xs').attr('href', baseURL+'lesson/'+lesson.lesson_id+'-'+lesson.lesson_slug+'/practice').appendTo(listItemButtons);
					var exercisesIcon = $('<span />').addClass('glyphicon glyphicon-pencil').appendTo(exercisesLink);
					var worksheetLink = $('<a />').addClass('btn btn-xs').attr('href', baseURL+'lesson/'+lesson.lesson_id+'-'+lesson.lesson_slug+'/worksheet').appendTo(listItemButtons);
					var worksheetIcon = $('<span />').addClass('glyphicon glyphicon-list-alt').appendTo(worksheetLink);
					var reportLink = $('<a />').addClass('btn btn-xs').attr('href', baseURL+'lesson/'+lesson.lesson_id+'-'+lesson.lesson_slug+'/reports').appendTo(listItemButtons);
					var reportIcon = $('<span />').addClass('glyphicon glyphicon-signal').appendTo(reportLink);
					
				}
			}
		},
		display_lesson_video : function(data) {
			if( data.display ) {
				aMethods.lesson_video_available( data );
			} else {
				if( data.is_logged_in ) {
					aMethods.lesson_video_upgrade_account( data );
				} else {
					aMethods.lesson_video_not_available( data );
				}
			}
		},
		
		lesson_video_not_available : function( data ) {
			var mainContent = $('#main-content').empty();
			var alert = $('<div />').addClass('alert alert-danger').appendTo( mainContent );
			alert.html("<strong>Authentication Error!</strong> This lesson is only available to registered users. <a href='/register'>Register Now!</a>");
			var videoContainer = $('<div />').attr('id', 'video_container').appendTo( mainContent );
			var videoPlayerWrapper = $('<div />').attr('id', 'video_wrapper').appendTo(videoContainer).css({
					width : aMethods.videoWidth,
					height : aMethods.videoHeight
				});
			var videoBlock = $('<div />').attr('id', 'video-block').appendTo(videoPlayerWrapper).css('cursor', 'pointer').css({
					width : aMethods.videoWidth,
					height : aMethods.videoHeight,
					opacity : 0.5,
			});
			var overlayContainerbg = $('<div />').attr('id', 'overlayContainerbg').appendTo(videoPlayerWrapper).css({
				width : aMethods.videoWidth,
				height : aMethods.videoHeight
				});
			var overlayContainer = $('<div />').addClass('interactive-quiz').attr('id', 'overlayContainer').appendTo(overlayContainerbg).css({
				height : '180px'
				});
				var lesson_url = 'lesson/' + data.lesson_id + '-' + data.lesson_slug + '/video';
			var loginButton = $('<a />').addClass('btn btn-success btn-lg btn-block').text("Login").attr('href', '/login?r=' + lesson_url).appendTo(overlayContainer);
			var signupButton = $('<a />').addClass('btn btn-danger btn-lg btn-block').text("Signup").attr('href', '/register').appendTo(overlayContainer);
		},
		
		lesson_video_upgrade_account : function( data ) {
			var mainContent = $('#main-content').empty();
			var alert = $('<div />').addClass('alert alert-danger').appendTo( mainContent );
			alert.html("<strong>Account Upgrade!</strong> This lesson is only available to you subscription. Upgrade your account!");
			var videoContainer = $('<div />').attr('id', 'video_container').appendTo( mainContent );
			var videoPlayerWrapper = $('<div />').attr('id', 'video_wrapper').appendTo(videoContainer).css({
					width : aMethods.videoWidth,
					height : aMethods.videoHeight
				});
			var videoBlock = $('<div />').attr('id', 'video-block').appendTo(videoPlayerWrapper).css('cursor', 'pointer').css({
					width : aMethods.videoWidth,
					height : aMethods.videoHeight,
					opacity : 0.5,
			});
			var overlayContainerbg = $('<div />').attr('id', 'overlayContainerbg').appendTo(videoPlayerWrapper).css({
				width : aMethods.videoWidth,
				height : aMethods.videoHeight
				});
			var overlayContainer = $('<div />').addClass('interactive-quiz').attr('id', 'overlayContainer').appendTo(overlayContainerbg).css({
				height : '180px'
				});
			
			var h2 = $('<h2>').text("This video is not accessible in you subscription!").appendTo(overlayContainer);
			},
		
		lesson_video_available : function( data ) {
			var self = this;
			aMethods.lesson_session = data;
			aMethods.lesson_total_mistakes = 0;
			if( data.results ) {
				var lesson = data.results;
				aMethods.lesson = data.results;
					$('#level-link-'+ lesson.lesson_level).parent('li').addClass('active');
				var videoContainer = $('<div />').attr('id', 'video_container').appendTo($('#main-content').empty());
				var videoTitle = $('<h4 />').html('<strong>Lesson '+lesson.lesson_number+':</strong> ' + lesson.lesson_title).appendTo( videoContainer );
				var buttonsGroup = $('<div />').css('margin-bottom', '10px').addClass('btn-group btn-group-sm btn-group-justified btn-group-actions').appendTo( videoContainer );
				var videoButton = $('<span />').addClass('btn btn-danger first').html('<span class="glyphicon glyphicon-film"></span> Video Lesson').appendTo( buttonsGroup );
				var practiceButton = $('<span />').addClass('btn btn-primary').html('<span class="glyphicon glyphicon-pencil"></span> Practice').appendTo( buttonsGroup );
				var worksheetButton = $('<span />').addClass('btn btn-primary').html('<span class="glyphicon glyphicon-list-alt"></span> Worksheet').appendTo( buttonsGroup );
				var reportButton = $('<span />').addClass('btn btn-primary last').html('<span class="glyphicon glyphicon-signal"></span> Report').appendTo( buttonsGroup );
				aMethods.videoPlayerWrapper = $('<div />').attr('id', 'video_wrapper').appendTo(videoContainer).css({
					width : aMethods.videoWidth,
					height : aMethods.videoHeight
				});
				aMethods.videoBlock = $('<div />').attr('id', 'video-block').appendTo(aMethods.videoPlayerWrapper).css('cursor', 'pointer').click(function(){
					aMethods.jwplayer.play();
				}).css({
					width : aMethods.videoWidth,
					height : ( aMethods.videoHeight - 30 )
				});
				var videoPlayer = $('<div />').attr('id', 'video_player');
					videoPlayer.html('<center style="margin-top:100px"><img src="' + baseURL +'assets/img/bx_loader.gif" /></center>');
					videoPlayer.appendTo(aMethods.videoPlayerWrapper);
					if( lesson.lesson_quiz != '' ) {
						aMethods.require(baseURL + "assets/js/lessons/"+lesson.lesson_quiz+".js" , aMethods.load_lesson_quizzes);
					}
					var volumeControls =  $('<div>').css({
						position : 'absolute',
						zIndex : 999,
						backgroundColor : '#FFF',
						padding : '3px',
						top : '5px',
						left : '5px',
						opacity : 0.5,
						borderRadius : 5,
					}).appendTo( aMethods.videoPlayerWrapper ).hover(function(){
						$(this).css('opacity', 1);
					}, function(){
						$(this).css('opacity', 0.5);
					});
					var volume_down = $('<button>').css({
							padding:  '3px',
							cursor: 'pointer',
							background : 'none',
							border : 'none'
						}).addClass("glyphicon glyphicon-volume-down").appendTo(volumeControls).click(function(){
							var current_volume = (Math.ceil(aMethods.jwplayer.getVolume() / 10 ) * 10);
							if( current_volume > 0 ) {
								aMethods.jwplayer.setVolume( current_volume - 10 );
							}
							console.log( current_volume );
						});;
					var volume_up = $('<button>').css({
							padding:  '3px',
							cursor: 'pointer',
							background : 'none',
							border : 'none'
						}).addClass("glyphicon glyphicon-volume-up").appendTo(volumeControls).click(function(){
							var current_volume = (Math.ceil(aMethods.jwplayer.getVolume() / 10 ) * 10);
							if( current_volume < 100 ) {
								aMethods.jwplayer.setVolume( current_volume + 10 );
							}
							console.log( current_volume );
						});
					
					
					aMethods.jwplayer = jwplayer("video_player").setup({
						file: lesson.lesson_video_url,
						height: aMethods.videoHeight, //550,
						width: aMethods.videoWidth, //717,
						autostart: true,
						allowfullscreen : false,
						icons: false,
						skin: baseURL + "assets/skin/five/five.xml",
						mode : 'html5',
						events : {
							onTime : aMethods.videoOnTime,
							onSeek : aMethods.videoOnSeek,
							onPause : aMethods.videoOnPause,
							onPlay : aMethods.videoOnPlay,
							onComplete: aMethods.videoOnComplete,
							},
						controlbar: {
							position: "bottom",
							idlehide: true
						},
					}).onReady(function(){
						aMethods.__disableFromPlayer();
					});
				
				aMethods.jwplayer.setVolume( 100 );
				aMethods.videoTotalTime_count = 0;
				aMethods.videoTotalTime_time = $.timer(function(){
						++aMethods.videoTotalTime_count;
						/* For Debug: Skip Quiz and resume video */
						var timerDisplayElapse = $('#videoTotalTime_display');
						if( timerDisplayElapse.length == 0 ) {
							timerDisplayElapse = $('<span/>').attr('id', 'videoTotalTime_display').addClass('alert alert-danger').insertAfter( aMethods.videoPlayerWrapper );
						}
						timerDisplayElapse.html( "<strong>Lesson Time:</strong> " + aMethods.videoTotalTime_count + " | " + aMethods.formatTime( aMethods.videoTotalTime_count ) );
						/* For Debug: Skip Quiz and resume video */
					}, 1000, false);
				aMethods.jwplayer.onPlay(function(){
					aMethods.videoTotalTime_time.play();
				});
			}
		},
		load_lesson_quizzes : function() {
			var quiz = window[aMethods.lesson.lesson_quiz];
			aMethods.popup_quiz_data = [];
			for( i in quiz.data ) {
				var seconds = aMethods.inSeconds( quiz.data[i].time );
				aMethods.popup_quiz_data[seconds] = quiz.data[i];
			}
			aMethods.video_current_position = 0;
			aMethods.video_position_occurence = 0;
			aMethods.video_seeking = false;
			aMethods.video_max_position = 0;
		},
		isTakingQuiz : function(taking) {
			if( typeof taking == 'undefined' ) {
				return (typeof aMethods.TakingQuizStatus == 'undefined') ? false : aMethods.TakingQuizStatus;
			} else {
				aMethods.TakingQuizStatus = taking;
			}
		},
		showPopupQuiz : function( data ) {
			aMethods.lesson_quiz_mistakes = 0;
			aMethods.lesson_quiz_taken = 0;
			aMethods.lesson_quiz_session = data;
			aMethods.isTakingQuiz(true);
			var overlayContainerbg = $('<div />').attr('id', 'overlayContainerbg').appendTo(aMethods.videoPlayerWrapper).css({
				width : aMethods.videoWidth,
				height : aMethods.videoHeight
				});
			var overlayContainer = $('<div />').addClass('interactive-quiz').attr('id', 'overlayContainer').appendTo(aMethods.videoPlayerWrapper);
				if( typeof data.html == 'function' ) {
					$( data.html( QuizControllers ) ).appendTo( overlayContainer );
				} else {
					$( data.html ).appendTo( overlayContainer );
				}
			data.controls($, aMethods.jwplayer, data, aMethods.showCorrectAnswer, aMethods.showWrongAnswer, QuizControllers);
			
			aMethods.lesson_quiz_taken = 0;
			aMethods.QuizItemTime = $.timer(function(){
					++aMethods.lesson_quiz_taken;
					/* For Debug: Skip Quiz and resume video */
					var timerDisplayElapse = $('#QuizItemTime_display');
					if( timerDisplayElapse.length == 0 ) {
						timerDisplayElapse = $('<span/>').attr('id', 'QuizItemTime_display').addClass('alert alert-danger').insertAfter( aMethods.videoPlayerWrapper );
					}
					timerDisplayElapse.html( "<strong>Quiz Time:</strong> " + aMethods.lesson_quiz_taken + " | " + aMethods.formatTime( aMethods.lesson_quiz_taken ) );
					/* For Debug: Skip Quiz and resume video */
				}, 1000, true);
			
			/* For Debug: Skip Quiz and resume video */
			 var playButton = $('<button />').addClass('btn btn-danger btn-xs playVideo pull-right quiz-skip-button').text('Skip').appendTo(overlayContainer);
			playButton.click(function() {
				
				aMethods.QuizItemTime.stop();
				$('#QuizItemTime_display').remove();
				aMethods.isTakingQuiz(false);
				
				aMethods.jwplayer.play();
				$('#overlayContainerbg').remove();
				$('#overlayContainer').remove();
			}); 
			/* Skip Quiz and resume video */
		},
		showCorrectAnswer : function() {
			
			/* Quiz Time */
			aMethods.QuizItemTime.stop();
			$('#QuizItemTime_display').remove();
			
			var overlayContainer = $('#video_wrapper #overlayContainer');
			var inner_width = parseInt(overlayContainer.css('width')) - (parseInt(overlayContainer.css('padding-left')) + parseInt(overlayContainer.css('padding-right')));
			
			$('#video_wrapper #overlayContainer .alert').fadeOut('slow', function() {
					$(this).remove();
				});
			
			var right_alert = $('<div class="alert alert-success" id="quiz-alert" style="display:none;">Correct</div>');
			right_alert.prependTo(overlayContainer);
			right_alert.css({
						width : inner_width,
						position : 'absolute',
						zIndex : 99999,
					});
			right_alert.slideDown('slow');
			
			setTimeout(function(){
						right_alert.slideUp('slow', function(){
							aMethods.jwplayer.play();
							$('#overlayContainerbg').remove();
							$('#overlayContainer').remove();
						});
						
					}, 2000);

			aMethods.__recordLessonSession();
			aMethods.isTakingQuiz(false);

		},
		showWrongAnswer : function() {
			++aMethods.lesson_total_mistakes;
			++aMethods.lesson_quiz_mistakes;
			var self = this;
			if( self.wrong_audio == undefined) {
				if( self.wrong_audio2 != undefined ) {
					self.wrong_audio2.trigger('pause');
					delete self.wrong_audio2;
					self.wrong_audio2 = undefined;
				}
				self.wrong_audio = $(new Audio()).prop({ 
					 src : '/assets/media/incorrect.mp3',
					 autoplay: true
					 });
			} else {
				self.wrong_audio.trigger('pause');
				self.wrong_audio2 = $(new Audio()).prop({ 
					 src : '/assets/media/incorrect.mp3',
					 autoplay: true
					 });
				delete self.wrong_audio;
				self.wrong_audio = undefined;
			}
			var quiz_alert_timeout = '';
			var overlayContainer = $('#video_wrapper #overlayContainer');
			var inner_width = parseInt(overlayContainer.css('width')) - (parseInt(overlayContainer.css('padding-left')) + parseInt(overlayContainer.css('padding-right')));
			
			clearTimeout(quiz_alert_timeout);
			
				$('#video_wrapper #overlayContainer .alert').fadeOut('slow', function() {
					$(this).remove();
				});
					var wrong_alert = $('<div class="alert alert-danger" id="quiz-alert" style="display:none;">Wrong</div>');
					wrong_alert.prependTo(overlayContainer);
					wrong_alert.css({
						width : inner_width,
						position : 'absolute',
						zIndex : 99999,
					});
					wrong_alert.slideDown('slow');
					quiz_alert_timeout = setTimeout(function(){
						wrong_alert.slideUp('slow');
					}, 2000);
				
		},
		
		__recordLessonSession : function() {
			// console.log( aMethods.lesson_session );
			//console.log( aMethods.lesson_quiz_session );
			// console.log( aMethods.lesson_quiz_mistakes );
			// console.log( aMethods.lesson_quiz_taken );
			
			if( typeof aMethods.lesson_session.session_id != 'undefined' ) {
				aMethods.ajax({
					action: 'recordLessonSession', 
					session_id : aMethods.lesson_session.session_id,
					lesson_id :	aMethods.lesson_session.lesson_id,
					quiz_number :	aMethods.lesson_quiz_session.quiz_number,
					time_taken :	aMethods.lesson_quiz_taken,
					mistakes :	aMethods.lesson_quiz_mistakes,
					end_session : aMethods.lesson_quiz_session.end_session,
					session_time : aMethods.videoTotalTime_count,
					total_mistakes : aMethods.lesson_total_mistakes,
					quiz_data : {
						number : aMethods.lesson_quiz_session.quiz_number,
						time : aMethods.lesson_quiz_session.time,
						question : aMethods.lesson_quiz_session.question,
						answer : aMethods.lesson_quiz_session.answer_text,
						type : aMethods.lesson_quiz_session.type,
						details : aMethods.lesson_quiz_session.details(),
					}
				}, function( resp ) {});
			}
		},
		
		__disableFromPlayer : function() {
			$('.jwlogo, #video_player_logo').hide();
			$('.jwclick, .jwclick .jwclick_item, #video_player_menu, #video_player_menu .jwclick_item' ).hide();
			$('<span>').css({
				width : '100%',
				position: 'absolute',
				zIndex : '99999'
			}).prependTo($('#video_player .jwgroup.jwcenter'));
		},
		
		videoOnTime : function(event) {
			var pos = Math.round( event.position ) - 1;
			if( aMethods.video_seeking === false ) {
				aMethods.video_max_position = Math.max(event.position, aMethods.video_max_position);
			}
			if( pos != aMethods.video_current_position ) {
				aMethods.video_current_position = pos;
				aMethods.video_position_occurence = 0;
			} else {
				aMethods.video_position_occurence++;
			}
			if( aMethods.video_position_occurence == 0 ) {
				if(aMethods.popup_quiz_data[aMethods.video_current_position] != undefined) {
					aMethods.showPopupQuiz( aMethods.popup_quiz_data[aMethods.video_current_position] );
					aMethods.jwplayer.pause();
				}
			}
			aMethods.__disableFromPlayer();
		},
		
		videoOnSeek : function(event) {
				aMethods.__disableFromPlayer();
				if( aMethods.video_seeking === false ) {
						if( event.offset > aMethods.video_max_position ) {
							var overlayContainerbg = $('<div />').attr('id', 'overlayContainerbg').appendTo(aMethods.videoPlayerWrapper);
						   aMethods.video_seeking = true;
						   aMethods.jwplayer.pause();
						   setTimeout(function ()
							{  
							   aMethods.jwplayer.seek(aMethods.video_max_position);
							   $('#overlayContainerbg').remove();
							}, 50);
						}
				} else {
					aMethods.video_seeking = false;
				}
		},
		
		videoOnPause : function(event) {
			if( aMethods.isTakingQuiz() == false ) {
				aMethods.videoTotalTime_time.pause();
			}
		},
		
		videoOnPlay : function(event) {
			aMethods.videoTotalTime_time.play();
		},
		
		videoOnComplete : function(event) {
			aMethods.videoTotalTime_time.stop();
		},
		
		inSeconds : function(time) {
			var a = time.split(':'); // split it at the colons
			if( a.length == 1 ) {
				var seconds = (+a[0]); 
			} else if( a.length == 2 ) {
				var seconds = (+a[0]) * 60 + (+a[1]);
			} else if( a.length == 3 ) {
				var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);
			}
			return seconds;
		},
		
		loadLessonVideo : function( id, slug, videoWidth, videoHeight ) {
			aMethods.videoWidth = (typeof videoWidth == 'undefined') ? 717 : parseInt( videoWidth );
			aMethods.videoHeight = (typeof videoHeight == 'undefined') ? 550 : parseInt( videoHeight );
			aMethods.loadLessonSearch();
			var self = this;
			if( $(this).children('#video_container').length == 0 ) {
				aMethods.ajax({action: 'getLesson', lesson_id : id, lesson_slug : slug}, aMethods.display_lesson_video);
			}
		},
		
		loadChapterLessons : function( level, chapter ) {
			var self = this;
			if( $('#panel-body-'+level+'-'+chapter+' .list-group').length == 0 ) {
				self.ajax({action : 'getLessons', level_id : level, chapter_id : chapter }, aMethods.display_chapter_lessons);
			}
		},
		loadLevelChapters : function( lvl ) { 
			var self = this; 
			$('.level-chapter').each(function() {
				if( $(this).css('display') != 'none' && $(this).attr('id') != 'level-chapter-'+lvl) {
					$(this).slideUp();
				}
			});
			if( $('#level-chapter-'+lvl).length == 0 ) {
				$('#ajax-loader').slideDown();
				self.ajax({action : 'getLevelChapters', level_id : lvl }, aMethods.display_level_chapters);
			} else {
				if( $('#level-chapter-'+lvl).css('display') == 'none' ) {
					$('#ajax-loader').slideUp();
					$('#level-chapter-'+lvl).slideDown();
				}
			}
			
		},
		loadLessonsPage : function(level) {
			// init
			aMethods.loadLevelTab();
			// setup active default level
			$('#level-link-'+ level).parent('li').addClass('active');
			// load default chapters
			aMethods.loadLevelChapters( level );
		},
		
		loadLessonSearch : function() {
			var timeout, search = function(self) {
				var key = self.val();
				if(key.length > 2) {
					aMethods.display_search( self, key.trim() ); 
					aMethods.ajax({action: 'search', key : key.trim()}, aMethods.display_search_results);
				}
			};
			$('#lesson-search').keyup( function() {
				var self = $(this);
				clearTimeout(timeout);
				timeout = setTimeout(function(){
					search(self);
				}, 1000);
			}).click( function() {
				var self = $(this);
				search(self);
			});
		},
		
		loadLessonSummary : function() {
			$('.lesson-summary').each(function() {
				var id = $(this).attr('data-level');
				if( $(this).children('#lesson-summary-list-'+id).length == 0 ) {
					var summary_list = $('<div />').attr('id', '#lesson-summary-list-'+id).addClass('text-summary-body');
					$(this).html( summary_list );
					
					aMethods.ajax({action : 'getLessons', level_id : id, limit : 5 }, function(data){
						for (i in data.results) {
							var link = $('<a />').attr('href', baseURL+"lesson/"+data.results[i].lesson_id+"-"+data.results[i].lesson_slug+"/video").text( data.results[i].lesson_title ).appendTo( summary_list );
							var space = $('<span />').text(", ").appendTo( summary_list );
						}
					});
				}
			});
		},
		
		display_search : function( elm, key ){ 
			if( $('#search-autocomplete-box').length == 0 ) {
				var timeout;
				var outerBox = $('<div />').attr('id', 'search-autocomplete-box').hover(function(){
						clearTimeout(timeout);
					}, function(){
						var self = $(this);
						timeout = setTimeout(function(){
							self.remove();
						}, 1000);
					});
				var ajaxBox = $('<div />').attr('id', 'ajaxBox').css({
					display: 'block',
					textAlign: 'center',
					padding: '10px',
					}).html('<img src="/assets/images/ajax-loader2.gif">').appendTo( outerBox );
				var moreResults = $('<a />').addClass('more-result').prop('href', baseURL+"search?s="+key).html('More Results for `<span class="key">'+key+'</span>` <span class="glyphicon glyphicon-arrow-right"></span>').appendTo( outerBox );
				elm.after( outerBox );
			} else {
				$('#search-autocomplete-box .key').text( key );
				$('#search-autocomplete-box .more-result').prop('href', baseURL+"search?s="+key);
			}
		},
		
		display_search_results : function(data) {
			var outerBox = $('#search-autocomplete-box #ajaxBox');
			outerBox.html('').css('text-align', 'left').css('padding', 0);
			
			if( data.tags.length > 0 ) {
				if( $('#search-autocomplete-box #tags-box').length == 0 ) {
					var tagsBox = $('<div />').attr('id', 'tags-box').html('<div class="boxHeader"><div class="headerBar"></div><span class="headerText">Topics</span></div><div class="resultData"></div>').appendTo( outerBox );
				}

				var tagsBox = $('#search-autocomplete-box #tags-box .resultData').html('');
				var ul = $('<ul>').appendTo(tagsBox);
				for (i in data.tags) {
					var li = $('<li>').appendTo(ul);
					var a = $('<a />').addClass('item').appendTo(li);
					var tag = data.tags[i];
					a.text(tag.tag_name).prop('href', baseURL+'topic/'+tag.tag_slug);
				}
			}
			
			if( data.lessons.length > 0 ) {
				if( $('#search-autocomplete-box #lessons-box').length == 0 ) {
					var lessonsBox = $('<div />').attr('id', 'lessons-box').html('<div class="boxHeader"><div class="headerBar"></div><span class="headerText">Lessons</span></div><div class="resultData"></div>').appendTo( outerBox );
				}
				var lessonsBox = $('#search-autocomplete-box #lessons-box .resultData').html('');
				var ul2 = $('<ul>').appendTo(lessonsBox);
				for (i in data.lessons) {
					var li2 = $('<li>').appendTo(ul2);
					var level = $('<span />').addClass('grade-level').appendTo(li2);
					var a2 = $('<a />').addClass('item').appendTo(li2);
					var lesson = data.lessons[i];
					a2.text(lesson.lesson_title).prop('href', baseURL+'lesson/'+lesson.lesson_id+'-'+lesson.lesson_slug+'/video');
					level.text(lesson.level_name);
				}
			}
		},
		
		loadLevelTab : function() {
			$('.level-link').attr('href', 'javascript:void(0);').click(function() {
				var id = $(this).attr('data-id');
				$('.nav-gradelevel > li').removeClass('active');
				$(this).parent('li').addClass('active');
				$('.list-grid-icon').css('display', 'none');
				$('.collapse.in').collapse('toggle'); 
				aMethods.loadLevelChapters( id );
			});
		},
		
		require : function(script, callback) {
			$.ajax({
				url: script,
				dataType: "script",
				async: false, // <-- This is the key
				success: function () {
					callback()
				},
				error: function () {
					throw new Error("Could not load script " + script);
				}
			})
		},
		
		ajax : function(pData, callback) {
			var self = this;
			console.log( pData );
			$.ajax({
					url: ajaxURL,
					type: 'post',
					data: pData,
					dataType : 'json',                
					success: function(resp, status) {
						console.log( resp );
						callback(resp);
					},
					error: function(xhr, desc, err) {
					  console.log(xhr);
					  console.log("Details: " + desc + "\nError:" + err);
					}
				});
		},
		
		init : function() {
			//aMethods.loadLevelTab();
			aMethods.loadLessonSearch();
		},
		
		// optional insertions
		formatTime : function(sec_num) {
			var sec_num = parseInt(sec_num, 10); // don't forget the second param
			var hours   = Math.floor(sec_num / 3600);
			var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
			var seconds = sec_num - (hours * 3600) - (minutes * 60);

			if (hours   < 10) {hours   = "0"+hours;}
			if (minutes < 10) {minutes = "0"+minutes;}
			if (seconds < 10) {seconds = "0"+seconds;}
			var time    = hours+':'+minutes+':'+seconds;
			return time;
		}
		
	}; // aMethods
	
    $.fn.aceMyMath = function(methodOrOptions) {
        if ( aMethods[methodOrOptions] ) {
            return aMethods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {
            // Default to "init"
            return aMethods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  methodOrOptions + ' does not exist on jQuery.tooltip' );
        }    
    };
})(jQuery);
