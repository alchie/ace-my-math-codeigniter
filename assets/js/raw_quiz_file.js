var G5C1L2 = {
	title : '',
	duration : '',
	data : [
		{	quiz_number : 1,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 2,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 3,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 4,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 5,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 6,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 7,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 8,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 9,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 10,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 11,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 12,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 13,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 14,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 15,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 16,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 17,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 18,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 19,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 20,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 21,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 22,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 23,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 24,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 25,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 26,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 27,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 28,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 29,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 30,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 31,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 32,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 33,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 34,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 35,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 36,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 37,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 38,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 39,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 40,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 41,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 42,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 43,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 44,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 45,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 46,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 47,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 48,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 49,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
		{	quiz_number : 50,
			time : '',
			html : function(controllers) {
				var html = '<h2></h2>';
				var choices = [
					'A. ', 
					'B. ',
					'C. ',
				];
				html += controllers.button_choices(choices);
				return html;
			},
			answer : 2,
			answer_text : '',
			question : '',
			type : 'multiple-choice', // input-box
			details : function() {
				var html = "";
				return html;
			},
			controls : function($, jwp, data, correct_callback, wrong_callback, controllers) {
				//controllers.multiple_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				//controllers.single_input_box('answer-box', $, jwp, data, correct_callback, wrong_callback);
				controllers.multiple_choice_controls($, jwp, data, correct_callback, wrong_callback);
			},
		},
	],
};
